import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule} from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { fuseConfig } from 'app/fuse-config';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';      
import { MatToolbarModule } from '@angular/material/toolbar';
import { AuthModule } from './main/auth/auth.module';
import { Error403Component } from './main/Common/error403/error403.component';
import { JwtInterceptor } from './custom/jwt.interceptor';
import { ErrorInterceptor } from './custom/error.interceptor';
import { HttpClientsService } from './service/http-client.service';
import { AuthenticationService } from './service/authentication.service'; 
import { DashboardModule } from './main/HCB/dashboard/dashboard.module';
import { CrmDashboardModule } from './main/CRM/crm-dashboard/crm-dashboard.module';
import { ClientRecordModule } from './main/CRM/client-record/client-record.module';
import { AuthGuard, AuthGuard2 } from './custom/auth.guard';
import { CaseManagmentModule } from './main/HCB/case-managment/case-managment.module';
import { UserRecordsModule } from './main/CRM/administration/user-records/user-records.module';
import { ClientReportModule } from './main/CRM/administration/client-report/client-report.module';
import {TestMailModule} from './main/CRM/test-mail/test-mail.module';
import { MasterModule } from './main/CRM/administration/master/master.module';
import { Error404Component } from './main/Common/error404/error404.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { ImportCasesModule } from './main/HCB/import-cases/import-cases.module';
const appRoutes: Routes = [

    {
        path: 'auth',
        loadChildren: './main/auth/auth.module#AuthModule'
    },

    {    
        path: 'dashboard',
        loadChildren: './main/HCB/dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard2],
    },
    // {
    //     path: 'open-cases',
    //     loadChildren: './main/HCB/open-cases/open-cases.module#OpenCasesModule',
    //     canActivate: [AuthGuard2],
    // },
    {
        path: 'import-cases',
        loadChildren: './main/HCB/import-cases/import-cases.module#ImportCasesModule',
        canActivate: [AuthGuard2],
    },
    {
        path: 'case-managment',
        loadChildren: './main/HCB/case-managment/case-managment.module#CaseManagmentModule',
        canActivate: [AuthGuard2],
    },
    {  
        path: 'test-mail',
        loadChildren: './main/CRM/test-mail/test-mail.module#TestMailModule'
    },
    {
        path: 'crm-dashboard',
        loadChildren: './main/CRM/crm-dashboard/crm-dashboard.module#CrmDashboardModule'
    },
    {
        path: 'client-record',
        loadChildren: './main/CRM/client-record/client-record.module#ClientRecordModule'
    },

    { 
        path: 'administration/user-records',
        loadChildren: './main/CRM/administration/user-records/user-records.module#UserRecordsModule'
    },

    {
        path: 'administration/client-report',
        loadChildren: './main/CRM/administration/client-report/client-report.module#ClientReportModule'
    },

    {
        path: 'administration/master',
        loadChildren: './main/CRM/administration/master/master.module#MasterModule',
        canActivate: [AuthGuard2], 
        
        // canActivate: [RoleGuard]
    },
    {
        path: 'administration/master',
        loadChildren: './main/CRM/administration/master/master.module#MasterModule',
        
        // canActivate: [RoleGuard]
    },
    // {
    //     path: 'sample',
    //     loadChildren: './main/sample/sample.module#SampleModule'
    // },



    // {
    //     path: '**',
    //     component: Error404Component
    // },
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'auth',
        pathMatch: 'full'
    },

    {
        path: 'error403',
        component: Error403Component
    },
];
    
@NgModule({
    declarations: [
        AppComponent,
        Error403Component,
        Error404Component,
       
      
    ],
    imports: [
        NgxDocViewerModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes, { useHash: true }),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        AuthModule,
        LayoutModule,
        // SampleModule,
        // OpenCasesModule,
        CaseManagmentModule,
        DashboardModule,
        CrmDashboardModule,
        ClientRecordModule,
        UserRecordsModule,
        ClientReportModule,
        TestMailModule,
        MasterModule,
        ImportCasesModule

    ],
    exports: [RouterModule],
    
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },        
        HttpClientsService,
        AuthenticationService,
        AuthGuard,
        AuthGuard2
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {
}

