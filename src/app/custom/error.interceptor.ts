import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'app/main/auth/auth.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private _auth: AuthService,
        private __router:Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
               // this._auth.logout();
               this.__router.navigate(['/login']);
               // location.reload(true);
            }
            else if(err.status === 406)
            {
                return;
            }

            const error = err.error.message || err.statusText;
            
            return throwError(error);
        }))
    }
} 