import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from 'app/main/auth/auth.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private _auth: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
       // let currentUser = this._auth.currentUserValue;
        if (localStorage.getItem('Token')) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${localStorage.getItem('Token')}`
                }
            });
        }

        return next.handle(request);
    }
}