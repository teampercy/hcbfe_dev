
import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, PageEvent, Sort } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

//services
import * as _moment from 'moment';
import { CommonService } from 'app/service/common.service';
import { ClientReportGrid, GetClientReport, HCBClientReport } from './client-report.model';
import { ClientReportService } from './client-report.service';
import { ClientRecordService } from 'app/main/CRM/client-record/client-record.service'
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { from } from 'rxjs';
import { MasterService } from '../master/master.service';
const moment = _moment;
@Component({
  selector: 'app-client-report',
  templateUrl: './client-report.component.html',
  styleUrls: ['./client-report.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})

export class ClientReportComponent implements OnInit {

  errorText: string;
  working: boolean;
  ClientServices: any;
  pageIndex: number;
  pageSize: number;
  sortColumn: string;
  TotalRecords: number;
  val: any;
  sortDirection: 'asc' | 'desc' | '';
  crmClientReport: any;
  RelationShipOwnerId: any;
  getddlregion:any;
  getCrmClientReportGrid: ClientReportGrid;
  getCrmClientReportGridForExport: ClientReportGrid;
  getClientReport: HCBClientReport;
  FilterClientReport: FormGroup;

 // IsExport:boolean=true;
  NoReocrds:boolean;

  displayedColumns: string[] = ['CompanyName', 'ClientAddedDate', 'ClientType', 'RelationshipOwner', 'PrincipalContact'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    private _formbuilder: FormBuilder,
    private _clientReportservice: ClientReportService,
    private _clientRecord: ClientRecordService,
    private _commonService: CommonService,
    private _masterService: MasterService
  ) { }

  ngOnInit() {
    this.FilterClientReport = this._formbuilder.group({
      CompanyName: [''],
      ClientTypeId: [null],
      ClientCreatedDateFrom: [''],
      ClientCreatedDateTo: [''],
      PrincipalContactEmail:[''],
      ClientServices: [''],
      RelationShipOwnerId: [],
      RegionId:[],
      IsActive: [null]

    });

    this.pageIndex = 0;
    this.pageSize = 15;
    this.sortColumn = 'ClientID';
    this.sortDirection = 'asc';


    this.getCrmClientReportGrid = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      sort: this.sortColumn,
      direction: this.sortDirection,
      CompanyName: null,
      ClientTypeId: null,
      ClientCreatedDateFrom: null,
      ClientCreatedDateTo: null,
      PrincipalContactEmail:null,
      ClientServices: null,
      RelationShipOwnerId: null,
      RegionId: null,
      IsActive: null,
      export: 'No'

    }


    this.ServiceType();
    this.GetRelationshipOwner();
    this.GetddlRegion();
    this.GetCRMClientReportGrid(this.getCrmClientReportGrid);

  }
  GetRelationshipOwner(): void {
    this.working = true;
    this._clientRecord.GetRelationshipOwner()
      .subscribe((response: any) => {
        this.RelationShipOwnerId = response['result'];
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      });
  }

 GetddlRegion() :void{
  
    this._masterService.GetddlRegion(24)
    .subscribe(
    (response:any) => 
    {this.getddlregion = response['result']
    },
    (error: any) => {
      this.errorText = error;
    });
  
  }

  GetCRMClientReportGrid(model: ClientReportGrid) {
  
    this.crmClientReport = [];
    this.paginator = null;
    this._clientReportservice.GetCRMClientReportList(model)
      .subscribe((response: any) => {
      
       if(response['result'].getClientReport.length==0) 
       {
         this.NoReocrds=true;
       }else{ this.NoReocrds=false;}

        response['result'].getClientReport.forEach(childObj => {
          if (childObj.clientTypeId == 1) {
            childObj.ClientType = 'Insurance'
          }
          else {
            childObj.ClientType = 'Retail'
          }
        });
        this.crmClientReport = new MatTableDataSource<HCBClientReport>(response['result'].getClientReport);//
        this.TotalRecords = response['result'].hcbClientLength[0].totalLength;  //
        this.crmClientReport.sort = this.sort;
        this.crmClientReport.paginator = this.paginator;
      })
  }

  ServiceType(): void {
    this._clientReportservice.GetddlServices()
      .subscribe((response) => {
        this.ClientServices = response['result'];
      })
  }

  pageChanged(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getCrmClientReportGrid.pageIndex = event.pageIndex;
    this.getCrmClientReportGrid.pageSize = event.pageSize;
    this.GetCRMClientReportGrid(this.getCrmClientReportGrid);
  }

  sortChange(event: Sort): void {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getCrmClientReportGrid.sort = event.active;
    this.getCrmClientReportGrid.direction = event.direction;
    this.GetCRMClientReportGrid(this.getCrmClientReportGrid);
  }

  radioChange(event: any) {
    this.getCrmClientReportGrid.IsActive = event.value;
    this.GetCRMClientReportGrid(this.getCrmClientReportGrid);
  }


  Filter(): void {
  
   // this.IsExport=false;
    this.pageIndex = 0;
    this.getCrmClientReportGrid.pageIndex = 0;
    this.getCrmClientReportGrid.CompanyName = this.FilterClientReport.value.CompanyName;
    this.getCrmClientReportGrid.ClientTypeId = this.FilterClientReport.value.ClientTypeId;
    this.getCrmClientReportGrid.ClientCreatedDateFrom = (this.FilterClientReport.value.ClientCreatedDateFrom == "" || this.FilterClientReport.value.ClientCreatedDateFrom == null) ? "" : "" + this.FilterClientReport.value.ClientCreatedDateFrom._i.year + "-" + (this.FilterClientReport.value.ClientCreatedDateFrom._i.month + 1) + "-" + this.FilterClientReport.value.ClientCreatedDateFrom._i.date + " 00:00:00:000";
    this.getCrmClientReportGrid.ClientCreatedDateTo = (this.FilterClientReport.value.ClientCreatedDateTo == "" || this.FilterClientReport.value.ClientCreatedDateTo == null) ? "" : "" + this.FilterClientReport.value.ClientCreatedDateTo._i.year + "-" + (this.FilterClientReport.value.ClientCreatedDateTo._i.month + 1) + "-" + this.FilterClientReport.value.ClientCreatedDateTo._i.date + " 00:00:00:000";
   this.getCrmClientReportGrid.PrincipalContactEmail = this.FilterClientReport.value.PrincipalContactEmail;
    if (this.FilterClientReport.value.ClientServices == null) //this.FilterClientReport.value.ClientServices == "" ||
    {
      this.FilterClientReport.value.ClientServices = 0;
    }
    else {
      this.getCrmClientReportGrid.ClientServices = this.FilterClientReport.value.ClientServices.toString();
    }
    this.getCrmClientReportGrid.RelationShipOwnerId = this.FilterClientReport.value.RelationShipOwnerId;
   this.getCrmClientReportGrid.RegionId =this. FilterClientReport.value.RegionId;
    this.getCrmClientReportGrid.IsActive = this.FilterClientReport.value.IsActive;
    this.getCrmClientReportGrid.export = 'No';
    this.GetCRMClientReportGrid(this.getCrmClientReportGrid);
  }

  Clear(): void {
    // this.IsExport=true;
    this.FilterClientReport.reset();
    this.pageIndex = 0;
    this.getCrmClientReportGrid.pageIndex = 0;
    this.getCrmClientReportGrid.CompanyName = this.FilterClientReport.value.CompanyName;
    this.getCrmClientReportGrid.ClientTypeId = this.FilterClientReport.value.ClientTypeId;
    this.getCrmClientReportGrid.ClientCreatedDateFrom = (this.FilterClientReport.value.ClientCreatedDateFrom == "" || this.FilterClientReport.value.ClientCreatedDateFrom == null) ? "" : "" + this.FilterClientReport.value.ClientCreatedDateFrom._i.year + "-" + (this.FilterClientReport.value.ClientCreatedDateFrom._i.month + 1) + "-" + this.FilterClientReport.value.ClientCreatedDateFrom._i.date + " 00:00:00:000";
    this.getCrmClientReportGrid.ClientCreatedDateTo = (this.FilterClientReport.value.ClientCreatedDateTo == "" || this.FilterClientReport.value.ClientCreatedDateTo == null) ? "" : "" + this.FilterClientReport.value.ClientCreatedDateTo._i.year + "-" + (this.FilterClientReport.value.ClientCreatedDateTo._i.month + 1) + "-" + this.FilterClientReport.value.ClientCreatedDateTo._i.date + " 00:00:00:000";
    this.getCrmClientReportGrid.PrincipalContactEmail = this.FilterClientReport.value.PrincipalContactEmail;
    this.getCrmClientReportGrid.ClientServices = null;
    this.getCrmClientReportGrid.RelationShipOwnerId = this.FilterClientReport.value.RelationShipOwnerId;
    this.getCrmClientReportGrid.RegionId = this.FilterClientReport.value.RegionId;
    this.getCrmClientReportGrid.IsActive = this.FilterClientReport.value.IsActive;
    this.getCrmClientReportGrid.export = 'No';
    this.GetCRMClientReportGrid(this.getCrmClientReportGrid);
  }

  OnChange(event: any) {
    this.val = event.value;

  }
  export() {
  
    this.getCrmClientReportGrid.pageIndex = 0;
    this.getCrmClientReportGrid.CompanyName = this.FilterClientReport.value.CompanyName;
    this.getCrmClientReportGrid.ClientTypeId = this.FilterClientReport.value.ClientTypeId;
    this.getCrmClientReportGrid.ClientCreatedDateFrom = (this.FilterClientReport.value.ClientCreatedDateFrom == "" || this.FilterClientReport.value.ClientCreatedDateFrom == null) ? "" : "" + this.FilterClientReport.value.ClientCreatedDateFrom._i.year + "-" + (this.FilterClientReport.value.ClientCreatedDateFrom._i.month + 1) + "-" + this.FilterClientReport.value.ClientCreatedDateFrom._i.date + " 00:00:00:000";
    this.getCrmClientReportGrid.ClientCreatedDateTo = (this.FilterClientReport.value.ClientCreatedDateTo == "" || this.FilterClientReport.value.ClientCreatedDateTo == null) ? "" : "" + this.FilterClientReport.value.ClientCreatedDateTo._i.year + "-" + (this.FilterClientReport.value.ClientCreatedDateTo._i.month + 1) + "-" + this.FilterClientReport.value.ClientCreatedDateTo._i.date + " 00:00:00:000";
    this.getCrmClientReportGrid.PrincipalContactEmail = this.FilterClientReport.value.PrincipalContactEmail;
    if (this.FilterClientReport.value.ClientServices == null) //this.FilterClientReport.value.ClientServices == "" ||
    {
      this.FilterClientReport.value.ClientServices = 0;
    }
    else {
      this.getCrmClientReportGrid.ClientServices = this.FilterClientReport.value.ClientServices.toString();
    }
    this.getCrmClientReportGrid.RelationShipOwnerId = this.FilterClientReport.value.RelationShipOwnerId;
   this.getCrmClientReportGrid.RegionId =this. FilterClientReport.value.RegionId;
    this.getCrmClientReportGrid.IsActive = this.FilterClientReport.value.IsActive;
    this.getCrmClientReportGrid.export = 'Yes';
    this._clientReportservice.GetCRMClientReportList(this.getCrmClientReportGrid)
      .subscribe((response) => {
       
        if(response['result'].getClientReport.length==0)
        {
          swal.fire('Sorry','No Records to Export','error');
          
        }
        else{
        response['result'].getClientReport.forEach(childObj => {
          if (childObj.clientTypeId == 1) {
            childObj.ClientType = 'Insurance'
          }
          else {
            childObj.ClientType = 'Retail'
          }
        });
        if (this.val == 1) {
          debugger;
          // this._clientReportservice.exportAsExcelFileTest(this.getCrmClientReportGrid)
          // .subscribe((response) => {
          // });
          this._clientReportservice.exportAsExcelFile(response['result'].getClientReport);
        }

        else if (this.val == 0) {
       
         this._clientReportservice.generatePdf(response['result'].getClientReport);
        }
     }
     })
    
  }

  Edit(clientId: number): void {
    this._router.navigate(["/client-record/addedit-client-record/edit", clientId]);
  }


  // Onfocusin(event)
  // {
  //   debugger;
  //   this.IsExport=true;
  // }

}