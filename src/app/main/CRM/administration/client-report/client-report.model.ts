 
export class GetClientReport {
    CompanyName: string;
    ClientAddedDate:string;
    ClientType: string;
    PrincipalContact: string;
    RelationshipOwner :string;

}
export class ClientReportGrid {

    pageIndex: number;
    pageSize: number;
    sort: string;
    direction: string;
    CompanyName: string;
    ClientTypeId: number;
    ClientCreatedDateFrom: string;
    ClientCreatedDateTo: string;
    PrincipalContactEmail:string;
    RelationShipOwnerId: number;
    RegionId: number;
    ClientServices: string;
    IsActive: number;
    export:string;
   
}
  export class HCBClientLength{
     Length: number;
  }

  export class HCBClientReport {
     getClientReport: GetClientReport[];
     hcbClientLength: HCBClientLength[];
 }
 