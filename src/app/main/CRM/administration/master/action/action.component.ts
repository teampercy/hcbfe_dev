import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditActionComponent } from './add-edit-action/add-edit-action.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import {SuggestionComponent} from 'app/main/Common/suggestion/suggestion.component';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  illnessInjuryData: any;
  FilterIllnessInjury: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     ;
    this.FilterIllnessInjury = this._formbuilder.group({
      IllnessInjury: ['']
    });
    this.UserName = localStorage.getItem('UserName');
    this.GetIllnessInjuryGrid();
  }

  GetIllnessInjuryGrid(): void {
    this._masterService.GetddlIllnessInjury(39)
      .subscribe((response: any) => {
        this.illnessInjuryData = new MatTableDataSource<any>(response['result']);
        this.illnessInjuryData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
     
        this.illnessInjuryData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddIllnessInjury(): void {
   this.dialogRef = this.dialog.open(AddEditActionComponent, {
      disableClose: true,
      width: '400px',
      data: {
        IllnessInjuryId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetIllnessInjuryGrid();
      }
    });
  }

  EditIllnessInjury(illnessInjuryId: number): void {
    this.dialogRef = this.dialog.open(AddEditActionComponent, {
      disableClose: true,
      width: '400px',
      data: {
        IllnessInjuryId: illnessInjuryId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetIllnessInjuryGrid();
      }
    });
  }

  DeleteIllnessInjury(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {

      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Record cannot be deleted due to integrity references'} });
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',39,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetIllnessInjuryGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })

  }


  applyFilterIllnessInjury() {
    this.filterValue = this.FilterIllnessInjury.get('IllnessInjury').value;
    this.illnessInjuryData.filter = this.filterValue.trim().toLowerCase();

    if (this.illnessInjuryData.paginator) {
      this.illnessInjuryData.paginator.firstPage();
    }
  }

  ClearIllnessInjury(): void {
    this.FilterIllnessInjury.reset();
    this.GetIllnessInjuryGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
