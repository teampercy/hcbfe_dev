import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditBrandComponent } from './add-edit-brand/add-edit-brand.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import {SuggestionComponent} from 'app/main/Common/suggestion/suggestion.component';
@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class BrandComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  display:string;
  x:boolean=false;
  BrandData: any;
  Filterbrand: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.Filterbrand = this._formbuilder.group({
      Brand: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetBrandGrid();
  }

  GetBrandGrid(): void {
    this._masterService.GetMasterTypelistValues(13)
      .subscribe((response: any) => {

        if(response['result'].length == 0)
        {
          this.x=true;

        }
        else{
          this.x=false;
        }

        this.BrandData = new MatTableDataSource<any>(response['result']);
        this.BrandData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        this.BrandData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddTreatment(): void {
     this.dialogRef = this.dialog.open(AddEditBrandComponent, {
       disableClose: true,
       width: '400px',
       data: {
        brandId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetBrandGrid();
       }
     });
   }

   EditBrand(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditBrandComponent, {
       disableClose: true,
    width: '400px',
       data: {
        brandId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetBrandGrid();
      }
     });
   }

   Deletebrand(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
   
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',13,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetBrandGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })

  }
 
  applyFilterbrand() {
    this.filterValue = this.Filterbrand.get('Brand').value;
    this.BrandData.filter = this.filterValue.trim().toLowerCase();

    if (this.BrandData.paginator) {
      this.BrandData.paginator.firstPage();
    }
  }

  ClearTreatment(): void {
    this.Filterbrand.reset();
    this.GetBrandGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}

