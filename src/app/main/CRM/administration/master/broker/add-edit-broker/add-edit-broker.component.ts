import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MasterService } from '../../master.service';
import { SaveMasterList, updateAssessor } from '../../master.model';

@Component({
  selector: 'app-add-edit-broker',
  templateUrl: './add-edit-broker.component.html',
  styleUrls: ['./add-edit-broker.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class AddEditBrokerComponent implements OnInit {

  Count:number;
  name:string;
  NameTaken:boolean = false;
  heading:string;
  errorText: string;
  working: boolean;
  AssessorDetails: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditBrokerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
this.heading=this.data.heading;
    if (this.data.AssesorId > 0) {
      this.AssessorDetails = this._formBuilder.group({
        AssesorId: [this.data.AssesorId],
        // TypeId: [5],
        AssesorName:['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TeamEmailID:['',Validators.email],
        TeamPhoneNo:['']
      })
      this.SetValues();
    } else {
      this.AssessorDetails = this._formBuilder.group({
        AssesorName:['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TeamEmailID:['', Validators.email],
        TeamPhoneNo:['', Validators.pattern]
      })
    }
  }


  createvalidators(_master: MasterService) {
    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
        resolve(null);
    
      }
      else{
        this._master.CheckAssessorExistOrNot(control.value)
          .subscribe((response) => {
           // console.log(response);
            this.Count = response['result'].assessorName[0].resultCount;
           // console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }


  SetValues(): void {
    this._master.GetAssessorDetailById(this.data.AssesorId)
      .subscribe((response: any) => {
        this.name= response['result'][0].assessorName;
        this.AssessorDetails.patchValue({ AssesorName: response['result'][0].assessorName });
        this.AssessorDetails.patchValue({ TeamEmailID: response['result'][0].teamEmailID });
        this.AssessorDetails.patchValue({ TeamPhoneNo: response['result'][0].phoneNum });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SetFormat(event): boolean {
    return this._master.ValidatePhoneFormat(event);
  }


  Savebroker(): void {
    if (this.AssessorDetails.invalid)
      return;

    this.working = true;
    if (this.data.AssesorId>0) {
      const updatebroker: updateAssessor = Object.assign({}, this.AssessorDetails.value);

      this._master.UpdateAssessorDetail(updatebroker)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveAssessorDeatail(this.AssessorDetails.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}

