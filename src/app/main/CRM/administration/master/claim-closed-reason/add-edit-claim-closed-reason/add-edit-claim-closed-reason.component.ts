import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MasterService } from '../../master.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaveMasterList } from '../../master.model';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { promise } from 'selenium-webdriver';
import { resolve } from 'q';

@Component({
  selector: 'app-add-edit-claim-closed-reason',
  templateUrl: './add-edit-claim-closed-reason.component.html',
  styleUrls: ['./add-edit-claim-closed-reason.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class AddEditClaimClosedReasonComponent implements OnInit {

  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  ClaimClosedReason:FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditClaimClosedReasonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.Id > 0) {
      this.ClaimClosedReason = this._formBuilder.group({
        Id: [this.data.Id],
        TypeId: [6],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue:[null]
      })
      this.SetValues();
    } else {
      this.ClaimClosedReason = this._formBuilder.group({
        Name:['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [6],
        RelatedValue:[null]
      })
      
    }
    this.TypeId=this.ClaimClosedReason.value.TypeId;
  }

  createvalidators(_master: MasterService) {

    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
        resolve(null);
    
      }
      else{
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
           // console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
          //  console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }


  SetValues(): void {
    this._master.GetMasterListValueById(this.data.Id)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.ClaimClosedReason.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SaveClaimClosedReason(): void {
    if (this.ClaimClosedReason.invalid)
      return;

    this.working = true;
    if (this.data.Id) {
      const updateClaimClosedReason: SaveMasterList = Object.assign({}, this.ClaimClosedReason.value);

      this._master.UpdateMasterListValue(updateClaimClosedReason)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveMasterListValue(this.ClaimClosedReason.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}
