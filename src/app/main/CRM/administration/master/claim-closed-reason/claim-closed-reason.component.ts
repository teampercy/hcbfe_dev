import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditClaimClosedReasonComponent } from './add-edit-claim-closed-reason/add-edit-claim-closed-reason.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
import { String } from 'lodash';
@Component({
  selector: 'app-claim-closed-reason',
  templateUrl: './claim-closed-reason.component.html',
  styleUrls: ['./claim-closed-reason.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ClaimClosedReasonComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  filterValue: string;
  dialogdisplay: boolean=false;
  FilterClaimClosedReason: FormGroup;
  claimClosedReason: any;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.FilterClaimClosedReason = this._formbuilder.group({
      ClaimClosedReason: ['']
    });
    this.UserName = localStorage.getItem('UserName');
    this.GetClaimClosedReasonGrid();
  }

  GetClaimClosedReasonGrid(): void {
    this._masterService.GetddlClaimClosedReason(6)
      .subscribe((response: any) => {
        this.claimClosedReason = new MatTableDataSource<any>(response['result']);
        this.claimClosedReason.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }

        this.claimClosedReason.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddClaimClosedReason(): void {
    this.dialogRef = this.dialog.open(AddEditClaimClosedReasonComponent, {
      disableClose: true,
      width: '400px',
      data: {
        Id: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetClaimClosedReasonGrid();
      }
    });
  }

  EditClaimClosedReason(Id: number): void {

    this.dialogRef = this.dialog.open(AddEditClaimClosedReasonComponent, {
      disableClose: true,
      width: '400px',
      data: {
        Id: Id
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetClaimClosedReasonGrid();
      }
    });
  }

  DeleteClaimClosedReason(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
      .subscribe((response: any) => {
  
        if (response['result'][0].countId !=0)
         {
          this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
        } 
        else {
          const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
          dialogRef.afterClosed().subscribe(result => {
            if (result == 'Confirm') {
              this._masterService.deleteMasterListValueById(Id,'MasterList',6,this.UserName)
                .subscribe((response: any) => {
                  this.ConfirmDialog();
                  this.GetClaimClosedReasonGrid();
                }, (error: any) => {
                  if (error == null) {
                    this.errorText = 'Internal Server error';
                  }
                  else {
                    this.errorText = error;
                  }
                });
            }
          });
        }
      })

  }

  applyClaimClosedReason() {
    this.filterValue = this.FilterClaimClosedReason.get('ClaimClosedReason').value;
    this.claimClosedReason.filter = this.filterValue.trim().toLowerCase();

    if (this.claimClosedReason.paginator) {
      this.claimClosedReason.paginator.firstPage();
    }
  }

  ClearClaimClosedReason(): void {
    this.FilterClaimClosedReason.reset();
    this.GetClaimClosedReasonGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}
