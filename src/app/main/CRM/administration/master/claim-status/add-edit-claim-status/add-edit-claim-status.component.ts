import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MasterService } from '../../master.service';
import { SaveMasterList } from '../../master.model';
import { promise } from 'selenium-webdriver';
import { resolve } from 'q';


@Component({
  selector: 'app-add-edit-claim-status',
  templateUrl: './add-edit-claim-status.component.html',
  styleUrls: ['./add-edit-claim-status.component.scss']
})
export class AddEditClaimStatusComponent implements OnInit {
  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  IllnessInjury: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditClaimStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.IllnessInjuryId > 0) {
      this.IllnessInjury = this._formBuilder.group({
        Id: [this.data.IllnessInjuryId],
        TypeId: [37],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue:[null]
      })
      this.SetValues();
    } else {
      this.IllnessInjury = this._formBuilder.group({
        Name:  ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [37],
        RelatedValue:[null]
      })
    }
    this.TypeId=this.IllnessInjury.value.TypeId;
  }

  createvalidators(_master: MasterService) {
     ;

    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
         ;
        resolve(null);
    
      }
      else{
         ;
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
           // console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
         //   console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  SetValues(): void {
    this._master.GetMasterListValueById(this.data.IllnessInjuryId)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.IllnessInjury.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SaveIllnessInjury(): void {
    if (this.IllnessInjury.invalid)
      return;

    this.working = true;
    if (this.data.IllnessInjuryId) {
      const updateIlllnessinjury: SaveMasterList = Object.assign({}, this.IllnessInjury.value);

      this._master.UpdateMasterListValue(updateIlllnessinjury)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveMasterListValue(this.IllnessInjury.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}
