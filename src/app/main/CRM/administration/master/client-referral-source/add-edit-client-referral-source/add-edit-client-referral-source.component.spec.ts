import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditClientReferralSourceComponent } from './add-edit-client-referral-source.component';

describe('AddEditClientReferralSourceComponent', () => {
  let component: AddEditClientReferralSourceComponent;
  let fixture: ComponentFixture<AddEditClientReferralSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditClientReferralSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditClientReferralSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
