import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditCorporatePartnerComponent } from './add-edit-corporate-partner/add-edit-corporate-partner.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-corporate-partner',
  templateUrl: './corporate-partner.component.html',
  styleUrls: ['./corporate-partner.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class CorporatePartnerComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  servicesRequired: any;
  FilterCorPoratePartner: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterCorPoratePartner = this._formbuilder.group({
      Coporatepartner: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetCorporatePartnerGrid();
  }

  GetCorporatePartnerGrid(): void {
    this._masterService.GetMasterTypelistValues(9)
      .subscribe((response: any) => {
        this.servicesRequired = new MatTableDataSource<any>(response['result']);
        this.servicesRequired.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
    
        this.servicesRequired.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddCorporatePartner(): void {
     this.dialogRef = this.dialog.open(AddEditCorporatePartnerComponent, {
       disableClose: true,
       width: '400px',
       data: {
         CorporatePartnerId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetCorporatePartnerGrid();
       }
     });
   }

   EditCorporatePartner(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditCorporatePartnerComponent, {
       disableClose: true,
    width: '400px',
       data: {
        CorporatePartnerId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetCorporatePartnerGrid();
      }
     });
   }

  DeleteCorporatePartner(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',9,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetCorporatePartnerGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }


  applyFilterCorporatePartner() {
    this.filterValue = this.FilterCorPoratePartner.get('Coporatepartner').value;
    this.servicesRequired.filter = this.filterValue.trim().toLowerCase();

    if (this.servicesRequired.paginator) {
      this.servicesRequired.paginator.firstPage();
    }
  }

  ClearCorporatepartner(): void {
    this.FilterCorPoratePartner.reset();
    this.GetCorporatePartnerGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
