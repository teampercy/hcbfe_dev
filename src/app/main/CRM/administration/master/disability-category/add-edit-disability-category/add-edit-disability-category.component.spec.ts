import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditDisabilityCategoryComponent } from './add-edit-disability-category.component';

describe('AddEditDisabilityCategoryComponent', () => {
  let component: AddEditDisabilityCategoryComponent;
  let fixture: ComponentFixture<AddEditDisabilityCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditDisabilityCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDisabilityCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
