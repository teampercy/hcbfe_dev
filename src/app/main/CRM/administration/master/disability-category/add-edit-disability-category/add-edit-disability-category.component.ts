import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MasterService } from '../../master.service';
import { SaveMasterList } from '../../master.model';


@Component({
  selector: 'app-add-edit-disability-category',
  templateUrl: './add-edit-disability-category.component.html',
  styleUrls: ['./add-edit-disability-category.component.scss']
})
export class AddEditDisabilityCategoryComponent implements OnInit {
  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  employmentStatus: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditDisabilityCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.empStatusId > 0) {
      this.employmentStatus = this._formBuilder.group({
        Id: [this.data.empStatusId],
        TypeId: [34],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue:[null]
      })
      this.SetValues();
    } else {
      this.employmentStatus = this._formBuilder.group({
        Name:['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [34],
        RelatedValue:[null]
      })
    }
    this.TypeId=this.employmentStatus.value.TypeId;
  }

  createvalidators(_master: MasterService) {
     ;

    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
         ;
        resolve(null);
    
      }
      else{
         ;
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
          //  console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
          //  console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  SetValues(): void {
    this._master.GetMasterListValueById(this.data.empStatusId)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.employmentStatus.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SaveEmploymentStatus(): void {
    if (this.employmentStatus.invalid)
      return;

    this.working = true;
    if (this.data.empStatusId) {
      const updateEmpStatus: SaveMasterList = Object.assign({}, this.employmentStatus.value);

      this._master.UpdateMasterListValue(updateEmpStatus)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
       ;
      this.working = true;
      this._master.SaveMasterListValue(this.employmentStatus.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}

