import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisabilityCategoryComponent } from './disability-category.component';

describe('DisabilityCategoryComponent', () => {
  let component: DisabilityCategoryComponent;
  let fixture: ComponentFixture<DisabilityCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisabilityCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisabilityCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
