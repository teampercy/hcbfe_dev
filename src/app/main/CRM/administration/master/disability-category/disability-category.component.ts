import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditDisabilityCategoryComponent} from './add-edit-disability-category/add-edit-disability-category.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';


@Component({
  selector: 'app-disability-category',
  templateUrl: './disability-category.component.html',
  styleUrls: ['./disability-category.component.scss']
})
export class DisabilityCategoryComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  employmentStatus: any;
  FilterEmploymentStatus: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     ;
    this.FilterEmploymentStatus = this._formbuilder.group({
      Employmentstatus: ['']
    });
    this.UserName = localStorage.getItem('UserName');
    this.GetEmploymentstatusGrid();
  }

  GetEmploymentstatusGrid(): void {
    this._masterService.GetMasterTypelistValues(34)
      .subscribe((response: any) => {
        this.employmentStatus = new MatTableDataSource<any>(response['result']);
        this.employmentStatus.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
    
        this.employmentStatus.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddEmpStatus(): void {
    this.dialogRef = this.dialog.open(AddEditDisabilityCategoryComponent, {
      disableClose: true,
      width: '400px',
      data: {
        empStatusId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetEmploymentstatusGrid();
      }
    });
  }

  EditEmpStatus(Id: number): void {
    this.dialogRef = this.dialog.open(AddEditDisabilityCategoryComponent, {
      disableClose: true,
      width: '400px',
      data: {
        empStatusId: Id
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetEmploymentstatusGrid();
      }
    });
  }

  DeleteEmpStatus(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
     // console.log(response['result'][0].countId);
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Are you sure you want to delete this Item?'}  });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',34,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetEmploymentstatusGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText='Internal Server error';
                }
                else {
                  this.errorText=error;
                }
              });
          }
        });
      }
    })
  }
   
  applyFilterEmpStatus() {
     ;
  
    this.filterValue = this.FilterEmploymentStatus.get('Employmentstatus').value;
    this.employmentStatus.filter = this.filterValue.trim().toLowerCase();
    if (this.employmentStatus.paginator) {
      this.employmentStatus.paginator.firstPage();
    }
  }

  ClearEmpStatus(): void {
    this.FilterEmploymentStatus.reset();
    this.GetEmploymentstatusGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
