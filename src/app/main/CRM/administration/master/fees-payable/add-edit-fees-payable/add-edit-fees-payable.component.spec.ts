import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditFeesPayableComponent } from './add-edit-fees-payable.component';

describe('AddEditFeesPayableComponent', () => {
  let component: AddEditFeesPayableComponent;
  let fixture: ComponentFixture<AddEditFeesPayableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditFeesPayableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditFeesPayableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
