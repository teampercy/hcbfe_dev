import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditFundedTreatmentProviderComponent } from './add-edit-funded-treatment-provider/add-edit-funded-treatment-provider.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-funded-treatment-provider',
  templateUrl: './funded-treatment-provider.component.html',
  styleUrls: ['./funded-treatment-provider.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class FundedTreatmentProviderComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  TreatmentProviderData: any;
  FilterTreatmentProvider: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterTreatmentProvider = this._formbuilder.group({
     TreatmentProvider: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetFundedTreatmentGrid();
  }

  GetFundedTreatmentGrid(): void {
    this._masterService.GetMasterTypelistValues(18)
      .subscribe((response: any) => {
        this.TreatmentProviderData = new MatTableDataSource<any>(response['result']);
        this.TreatmentProviderData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
      
        this.TreatmentProviderData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddTreatment(): void {
     this.dialogRef = this.dialog.open(AddEditFundedTreatmentProviderComponent, {
       disableClose: true,
       width: '400px',
       data: {
        TreatmentProviderId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetFundedTreatmentGrid();
       }
     });
   }

   EditTreatment(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditFundedTreatmentProviderComponent, {
       disableClose: true,
    width: '400px',
       data: {
        TreatmentProviderId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetFundedTreatmentGrid();
      }
     });
   }

   DeleteTreatment(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Are you sure you want to delete this Item?'}  });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',18,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetFundedTreatmentGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }
 
  applyFilterTreatment() {
    this.filterValue = this.FilterTreatmentProvider.get('TreatmentProvider').value;
    this.TreatmentProviderData.filter = this.filterValue.trim().toLowerCase();

    if (this.TreatmentProviderData.paginator) {
      this.TreatmentProviderData.paginator.firstPage();
    }
  }

  ClearTreatment(): void {
    this.FilterTreatmentProvider.reset();
    this.GetFundedTreatmentGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}


