import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MasterService } from '../../master.service';
import { SaveMasterList } from '../../master.model';
@Component({
  selector: 'app-add-edit-incapacity-defination',
  templateUrl: './add-edit-incapacity-defination.component.html',
  styleUrls: ['./add-edit-incapacity-defination.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class AddEditIncapacityDefinationComponent implements OnInit {

  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  incapacityDefination: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditIncapacityDefinationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.DefinationId > 0) {
      this.incapacityDefination = this._formBuilder.group({
        Id: [this.data.DefinationId],
        TypeId: [14],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue:[null]
      })
      this.SetValues();
    } else {
      this.incapacityDefination = this._formBuilder.group({
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [14],
        RelatedValue:[null]
      })
    }
    this.TypeId=this.incapacityDefination.value.TypeId;
  }

  createvalidators(_master: MasterService) {
     ;

    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
         ;
        resolve(null);
    
      }
      else{
         ;
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
           // console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
           // console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  SetValues(): void {
    this._master.GetMasterListValueById(this.data.DefinationId)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.incapacityDefination.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  Savedefiantion(): void {
    if (this.incapacityDefination.invalid)
      return;

    this.working = true;
    if (this.data.DefinationId) {
      const updateDefination: SaveMasterList = Object.assign({}, this.incapacityDefination.value);

      this._master.UpdateMasterListValue(updateDefination)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveMasterListValue(this.incapacityDefination.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}
