import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelationshipOwnerComponent } from './relationship-owner/relationship-owner.component';
import { IllnessInjuryComponent } from './illness-injury/illness-injury.component';
import { AddEditIllnessInjuryComponent } from './illness-injury/add-edit-illness-injury/add-edit-illness-injury.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { ClaimClosedReasonComponent } from './claim-closed-reason/claim-closed-reason.component';
import { AuthGuard2 } from 'app/custom/auth.guard';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { AddEditClaimClosedReasonComponent } from './claim-closed-reason/add-edit-claim-closed-reason/add-edit-claim-closed-reason.component';
import { ReasonClosedComponent } from './reason-closed/reason-closed.component';
import { AddEditReasonClosedComponent } from './reason-closed/add-edit-reason-closed/add-edit-reason-closed.component';
import { ServicesRequiredComponent } from './services-required/services-required.component';
import { AddEditServicesRequiredComponent } from './services-required/add-edit-services-required/add-edit-services-required.component';
import { CorporatePartnerComponent } from './corporate-partner/corporate-partner.component';
import { AddEditCorporatePartnerComponent } from './corporate-partner/add-edit-corporate-partner/add-edit-corporate-partner.component';
import { ProductTypeComponent } from './product-type/product-type.component';
import { AddEditProductTypeComponent } from './product-type/add-edit-product-type/add-edit-product-type.component';
import { WaitingPeriodComponent } from './waiting-period/waiting-period.component';
import { AddEditWaitingPeriodComponent } from './waiting-period/add-edit-waiting-period/add-edit-waiting-period.component';
import { EmploymentStatusComponent } from './employment-status/employment-status.component';
import { AddEditEmploymentStatusComponent } from './employment-status/add-edit-employment-status/add-edit-employment-status.component';
import { IncapacityDefinationComponent } from './incapacity-defination/incapacity-defination.component';
import { AddEditIncapacityDefinationComponent } from './incapacity-defination/add-edit-incapacity-defination/add-edit-incapacity-defination.component';
import { TypeOfVisitComponent } from './type-of-visit/type-of-visit.component';
import { AddEditTypeOfVisitComponent } from './type-of-visit/add-edit-type-of-visit/add-edit-type-of-visit.component';
import { TypeOfCallComponent } from './type-of-call/type-of-call.component';
import { AddEditTypeOfCallComponent } from './type-of-call/add-edit-type-of-call/add-edit-type-of-call.component';
import { FundedTreatementTypeComponent } from './funded-treatement-type/funded-treatement-type.component';
import { AddEditFundedTreatmentTypeComponent } from './funded-treatement-type/add-edit-funded-treatment-type/add-edit-funded-treatment-type.component';
import { FundedTreatmentProviderComponent } from './funded-treatment-provider/funded-treatment-provider.component';
import { AddEditFundedTreatmentProviderComponent } from './funded-treatment-provider/add-edit-funded-treatment-provider/add-edit-funded-treatment-provider.component';
import { SchemeNameComponent } from './scheme-name/scheme-name.component';
import { AddEditSchemeNameComponent } from './scheme-name/add-edit-scheme-name/add-edit-scheme-name.component';
import { BrandComponent } from './brand/brand.component';
import { AddEditBrandComponent } from './brand/add-edit-brand/add-edit-brand.component';
import { BrokerComponent } from './broker/broker.component';
import { AddEditBrokerComponent } from './broker/add-edit-broker/add-edit-broker.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
import { OccupationalClassificationComponent } from './occupational-classification/occupational-classification.component';
import { AddEditOccupationalClassificationComponent } from './occupational-classification/add-edit-occupational-classification/add-edit-occupational-classification.component';
import { HealthTrustComponent } from './health-trust/health-trust.component';
import { AddEditHealthTrustComponent } from './health-trust/add-edit-health-trust/add-edit-health-trust.component';
import { RegionComponent } from './region/region.component';
import { AddEditRegionComponent } from './region/add-edit-region/add-edit-region.component';
import { BrokersComponent} from './brokers/brokers.component';
import { ClientReferralSourceComponent } from './client-referral-source/client-referral-source.component';
import { AddEditClientReferralSourceComponent } from './client-referral-source/add-edit-client-referral-source/add-edit-client-referral-source.component';
import { FeesPayableComponent } from './fees-payable/fees-payable.component';
import { AddEditFeesPayableComponent } from './fees-payable/add-edit-fees-payable/add-edit-fees-payable.component';
//import { AgreedSLAsComponent } from './agreed-slas/agreed-slas.component';
//import { AddeditAgreedSLAComponent } from './agreed-slas/addedit-agreed-sla/addedit-agreed-sla.component';
//import { ReportingFormatComponent } from './reporting-format/reporting-format.component';
//import { AddeditReportingFormatComponent } from './reporting-format/addedit-reporting-format/addedit-reporting-format.component';
//import { InvoiceFrequencyComponent } from './invoice-frequency/invoice-frequency.component';
//import { AddEditInvoiceFrequencyComponent } from './invoice-frequency/add-edit-invoice-frequency/add-edit-invoice-frequency.component';
import { DisabilityCategoryComponent } from './disability-category/disability-category.component';
import{ AddEditDisabilityCategoryComponent} from './disability-category/add-edit-disability-category/add-edit-disability-category.component';
import { PrincipalDeterminantAbsenceComponent } from './principal-determinant-absence/principal-determinant-absence.component';
import { AddEditPDAComponent } from './principal-determinant-absence/add-edit-pda/add-edit-pda.component';
import { ServiceRequestedComponent } from './service-requested/service-requested.component';
import { AddEditServiceRequestedComponent } from './service-requested/add-edit-service-requested/add-edit-service-requested.component';
import { ClaimStatusComponent } from './claim-status/claim-status.component';
import { ClaimsAssessorComponent } from './claims-assessor/claims-assessor.component';
import { ActionComponent } from './action/action.component';
import { OutcomeComponent } from './outcome/outcome.component';
import { AddEditClaimStatusComponent } from './claim-status/add-edit-claim-status/add-edit-claim-status.component';
import { AddEditClaimassessorComponent } from './claims-assessor/add-edit-claimassessor/add-edit-claimassessor.component';
import { AddEditActionComponent } from './action/add-edit-action/add-edit-action.component';
import { AddEditOutcomeComponent } from './outcome/add-edit-outcome/add-edit-outcome.component';
import { ServiceProviderComponent } from './service-provider/service-provider.component';
import { AddEditServiceProviderComponent } from './service-provider/add-edit-service-provider/add-edit-service-provider.component';
//import {AddReferenceOwnerComponent } from 'app/main/CRM/client-record/add-reference-owner/add-reference-owner.component';

const routes = [
  {
    path: 'relationship-owner',
    component: RelationshipOwnerComponent,
    canActivate: [AuthGuard2]
  },

  {
    path: 'illness-injury',
    component: IllnessInjuryComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'claim-closed-reason',
    component: ClaimClosedReasonComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'reason-closed',
    component: ReasonClosedComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'broker',
    component: BrokerComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'services-required',
    component: ServicesRequiredComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'corporate-partner',
    component: CorporatePartnerComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'product-type',
    component: ProductTypeComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'waiting-period',
    component: WaitingPeriodComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'brand',
    component: BrandComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'employment-status',
    component: EmploymentStatusComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'incapacity-defination',
    component: IncapacityDefinationComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'type-of-visit',
    component: TypeOfVisitComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'type-of-call',
    component: TypeOfCallComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'funded-treatement-type',
    component: FundedTreatementTypeComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'funded-treatement-provider',
    component: FundedTreatmentProviderComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'scheme-name',
    component: SchemeNameComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'occupational-classification',
    component: OccupationalClassificationComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'health-trust',
    component: HealthTrustComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'region',
    component: RegionComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'brokers',
    component: BrokersComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'client-referral-source',
    component: ClientReferralSourceComponent,
     canActivate: [AuthGuard2]
  },

  {
    path: 'fees-payable',
    component: FeesPayableComponent,
     canActivate: [AuthGuard2]
  },

  {
    path: 'disability-category',
    component: DisabilityCategoryComponent,
     canActivate: [AuthGuard2]
  },
 // {
//    path: 'agreed-sla',
//    component: AgreedSLAsComponent,
//     canActivate: [AuthGuard2]
//  },
//  {
//    path: 'reporting-format',
//    component: ReportingFormatComponent,
  //   canActivate: [AuthGuard2]
//  },
 // {
 //   path: 'invoice-frequency',
 //   component: InvoiceFrequencyComponent,
 //    canActivate: [AuthGuard2]
//  },
  {
    path:'principal-determinant-absence',
    component:PrincipalDeterminantAbsenceComponent ,
    canActivate:[AuthGuard2]
  },
  {
    path:'service-requested',
    component:ServiceRequestedComponent ,
    canActivate:[AuthGuard2]
  },
  {
    path:'claim-status',
    component:ClaimStatusComponent,
    canActivate:[AuthGuard2]
  },
  {
    path:'claims-assessor',
    component:ClaimsAssessorComponent,
    canActivate:[AuthGuard2]
  },
  {
    path:'action',
    component:ActionComponent,
    canActivate:[AuthGuard2]
  },
  {
    path:'outcome',
    component:OutcomeComponent,
    canActivate:[AuthGuard2]
  },
  {
    path:'service-provider',
    component:ServiceProviderComponent,
    canActivate:[AuthGuard2]
  }
];

@NgModule({
  declarations: [
    RelationshipOwnerComponent,
    IllnessInjuryComponent,
    ClaimClosedReasonComponent,
    AddEditIllnessInjuryComponent,
    AddEditClaimClosedReasonComponent,
    ReasonClosedComponent,
    AddEditReasonClosedComponent,
    ServicesRequiredComponent,
    AddEditServicesRequiredComponent,
    CorporatePartnerComponent,
    AddEditCorporatePartnerComponent,
    ProductTypeComponent,
    AddEditProductTypeComponent,
    WaitingPeriodComponent,
    AddEditWaitingPeriodComponent,
    EmploymentStatusComponent,
    AddEditEmploymentStatusComponent,
    IncapacityDefinationComponent,
    AddEditIncapacityDefinationComponent,
    TypeOfVisitComponent,
    AddEditTypeOfVisitComponent,
    TypeOfCallComponent,
    AddEditTypeOfCallComponent,
    FundedTreatementTypeComponent,
    AddEditFundedTreatmentTypeComponent,
    FundedTreatmentProviderComponent,
    AddEditFundedTreatmentProviderComponent,
    SchemeNameComponent,
    AddEditSchemeNameComponent,
    BrandComponent,
    AddEditBrandComponent,
    BrokerComponent,
    AddEditBrokerComponent,
    SuggestionComponent,
    OccupationalClassificationComponent,
    AddEditOccupationalClassificationComponent,
    HealthTrustComponent,
    AddEditHealthTrustComponent,
    RegionComponent,
    AddEditRegionComponent,
    BrokersComponent,
    ClientReferralSourceComponent,
    AddEditClientReferralSourceComponent,
    FeesPayableComponent,
    AddEditFeesPayableComponent,
   // AgreedSLAsComponent,
   // AddeditAgreedSLAComponent,
   // ReportingFormatComponent,
   // AddeditReportingFormatComponent,
  //  InvoiceFrequencyComponent,
  //  AddEditInvoiceFrequencyComponent,
    DisabilityCategoryComponent,
    AddEditDisabilityCategoryComponent,
    PrincipalDeterminantAbsenceComponent,
    AddEditPDAComponent,
    ServiceRequestedComponent,
    AddEditServiceRequestedComponent,
    ClaimStatusComponent,
    ClaimsAssessorComponent,
    ActionComponent,
    OutcomeComponent,
    AddEditClaimStatusComponent,
    AddEditClaimassessorComponent,
    AddEditActionComponent,
    AddEditOutcomeComponent,
    ServiceProviderComponent,
    AddEditServiceProviderComponent
    //AddReferenceOwnerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FuseSharedModule,
    MaterialModule,
  ],
  entryComponents: [
    AddEditIllnessInjuryComponent,
    AddEditClaimClosedReasonComponent,
    AddEditReasonClosedComponent,
    AddEditServicesRequiredComponent,
    AddEditCorporatePartnerComponent,
    AddEditProductTypeComponent,
    AddEditWaitingPeriodComponent,
    AddEditEmploymentStatusComponent,
    AddEditIncapacityDefinationComponent,
    AddEditTypeOfVisitComponent,
    AddEditTypeOfCallComponent,
    AddEditFundedTreatmentTypeComponent,
    AddEditFundedTreatmentProviderComponent,
    AddEditSchemeNameComponent,
    AddEditBrandComponent,
    AddEditBrokerComponent,
    SuggestionComponent,
    AddEditOccupationalClassificationComponent,
    AddEditHealthTrustComponent,
    AddEditRegionComponent,
    AddEditFeesPayableComponent,
    AddEditClientReferralSourceComponent,
    AddEditDisabilityCategoryComponent,
    //AddReferenceOwnerComponent,
    DeleteDialogComponent,
    AddEditPDAComponent,
    AddEditServiceRequestedComponent,
    AddEditClaimStatusComponent,
    AddEditClaimassessorComponent,
    AddEditActionComponent,
    AddEditOutcomeComponent,
    AddEditServiceProviderComponent
  ]
})
export class MasterModule { }
