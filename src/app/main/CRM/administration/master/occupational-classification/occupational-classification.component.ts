import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { ActivatedRoute } from '@angular/router';
import { AddEditOccupationalClassificationComponent } from './add-edit-occupational-classification/add-edit-occupational-classification.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-occupational-classification',
  templateUrl: './occupational-classification.component.html',
  styleUrls: ['./occupational-classification.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class OccupationalClassificationComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  occupationalClassification: any;
  FilterOccupationalClassification: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterOccupationalClassification = this._formbuilder.group({
      OccupationalClassification: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetOccupationalClassificationGrid();
  }

  GetOccupationalClassificationGrid(): void {
    this._masterService.GetMasterTypelistValues(26)
      .subscribe((response: any) => {
        this.occupationalClassification = new MatTableDataSource<any>(response['result']);
        this.occupationalClassification.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.occupationalClassification.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddOccupationalClassification(): void {
     this.dialogRef = this.dialog.open(AddEditOccupationalClassificationComponent, {
       disableClose: true,
       width: '400px',
       data: {
        OccupationalClassificationId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetOccupationalClassificationGrid();
       }
     });
   }

   EditOccupationalQualification(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditOccupationalClassificationComponent, {
       disableClose: true,
       width: '400px',
       data: {
        OccupationalClassificationId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetOccupationalClassificationGrid();
      }
     });
   }

   DeleteOccupationalQualification(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
     // console.log(response['result'][0].countId);
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Record cannot be deleted due to integrity references'} });
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',26,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetOccupationalClassificationGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })
  }

  applyFilterOccupationalClassification() {
    this.filterValue = this.FilterOccupationalClassification.get('Defination').value;
    this.occupationalClassification.filter = this.filterValue.trim().toLowerCase();

    if (this.occupationalClassification.paginator) {
      this.occupationalClassification.paginator.firstPage();
    }
  }

  ClearOccupationalClassification(): void {
    this.FilterOccupationalClassification.reset();
    this.GetOccupationalClassificationGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }
}
