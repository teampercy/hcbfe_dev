import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPDAComponent } from './add-edit-pda.component';

describe('AddEditPDAComponent', () => {
  let component: AddEditPDAComponent;
  let fixture: ComponentFixture<AddEditPDAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPDAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPDAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
