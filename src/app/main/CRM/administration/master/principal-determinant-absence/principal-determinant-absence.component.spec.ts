import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincipalDeterminantAbsenceComponent } from './principal-determinant-absence.component';

describe('PrincipalDeterminantAbsenceComponent', () => {
  let component: PrincipalDeterminantAbsenceComponent;
  let fixture: ComponentFixture<PrincipalDeterminantAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrincipalDeterminantAbsenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrincipalDeterminantAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
