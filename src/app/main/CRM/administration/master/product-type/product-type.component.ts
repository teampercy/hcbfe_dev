import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditProductTypeComponent } from './add-edit-product-type/add-edit-product-type.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ProductTypeComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  ProductType: any;
  FilterProductType: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterProductType = this._formbuilder.group({
      ServicesRequired: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetProductTypedGrid();
  }

  GetProductTypedGrid(): void {
    this._masterService.GetMasterTypelistValues(10)
      .subscribe((response: any) => {
        this.ProductType = new MatTableDataSource<any>(response['result']);
        this.ProductType.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.ProductType.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddProductType(): void {
     this.dialogRef = this.dialog.open(AddEditProductTypeComponent, {
       disableClose: true,
       width: '400px',
       data: {
         ProducttypeId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetProductTypedGrid();
       }
     });
   }

   EditProductType(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditProductTypeComponent, {
       disableClose: true,
    width: '400px',
       data: {
        ProducttypeId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetProductTypedGrid();
      }
     });
   }

   DeleteProductType(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',10,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetProductTypedGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }

  applyFilterProductType() {
     ;
    this.filterValue = this.FilterProductType.get('ServicesRequired').value;
    this.ProductType.filter = this.filterValue.trim().toLowerCase();

    if (this.ProductType.paginator) {
      this.ProductType.paginator.firstPage();
    }
  }

  ClearProductType(): void {
    this.FilterProductType.reset();
    this.GetProductTypedGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
