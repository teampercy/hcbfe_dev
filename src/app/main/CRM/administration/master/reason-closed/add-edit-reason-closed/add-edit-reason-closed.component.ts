import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MasterService } from '../../master.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaveMasterList } from '../../master.model';
import { promise } from 'selenium-webdriver';
import { resolve } from 'q';


@Component({
  selector: 'app-add-edit-reason-closed',
  templateUrl: './add-edit-reason-closed.component.html',
  styleUrls: ['./add-edit-reason-closed.component.scss']
})
export class AddEditReasonClosedComponent implements OnInit {

  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  ReasonClosed: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditReasonClosedComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.Id > 0) {
      this.ReasonClosed = this._formBuilder.group({
        Id: [this.data.Id],
        TypeId: [7],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue: [null]
      })
      this.SetValues();
    } else {
      this.ReasonClosed = this._formBuilder.group({
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [7],
        RelatedValue: [null]
      })
    }
    this.TypeId=this.ReasonClosed.value.TypeId;
  }


  createvalidators(_master: MasterService) {
     ;

    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
         ;
        resolve(null);
    
      }
      else{
         ;
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
          //  console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
          //  console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  SetValues(): void {
    this._master.GetMasterListValueById(this.data.Id)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.ReasonClosed.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SaveReasonClosed(): void {
    if (this.ReasonClosed.invalid)
      return;

    this.working = true;
    if (this.data.Id) {
      const updateReasonClosed: SaveMasterList = Object.assign({}, this.ReasonClosed.value);

      this._master.UpdateMasterListValue(updateReasonClosed)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveMasterListValue(this.ReasonClosed.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }
}
