import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditReasonClosedComponent } from './add-edit-reason-closed/add-edit-reason-closed.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-reason-closed',
  templateUrl: './reason-closed.component.html',
  styleUrls: ['./reason-closed.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ReasonClosedComponent implements OnInit {
  
  working: boolean;
  UserId:number;
  UserName:string;
  errorText: string;
  dialogRef: any;
  filterValue: string;
  reasonClosed:any;
  FilterReasonClosed:FormGroup;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.FilterReasonClosed = this._formbuilder.group({
      ReasonClosed: ['']
    });
    this.UserId = parseInt(localStorage.getItem('UserId'));
    this.UserName = localStorage.getItem('UserName');
    this.GetReasonClosedGrid();
  }

  GetReasonClosedGrid(): void {
    this._masterService.GetddlReasonClosed(7)
      .subscribe((response: any) => {
        this.reasonClosed = new MatTableDataSource<any>(response['result']);
        this.reasonClosed.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
       // this.illnessInjuryData.sort = this.sort;
        this.reasonClosed.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddReasonClosed(): void {
    this.dialogRef = this.dialog.open(AddEditReasonClosedComponent, {
      disableClose: true,
      width: '400px',
      data: {
        Id: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this. GetReasonClosedGrid();
      }
    });
  }

  EditReasonClosed(Id: number): void {
    this.dialogRef = this.dialog.open(AddEditReasonClosedComponent, {
      disableClose: true,
      width: '400px',
      data: {
       Id: Id
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this. GetReasonClosedGrid();
      }
    });
  }
  
  DeleteReasonClosed(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',7,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this. GetReasonClosedGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }

  applyReasonClosed() {
    this.filterValue = this.FilterReasonClosed.get('ReasonClosed').value;
    this.reasonClosed.filter = this.filterValue.trim().toLowerCase();

    if (this.reasonClosed.paginator) {
      this.reasonClosed.paginator.firstPage();
    }
  }

  ClearReasonClosed(): void {
    this.FilterReasonClosed.reset();
    this. GetReasonClosedGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}
