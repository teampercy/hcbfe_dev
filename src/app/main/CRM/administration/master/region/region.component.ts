import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditRegionComponent } from './add-edit-region/add-edit-region.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import {SuggestionComponent} from 'app/main/Common/suggestion/suggestion.component';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class RegionComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  display:string;
  NoReocrds:boolean=false;
  RegionData: any;
  FilterRegion: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterRegion = this._formbuilder.group({
      Region: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetRegionGrid();
  }

  GetRegionGrid(): void {
    this._masterService.GetMasterTypelistValues(24)
      .subscribe((response: any) => {
       // console.log(response['result'].length);
        if(response['result'].length == 0)
        {
          this.NoReocrds=true;

        }
        else{
          this.NoReocrds=false;
        }

        this.RegionData = new MatTableDataSource<any>(response['result']);
        this.RegionData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.RegionData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddRegion(): void {
     this.dialogRef = this.dialog.open(AddEditRegionComponent, {
       disableClose: true,
       width: '400px',
       data: {
        RegionId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetRegionGrid();
       }
     });
   }

   EditRegion(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditRegionComponent, {
       disableClose: true,
    width: '400px',
       data: {
        RegionId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetRegionGrid();
      }
     });
   }

   DeleteRegion(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
     // console.log(response['result'][0].countId);
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',24,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetRegionGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })

  }
 
  applyFilterRegion() {

    this.filterValue = this.FilterRegion.get('Region').value;
    this.RegionData.filter = this.filterValue.trim().toLowerCase();

    if (this.RegionData.paginator) {
      this.RegionData.paginator.firstPage();
    }
  }

  ClearFilter(): void {
    this.FilterRegion.reset();
    this.GetRegionGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}

