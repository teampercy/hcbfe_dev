import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditSchemeNameComponent } from './add-edit-scheme-name/add-edit-scheme-name.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
 
@Component({
  selector: 'app-scheme-name',
  templateUrl: './scheme-name.component.html',
  styleUrls: ['./scheme-name.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class SchemeNameComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  schemeData: any;
  FilterScheme: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name','RelatedValue', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterScheme = this._formbuilder.group({
      SchemeName: [null],
      SchemeNumber: [null]
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetSchemeGrid();
  }

  GetSchemeGrid(): void {
    this._masterService.GetMasterTypelistValues(19)
      .subscribe((response: any) => {
        this.schemeData = new MatTableDataSource<any>(response['result']);
        this.schemeData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.schemeData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddScheme(): void {
     this.dialogRef = this.dialog.open(AddEditSchemeNameComponent, {
       disableClose: true,
       width: '400px',
       data: {
         schemeId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetSchemeGrid();
       }
     });
   }

   EditScheme(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditSchemeNameComponent, {
       disableClose: true,
    width: '400px',
       data: {
        schemeId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetSchemeGrid();
      }
     });
   }

   DeleteSheme(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
     // console.log(response['result'][0].countId);
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Record cannot be deleted due to integrity references'} });
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' , data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',19,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetSchemeGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })
  }

  applyFilterScheme() {
     ;
    this.filterValue = this.FilterScheme.get('SchemeName').value;
    this.schemeData.filter = this.filterValue.trim().toLowerCase();
    this.filterValue = this.FilterScheme.get('SchemeNumber').value;
    this.schemeData.filter = this.filterValue.trim().toLowerCase();

    if (this.schemeData.paginator) {
      this.schemeData.paginator.firstPage();
    }
  }

  ClearCall(): void {
    this.FilterScheme.reset();
    this.GetSchemeGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}


