import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditServiceProviderComponent } from './add-edit-service-provider/add-edit-service-provider.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import {SuggestionComponent} from 'app/main/Common/suggestion/suggestion.component';
import {GetSPCompanyGrid} from '../master.model'

@Component({
  selector: 'app-service-provider',
  templateUrl: './service-provider.component.html',
  styleUrls: ['./service-provider.component.scss']
})
export class ServiceProviderComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  SPCompanyData: any;
  FilterSpCompany: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['CompanyName', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  
    this.FilterSpCompany = this._formbuilder.group({
      CompanyName: ['']
    });
    this.UserName = localStorage.getItem('UserName');
    this.GetServiceProviderCompany();
  }

  GetServiceProviderCompany(): void {
    debugger;
    this._masterService.GetServiceProviderCompany()
      .subscribe((response: any) => {
        debugger;
        console.log(response);
        this.SPCompanyData = new MatTableDataSource<any>(response['result']);
        this.SPCompanyData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
     
        this.SPCompanyData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddCompany(): void {
    this.dialogRef = this.dialog.open(AddEditServiceProviderComponent, {
      disableClose: true,
      width: '400px',
      data: {
        SpCompanyId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetServiceProviderCompany();
      }
    });
  }

  EditCompany(CompanyId: number): void {
    debugger;
    this.dialogRef = this.dialog.open(AddEditServiceProviderComponent, {
      disableClose: true,
      width: '400px',
      data: {
        SpCompanyId: CompanyId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetServiceProviderCompany();
      }
    });
  }

  DeleteServiceProvider(Id: number): void {
    debugger;
    this._masterService.SPCompanyDeleteOrNot(Id)
    .subscribe((response: any) => {
debugger;
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Record cannot be deleted due to integrity references'} });
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.DeleteSPcompany(Id)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetServiceProviderCompany();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })

  }


  applyFilterOnSPCompany() {
    this.filterValue = this.FilterSpCompany.get('CompanyName').value;
    this.SPCompanyData.filter = this.filterValue.trim().toLowerCase();

    if (this.SPCompanyData.paginator) {
      this.SPCompanyData.paginator.firstPage();
    }
  }

  ClearIllnessInjury(): void {
    this.FilterSpCompany.reset();
    this.GetServiceProviderCompany();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
