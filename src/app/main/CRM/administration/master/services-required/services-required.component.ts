import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditServicesRequiredComponent } from './add-edit-services-required/add-edit-services-required.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-services-required',
  templateUrl: './services-required.component.html',
  styleUrls: ['./services-required.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ServicesRequiredComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  servicesRequired: any;
  FilterServicesRequired: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterServicesRequired = this._formbuilder.group({
      ServicesRequired: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetServicesRequiredGrid();
  }

  GetServicesRequiredGrid(): void {
    this._masterService.GetddlServiceRequired(1)
      .subscribe((response: any) => {
        this.servicesRequired = new MatTableDataSource<any>(response['result']);
        this.servicesRequired.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.servicesRequired.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddServicesRequired(): void {
     this.dialogRef = this.dialog.open(AddEditServicesRequiredComponent, {
       disableClose: true,
       width: '400px',
       data: {
         servicesRequiredId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetServicesRequiredGrid();
       }
     });
   }

   EditServicesRequired(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditServicesRequiredComponent, {
       disableClose: true,
    width: '400px',
       data: {
        servicesRequiredId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetServicesRequiredGrid();
      }
     });
   }

   DeleteServicesRequired(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',1,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetServicesRequiredGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }

  // deleteMember(MemberId): void {
  //   const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '500px', height: '200px' });
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result == 'Confirm') {
  //       this._MemberService.DeleteMember(MemberId)
  //         .subscribe((response: any) => {
  //           this._matSnackBar.open('Member deleted successfully', 'x', {
  //             verticalPosition: 'top',
  //             duration: 5000,
  //             panelClass: ['snackbarSuccess']
  //           });
  //           this.getMembers(this.page);
  //         }, (error: any) => {
  //           if (error == null) {
  //             this.errorMessage('Internal Server error');
  //           }
  //           else {
  //             this.errorMessage(error);
  //           }
  //         });
  //     }
  //     console.log('The dialog was closed');
  //     this.spinnerWithoutBackdrop = false;
  //   });
  // }

  applyFilterServicesRequired() {
    this.filterValue = this.FilterServicesRequired.get('ServicesRequired').value;
    this.servicesRequired.filter = this.filterValue.trim().toLowerCase();

    if (this.servicesRequired.paginator) {
      this.servicesRequired.paginator.firstPage();
    }
  }

  ClearServicesRequired(): void {
    this.FilterServicesRequired.reset();
    this.GetServicesRequiredGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
