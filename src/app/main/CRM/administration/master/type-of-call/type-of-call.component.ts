import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditTypeOfCallComponent } from './add-edit-type-of-call/add-edit-type-of-call.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-type-of-call',
  templateUrl: './type-of-call.component.html',
  styleUrls: ['./type-of-call.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class TypeOfCallComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  typeOfCallData: any;
  FilterTypeOfCall: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterTypeOfCall = this._formbuilder.group({
      Calls: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetCallGrid();
  }

  GetCallGrid(): void {
    this._masterService.GetMasterTypelistValues(16)
      .subscribe((response: any) => {
        this.typeOfCallData = new MatTableDataSource<any>(response['result']);
        this.typeOfCallData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.typeOfCallData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddCall(): void {
     this.dialogRef = this.dialog.open(AddEditTypeOfCallComponent, {
       disableClose: true,
       width: '400px',
       data: {
         CallId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetCallGrid();
       }
     });
   }

   EditCall(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditTypeOfCallComponent, {
       disableClose: true,
    width: '400px',
       data: {
        CallId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetCallGrid();
      }
     });
   }

   DeleteCall(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Are you sure you want to delete this Item?'}  });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',16,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetCallGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }

  applyFilterCall() {
     ;
    this.filterValue = this.FilterTypeOfCall.get('Calls').value;
    this.typeOfCallData.filter = this.filterValue.trim().toLowerCase();

    if (this.typeOfCallData.paginator) {
      this.typeOfCallData.paginator.firstPage();
    }
  }

  ClearCall(): void {
    this.FilterTypeOfCall.reset();
    this.GetCallGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}


