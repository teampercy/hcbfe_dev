import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditUserRecordComponent } from './addedit-user-record.component';

describe('AddeditUserRecordComponent', () => {
  let component: AddeditUserRecordComponent;
  let fixture: ComponentFixture<AddeditUserRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditUserRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditUserRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
