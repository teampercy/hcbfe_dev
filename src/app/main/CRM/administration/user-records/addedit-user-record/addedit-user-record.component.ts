import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ClientRecordService } from 'app/main/CRM/client-record/client-record.service';
import { UserRecordsService } from '../user-records.service';
import { UserRecordsComponent } from '../user-records.component';
import { AddUpdateUser } from './addedit-user-record.model';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import {MasterService} from 'app/main/CRM/administration/master/master.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-addedit-user-record',
  templateUrl: './addedit-user-record.component.html',
  styleUrls: ['./addedit-user-record.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddeditUserRecordComponent implements OnInit {

  formgroup: FormGroup;
  UserID: number;
  dialogTitle: string;
  userRecordByID: any;
  //UserTypeId: any;
  UserType: any;
  userList: any;
  working: any;
  readable: boolean;
  disableCheckCM: boolean;
  errorText: string;
  Password: any;
  emailAddress: string;
  Count: number;
  CanWrokAsCM: boolean;
  ShowCMCheckbox: boolean;
  UserTypeId :any;
  SpCompanyList :any;
  ShowSPDropDown:boolean;
  objuserRecordComponent: UserRecordsComponent
  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    public dialog: MatDialog,
    private _route: ActivatedRoute,
    private _UserRecord: UserRecordsService,
    private _clientRecord: ClientRecordService,
    private _masterService: MasterService,
    private _matSnackBar: MatSnackBar,
    private fb: FormBuilder
  ) { }

  ngOnInit() {


    this._route.params.forEach(
      (params: Params) => {
        this.UserID = params["id"];
      }
    );
    if (this.UserID == null || this.UserID == undefined) {
      this.UserID = 0;
    }
    this._masterService.GetServiceProviderCompany()
    .subscribe((response: any) => {
      debugger;
      console.log(response);
      this.SpCompanyList=response['result'];
    });
    this.GetCRMUserType();
    if (this.UserID) {
      this.dialogTitle = "Edit";
      this.formgroup = this._formbuilder.group({
        UserID: [this.UserID],
        FullName: ['', Validators.required],
        EmailAddress: ['', [Validators.required], Validators.composeAsync([this.createvalidators(this._UserRecord)])],
        Password: ['', Validators.required],
        Address: [''],
        City: [''],
        TelephoneNumber: ['', [Validators.maxLength]],
        AccountActive: [0],
        IncoID: [],
        NominatedNurseID: [],
        LastLoginDate: [],
        PasswordExpiryDate: [],
        userList: [],
        UserTypeId: ['', [Validators.required]],
        CanWrokAsCM:[],
        SPComapny:['', [Validators.required]],

      });
      this.setData();
    } else {
      this.dialogTitle = "Add";
      this.formgroup = this._formbuilder.group({
        UserID: [this.UserID],
        FullName: ['', Validators.required],
       EmailAddress: ['', [Validators.required], Validators.composeAsync([this.createvalidators(this._UserRecord)])],
        Password: ['', Validators.required],
        Address: [''],
        City: [''],
        TelephoneNumber: ['', [Validators.maxLength]],
        AccountActive: [0],
        IncoID: [],
        NominatedNurseID: [],
        LastLoginDate: [],
        PasswordExpiryDate: [],
        userList: [],
        UserTypeId: ['', [Validators.required]],
        CanWrokAsCM:[],
        SPComapny:['', [Validators.required]],
      });
      this.readable = true;
    }
  }


  createvalidators(_UserRecord: UserRecordsService) {
  
    return control => new Promise((resolve: any) => {
      
      // if (this.emailAddress == control.value || this.emailAddress == "") {
      
      //   resolve(null);
      // }
      // else {
       var emailAddress;
        emailAddress = control.value.replace(/\s/g, "");
        this._UserRecord.CheckCRMUser(emailAddress,this.UserID)
          .subscribe((response) => {
          //  console.log(response);
            this.Count = response['result'].emailCheck[0].resultCount;
         //   console.log(this.Count);
            if (this.Count != 0) {
              resolve({ EmailTaken: true });

            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ EmailTaken: true });
            } else {
              resolve(null);
            }
          })
     // }
    })

  }


  setData(): void {

    if(parseInt(localStorage.getItem('UserTypeId')) ==2)
    {
      this.disableCheckCM =false;
    }else{ this.disableCheckCM =true;}

    this._UserRecord.GetCRMUserRecord(this.UserID)
      .subscribe((response) => {
        this.UserTypeId=response['result'][0].userTypeId;
        if(this.UserTypeId==1)
        {
          this.ShowCMCheckbox=true;
        }else{
          this.ShowCMCheckbox=false;
        }
       // console.log(response);

this.emailAddress = response['result'][0].emailAddress;
        this.formgroup.patchValue({ EmailAddress: response['result'][0].emailAddress });
        this.formgroup.patchValue({ FullName: response['result'][0].fullName });
        this.formgroup.patchValue({ Password: response['result'][0].password });
        this.formgroup.patchValue({ Address: response['result'][0].address });
        this.formgroup.patchValue({ City: response['result'][0].city });
        this.formgroup.patchValue({ TelephoneNumber: response['result'][0].telephoneNumber });
        this.formgroup.patchValue({ UserTypeId: response['result'][0].userTypeId });
        this.formgroup.patchValue({CanWrokAsCM:response['result'][0].canWrokAsCM==1?true:false});
        
        if(this.UserTypeId==5)
        {
    this.ShowSPDropDown=true;
    this.formgroup.get('SPComapny').updateValueAndValidity();
    this.formgroup.get('SPComapny').setValidators(Validators.required);
    this.formgroup.patchValue({ SPComapny: response['result'][0].spCompanyId });
        }
        else{
          this.ShowSPDropDown=false;
          this.formgroup.get('SPComapny').reset();
          this.formgroup.get('SPComapny').clearValidators();
          this.formgroup.get('SPComapny').updateValueAndValidity();
        }
        if (response['result'][0].userType == "SuperUser") {
          this.readable = false;
        }
        else {
          this.readable = true;
        }

        this.formgroup.patchValue({ userList: response['result'][0].userID });
        this.formgroup.patchValue({ LastLoginDate: response['result'][0].lastLoginDate });
        if (response['result'][0].accountActive == 1) {
          this.formgroup.patchValue({ AccountActive: 1 });
        }
        else {
          this.formgroup.patchValue({ AccountActive: 0 });
        }

      })

  }

  GetCRMUserType(): void {

    this._UserRecord.GetCRMUserType()
      .subscribe(responsdata => {

       // this.UserTypeId = responsdata['result'];
        this.UserType = responsdata['result'];

      })
  }


  back() {
    this._router.navigate(['/administration/user-records']);
  }

  Save(): void {

    if (this.UserID) {
      this.working = true;
      const updateCRMUserModel: AddUpdateUser = Object.assign({}, this.formgroup.value);
      this._UserRecord.UpdateCRMUser(updateCRMUserModel)
        .subscribe((response: number) => {
          var Result = response['result'];
          this.ConfirmDialog();
          //alert("succcess");
          // this.ConfirmDialog();

        }, (error: any) => {
          this.errorText = error;
          // this.errorMessage(this.errorText);
          this.working = false;
        });

    }
    else {

      this.working = true;
      if(this.formgroup.get('CanWrokAsCM').value==null || this.formgroup.get('CanWrokAsCM').value==undefined)
      {
        this.formgroup.value.CanWrokAsCM=false;
      }
      const addCRMUserRecord: AddUpdateUser = Object.assign({}, this.formgroup.value);
      this._UserRecord.AddCRMUser(addCRMUserRecord)
        .subscribe((response: number) => {
    
          var res = response['result'];
          this.ConfirmDialog();
          //alert("success");
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        })

    }
  }
  onUsertypeChange(){
    debugger;
    var UserType=this.formgroup.get('UserTypeId').value;
    if(this.UserID==0){
    if(UserType==5)
    {
this.ShowSPDropDown=true;
this.formgroup.get('SPComapny').updateValueAndValidity();
this.formgroup.get('SPComapny').setValidators(Validators.required);
    }
    else{
      this.ShowSPDropDown=false;
      this.formgroup.get('SPComapny').reset();
      this.formgroup.get('SPComapny').clearValidators();
      this.formgroup.get('SPComapny').updateValueAndValidity();
    }
  }
    else
    {
    this._UserRecord.CheckCaseIsAssigned(this.UserID, this.UserTypeId)
    .subscribe((response: number) => {
      debugger;
      var Result =response['result'][0].totalCases;
      if(Result>0)
      {
      
        swal.fire("User Type cannot be changed as this User is associated with some Case Records");
        this.formgroup.patchValue({ UserTypeId: this.UserTypeId });
      }else
      {
        if(UserType==5)
        {
    this.ShowSPDropDown=true;
    this.formgroup.get('SPComapny').updateValueAndValidity();
    this.formgroup.get('SPComapny').setValidators(Validators.required);
        }
        else{
          this.ShowSPDropDown=false;
          this.formgroup.get('SPComapny').reset();
          this.formgroup.get('SPComapny').clearValidators();
          this.formgroup.get('SPComapny').updateValueAndValidity();
        }
      }
      //alert("succcess");
      // this.ConfirmDialog();

    }, (error: any) => {
      this.errorText = error;
      // this.errorMessage(this.errorText);
      this.working = false;
    });
  }
  }


  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }
  SetFormat(event): boolean {
    return this._clientRecord.ValidatePhoneFormat(event);
  }

}
