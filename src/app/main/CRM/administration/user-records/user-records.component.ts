import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, PageEvent, Sort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { UserRecordsService } from './user-records.service';
import { CRMUserRecordGrid, GetCRMUserGrid} from './user-records.model';


@Component({
  selector: 'app-user-records',
  templateUrl: './user-records.component.html',
  styleUrls: ['./user-records.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})

export class UserRecordsComponent implements OnInit {
  errorText: string;
  crmUserRecord: any;
  pageIndex: number;
  pageSize: number;
  sortColumn: string;
  UserType: any;
  TotalRecords: number;
  sortDirection: 'asc' | 'desc' | '';
  FilterUserRecord: FormGroup;
  getCrmUserRecordGrid: GetCRMUserGrid;
  displayedColumns: string[] = ['FullName', 'EmailAddress', 'UserType', 'AccountActive', 'Id'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


  constructor(
    private _router: Router,
    private _formbuilder: FormBuilder,
    private dialog: MatDialog,
    private _UserRecordservice: UserRecordsService) { }

  ngOnInit() {
    this.FilterUserRecord = this._formbuilder.group({
      FullName: [''],
      UserTypeId: [''],

    });

    this.pageIndex = 0;
    this.pageSize = 15;
    this.sortColumn = 'UserID';
    this.sortDirection = 'asc';

    this.getCrmUserRecordGrid = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      sort: this.sortColumn,
      direction: this.sortDirection,
      FullName: null,
      UserTypeId:null,

    }
    this.GetCRMUserType();
    this.GetCRMUserGrid(this.getCrmUserRecordGrid);
  }


  GetCRMUserGrid(model: GetCRMUserGrid) {
    this.crmUserRecord=[];
    this.paginator=null;
    this._UserRecordservice.GetCRMUserRecordList(model)
      .subscribe((response: any) => {
        this.crmUserRecord = new MatTableDataSource<CRMUserRecordGrid>(response['result'].crmUserRecordGrid); //
        this.TotalRecords = response['result'].hcbUsersLength[0].totalLength; 
        response['result'].crmUserRecordGrid.forEach(childObj => {
          if (childObj.accountActive == 1) {
            childObj.accountActive = 'Active'
          }
          else {
            childObj.accountActive = 'InActive'
          }
        });  //
        this.crmUserRecord.sort = this.sort;
        this.crmUserRecord.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  GetCRMUserType(): void {
    this._UserRecordservice.GetCRMUserType()
      .subscribe(responsdata => {
        this.UserType = responsdata['result'];
      //  console.log(this.UserType);

      })
  }


  AddRecord() {
    this._router.navigate(['administration/user-records/addedit-user-record/add']);
  }

  EditUserRecord(userID: number): void {
    //this.open();
    this._router.navigate(["administration/user-records/addedit-user-record/edit", userID]);

  }


  pageChanged(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getCrmUserRecordGrid.pageIndex = event.pageIndex;
    this.getCrmUserRecordGrid.pageSize = event.pageSize;
    this.GetCRMUserGrid(this.getCrmUserRecordGrid);
  }

  sortChange(event: Sort): void {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getCrmUserRecordGrid.sort = event.active;
    this.getCrmUserRecordGrid.direction = event.direction;
    this.GetCRMUserGrid(this.getCrmUserRecordGrid);
  }

  Filter(): void {
    // this.open();
    this.pageIndex=0;
    this.getCrmUserRecordGrid.pageIndex=0;
    this.getCrmUserRecordGrid.FullName = this.FilterUserRecord.value.FullName;
    this.getCrmUserRecordGrid.UserTypeId = this.FilterUserRecord.value.UserTypeId;
    this.GetCRMUserGrid(this.getCrmUserRecordGrid);
  }

  Clear(): void {
    this.FilterUserRecord.reset();
    this.pageIndex=0;
    this.getCrmUserRecordGrid.pageIndex=0;
    this.getCrmUserRecordGrid.FullName = this.FilterUserRecord.value.FullName;
    this.getCrmUserRecordGrid.UserTypeId = this.FilterUserRecord.value.UserTypeId;
    this.GetCRMUserGrid(this.getCrmUserRecordGrid);

  }

}
