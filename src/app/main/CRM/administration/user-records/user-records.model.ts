export class CRMUserRecordGrid {
    FullName: string;
    EmailAddress:string;
    userType: string;
    accountActive: number;
    UserID: number;
}

export class GetCRMUserGrid {
    pageIndex: number;
    pageSize: number;
     sort: string;
     direction:  string;  
     UserTypeId:number;
     FullName:string;
}

 
 export class HCBUsersLength {
     Length: number;
 }

 export class HCBClient {
     crmUserRecordGrid: CRMUserRecordGrid[];
     hcbUsersLength: HCBUsersLength[];
 }
