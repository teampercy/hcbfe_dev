import { NgModule } from '@angular/core';
import { UserRecordsComponent } from './user-records.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { AuthGuard } from 'app/custom/auth.guard';
import { AddeditUserRecordComponent } from './addedit-user-record/addedit-user-record.component';
//import { AdminComponent } from './admin/admin.component';


const routes = [
  {
    path: 'administration/user-records',
    component: UserRecordsComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'addedit-user-record/add',
    component: AddeditUserRecordComponent,
    canActivate: [AuthGuard]
  },
   {
     path: 'addedit-user-record/edit/:id',
     component: AddeditUserRecordComponent,
     canActivate: [AuthGuard]
   },
];

@NgModule({
    declarations: [
        UserRecordsComponent,
        AddeditUserRecordComponent
    ],
    imports: [
      RouterModule.forChild(routes),
  
      TranslateModule,
  
      FuseSharedModule,
      MaterialModule
    ],
    entryComponents: [
    
    ]
  })
export class UserRecordsModule{}