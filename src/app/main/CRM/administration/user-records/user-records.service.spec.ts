import { TestBed } from '@angular/core/testing';

import { UserRecordsService } from './user-records.service';

describe('UserRecordsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserRecordsService = TestBed.get(UserRecordsService);
    expect(service).toBeTruthy();
  });
});
