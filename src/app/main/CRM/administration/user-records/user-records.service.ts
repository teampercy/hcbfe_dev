import { Injectable } from '@angular/core';
import { HttpClientsService } from 'app/service/http-client.service';
import { Observable } from 'rxjs';
import { CRMUserRecordGrid,GetCRMUserGrid } from './user-records.model';
import { GetCRMUserModel, AddUpdateUser } from './addedit-user-record/addedit-user-record.model'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from 'app/service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserRecordsService {

  constructor(
    //private _exceptionHandler: ExceptionService,
    private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { }

    GetCRMUserRecordList(model:GetCRMUserGrid): Observable<CRMUserRecordGrid[]>  {  
    return this._http.postAPI("UserRecord/GetCRMUserList", model);
 
  }
  GetCRMUserRecord(UserID: number):Observable<GetCRMUserModel | {}> {  
    return this._http.getAPI("UserRecord/GetUserRecordById?UserID=" + UserID);
  }

  GetCRMUserType(){
    return this._http.getAPI("UserRecord/GetCRMUserType");
  }

  AddCRMUser(model: AddUpdateUser): Observable<number |{}>{
    return this._http.postAPI("UserRecord/AddcrmUser", model);
  }

  UpdateCRMUser(model: AddUpdateUser): Observable<number | {}>
  {
    return this._http.postAPI("UserRecord/UpdateCRMUserRecord", model);
  }
  CheckCRMUser(Email: string, UserId:number) {
    return this._http.getAPI("UserRecord/CheckCRMUseremail?emailAddress=" + Email +"&UserId="+UserId);
  }
  CheckCRMUserFullName(FullName: string, UserId:number){
    return this._http.getAPI("UserRecord/CheckCRMUserFullName?emailAddress=" + FullName+"&UserId="+UserId);
  }
  CheckCaseIsAssigned(UserId:number, UserTypeId:number)
  {
    return this._http.getAPI("UserRecord/GetCaseRecordsForCM?UserID=" + UserId+ "&UserTypeId=" + UserTypeId);
  }

}
