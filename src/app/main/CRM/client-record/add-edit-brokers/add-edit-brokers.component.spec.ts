import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditBrokersComponent } from './add-edit-brokers.component';

describe('AddEditBrokersComponent', () => {
  let component: AddEditBrokersComponent;
  let fixture: ComponentFixture<AddEditBrokersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditBrokersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditBrokersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
