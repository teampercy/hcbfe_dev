import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { ClientRecordService } from '../client-record.service';
import { ActivatedRoute, Params } from '@angular/router';
import { IfStmt } from '@angular/compiler';
import { SaveUpadteBroker } from './SaveUpdateBroker.model';
import { debug } from 'util';


@Component({
  selector: 'app-add-edit-brokers',
  templateUrl: './add-edit-brokers.component.html',
  styleUrls: ['./add-edit-brokers.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddEditBrokersComponent implements OnInit {

  errorText: string;
  working: boolean;
  BrokerDetails: FormGroup;
  BrokerId: number;

  constructor(
    private _formBuilder: FormBuilder,
    private _clientRecord: ClientRecordService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditBrokersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {


    if (this.data.BrokerId>0) {
      this.BrokerDetails = this._formBuilder.group({
        BrokerId: [this.data.BrokerId],
        BrokerName: ['',Validators.required],
        EmailAddress:['',Validators.email]  //,[Validators.required, Validators.email]
      })
      this.SetValues();
    } else {
      this.BrokerDetails = this._formBuilder.group({
        BrokerName: ['',Validators.required],
        EmailAddress:['',Validators.email] //,[Validators.required, Validators.email]
      })
    }

  }

  SetValues(): void {
   
    this._clientRecord.GetBrokerById(this.data.BrokerId)
      .subscribe((response: any) => {
        this.BrokerDetails.patchValue({ BrokerName: response['result'][0].brokerName });
        this.BrokerDetails.patchValue({ EmailAddress: response['result'][0].emailAddress });
      }, (error: any) => {
        this.data.BrokerId = undefined;
        this.errorText = error;
      });
  }

  SaveOwner(): void {
    if (this.BrokerDetails.invalid)
      return;

    this.working = true;
    if (this.data.BrokerId) {
   
      const updateOwner: SaveUpadteBroker = Object.assign({}, this.BrokerDetails.value);
     // updateOwner.OwnerId=this.data.OwnerId;
      this._clientRecord.UpdateBroker(updateOwner)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
       this.working=true;
      this._clientRecord.SaveBroker(this.BrokerDetails.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}
