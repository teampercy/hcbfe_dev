import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { ClientRecordService } from '../client-record.service';
import { ActivatedRoute, Params } from '@angular/router';
import { IfStmt } from '@angular/compiler';
import { SaveRelationshipOwner } from './reference-owner.model';
import { debug } from 'util';

@Component({
  selector: 'app-add-reference-owner',
  templateUrl: './add-reference-owner.component.html',
  styleUrls: ['./add-reference-owner.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddReferenceOwnerComponent implements OnInit {

  errorText: string;
  working: boolean;
  RelationshipOwner: FormGroup;
  OwnerId: number;

  constructor(
    private _formBuilder: FormBuilder,
    private _clientRecord: ClientRecordService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddReferenceOwnerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

    // this._route.params.forEach(
    //   (params: Params) => {
    //     this.OwnerId = params["id"];
    //   }
    // );

    if (this.data.OwnerId>0) {
      this.RelationshipOwner = this._formBuilder.group({
        OwnerId: [this.data.OwnerId],
        OwnerName: ['',Validators.required],
        EmailAddress:['',[Validators.required, Validators.email]]
      })
      this.SetValues();
    } else {
      this.RelationshipOwner = this._formBuilder.group({
        OwnerName: ['',Validators.required],
        EmailAddress:['',[Validators.required, Validators.email]]
      })
    }

  }

  SetValues(): void {
    this._clientRecord.GetOwnerById(this.data.OwnerId)
      .subscribe((response: any) => {
        this.RelationshipOwner.patchValue({ OwnerName: response['result'][0].ownerName });
        this.RelationshipOwner.patchValue({ EmailAddress: response['result'][0].emailAddress });
      }, (error: any) => {
        this.data.OwnerId = undefined;
        this.errorText = error;
      });
  }

  SaveOwner(): void {
    if (this.RelationshipOwner.invalid)
      return;

    this.working = true;
    if (this.data.OwnerId) {
      const updateOwner: SaveRelationshipOwner = Object.assign({}, this.RelationshipOwner.value);
     // updateOwner.OwnerId=this.data.OwnerId;
      this._clientRecord.UpdateOwner(updateOwner)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
       this.working=true;
      this._clientRecord.SaveOwner(this.RelationshipOwner.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}
