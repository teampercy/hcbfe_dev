import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditClientRecordComponent } from './addedit-client-record.component';

describe('AddeditClientRecordComponent', () => {
  let component: AddeditClientRecordComponent;
  let fixture: ComponentFixture<AddeditClientRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditClientRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditClientRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
