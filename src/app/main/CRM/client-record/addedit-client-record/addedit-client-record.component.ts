import { Component, OnInit, ViewEncapsulation, ViewChild, } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource, MatDialog, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { ClientRecordService } from '../client-record.service';
import { GetCRMClientModel, UpdateCRMClientModel, SaveCRMClientModel, DocumentGrid, DocumentUpload, ReportEmailList, Note, SaveUpdateClientInfo } from './addedit-client-record.model';
import { ReferralAuthorityContactComponent } from '../addedit-contacts/referral-authority-contact/referral-authority-contact.component';
import { AccountContactComponent } from '../addedit-contacts/account-contact/account-contact.component';
import { GetReferralContact } from '../addedit-contacts/referral-authority-contact/referral-authority-contact.model';
import { GetAccountContact, SaveAccountContact } from '../addedit-contacts/account-contact/account-contact.model';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { SelectionModel } from '@angular/cdk/collections';
import { CommonService } from 'app/service/common.service';
import { MasterService } from 'app/main/CRM/administration/master/master.service';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { AddReferenceOwnerComponent } from '../add-reference-owner/add-reference-owner.component';
import { AppCustomDirective } from 'app/main/Common/appValidation';
import { IfStmt } from '@angular/compiler';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import * as FileSaver from 'file-saver';
import { saveAs } from 'file-saver';
import{GetddlTypelistValues} from 'app/main/HCB/case-managment/addedit-case-managment/addedit-case-management.model';
import{AddEditRegionComponent} from 'app/main/CRM/administration/master/region/add-edit-region/add-edit-region.component';
import{AddEditClientReferralSourceComponent} from 'app/main/CRM/administration/master/client-referral-source/add-edit-client-referral-source/add-edit-client-referral-source.component';
import{AddEditBrokerComponent} from 'app/main/CRM/administration/master/broker/add-edit-broker/add-edit-broker.component';
//import {AddEditBrokersComponent} from '../add-edit-brokers/add-edit-brokers.component';
import { Location } from '@angular/common';
import * as _moment from 'moment';
const moment = _moment;
import swal from 'sweetalert2';
//import { RequestOptions, ResponseContentType, Http } from '@angular/http';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService } from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  selector: 'app-addedit-client-record',
  templateUrl: './addedit-client-record.component.html',
  styleUrls: ['./addedit-client-record.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddeditClientRecordComponent implements OnInit {

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', '|',
      'FontName', 'FontColor', 'BackgroundColor', '|',
      'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList']
      
};

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  ClientProcessInfo: FormGroup;
  getddlFeesPayable: GetddlTypelistValues[];
  getddlregion: any;
  getddlRefSource: any;
  getddlAgreedSla: any;
  getddlReportingFormat: any;
  getddlFrequencyInvoice: any;
  errorText: string;
  errorTextGrid: string;
  dialogRef: any;
  refferalContactGrid: MatTableDataSource<any>;
  refferalContactList: any[] = [];
  accountcontactList: any[] = [];
  NotesList: any[] = [];
  accountContactGrid: MatTableDataSource<any>;//any[] = [];  //any
  documentDataSource: any;
  noteDataSource: any[] = [];
  EmailAddressList: any[] = [];
  ClientServiceSource: any;
  working: boolean;
  selectedIndex: number = 0;
  ClientID: number;
  ClienttypeId: number;
  dialogTitle: string;
  public show: boolean = false;
  public show1: boolean = false;
  public showSalary: boolean = false;
  public Services: Array<any> = [];
  fileToUpload: File = null;
  StatusToggle: boolean = false;
  disableContactButton: boolean = false;
  ShowInsurenceService: boolean;
  ShowRetailService: boolean;
  progress: number;
  taken: string;
  EmailTaken: boolean = false;
  msg: boolean = false;
  ContactMsg: boolean = false;
  ContactMsgString: string;
  Count: number;
  name: string;
  uploading: boolean;
  getddlOwner: any[];
  getddlbroker: any[];
  FullName: string;
  Region: string;
  SearchNoteValue: string;
  SendEmail: boolean;
  message: string;
  NoteId: number;
  clienttypeId: number;
  changeClientId: number;
  SelectedSLA: string;
  EncryptedFormat: string;

  ShowDefinedSLA: boolean;
  ShowReportingFormatField: boolean;
  ShowDate: boolean;
  ClientProcessId: number;
  InfoDate: boolean = true;
  Displayheader: boolean = true;
  ShowPOExpiryDate: boolean;
  PONumber: string;


  displayedColumns: string[] = ['Name', 'JobTitle', 'TelephoneNo', 'WhereBased', 'EmailAddress', 'edit'];
  displayedColumns1: string[] = ['ActContactName', 'ActPhoneNo', 'ActContactEmail', 'Instructions', 'PaymentTerms', 'edit'];
  displayedDocument: string[] = ['Name', 'UploadedBy', 'Date', 'edit', 'delete'];

  selectedServices = new SelectionModel<Element>(true, []);

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(FusePerfectScrollbarDirective, { static: true })
  directiveScroll: FusePerfectScrollbarDirective;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    public dialog: MatDialog,
    private _commonService: CommonService,
    private _route: ActivatedRoute,
    private _masterService: MasterService,
    private _clientRecord: ClientRecordService,
    private _matSnackBar: MatSnackBar,
    private _location: Location
    //private http: Http
  ) { }

  ngOnInit() {

    this._route.params.forEach(
      (params: Params) => {
        this.ClientID = params["id"];
      }
    );
    this.FullName=localStorage.getItem('UserName');
    if (this.ClientID == null || this.ClientID == undefined) {
      this.ClientID = 0;
    }

    this._masterService.GetddlValues(31)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlAgreedSla = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlValues(32)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlReportingFormat = response['result'],
        (error: any) => {
          this.errorText = error;
        });


    this._masterService.GetddlValues(33)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlFrequencyInvoice = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this.GetRelationshipOwner()
    this.GetBroker();
    this.GetddlRefSource();

    if (this.ClientID) {
      this.dialogTitle = "Edit";
      this.firstFormGroup = this._formbuilder.group({
        ClientID: [this.ClientID],
        ClientAddedDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        RefSourceId:['',[Validators.required]],
        ClientTypeId: [1],
        Status: [false],
        RegionId:[],
        HeadOfficeAddress: ['', [Validators.required]],
        Town:['',Validators.required],
        Postcode:['', Validators.required],
        LandlineNo:[''],
        MobileNo:[''],
        PrincipalContactName: ['', [Validators.required]],
        PrincipalContactNo: ['', [Validators.required, Validators.maxLength]],
        PrincipalContactEmail: ['', [Validators.email, Validators.required]],
      CompanyName: ['', [Validators.required], Validators.composeAsync([this.createvalidators(this._clientRecord)])]
    //  CompanyName:['',[Validators.required]]
      });
      this.secondFormGroup = this._formbuilder.group({
        BrokerId: [''],
        PensionScheme: [''],
        FinalSalary: [false],
        IllHealthEarlyRetirement: [false],
        IPInsurerName: [''],
        DeferredPeriod: [''],
        Duration: [''],
        RenewalDue: [''],
        DefinitionOfOccupation: [''],
        MIInsurerAdminName: [''],
        CPInsurerName: [''],
        EAPProviderName: [''],
        DaysPaidSickLeave: [''],
        OHPprovider: [''],
        Recording: ['']
      })

      this.thirdFormGroup = this._formbuilder.group({
        RelationshipOwner: ['', [Validators.required]],
        MeetingIntervals: [''],
        FormalContractReqd: [false],
        ContractSignedDate: ['', AppCustomDirective.fromDateValidator],
        ContractExpiryDate: ['', AppCustomDirective.fromDateValidator],
        ConsentToUseLogo: [0],

      });
      this.fourthFormGroup = this._formbuilder.group({
        NoteDescription: [],
        SearchNote:[],
        NextActionDate:['', AppCustomDirective.fromDateValidator]
      });

      this.progress = 0;
      this.fifthFormGroup = this._formbuilder.group({
        ClientID: [this.ClientID],
        // DocumentName: ['']
      });
      this.ClientProcessInfo = this._formbuilder.group({
        DateOfInformation: ['', AppCustomDirective.fromDateValidator],
        AgreedSLAId: [''],
        DefinedSLA: [''],
        ReportingFormat: [''],
        EncryptedEmailAddress: [''],
        IsTLSEnabled: [],
        TLSDate: ['', AppCustomDirective.fromDateValidator],
        EncryptedPassword: [''],
        InvoiceFrequency: [''],
        IsPurchesOrderRequired: [''],
        PONumber: [''],
        DatePOExpiry: [''],
        SpecificInstruction: ['']

      });
      this.disableContactButton = false;
      this.SetValues();
      this.SetClientProcessValues();
      this.GetReferralContactGrid();
      this.GetAccountContactGrid();
      this.GetDocumentGrid(this.ClientID);
      this.loadNotes();
      this.loadEmailList();

    } else {
      this.dialogTitle = "Add";
      this.firstFormGroup = this._formbuilder.group({
        ClientAddedDate: ['', Validators.required],
        RefSourceId:['',Validators.required],
        ClientTypeId: [1],
        Status: [false],
        RegionId:[13951],
        HeadOfficeAddress: ['', [Validators.required]],
        Town:['',Validators.required],
        Postcode:['', Validators.required],
        LandlineNo:[''],
        MobileNo:[''],
        PrincipalContactName: ['', [Validators.required]],
        PrincipalContactNo: ['', [Validators.required, Validators.maxLength]],
        PrincipalContactEmail: ['', [Validators.email, Validators.required]],
        CompanyName: ['', [Validators.required], Validators.composeAsync([this.createvalidators(this._clientRecord)])]
      // CompanyName:['',[Validators.required]]
      });

      this.secondFormGroup = this._formbuilder.group({
        BrokerId: [''],
        PensionScheme: [''],
        FinalSalary: [false],
        IllHealthEarlyRetirement: [false],
        IPInsurerName: [''],
        DeferredPeriod: [''],
        Duration: [''],
        RenewalDue: [''],
        DefinitionOfOccupation: [''],
        MIInsurerAdminName: [''],
        CPInsurerName: [''],
        EAPProviderName: [''],
        DaysPaidSickLeave: [''],
        OHPprovider: [''],
        Recording: ['']
      });

      this.thirdFormGroup = this._formbuilder.group({
        RelationshipOwner: ['', [Validators.required]],
        MeetingIntervals: [''],
        FormalContractReqd: [false],
        ContractSignedDate: ['', AppCustomDirective.fromDateValidator],
        ContractExpiryDate: ['', AppCustomDirective.fromDateValidator],
        ConsentToUseLogo: [0],


      });

      this.fourthFormGroup = this._formbuilder.group({
        NoteDescription: [],
        SearchNote:[],
        NextActionDate:[]

      });
      this.disableContactButton = false;
      this.progress = 0;
      this.fifthFormGroup = this._formbuilder.group({
        // DocumentName: ['']
      });

      this.ClientProcessInfo = this._formbuilder.group({
        DateOfInformation: ['', AppCustomDirective.fromDateValidator],
        AgreedSLAId: [''],
        DefinedSLA: [''],
        ReportingFormat: [''],
        EncryptedEmailAddress: [''],
        IsTLSEnabled: [],
        TLSDate: ['', AppCustomDirective.fromDateValidator],
        EncryptedPassword: [''],
        InvoiceFrequency: [''],
        IsPurchesOrderRequired: [''],
        PONumber: [''],
        DatePOExpiry: [''],
        SpecificInstruction: ['']

      });

      if (this.firstFormGroup.get('ClientTypeId').value == 1) {
        this.ShowInsurenceService = true;
        //   this.ShowRetailService = false;
      }
      else {
        this.ShowInsurenceService = false;
        //  this.ShowRetailService = true;
      }
      this.Displayheader = false;
    }
    this.ClienttypeId = this.firstFormGroup.get('ClientTypeId').value;
    this.GetClientService(this.ClienttypeId);
    this.GetddlRegion();
    this.GetddlRefSource();
  }

  ngAfterViewInit(): void {
    this.scrollToTop();
  }

  scrollToTop(speed?: number): void {
    speed = speed || 10;
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToTop(0, speed);
      });
    }
  }

  onChangeOrderRequired() {
    if (this.ClientProcessInfo.get('IsPurchesOrderRequired').value == 1) {
      this.ShowPOExpiryDate = true;
    } else { this.ShowPOExpiryDate = false; }
  }

  onChangeTLSEnabled() {

    if (this.ClientProcessInfo.get('IsTLSEnabled').value == 1) {
      this.ShowDate = true;
    } else { this.ShowDate = false; }
  }

  OnChangeReportingFormat() {
    //this.EncryptedFormat = name;
    if (this.ClientProcessInfo.get('ReportingFormat').value == 16030)    //16036 16046
    {
      this.ShowReportingFormatField = true;
      if (this.ClientProcessInfo.value.IsTLSEnabled == 1) {
        this.ShowDate = true;
      }
      else {
        this.ShowDate = false;
      }
    } else { this.ShowReportingFormatField = false; this.ShowDate = false; }


  }
  onChangeSLA() {
    if (this.ClientProcessInfo.get('AgreedSLAId').value == 16029)  //Prod-copy  //chnage 16031 for Local machine 16045
    {
      this.ShowDefinedSLA = true;
    } else { this.ShowDefinedSLA = false }
    // this.SelectedSLA=name;
    // if(name=="Personalised SLA's")  //chnage 16031 for Local machine 16045
    // {
    //   this.ShowDefinedSLA=true;
    // }else{this.ShowDefinedSLA=false}
  }

  radioChange(event: any) {
    if (this.clienttypeId != event.value) {

    if (event.value == 1) {
    this._clientRecord.GetClientServices(event.value, 0)
    .subscribe((response: any) => {    
        this.ClientServiceSource = response['result'];
        this.ClientServiceSource.forEach(element => {   
          if (this.Services.indexOf(element.serviceTypeId) == -1 && element.checkedService == '1')
            this.Services.push(element.serviceTypeId);
            this.Services=[];        
        });
      }, (error: any) => {
        alert(error);
      });
      this.ShowInsurenceService = true;
    }
    else{
      this._clientRecord.GetClientServices(event.value, 0)
      .subscribe((response: any) => {
       
          this.ClientServiceSource = response['result'];
      
          this.ClientServiceSource.forEach(element => {
         
            if (this.Services.indexOf(element.serviceTypeId) == -1 && element.checkedService == '1')
              this.Services.push(element.serviceTypeId);
              this.Services=[];
           
          });

        }, (error: any) => {
          alert(error);
        });
        this.ShowInsurenceService = true;
      }
    }
    else {
      if (event.value == 1) {
      this.GetClientService(event.value)
      this.ShowInsurenceService = true;
    }
    else {
      this.GetClientService(event.value)
      this.ShowInsurenceService = false;
    }

  }

  }

  createvalidators(_clientRecord: ClientRecordService) {
    return control => new Promise((resolve: any) => {

var name;
var changename;
name=control.value;

let newVal = name.replace(/[^\w\s]/gi, '%26').toLocaleLowerCase();
newVal=newVal.trim();

      if (this.name == control.value || this.name == "") {
        resolve(null);
      }
      else {
        this._clientRecord.CheckCRMUser(newVal,this.ClientID)
          .subscribe((response) => {
            
            this.Count = response['result'].companyName[0].resultCount;
            if (this.Count != 0) {
              resolve({ EmailTaken: true });

            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ EmailTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  toggle() {
    this.show = !this.show;
  }

  ToggleFinalSalary() {
    this.showSalary = !this.showSalary;
  }

  FormalContractToggle() {
    this.show1 = !this.show1;
  }

  ChangeStatusToggle(): void {
    this.StatusToggle = !this.StatusToggle;
  }

  SetClientProcessValues(): void {

    this._clientRecord.GetCRMClientProcessInfo(this.ClientID)
      .subscribe((response: GetCRMClientModel[]) => {

        var today = new Date();
        this.ClientProcessInfo.patchValue({ DateOfInformation: moment({ year: this._commonService.GetYearFromDateString(today.toString()), month: this._commonService.GetMonthFromDateString(today.toString()), date: this._commonService.GetDateFromDateString(today.toString()) }) });
        if (response['result'].length > 0) {
        this.ClientProcessId = response['result'][0].id;
        //this.ClientProcessInfo.patchValue({ DateOfInformation: response['result'][0].dateOfInformation});
        this.ClientProcessInfo.patchValue({ AgreedSLAId: response['result'][0].agreedSLAId });
        this.onChangeSLA();
        this.ClientProcessInfo.patchValue({ DefinedSLA: response['result'][0].definedPersonalisedSLA });
        this.ClientProcessInfo.patchValue({ ReportingFormat: response['result'][0].reportingFormatId });
        this.OnChangeReportingFormat();
        this.ClientProcessInfo.patchValue({ EncryptedEmailAddress: response['result'][0].emailAddress });
        if (response['result'][0].reportingFormatId == 16030)  //16036   //16046
        {
          if (response['result'][0].isTLSEnabled == true) {
            this.ClientProcessInfo.patchValue({ IsTLSEnabled: 1 });
            this.ShowDate = true;
          } else { this.ClientProcessInfo.patchValue({ IsTLSEnabled: 0 }); this.ShowDate = false; }
        }
        this.ClientProcessInfo.patchValue({ TLSDate: response['result'][0].tlsDate });
        if (response['result'][0].tlsDate != "" && response['result'][0].tlsDate != null) {
          this.firstFormGroup.patchValue({ TLSDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].tlsDate), month: this._commonService.GetMonthFromDateString(response['result'][0].tlsDate), date: this._commonService.GetDateFromDateString(response['result'][0].tlsDate) }) });
        }

        this.ClientProcessInfo.patchValue({ EncryptedPassword: response['result'][0].encryptedPassword });
        this.ClientProcessInfo.patchValue({ InvoiceFrequency: response['result'][0].invoiceFrequencyId });
        this.ClientProcessInfo.patchValue({ SpecificInstruction: response['result'][0].specificInstruction });
          this.ClientProcessInfo.patchValue({ PONumber: response['result'][0].poNumber });  //check
          this.PONumber = response['result'][0].poNumber;

          if (response['result'][0].isPurchesOrderRequired == true) {
            this.ClientProcessInfo.patchValue({ IsPurchesOrderRequired: 1 });

          } else if (response['result'][0].isPurchesOrderRequired == false) {
            this.PONumber = "No PO Required";
            this.ClientProcessInfo.patchValue({ IsPurchesOrderRequired: 0 });
          }
          this.onChangeOrderRequired();
          //         this.ClientProcessInfo.patchValue({ PONumber: response['result'][0].poNumber });  //check
          // this.PONumber=response['result'][0].poNumber;
          if (response['result'][0].datePOExpiry != "" && response['result'][0].datePOExpiry != null) {
            this.ClientProcessInfo.patchValue({ DatePOExpiry: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].datePOExpiry), month: this._commonService.GetMonthFromDateString(response['result'][0].datePOExpiry), date: this._commonService.GetDateFromDateString(response['result'][0].datePOExpiry) }) });
          }
        }
        else {
          this.ClientProcessId = 0;
        }  ////////////////////////////
      })
  }

  SetValues(): void {

    this._clientRecord.GetCRMClientRecord(this.ClientID)
      .subscribe((response: GetCRMClientModel[]) => {

        console.log(response['result'][0].contractExpiryDate);
        this.name = response['result'][0].companyName;
        this.firstFormGroup.patchValue({ ClientAddedDate: response['result'][0].createdOn });
        this.firstFormGroup.patchValue({RefSourceId: response['result'][0].refSourceId});

        this.firstFormGroup.patchValue({ ClientTypeId: response['result'][0].clientTypeId });
        this.clienttypeId=response['result'][0].clientTypeId;
        if (response['result'][0].clientTypeId == 1) {
          this.ShowInsurenceService = true;
          // this.ShowRetailService = false;
          this.GetClientService(response['result'][0].clientTypeId);
        }
        else {
          this.ShowInsurenceService = false;
          this.GetClientService(response['result'][0].clientTypeId);
        }
        this.firstFormGroup.patchValue({ Status: response['result'][0].isActive });
        if (response['result'][0].isActive != null && response['result'][0].isActive == true) {
          this.ChangeStatusToggle();
        }
    
        this.firstFormGroup.patchValue({ CompanyName: response['result'][0].companyName });
        this.firstFormGroup.patchValue({ HeadOfficeAddress: response['result'][0].headOfficeAddress });
        this.firstFormGroup.patchValue({Town: response['result'][0].town});
        this.firstFormGroup.patchValue({Postcode: response['result'][0].postcode});
        this.firstFormGroup.patchValue({RegionId: response['result'][0].regionId});
        this.firstFormGroup.patchValue({LandlineNo: response['result'][0].landlineNo});
        this.firstFormGroup.patchValue({MobileNo: response['result'][0].mobileNo});
        this.firstFormGroup.patchValue({ PrincipalContactName: response['result'][0].principalContactName });
        this.firstFormGroup.patchValue({ PrincipalContactNo: response['result'][0].principalContactNo });
        this.firstFormGroup.patchValue({ PrincipalContactEmail: response['result'][0].principalContactEmail });
        this.secondFormGroup.patchValue({ BrokerId: response['result'][0].brokerNames });
        this.secondFormGroup.patchValue({ PensionScheme: response['result'][0].pensionScheme });
        this.secondFormGroup.patchValue({ FinalSalary: response['result'][0].finalSalary });
        if (response['result'][0].finalSalary != null && response['result'][0].finalSalary == true) {
          this.ToggleFinalSalary();
        }
        this.secondFormGroup.patchValue({ IllHealthEarlyRetirement: response['result'][0].illHealthEarlyRetirement });
        if (response['result'][0].illHealthEarlyRetirement != null && response['result'][0].illHealthEarlyRetirement == true) {
          this.toggle();
        }
        this.secondFormGroup.patchValue({ IPInsurerName: response['result'][0].ipInsurerName });
        this.secondFormGroup.patchValue({ DeferredPeriod: response['result'][0].deferredPeriod });
        this.secondFormGroup.patchValue({ Duration: response['result'][0].duration });
        this.secondFormGroup.patchValue({ RenewalDue: response['result'][0].renewalDue });
        this.secondFormGroup.patchValue({ DefinitionOfOccupation: response['result'][0].definitionOfOccupation });
        this.secondFormGroup.patchValue({ MIInsurerAdminName: response['result'][0].miInsurerAdminName });
        this.secondFormGroup.patchValue({ CPInsurerName: response['result'][0].cpInsurerName });
        this.secondFormGroup.patchValue({ EAPProviderName: response['result'][0].eapProviderName });
        this.secondFormGroup.patchValue({ DaysPaidSickLeave: response['result'][0].daysPaidSickLeave });
        this.secondFormGroup.patchValue({ OHPprovider: response['result'][0].ohPprovider });
        this.secondFormGroup.patchValue({ Recording: response['result'][0].recording });
        this.thirdFormGroup.patchValue({ RelationshipOwner: response['result'][0].relationshipOwner });
        this.thirdFormGroup.patchValue({ MeetingIntervals: response['result'][0].meetingIntervals });
        this.thirdFormGroup.patchValue({ FormalContractReqd: response['result'][0].formalContractReqd });
        if (response['result'][0].formalContractReqd != null && response['result'][0].formalContractReqd == true) {
          this.FormalContractToggle()
        }
        this.thirdFormGroup.patchValue({ ContractSignedDate: response['result'][0].contractSignedDate });
        if (response['result'][0].contractExpiryDate != "" && response['result'][0].contractExpiryDate != null) {
          this.thirdFormGroup.patchValue({ ContractExpiryDate: response['result'][0].contractExpiryDate });
        }
        if (response['result'][0].consentToUseLogo == true) {
          this.thirdFormGroup.patchValue({ ConsentToUseLogo: 1 });
        }
        else {
          this.thirdFormGroup.patchValue({ ConsentToUseLogo: 0 });
        }
      }, (error: any) => {
        this.ClientID = undefined;
        this.errorText = error;
      });
  }

  Save(): void {
    
    if (!this.firstFormGroup.valid)
      return;

    this.msg = false;
    this.ContactMsg = false;
    const updateCRMClientModel: UpdateCRMClientModel = Object.assign({}, this.firstFormGroup.value, this.secondFormGroup.value, this.thirdFormGroup.value);
   this.name=updateCRMClientModel.CompanyName;
    updateCRMClientModel.ContractSignedDate = (this.thirdFormGroup.value.ContractSignedDate == "" || this.thirdFormGroup.value.ContractSignedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ContractSignedDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ContractSignedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ContractSignedDate) + " 00:00:00:000";
    updateCRMClientModel.ContractExpiryDate = (this.thirdFormGroup.value.ContractExpiryDate == "" || this.thirdFormGroup.value.ContractExpiryDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ContractExpiryDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ContractExpiryDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ContractExpiryDate) + " 00:00:00:000";
    updateCRMClientModel.NextActionDate    =(this.fourthFormGroup.value.NextActionDate == "" || this.fourthFormGroup.value.ActionDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.NextActionDate) + "-" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.NextActionDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.fourthFormGroup.value.NextActionDate) + " 00:00:00:000";
    if (this.ClientID) {
      this.working = true;

      updateCRMClientModel.ClientServices = this.Services.toString();
      if (updateCRMClientModel.ClientServices == "") {
        this.msg = true;
        this.taken = "please select atleast one service";
      }
      else if (this.refferalContactList.length < 1) {
        this.ContactMsg = true;
        this.ContactMsgString = "Please add atleast one contact";
      }
      else {
      
        this.msg = false;
        this._clientRecord.UpdateCRMClient(updateCRMClientModel)
          .subscribe((response: number) => {
            var Result = response['result'];
            this._router.navigateByUrl('/client-record', { skipLocationChange: true }).then(() => {
              this._router.navigate(['/client-record/addedit-client-record/edit/', this.ClientID]);
            });

          }, (error: any) => {
            this.errorText = error;
            // this.errorMessage(this.errorText);
            this.working = false;
          });
        
      
        this.ConfirmDialog();
      }
    } else {

      this.working = true;
      const saveCRMClientModel: SaveCRMClientModel = Object.assign({}, this.firstFormGroup.value, this.secondFormGroup.value, this.thirdFormGroup.value);
      this.name = saveCRMClientModel.CompanyName;
      saveCRMClientModel.SendNotificationEmail=this.SendEmail;
      saveCRMClientModel.UserName=this.FullName;
      saveCRMClientModel.ContractSignedDate = (this.thirdFormGroup.value.ContractSignedDate == "" || this.thirdFormGroup.value.ContractSignedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ContractSignedDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ContractSignedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ContractSignedDate) + " 00:00:00:000";
      saveCRMClientModel.ContractExpiryDate = (this.thirdFormGroup.value.ContractExpiryDate == "" || this.thirdFormGroup.value.ContractExpiryDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ContractExpiryDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ContractExpiryDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ContractExpiryDate) + " 00:00:00:000";
    
      saveCRMClientModel.ClientServices = this.Services.toString();
      this.ContactMsg = false;
      if (saveCRMClientModel.ClientServices == "") {
        this.msg = true;
        this.taken = "please select atleast one service";
        this.selectedIndex=0;
      }
      else if (this.refferalContactList.length < 1) {
      
        this.ContactMsg = true;
        this.ContactMsgString = "Please add atleast one contact";
        this.selectedIndex=0;
      }
      else {
      
        this.msg = false;
        this.ContactMsg = false;
        var obj = {
          saveCRMClientRecord: saveCRMClientModel,
          saveReferralContact: this.refferalContactList,
          saveAccountContact: this.accountcontactList,
          saveNoteModel: this.NotesList
        };
     

        this._clientRecord.SaveCRMClient(obj)
          .subscribe((response: number) => {
            var ID = response['result'];
            this._router.navigateByUrl('/client-record', { skipLocationChange: true }).then(() => {
              this._router.navigate(['/client-record/addedit-client-record/edit', ID]);
            });
            // this._router.navigate(["/client-record/addedit-client-record/edit", ID]);
          }, (error: any) => {
            this.errorText = error;
            //this.errorMessage(this.errorText);
          });
          debugger;
          var Compnayname=this.name.replace(/[^\w\s]/gi, '%26');
        Compnayname=Compnayname.trim();
            this._clientRecord.NewClientAddedMail(this.FullName,Compnayname)
            .subscribe((response:any)=>
            {
            console.log(response);
            }
            )
            this.ConfirmDialog();
      }



    }
  }

  /////////////////////02_09_2020///////////////////////////////
  onCheckEmsilAddress(Mailaddress, event) {
    var i = 0;
   
    // var index = this.Services.indexOf(event.source.value);

    for (i; i < this.EmailAddressList.length; i++) {
      if ((Mailaddress == this.EmailAddressList[i].emailAddress) && (event.checked == true)) {
        this.EmailAddressList[i].checkEmailAddress = true;
      }
      if ((Mailaddress == this.EmailAddressList[i].emailAddress) && (event.checked == false)) {
        this.EmailAddressList[i].checkEmailAddress = false;
      }
    }
  }


  SaveClientInfo() {

    this.ClearClientInfoFields();

    const SaveUpdateClientInfo: SaveUpdateClientInfo = Object.assign({}, this.ClientProcessInfo.value);
    SaveUpdateClientInfo.DateOfInformation = this.ClientProcessInfo.value.DateOfInformation;
    SaveUpdateClientInfo.TLSDate = (this.ClientProcessInfo.value.TLSDate == "" ||
      this.ClientProcessInfo.value.TLSDate == null) ? null : "" +
      this._commonService.GetYearFromDateString(this.ClientProcessInfo.value.TLSDate) +
      "-" + (this._commonService.GetMonthFromDateString(this.ClientProcessInfo.value.TLSDate) + 1)
      + "-" + this._commonService.GetDateFromDateString(this.ClientProcessInfo.value.TLSDate) +
      " 00:00:00:000";

    SaveUpdateClientInfo.DatePOExpiry = (this.ClientProcessInfo.value.DatePOExpiry == "" ||
      this.ClientProcessInfo.value.DatePOExpiry == null) ? null : "" +
      this._commonService.GetYearFromDateString(this.ClientProcessInfo.value.DatePOExpiry) +
      "-" + (this._commonService.GetMonthFromDateString(this.ClientProcessInfo.value.DatePOExpiry) + 1)
      + "-" + this._commonService.GetDateFromDateString(this.ClientProcessInfo.value.DatePOExpiry) +
      " 00:00:00:000";
    SaveUpdateClientInfo.ClientId = this.ClientID;

    console.log(this.EmailAddressList);
    var obj = {
      SaveUpdateClientInfo: SaveUpdateClientInfo,
      ReportEmailList: this.EmailAddressList
    };


    if (this.ClientProcessId > 0) {
      SaveUpdateClientInfo.ID = this.ClientProcessId;
      this._clientRecord.UpdateCRMClientProcessInfo(obj)
        .subscribe((response: number) => {


          this._router.navigateByUrl('/client-record', { skipLocationChange: true }).then(() => {
            this._router.navigate(['/client-record/addedit-client-record/edit', this.ClientID]);
          });

          // this._router.navigate(["/client-record/addedit-client-record/edit", this.ClientID]);
          this.ConfirmDialog();
          //  var ID = response['result'];
        }, (error: any) => {
          this.errorText = error;
          //this.errorMessage(this.errorText);
        });

    } else {
      if (this.ClientID == 0) {
        // if(this.ClientProcessInfo.value.AgreedSLAId==16031 && (this.ClientProcessInfo.value.DefinedSLA =="" ||this.ClientProcessInfo.value.DefinedSLA ==undefined))
        // {
        swal.fire({

          text: 'Please First Save Client Details',

          // showCancelButton: true,
          // confirmButtonText: 'Yes',
          cancelButtonText: 'OK'
        })
        // }
      } else {

        this._clientRecord.SaveCRMClientProcessInfo(obj)
          .subscribe((response: number) => {

            this.ConfirmDialog();
            //  var ID = response['result'];
            this._router.navigateByUrl('/client-record', { skipLocationChange: true }).then(() => {
              this._router.navigate(['/client-record/addedit-client-record/edit', this.ClientID]);
            });
          }, (error: any) => {
            this.errorText = error;
            //this.errorMessage(this.errorText);
          });
      }
    }
    //}
  }

  ClearClientInfoFields() {

    if (this.ClientProcessInfo.value.ReportingFormat != 16030)  //16036  //16046
    {
      this.ClientProcessInfo.value.EncryptedEmailAddress = "";
      this.ClientProcessInfo.value.IsTLSEnabled = 0;
      this.ClientProcessInfo.value.TLSDate = "";
      this.ClientProcessInfo.value.EncryptedPassword = "";
    }
    if (this.ClientProcessInfo.value.AgreedSLAId != 16029)  //16031  //16045
    {
      this.ClientProcessInfo.value.DefinedSLA = "";
    }
    if (this.ClientProcessInfo.value.IsTLSEnabled == 0) {
      this.ClientProcessInfo.value.TLSDate = null;
    }
    if (this.ClientProcessInfo.value.IsPurchesOrderRequired == 0) {
      this.ClientProcessInfo.value.PONumber = "";
      this.ClientProcessInfo.value.DatePOExpiry = "";
    }
  }
  /////////////////////////////////////////

  //   loadEmailList(){
  //     this._clientRecord.GetEmailList(this.ClientID)
  //     .subscribe((response:ReportEmailList) =>
  //     {
  // this.EmailAddressList =response['result'];
  //     }, (error: any) => {
  //         this.errorText = error;
  //       });
  //   }

  GetReferralContactGrid() {
    this._clientRecord.GetReferralContactGrid(this.ClientID)
      .subscribe((response: GetReferralContact) => {
        this.refferalContactList = response['result'];
        this.refferalContactGrid = new MatTableDataSource<GetReferralContact>(response['result']);
        this.refferalContactGrid.sort = this.sort;
        this.refferalContactGrid.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddReferralAuthority(): void {
    this.ContactMsg = false;
    this.dialogRef = this.dialog.open(ReferralAuthorityContactComponent, {
      disableClose: true,
      width: '600px',
      data: {
        ClientId: this.ClientID,
        ClientReferralContactsId: 0,
        refferalContactGrid: this.refferalContactList
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (this.ClientID > 0) {
          this.GetReferralContactGrid();
        }
        else if (this.refferalContactList.length > 0) {
          this.refferalContactGrid = new MatTableDataSource<GetReferralContact>(this.refferalContactList);
          this.refferalContactGrid.sort = this.sort;
          this.refferalContactGrid.paginator = this.paginator;
        }

      }
    });
  }

  EditReferralAuthority(ClientReferralContactsId: number): void {
   
    this.dialogRef = this.dialog.open(ReferralAuthorityContactComponent, {
      disableClose: true,
      width: '600px',
      data: {
        ClientId: this.ClientID,
        ClientReferralContactsId: ClientReferralContactsId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.GetReferralContactGrid();
        this.ConfirmDialog();
        
      }
    });
  }

  DeleteReferralAuthority(ClientReferralContactsId: number): void  {
 
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' , data:{ dispalymessage: 'Are you sure you want to delete this contact?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._clientRecord.DeleteReferralcontact(ClientReferralContactsId)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetReferralContactGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }



  GetAccountContactGrid() {
    this._clientRecord.GetAccountContactGrid(this.ClientID)
      .subscribe((response: GetAccountContact) => {
        this.accountcontactList = (response['result']);
        this.accountContactGrid = new MatTableDataSource<GetAccountContact>(response['result']);
        this.accountContactGrid.sort = this.sort;
        this.accountContactGrid.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddAccountContact(): void {
  
    this.ContactMsg = false;
    this.dialogRef = this.dialog.open(AccountContactComponent, {
      width: '600px',
      data: {
        ClientId: this.ClientID,
        ClientAccountContactsId: 0,
        accountContactGrid: this.accountcontactList, // this.accountcontactList,
        PONumber: this.PONumber
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (this.accountcontactList.length > 0) {
          this.accountContactGrid = new MatTableDataSource<GetAccountContact>(this.accountcontactList);
          this.accountContactGrid.sort = this.sort;
          this.accountContactGrid.paginator = this.paginator;
        }
        if(this.ClientID >0) 
        {
        this.GetAccountContactGrid();
        }
      }
    });
  }

  EditAccountContact(userId: number): void {
    
    this.dialogRef = this.dialog.open(AccountContactComponent, {
      disableClose: true,
      width: '600px',
      data: {
        ClientId: this.ClientID,
         // Id: Data,
          accountContactGrid: this.accountcontactList,
        ClientAccountContactsId: userId,
          PONumber: this.PONumber
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetAccountContactGrid();
      }
    });
  }

  DeleteAccountContact(UserId: number, Data: any): void {

    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Account Contact?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

        if (this.ClientID == 0) {
          this.accountcontactList.splice(Data, 1);
          this.accountContactGrid = new MatTableDataSource<any>(this.accountcontactList);

        }
        else {
          this._clientRecord.DeleteAccountcontact(UserId)
            .subscribe((response: any) => {
              this.ConfirmDialog();
              this.GetAccountContactGrid();
            }, (error: any) => {
              if (error == null) {
                this.errorText = 'Internal Server error';
              }
              else {
                this.errorText = error;
              }
            });
        }
      }
    });
  }

  loadEmailList(): void {
    this.working = true;
    this._clientRecord.GetEmailAddress(this.ClientID)
      .subscribe(
        (response: any) => {
         
          this.EmailAddressList = response['result'];
          // this.EmailAddressList = this.NotesList ;

          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
  }

  AddEmailAddress() {
  

    const EmailList: ReportEmailList = {
      id: 0,
      ClientId: this.ClientID,
      emailAddress: this.ClientProcessInfo.value.EncryptedEmailAddress,
      checkEmailAddress: false
    };
    this.EmailAddressList.push(EmailList);
    this.ClientProcessInfo.get('EncryptedEmailAddress').reset();
  }


  DeleteEmailAddress(ID: number, Mailaddress) {
   
    var count = 0;
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
     
      if (result == 'Confirm') {

        if (ID == 0) {

          for (var i = 0; i < this.EmailAddressList.length; i++) {

            if ((Mailaddress == this.EmailAddressList[i].emailAddress)) {
              if (count < 1) {
                //this.EmailAddressList[i].checkEmailAddress=1;
                this.EmailAddressList.splice(i, 1);
                count = count + 1;
              }
            }

          }
          //  this.FailedCallList = new MatTableDataSource<any>(this.HCBFailedCallList);       
        } else {
          for (var i = 0; i < this.EmailAddressList.length; i++) {

            if ((Mailaddress == this.EmailAddressList[i].emailAddress)) {

              if (count < 1) {
                //this.EmailAddressList[i].checkEmailAddress=1;
                this.EmailAddressList.splice(i, 1);
                count = count + 1;
              }
            }
          }
          this._clientRecord.DeleteEmailList(ID)
            .subscribe((response: any) => {

              // this.ConfirmDialog();
              // this.loadEmailList();
            }, (error: any) => {
              if (error == null) {
                this.errorText = 'Internal Server error';
              }
              else {
                this.errorText = error;
              }
            });
        }
      }
    });

  }


  loadNotes(): void {
    this.working = true;
    this._clientRecord.GetNote(this.ClientID)
      .subscribe(
        (response: any) => {
 
          this.NotesList = response['result'];
          this.noteDataSource = this.NotesList ;

          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
  }

  SendNotification(event)
  {
   
     this.SendEmail=event.checked;
  }
  SearchNote(event:string):void{
    
    this.working=true;
   // const value = event;

    if(event=="" || event==null)
    {
      this.loadNotes();
    }
    else{
    this._clientRecord.SearchNoteFromList(event,this.ClientID)
    .subscribe(
      (response: Note[]) => {
      
          this.NotesList = response['result'];
          this.noteDataSource = response['result'];
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
      }
      }
  
  Clear(){
    this.fourthFormGroup.get('SearchNote').reset();
    this.SearchNote('');
  }

  SaveNote(): void {

var today = new Date();
    const note: Note = {
      ClientId: this.ClientID,
      fullName: this.FullName,
      noteCreatedOn: today,
      NextActionDate: (this.fourthFormGroup.value.NextActionDate == "" || this.fourthFormGroup.value.NextActionDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.NextActionDate) + "-" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.NextActionDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.fourthFormGroup.value.NextActionDate) + " 00:00:00:000",
      description: this.fourthFormGroup.value.NoteDescription,
      displayMessage:null
    };
   
    this._clientRecord.SaveNote(note)
    .subscribe((response: number) => {
      this.loadNotes();
     
      this.NoteId=response['result']
     // 
      this.fourthFormGroup.reset();
    
   
 
    if (this.ClientID > 0 && note.description != null) {
      if(this.SendEmail==true)
      {
        var Compnayname=this.name.replace(/[^\w\s]/gi, '%26');
        Compnayname=Compnayname.trim();
        this._clientRecord.NotifyTeam(Compnayname, this.FullName,this.NoteId)
        .subscribe((response: any) =>
        {  
          this.loadNotes();
        }, (error: any) => {
          this.errorText = error;
        });
        this.loadNotes();
        //this.ConfirmDialog();
        
      }

    } else {
    
      if( note.description != undefined || note.description  != null)
      {
   
         if(note.NextActionDate=null)
          note.displayMessage=="Next Action Date:";;
       
      this.NotesList.push(note);
      this.noteDataSource=this.NotesList;
      }
       this.fourthFormGroup.reset();
    }

  });

  this.loadNotes();
   this.ConfirmDialog();
   this._router.navigate(["/client-record/addedit-client-record/edit", this.ClientID]);
  }


  GetDocumentGrid(ClientId: number): void {
    this.working = true;
    this._clientRecord.GetClientDocuments(ClientId)
      .subscribe((response: DocumentGrid[]) => {
      
        this.documentDataSource = new MatTableDataSource<DocumentGrid>(response['result']);
        this.working = false;
      }, (error: any) => {
        this.errorTextGrid = error;
        this.working = false;
      });
  }

  UploadDocument(): void {
    this.progress = 0;
    this.uploading = true;
    const myData: DocumentUpload = {
      ClientId: this.ClientID,
    };
    this._clientRecord.UploadFile(this.fileToUpload, myData)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          // this.showAddDocument = false;
          this.GetDocumentGrid(this.ClientID);
        }
      }, (error: any) => {
        this.errorText = error;
      });
  }

  onCheckChange(event) {
    var index = this.Services.indexOf(event.source.value);
    if (event.checked && index === -1) {
      this.msg = false;
      this.Services.push(event.source.value);
      this.Services.join(","); 
    }
    else {
      this.Services.splice(index, 1);
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  back() {

   this._location.back();
  }

  public tabChanged(tabChangeEvent): void {
 
    this.selectedIndex = tabChangeEvent.index;
  }

  public nextStep() {
    this.scrollToTop();
    this.selectedIndex += 1;
  }

  public previousStep() {
    this.scrollToTop();
    this.selectedIndex -= 1;
  }

  GetClientService(ClienttypeId: number): void {
    this._clientRecord.GetClientServices(ClienttypeId, this.ClientID)
      .subscribe((response: any) => {
        this.ClientServiceSource = response['result'];
 
        this.ClientServiceSource.forEach(element => {
          if (this.Services.indexOf(element.serviceTypeId) == -1 && element.checkedService == '1')
            this.Services.push(element.serviceTypeId);
         
        });
  
      }, (error: any) => {
        alert(error);
      });
  }



  AddReferenceOwner(): void {
    this.dialogRef = this.dialog.open(AddReferenceOwnerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        OwnerId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetRelationshipOwner();
      }
    });
  }

  AddBroker(): void {
    this.dialogRef = this.dialog.open(AddEditBrokerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        AssesorId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetBroker();
      }
    });
  }

  GetRelationshipOwner(): void {
    this.working = true;
    this._clientRecord.GetRelationshipOwner()
      .subscribe((response: any) => {
        this.getddlOwner = response['result'];
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      });
  }

  GetBroker(): void {

    this.working = true;
    this._masterService.GetAssessorGrid()
      .subscribe((response: any) => {
       
        this.getddlbroker = response['result'];
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      });
  }


  SetFormat(event): boolean {
    return this._clientRecord.ValidatePhoneFormat(event);
  }

  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }



  DownloadDocument(id: number, Ext: any) {
  
    this._clientRecord.DownloadDocument(id)
      .subscribe(fileData => {
        const downloadedFile = new Blob([fileData], { type: fileData.type });
        FileSaver.saveAs(downloadedFile,Ext);
        // window.open(URL.createObjectURL(downloadedFile));
      }
      );
  }

  viewDoc(id: number)
  { 
    this._router.navigate(["/client-record/crm-ngx-document-viewer",id]);

    
  }

  DeleteDocument(id:number)
  {
  
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' , data:{ dispalymessage: 'Are you sure you want to delete this File?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._clientRecord.DeleteDocument(id)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetDocumentGrid(this.ClientID);
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }
  GetddlRegion():void{
  
    this._masterService.GetddlRegion(24)
    .subscribe(
    (response:any) => 
    {this.getddlregion = response['result']
    },
    (error: any) => {
      this.errorText = error;
    });
  
  }

  GetddlRefSource():void{
  
    this._masterService.GetddlRegion(29)
    .subscribe(
    (response:any) => 
    {this.getddlRefSource = response['result']
    },
    (error: any) => {
      this.errorText = error;
    });
  
  }
  
  AddRegion(): void {
    this.dialogRef = this.dialog.open(AddEditRegionComponent, {
      disableClose: true,
      width: '400px',
      data: {
        RegionId: 0
      }
    });
    
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetddlRegion();
      }
    });
  }
  AddRefSource(): void {
    this.dialogRef = this.dialog.open(AddEditClientReferralSourceComponent, {
      disableClose: true,
      width: '400px',
      data: {
        RefSourceId: 0
      }
    });
    
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetddlRefSource();
      }
    });
  }

  
}




