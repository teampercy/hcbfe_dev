export class GetAccountContact {
    ClientAccountContactsId: number;
    ClientId: number;
    fullName: string;
    emailAddress: string;
    telephoneNumber: string;
    NonHOLocation: string;
    ContactEmail: string;
}

export class SaveAccountContact {
    Id:number;
    clientId: number;
    fullName: string;
    emailAddress: string;
    telephoneNumber: string;
    instructions: string;
    PaymentTerms: string;
}

export class UpdateAccountContact {
    ClientAccountContactsId: number;
     Id:number;
    clientId: number;
    fullName: string;
    emailAddress: string;
    telephoneNumber: string;
    instructions: string;
    PaymentTerms: string;
}