import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralAuthorityContactComponent } from './referral-authority-contact.component';

describe('ReferralAuthorityContactComponent', () => {
  let component: ReferralAuthorityContactComponent;
  let fixture: ComponentFixture<ReferralAuthorityContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralAuthorityContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralAuthorityContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
