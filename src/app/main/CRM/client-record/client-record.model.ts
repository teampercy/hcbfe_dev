export class CRMClientRecordGrid {
    ClientID: number;
    ClientTypeId: number;
    ClientType: string;
    CompanyName: string;
    PrincipalContactName: string;
    PrincipalContactNo: string;
    PrincipalContactEmail: string;
    clientAddedDate: Date;
}

export class GetCRMClientGrid {
    pageIndex: number;
    pageSize: number;
    sort: string;
    direction: string;
    CompanyName: string;
    // ClientType:string;
    ClientTypeId: number;
    clientAddedDate: string;
}
export class CRMClientRecordLength {
    Length: number;
}

export class CRMClientRecord {
    crmClientRecordGrid: CRMClientRecordGrid[];
    crmClientRecordLength: CRMClientRecordLength[];
}