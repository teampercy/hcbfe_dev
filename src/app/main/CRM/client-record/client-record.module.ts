import { NgModule } from '@angular/core';
import { ClientRecordComponent } from './client-record.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { AddeditClientRecordComponent } from './addedit-client-record/addedit-client-record.component';
import { AuthGuard } from 'app/custom/auth.guard';
import { ReferralAuthorityContactComponent } from './addedit-contacts/referral-authority-contact/referral-authority-contact.component';
import { AccountContactComponent } from './addedit-contacts/account-contact/account-contact.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { AddReferenceOwnerComponent } from './add-reference-owner/add-reference-owner.component';
import { DeleteDialogComponent} from 'app/main/Common/delete-dialog/delete-dialog.component';
import { AddEditBrokersComponent } from './add-edit-brokers/add-edit-brokers.component';
import { CrmNgxDocumentViewerComponent } from './crm-ngx-document-viewer/crm-ngx-document-viewer.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import {CRMSafeHtmlPipe} from '@fuse/pipes/CRMSafeHtml.pipe';

const moment = _moment;
    
const routes = [
  {
    path: 'client-record',
    component: ClientRecordComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'addedit-client-record/add',
    component: AddeditClientRecordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'addedit-client-record/edit/:id',
    component: AddeditClientRecordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'crm-ngx-document-viewer/:docName',
    component: CrmNgxDocumentViewerComponent,
    canActivate: [AuthGuard]
  },

];
export const MY_FORMATS = {
  parse: {
      dateInput: 'DD/MM/YYYY',
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MM YYYY',
      dateA11yLabel: 'DD/MM/YYYY',
      monthYearA11yLabel: 'MM YYYY',
  },
};
@NgModule({
  declarations: [
    ClientRecordComponent,
    AddeditClientRecordComponent,
    ReferralAuthorityContactComponent,
    AccountContactComponent,
    ConfirmDialogComponent,
    AddReferenceOwnerComponent,
    AddEditBrokersComponent,
    CrmNgxDocumentViewerComponent,
    CRMSafeHtmlPipe
  ],
  imports: [
    RouterModule.forChild(routes),

    TranslateModule,

    FuseSharedModule,
    MaterialModule,
    RichTextEditorAllModule
  ],

  providers: [
  
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
  entryComponents: [
   AccountContactComponent,
   ReferralAuthorityContactComponent,
   ConfirmDialogComponent,
   AddReferenceOwnerComponent,
   AddEditBrokersComponent,
   DeleteDialogComponent
  ]
})
export class ClientRecordModule { }
