import { Injectable } from '@angular/core';
import { HttpClientsService } from 'app/service/http-client.service';
import { Observable } from 'rxjs';
import { GetCRMClientGrid, CRMClientRecord } from './client-record.model';
import { GetCRMClientModel, UpdateCRMClientModel, SaveCRMClientModel, DocumentUpload, Note, DocumentGrid,SaveUpdateClientInfo } from './addedit-client-record/addedit-client-record.model';
import { GetReferralContact, UpdateReferralContact, SaveReferralContact } from './addedit-contacts/referral-authority-contact/referral-authority-contact.model';
import { GetAccountContact, UpdateAccountContact, SaveAccountContact } from './addedit-contacts/account-contact/account-contact.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from 'app/service/authentication.service';
import { SaveRelationshipOwner } from './add-reference-owner/reference-owner.model';
import { SaveUpadteBroker } from './add-edit-brokers/SaveUpdateBroker.model';


@Injectable({
  providedIn: 'root'
})
export class ClientRecordService {

  constructor(
    //private _exceptionHandler: ExceptionService,
    private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { }

  GetCRMClientRecordGrid(model: GetCRMClientGrid): Observable<CRMClientRecord | {}> {
    return this._http.postAPI("CRMClientRecord/GetCRMClientList", model);
  }

  GetCRMClientProcessInfo(ClientID: number): Observable<GetCRMClientModel | {}> {
    return this._http.getAPI("CRMClientRecord/GetClientProcessInfoById?ClientId=" + ClientID);
  }

  GetCRMClientRecord(ClientID: number): Observable<GetCRMClientModel | {}> {
    return this._http.getAPI("CRMClientRecord/GetClientRecordById?ClientId=" + ClientID);
  }

  UpdateCRMClient(model: any): Observable<number | {}> {

    return this._http.postAPI("CRMClientRecord/UpdateCRMClientRecord", model);
  }
  UpdateCRMClientProcessInfo(model: any): Observable<number | {}>  {

    return this._http.postAPI("CRMClientRecord/UpdateCRMClientProcessInfo", model);
  }
  SaveCRMClientProcessInfo(model: any): Observable<number | {}> {
   
    return this._http.postAPI("CRMClientRecord/SaveCRMClientProcessInfo", model);
  }

  SaveCRMClient(model: any): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/SaveCRMClientRecord", model);
  }

  DeleteClientRecord(ClientID: number): Observable<any | {}> {
    return this._http.getAPI("CRMClientRecord/DeleteClientRecord?ClientID=" + ClientID);
  }

  GetReferralContactGrid(ClientID: number): Observable<GetReferralContact | {}> {
    return this._http.getAPI("CRMClientRecord/GetReferralContactGrid?ClientId=" + ClientID);
  }

  GetReferralContact(ClientReferralContactsId: number): Observable<GetReferralContact | {}> {
    return this._http.getAPI("CRMClientRecord/GetReferralContactById?ClientReferralContactsId=" + ClientReferralContactsId);
  }

  DeleteReferralcontact(ClientReferralContactsId: number): Observable<GetReferralContact | {}> {
    return this._http.getAPI("CRMClientRecord/DeleteReferralcontact?ClientReferralContactsId=" + ClientReferralContactsId);
  }

  UpdateReferralContact(model: UpdateReferralContact): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/UpdateReferralContact", model);
  }

  SaveReferralContact(model: SaveReferralContact): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/SaveReferralContact", model);
  }

  GetAccountContactGrid(ClientID: number): Observable<GetAccountContact | {}> {
    return this._http.getAPI("CRMClientRecord/GetAccountContactGrid?ClientId=" + ClientID);
  }

  GetAccountContact(ClientAccountContactsId: number): Observable<GetReferralContact | {}> {
    return this._http.getAPI("CRMClientRecord/GetAccountContactById?ClientAccountContactsId=" + ClientAccountContactsId);
  }

  UpdateAccountContact(model: UpdateAccountContact): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/UpdateAccountContact", model);
  }

  SaveAccountContact(model: SaveAccountContact): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/SaveAccountContact", model);
  }

  DeleteAccountcontact(UserId: number): Observable<GetReferralContact | {}> {
    return this._http.getAPI("CRMClientRecord/DeleteAccountcontact?UserId=" + UserId);
  }

  GetNote(ClientId: number): Observable<Note | {}> {
    return this._http.getAPI("CRMClientRecord/GetNote?ClientId=" + ClientId);
  }

  SaveNote(model: Note): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/SaveNote", model);
  }

  UploadFile(model: File, DocumentModel: DocumentUpload): Observable<any | {}> {
    const formData: FormData = new FormData();
    formData.append('fileKey', model, model.name);
    formData.append('ClientId', DocumentModel.ClientId.toString());
    return this.http.post<any>(environment.baseUrl + "CRMClientRecord/UploadDocument", formData, {
      reportProgress: true,
      observe: 'events',
      headers: this._authentication.GetFileHeader()
    });
  }

  GetClientDocuments(ClientId: number): Observable<DocumentGrid | {}> {
    return this._http.getAPI("CRMClientRecord/GetClientDocuments?ClientId=" + ClientId);
  }

  GetClientServices(ClienttypeId: number, ClientId: number): Observable<GetReferralContact | {}> {
    return this._http.getAPI("CRMClientRecord/GetClientServices?ClientTypeId=" + ClienttypeId + "&ClientId=" + ClientId);
  }

  GetRelationshipOwner(): Observable<any | {}> {
    return this._http.getAPI("CRMClientRecord/GetRelationshipOwner");
  }

  GetBroker(): Observable<any | {}> {
    return this._http.getAPI("CRMClientRecord/GetBroker");
  }
  GetOwnerById(OwnerId: number): Observable<any | {}> {
    return this._http.getAPI("CRMClientRecord/GetOwnerById?OwnerId=" + OwnerId);
  }
  GetBrokerById(BrokerId: number): Observable<any | {}> {
    return this._http.getAPI("CRMClientRecord/GetBrokerById?BrokerId=" + BrokerId);
  }
  SaveOwner(model: SaveRelationshipOwner): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/SaveRelationShipOwner", model);
  }

  UpdateOwner(model: SaveRelationshipOwner): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/UpdateRelationShipOwner", model);
  }

  SaveBroker(model: SaveUpadteBroker): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/SaveBroker", model);
  }

  UpdateBroker(model: SaveUpadteBroker): Observable<number | {}> {
    return this._http.postAPI("CRMClientRecord/UpdateBroker", model);
  }
  CheckCRMUser(CompanyName: string,ClientId:number) {
 
    return this._http.getAPI("CRMClientRecord/CheckCompanyName?CompanyName=" + CompanyName + "&ClientId="+ClientId);
  }

  DownloadDocument(Id: number): Observable<any> {
    return this.http.get(environment.baseUrl + "CRMClientRecord/DownloadDocument?DocumentId=" + Id, {
      reportProgress: true,
      responseType: 'blob'
    });

  }

  ViewCRMDocument(Id: number): Observable<any> {
    return this._http.getAPI("CRMClientRecord/ViewCRMDcument?DocumentId=" + Id);
  }

  NotifyTeam(CompanyName:string, FullName:string, NoteId:number){
  
    return this._http.getAPI("CRMClientRecord/SendNotificationEmail?CompanyName=" + CompanyName + "&FullName="+ FullName + "&NoteId=" +NoteId);
  }
  NewClientAddedMail(Fullname:string, CompanyName:string)
  {
   
    return this._http.getAPI("CRMClientRecord/SendNewClientAddedEmail?FullName=" +Fullname +"&CompanyName=" +CompanyName);
  }

  DeleteDocument(Id:number)
  {
    return this._http.getAPI("CRMClientRecord/DeleteDocument?DocumentId=" + Id);
  }

  SearchNoteFromList(NoteInput: string,ClientId:number) {
   
    return this._http.getAPI("CRMClientRecord/SearchNoteFromList?NoteInput=" + NoteInput + "&ClientId="+ ClientId);
  }
  GetEmailAddress(ClientId: number): Observable<Note | {}> {
    return this._http.getAPI("CRMClientRecord/GetEmailAddress?ClientId=" + ClientId);
  }

  DeleteEmailList(Id:number)
  {
    return this._http.getAPI("CRMClientRecord/DeleteEmailList?Id=" +Id);
  }

  ValidatePhoneFormat(event: any): boolean {
    let output: boolean = true;
    if (event.target.value.length > 9 && event.key !== "Delete" && event.key !== "Backspace" && event.key !== "ArrowLeft" && event.key !== "ArrowRight") {
      return false;
    }
    if (event.key === "0" || event.key === "1" || event.key === "2" || event.key === "3" || event.key === "4"
      || event.key === "5" || event.key === "6" || event.key === "7" || event.key === "8" || event.key === "9"
      || event.key === "Delete" || event.key === "Backspace" || event.key === "ArrowLeft" || event.key === "ArrowRight"
      || event.keyCode === "TABKEY" || event.key === "(" || event.key === ")" || event.key === "-"
    ) {
      output = true;
    } else {
      output = false;
    }
    return output;
  }

}
