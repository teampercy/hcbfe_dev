import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrmNgxDocumentViewerComponent } from './crm-ngx-document-viewer.component';

describe('CrmNgxDocumentViewerComponent', () => {
  let component: CrmNgxDocumentViewerComponent;
  let fixture: ComponentFixture<CrmNgxDocumentViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrmNgxDocumentViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrmNgxDocumentViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
