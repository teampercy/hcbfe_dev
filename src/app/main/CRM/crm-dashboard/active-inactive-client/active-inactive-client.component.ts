import { Component, OnInit, ViewEncapsulation, ViewChild, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatSort, MatPaginator, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-active-inactive-client',
  templateUrl: './active-inactive-client.component.html',
  styleUrls: ['./active-inactive-client.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ActiveInactiveClientComponent implements OnInit {
  errorText: string;
  Working: boolean;
  dataSource: any;
  dialogTitle: string;

  displayedColumns: string[] = ['CompanyName', 'PrincipalContact'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<ActiveInactiveClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

    if (this.data.Id == 1) {
      this.dialogTitle = "Active Client List"
    }
    else {
      this.dialogTitle = "InActive Client List"
    }
    this.data.case;
    this.dataSource = new MatTableDataSource<any>(this.data.case);

    this.dataSource.paginator = this.paginator;
  }

  EditACtiveInActiveClientList(clientID:number)
  {
    this._router.navigate(["/client-record/addedit-client-record/edit",clientID]);
    this.dialogRef.close();
  }

}
