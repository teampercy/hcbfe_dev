import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from 'app/service/common.service';
import { HcbDashboardService } from 'app/main/HCB/dashboard/hcb-dashboard.service';
import { fuseAnimations } from '@fuse/animations';
import { CrmDashboardService } from './crm-dashboard.service';
import { CaseManagmentService } from 'app/main/HCB/case-managment/case-managment.service';
import { GetddlClientModel, GetddlCasemanagmentModel } from 'app/main/HCB/case-managment/addedit-case-managment/addedit-case-management.model';
import * as _moment from 'moment';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { OpenCloseCaseClientComponent } from './open-close-case-client/open-close-case-client.component';
import { OpenCloseCaseMgrComponent } from './open-close-case-mgr/open-close-case-mgr.component';
import { ClientReportService } from '../administration/client-report/client-report.service';
import { MasterService } from '../administration/master/master.service';
import { OpenClientListComponent } from './open-client-list/open-client-list.component';
import { ActiveInactiveClientComponent } from './active-inactive-client/active-inactive-client.component';
const moment = _moment;

@Component({
  selector: 'app-crm-dashboard',
  templateUrl: './crm-dashboard.component.html',
  styleUrls: ['./crm-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CrmDashboardComponent implements OnInit {

  errorText: string;
  ErrorMessage: string;
  working: boolean;
  dialogRef: any;
  ActiveClients: FormGroup;
  ClientCases: FormGroup;
  CaseMgrCases: FormGroup;
  ClientsServices: FormGroup;
  // NoActivityReport: FormGroup;
  getddlClient: GetddlClientModel[];
  getddlServices: any;
  getddlNoActivityInterval: any;
  getddlCaseManager: GetddlCasemanagmentModel[];
  getOpenCases: any[];
  getClosedCases: any[];
  getOpenCasesPerCaseMgr: any[];
  getClosedCasesPerCaseMgr: any[];
  getClientList: any[];
  getClientService: any[];
  getNoActivityReport: any[];
  getTotalActiveClients: any;
  getTotalInActiveClients: any[];
  getActiveInActiveClient:any[];
  widget6: any = {};
  //view: any[] = [600, 200];

  getSchemeList: any;
  //getMostActiveClients: any;
  getActiveClientsType: any;
  getRegionPercentage:any;
  UserType: string;
  UserId: number;
  ClientId: number;
  StartDate: string;
  EndDate: string;
  OpenCaseClient: number;
  ClosedCaseClient: number;
  OpenCaseCaseMgr: number;
  ClosedCaseCaseMgr: number;
  ClientListlength: number;
  TotalOpenCases: number;
  TotalClosedCases: number;
  NoActivityMessage: string;

  public SchemeList: any;
  // public MostActiveClientList: any;
  public ActiveClientTypeList: any;
  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    public dialog: MatDialog,
    private _crmDashboardService: CrmDashboardService,
    private _caseManagment: CaseManagmentService,
    private _clientReportservice: ClientReportService,
    private _masterservice: MasterService
  ) { }

  title = 'Clients by Sector';
  type = 'PieChart';
  data = []
  

  columnNames = ['Company Name', 'Percentage'];
  options = { is3D: true,colors: ['#669933', '#0085b6']};
  width = 400;
  height = 250;

  title1 = 'Client by Region';
  type1 = 'PieChart';
  data1 = []
  options1 = { is3D: true,colors: ['#669933', '#0085b6'] };
  columnNames1 = ['Region', 'Percentage'];

  displayColumns: string[] = ['ServiceName', 'ClientCount'];
  // displayedColumns: string[] = ['ReferenceNo', 'ClientName', 'Id'];

  ngOnInit() {
    this.UserId = parseInt(localStorage.getItem('UserId'));
    this.UserType = localStorage.getItem('UserType');

    this.SchemeChart();
    this.GetTotalClient();
    this.ServiceType();
    this.GetddlNoActivityInterval();


    this._caseManagment.GetddlClient()
      .subscribe((response: GetddlClientModel[]) => this.getddlClient = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._caseManagment.GetddlCaseManager()
      .subscribe(
        (response: GetddlCasemanagmentModel[]) => this.getddlCaseManager = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this.ActiveClients = this._formBuilder.group({
      StartDate: [''],
      EndDate: ['']
    });

    this.ClientCases = this._formBuilder.group({
      Client: [''],
      StartDateClient: [''],
      EndDateClient: ['']
    });

    this.CaseMgrCases = this._formBuilder.group({
      CaseManager: [''],
      CaseMgrStartDate: [''],
      CaseMgrEndDate: ['']
    });

    this.ClientsServices = this._formBuilder.group({
      ServiceName: [''],
      ServiceDatePicker: ['']
    });

    this.SearchActiveClientsType();
    this.SearchRegionPercentage();
    this.SearchCasesClient();
    this.SearchCasesPerCaseMgr();
    this.SearchClientsPerService();
    // this.SearchNoActivityReport();
  }

  ServiceType(): void {
    this._clientReportservice.GetddlServices()
      .subscribe((response) => {
        this.getddlServices = response['result'];
      })
  }

  GetddlNoActivityInterval(): void {
    this._masterservice.GetNoActivityInterval(27)
      .subscribe((response) => {
        this.getddlNoActivityInterval = response['result'];
      })
  }


  SearchActiveClientsType(): void {
    this.working = true;
    this._crmDashboardService.GetActiveClientsType()
      .subscribe((response: any) => {
        this.getActiveClientsType = response['result'];
        let mainChart = [];
        if (this.getActiveClientsType) {
          this.getActiveClientsType.forEach(element => {
            let mod = new ChartBasicElements();

            if (element.clientType == 1) {
              mod.name = 'Insurance';
            }
            else {
              mod.name = 'Retail';
            }
            mod.value = element.clientCount;
            mainChart.push(mod);
          });
        }
        const mappedToArray = mainChart.map(d => Array.from(Object.values(d)))
        this.ActiveClientTypeList = mappedToArray;
        this.data = this.ActiveClientTypeList;
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
        });
  }


  SearchRegionPercentage(): void {
    this.working = true;
    this._crmDashboardService.GetDashboardRegionPercentage()
      .subscribe((response: any) => {
        this.getRegionPercentage = response['result'];
        let mainChart1 = [];
        if (this.getRegionPercentage) {
          this.getRegionPercentage.forEach(element => {
            let mod = new ChartBasicElements();
            mod.name=element.region;
            mod.value = element.regionCount;
            mainChart1.push(mod);
          });
        }
        const mappedToArray = mainChart1.map(d => Array.from(Object.values(d)))
        this.data1 =mappedToArray;
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
        });
  }


  SchemeChart() {
    this.widget6 = {
      legend: true,
      explodeSlices: false,
      labels: true,
      //  doughnut: true,
      pie: true,
      gradient: false,

      scheme: {
        domain: ['rgb(52, 191, 163)', 'rgb(253, 57, 149)', 'rgb(255, 184, 34)', 'rgb(54, 163, 247)', ' rgb(93, 120, 255)'],
        is3D: true
      },
    };
  }


  GetTotalClient(): void {
    this._crmDashboardService.GetTotalClients()
      .subscribe((response: any) => {
        this.getTotalActiveClients = response['result'][0].tempTotalActiveClients
        this.getTotalInActiveClients = response['result'][0].tempTotalInActiveClients
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  GetActiveInActiveClient(IsActive: number) {
    this._crmDashboardService.GetActiveInActiveClient(IsActive)
    .subscribe((response:any)=>{
       this.getActiveInActiveClient=response['result'];

       this.dialogRef = this.dialog.open(ActiveInactiveClientComponent, {
        disableClose: true,
        width: '400px',
        height: 'auto',
        maxHeight: '10vh !important',
        data: {
          case:  this.getActiveInActiveClient,
          Id:IsActive
          
        }
      });
  
      this.dialogRef.afterClosed().subscribe(result => {
        if (result) {
  
        }
      });
    })
  }

  SearchCasesClient(): void {
    this.ClientId = this.ClientCases.get('Client').value == '' || this.ClientCases.get('Client').value == undefined ? 0 : this.ClientCases.get('Client').value;
    var ClientStartDate = (this.ClientCases.value.StartDateClient == "" || this.ClientCases.value.StartDateClient == null) ? "" : "" + this.ClientCases.value.StartDateClient._i.year + "-" + (this.ClientCases.value.StartDateClient._i.month + 1) + "-" + this.ClientCases.value.StartDateClient._i.date + " 00:00:00:000";
    var ClientEndDate = (this.ClientCases.value.EndDateClient == "" || this.ClientCases.value.EndDateClient == null) ? "" : "" + this.ClientCases.value.EndDateClient._i.year + "-" + (this.ClientCases.value.EndDateClient._i.month + 1) + "-" + this.ClientCases.value.EndDateClient._i.date + " 00:00:00:000";
    this._crmDashboardService.GetCasesPerClient(true, this.ClientId, ClientStartDate, ClientEndDate)
      .subscribe((response: any[]) => {
        this.getOpenCases = response['result'];
        this.OpenCaseClient = response['result'].length;
      },
        (error: any) => {
          this.errorText = error;
        });

    this._crmDashboardService.GetCasesPerClient(false, this.ClientId, ClientStartDate, ClientEndDate)
      .subscribe((response: any[]) => {
        this.getClosedCases = response['result'];
        this.ClosedCaseClient = response['result'].length;
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  SearchCasesPerCaseMgr(): void {
    var CaseMgrId = this.CaseMgrCases.get('CaseManager').value == '' || this.CaseMgrCases.get('CaseManager').value == undefined ? 0 : this.CaseMgrCases.get('CaseManager').value;
    var CaseStartDate = (this.CaseMgrCases.value.CaseMgrStartDate == "" || this.CaseMgrCases.value.CaseMgrStartDate == null) ? "" : "" + this.CaseMgrCases.value.CaseMgrStartDate._i.year + "-" + (this.CaseMgrCases.value.CaseMgrStartDate._i.month + 1) + "-" + this.CaseMgrCases.value.CaseMgrStartDate._i.date + " 00:00:00:000";
    var CaseEndDate = (this.CaseMgrCases.value.CaseMgrEndDate == "" || this.CaseMgrCases.value.CaseMgrEndDate == null) ? "" : "" + this.CaseMgrCases.value.CaseMgrEndDate._i.year + "-" + (this.CaseMgrCases.value.CaseMgrEndDate._i.month + 1) + "-" + this.CaseMgrCases.value.CaseMgrEndDate._i.date + " 00:00:00:000";

    this._crmDashboardService.GetCasesPerCaseMgr(true, CaseMgrId, CaseStartDate, CaseEndDate)
      .subscribe((response: any[]) => {
        this.getOpenCasesPerCaseMgr = response['result'];
        this.OpenCaseCaseMgr = response['result'].length;
      },
        (error: any) => {
          this.errorText = error;
        });

    this._crmDashboardService.GetCasesPerCaseMgr(false, CaseMgrId, CaseStartDate, CaseEndDate)
      .subscribe((response: any[]) => {
        this.getClosedCasesPerCaseMgr = response['result'];
        this.ClosedCaseCaseMgr = response['result'].length;
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  OpenCloseCaseClient(value: number): void {
    if (value == 1) {
      var CaseClient = this.getOpenCases
      var ID = 1
    }
    else {
      CaseClient = this.getClosedCases
      ID = 0
    }
    this.dialogRef = this.dialog.open(OpenCloseCaseClientComponent, {
      disableClose: true,
      width: '800px',
      height: 'auto',
      maxHeight: '10vh !important',
      data: {
        case: CaseClient,
        Id: ID
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  OpenCloseCaseMgr(value: number): void {
    if (value == 1) {
      var CaseMgr = this.getOpenCasesPerCaseMgr
      var ID = 1
    }
    else {
      CaseMgr = this.getClosedCasesPerCaseMgr
      ID = 0
    }
    this.dialogRef = this.dialog.open(OpenCloseCaseMgrComponent, {
      disableClose: true,
      width: '800px',
      height: 'auto',
      maxHeight: '10vh !important',
      data: {
        caseMgr: CaseMgr,
        Id: ID
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  SearchClientsPerService(): void {

    var ServiceId = this.ClientsServices.get('ServiceName').value == '' || this.ClientsServices.get('ServiceName').value == undefined ? 0 : this.ClientsServices.get('ServiceName').value;

    this._crmDashboardService.GetDashboardClientsPerServices(ServiceId)
      .subscribe((response: any[]) => {
        this.getClientService = response['result'];
      },
        (error: any) => {
          this.errorText = error;
        });
  }


  OpenClientListForService1(value: number): void {

    this._crmDashboardService.GetClientListForService(value)
      .subscribe((response: any[]) => {

        this.getClientList = response['result'];
        this.ClientListlength = response['result'].length;
        this.OpenClientListForService(this.getClientList);
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  OpenClientListForService(getClientList: any): void {
    this.dialogRef = this.dialog.open(OpenClientListComponent, {

      disableClose: true,
      width: '400px',
      data: {
        case: this.getClientList,
        //  ServiceTypeId: value
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }


  ClearClientCases(): void {
    this.ClientCases.reset();
    this.SearchCasesClient();
  }

  ClearCaseMgrCases(): void {
    this.CaseMgrCases.reset();
    this.SearchCasesPerCaseMgr();
  }

  ClearClientsPerService(): void {
    this.ClientsServices.reset();
    this.SearchClientsPerService();
  }


}
export class ChartBasicElements {
  public name: any;
  public value: any;
}
