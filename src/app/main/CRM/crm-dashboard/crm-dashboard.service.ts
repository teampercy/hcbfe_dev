import { Injectable } from '@angular/core';
import { HttpClientsService } from 'app/service/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrmDashboardService {

  constructor( private _http: HttpClientsService) { }

  GetActiveInActiveClient(IsActive:number):Observable<any|{}>{
    return this._http.getAPI("CRMDashboard/GetActiveInActiveClient?IsActive="+IsActive);
  }
  GetActiveClientsType(): Observable<any | {}> {
    return this._http.getAPI("CRMDashboard/GetActiveClientsType");
  }
  GetDashboardRegionPercentage(): Observable<any | {}> {
    return this._http.getAPI("CRMDashboard/GetDashboardRegionPercentage");
  }

  GetCasesPerClient(IsOpen:boolean,ClientId:number,startdate: string,enddate:string): Observable<any | {}> {
    return this._http.getAPI("CRMDashboard/GetCasesPerClient?IsOpen="+IsOpen+"&ClientId=" + ClientId+"&startdate=" + startdate+ "&enddate=" + enddate);
  }

  GetCasesPerCaseMgr(IsOpen:boolean,CaseMgrId:number,startdate: string,enddate:string): Observable<any | {}> {
    return this._http.getAPI("CRMDashboard/GetCasesPerCaseMgr?IsOpen="+IsOpen+"&CaseMgrId=" + CaseMgrId+"&startdate=" + startdate+ "&enddate=" + enddate);
  }

  GetTotalClients(): Observable<any | {}> {
    return this._http.getAPI("CRMDashboard/GetTotalClients");
  }

  GetDashboardClientsPerServices(ServiceId:number): Observable<any | {}> {
    return this._http.getAPI("CRMDashboard/GetDashboardClientsPerServices?ServiceId=" + ServiceId);
  }


  GetClientListForService(ServiceTypeId: number): Observable<any | {}>{
  return this._http.getAPI("CRMDashboard/GetClientListForService?ServiceTypeId=" + ServiceTypeId);
}
}
