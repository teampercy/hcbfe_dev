import { Component, OnInit, Inject, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';
@Component({
  selector: 'app-open-client-list',
  templateUrl: './open-client-list.component.html',
  styleUrls: ['./open-client-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OpenClientListComponent implements OnInit {

  errorText: string;
  Working: boolean;
  dataSource: any;
  dialogTitle: string;

  displayedColumns: string[] = ['CompanyName', 'PrincipalContact'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<OpenClientListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    this.dialogTitle = "Client List"
    this.data.case;
    this.dataSource = new MatTableDataSource<any>(this.data.case);

    this.dataSource.paginator = this.paginator;
  }

  EditClientList(clientID: number) {
    this._router.navigate(["/client-record/addedit-client-record/edit", clientID]);
    this.dialogRef.close();
  }



}
