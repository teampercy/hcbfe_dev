import { Component, OnInit, Inject, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { debug } from 'util';
import { Router } from '@angular/router';
@Component({
  selector: 'app-open-close-case-client',
  templateUrl: './open-close-case-client.component.html',
  styleUrls: ['./open-close-case-client.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OpenCloseCaseClientComponent implements OnInit {

  errorText: string;
  Working: boolean;
  dataSource: any;
  dialogTitle: string;

  displayedColumns: string[] = ['HCBReferenceNo', 'CompanyName', 'HCBReceivedDate', 'CaseClosedDate', 'Id'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<OpenCloseCaseClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.Id == 1) {
      this.dialogTitle = "Open Cases"
    } else {
      this.dialogTitle = "Closed Cases"
    }
      
    this.dataSource = new MatTableDataSource<any>(this.data.case);

    this.dataSource.paginator = this.paginator;
  }

  Edit(hcbReferenceId: number): void {
      
    this._router.navigate(["/case-managment/addedit-case-managment/edit", hcbReferenceId]);

}

}
