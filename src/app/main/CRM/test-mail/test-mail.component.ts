import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import{TestMailService} from './test-mail.service';

@Component({
  selector: 'app-test-mail',
  templateUrl: './test-mail.component.html',
  styleUrls: ['./test-mail.component.scss']
})
export class TestMailComponent implements OnInit {

  testMail: FormGroup;
  loginError: string;
  working: boolean;
  displayImage: boolean;
  Result:string;

  constructor(

    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private _router: Router,
   private _TestMail:TestMailService,
    private _fuseNavigationService: FuseNavigationService
  ) {
    // Configure the layout
    var queryStringValue = window.location.href.split('#')[0];
  if (queryStringValue.toLowerCase().indexOf("/crm") > -1) {     
     localStorage.setItem('Site', "2");
    }
    else {
      localStorage.setItem('Site', "1");
    }
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    
    this.loginError = undefined;
    this.working = false;
    this.testMail = this._formBuilder.group({
      SMTPHost:['smtp.office365.com'],
      
      username: ['enquiries@hcbgroup.co.uk'],
      password: ['T3mppass!2347'],
      FromEmail:['enquiries@hcbgroup.co.uk'],
      SenderEmail:['pallavi.mandre@rhealtech.com'],
      Port:['587']
    });
    var queryStringValue = window.location.href.split('#')[0];
    if ((queryStringValue.toLowerCase().indexOf("/crm") > -1)) {
      //  this._router.navigate(['/crm-dashboard']);
      // localStorage.setItem('Site', "2");
      this.displayImage = true;
    }
    else {
      // this.navigation = navigation.map(x => Object.assign({}, x));
      //    localStorage.setItem('Site', "1");
      this.displayImage = false;
    }
  }

  Send(): void {

    this.loginError = undefined;
    this.working = true;
    


    this._TestMail.TestMail(this.testMail.value.SMTPHost,this.testMail.value.username,
       this.testMail.value.password,this.testMail.value.FromEmail,this.testMail.value.SenderEmail,this.testMail.value.Port)
      .subscribe((Result: any) => {
    
    this.Result=Result.result;
    this.working = false;
        // var queryStringValue = window.location.href.split('#')[0];
        // if ((queryStringValue.toLowerCase().indexOf("/crm") > -1) && (Result.userTypeId == 1)) {
          
        //   this._router.navigate(['/crm-dashboard']);

        // }
        // else if ((queryStringValue.toLowerCase().indexOf("/crm") > -1) && (Result.userTypeId != 1)) {
       
        //   this._router.navigate(['/login']);
        //   this.loginError = "Invalid User";
        //   this.working = false;

        // }
        // else if (Result.userTypeId == 4) {
        //   this._router.navigate(['/login']);
        //   this.loginError = "Invalid User";
        //   this.working = false;
        // }
        // else {
       
        // //  this._router.navigate(['/login']);  //     /dashboard
        //   this._router.navigate(['/dashboard']);
        // }

      }, (error: any) => {
        this.loginError = error;
        this.working = false;
      });

  }

}