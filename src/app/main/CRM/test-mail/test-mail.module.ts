import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { AuthGuard } from 'app/custom/auth.guard';
//import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { TestMailComponent } from './test-mail.component';


const routes = [
    {
        path: 'test-mail',
        component: TestMailComponent,       
    }
];

@NgModule({
    declarations: [
        TestMailComponent     
    ],
    imports: [
      RouterModule.forChild(routes),
      TranslateModule,      
      FuseSharedModule,
      MaterialModule
    ]
  })
export class TestMailModule{}