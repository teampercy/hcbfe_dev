import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'app/model/user';
import { map } from 'rxjs/operators';
import { HttpClientsService } from 'app/service/http-client.service';
import { AuthenticationService } from 'app/service/authentication.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'


})
export class TestMailService {

  constructor(
    //private _exceptionHandler: ExceptionService,
    private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { }

  TestMail(SMTPHost:string,UserName:string,Password:string,FromEmail:string,SenderEmail:String,Port:string) {
    return this._http.getAPI("TestMail/TestMail?SMTPHost="+SMTPHost +"&Username="+UserName+"&Password="+Password+ "&FromEmail="+FromEmail
    +"&SenderEmail="+SenderEmail+"&Port="+Port);
  }
}
