export class GetCaseManagmentModel {
    HCBReferenceNo: string;
    ClientID: number;
    CaseMgrId: number;
    ClientContactId: number;
    HCBReceivedDate: string;
    DateReferredToCaseOwner: string;
    ServiceType: number;
    ClaimantFirstName: string;
    ClaimantLastName: string;
    ClaimantDOB: Date;
    ClaimantGender: string;
    ClaimantPhone: string;
    ClaimantMobile: string;
    ClaimantOccupation: string;
    ClaimantServiceReq: number;
    ClaimantAddress1: string;
    ClaimantAddress2: string;
    ClaimantCity: string;
    ClaimantCounty: string;
    EmployerName: string;
    EmployerContact: string;
    EmployerEmail: string;
    ClientClaimRef: string;
    DisabilityCategoryId:number;
    DisabilityDescription:string;
    IncapacityDefination: string;
    SchemeId: number;
    DeferredPeriod: number;
    EndOfBenefitDate: Date;
    MonthlyBenefit: number;
    IsEligibleforEAP: boolean;
    SchemeNumer: string;
    FirstAbsentDate: Date;
    IllnessInjuryId: number;
    DisabilityType: string;
    // HCBGroupInstructedDate: string;
    ClaimClosedDate: Date;
    ClaimClosedReasonId: number;
    ClaimDetails: string;
    SentToNurseDate: Date;
    ClaimantFirstContactedDate: Date;
    FirstVerbalContactClaimantDate: Date;
    NextReviewDate: Date;
    FeeCharged: string;
    RehabCost: string;
    DateCancelled: string;
    ReturnToWorkDate: string;
    ReturnToWorkReason: string;
    CaseClosedReasonId: number;
    PDAID: number;
    SecondaryPDAID: number;
    CaseClosedDate: string;
    TypeOfContactId: number;
    TypeOfContactDetails: string;
    ClientRefEmail:string;
    ClientRefTelephone:string;
    DateFirstContactApplicant: string;
    DateClaimFormSent: string;
    ClaimantPostcode: string;
    SubsequentActivity: string;
    ServiceCaseClosed: string;
    Outcome: string;
    ReasonForDeclinature: string;
    SubmitedTraceSmart:string;
    DataReturned: string;
    PositiveTraceAge:string;
    ReportSubmitted: string;
    OverallTraces: string;
    HCBAppointedDate:string;
    AnnualReportDate:string;
    AnnualReportOutcome:string;
    AnnualReportValue:number;
    NameOfTrust:string;
    LiveCoveredNumber:number;
    ReportingIntervals:string;
    Assessor:string;
    AssessorRate:string;
    ClientRate:number;
    ContractStartDate:string;
    ContractEndDate:string;
    SubmitedToProvider:string;
    ReportDueBy:string;
    ReportReceived:string;
    ReportClient:string;
}

export class SaveUpdateCaseManagment {
    Old_HCBRefId:string;
    HCBReferenceId: number;
    HCBReferenceNo: string;
    HCBReceivedDate: string;
    ClientID: number;
    CaseMgrId: number;
    ClientContactId: number;
    DateReferredToCaseOwner: string;
    ServiceType: number;
    ClaimantFirstName: string;
    ClaimantLastName: string;
    ClaimantDOB: string;
    ClaimantGender: string;
    ClaimantPhone: string;
    ClaimantMobile: string;
    ClaimantOccupation: string;
    ClaimantServiceReq: number;
    ClaimantAddress1: string;
    ClaimantAddress2: string;
    ClaimantCity: string;
    ClaimantCounty: string;
    EmployerName: string;
    EmployerContact: string;
    EmployerEmail: string;
    EmployerTeleNo: string;
    EmployerContact1: string;
    EmployerContact2: string;
    ClientClaimRef: string;
    DisabilityCategoryId: number;
    DisabilityDescription:string;
    IncapacityDefination: string;
    SchemeId: number;
    DeferredPeriod: number;
    EndOfBenefitDate: string;
    MonthlyBenefit: number;
    IsEligibleforEAP: boolean;
    SchemeNumer: string;
    FirstAbsentDate: string;
    IllnessInjuryId: number;
    DisabilityType: string;
    //HCBGroupInstructedDate:Date;		
    ClaimClosedDate: string;
    ClaimClosedReasonId: number;
    ClaimDetails: string;
    SentToNurseDate: string;
    ClaimantFirstContactedDate: string;
    //FirstHRContactedDate:string;			
    FirstVerbalContactClaimantDate: string;
    NextReviewDate: string;
    FeeCharged: string;
    RehabCost: string;
    DateCancelled: string;
    ReturnToWorkDate: string;
    ReturnToWorkReason: string;
    CaseClosedReasonId: number;
    PDAID:number;
    SecondaryPDAID:number;
    CaseClosedDate: string;
    TypeOfContactId: number;
    TypeOfContactDetails: string;
    ClientRefEmail:string;
    ClientRefTelephone:string;
    DateFirstContactApplicant: string;
    DateClaimFormSent: string;
    ClaimantPostcode: string;
    SubsequentActivity: string;
    ServiceCaseClosed: string;
    Outcome: string;
    ReasonForDeclinature: string;
    SubmitedTraceSmart:string;
    DataReturned: string;
    PositiveTraceAge:string;
    ReportSubmitted: string;
    DateProjectClosed:string;
    OverallTraces: string;
    AnnualReportDate:string;
    AnnualReportOutcome:string;
    AnnualReportValue:number;
    NameOfTrust:number;
    LiveCoveredNumber:number;
    HCBAppointedDate:string;
    ReportingIntervals:string;
    Assessor:number;
    AssessorRate:number;
    ClientRate:number;
    ContractStartDate:string;
    ContractEndDate:string;
    SubmitedToProvider:string;
    ReportDueBy: string;
    ReportReceived:string;
    ReportClient:string;
    PensionSchmName:string;    //08/07/2020 
    ServiceProviderId:number;
    ServiceRequestedId:number;
    TimeSpent:string;
    FeesPayable:number;
    DateofAssessment:string;
    ReopenStatus:number;

    ClaimantTitle:string;     //13/10/2020
    ClaimantEmail:string;
    EmployerAddress:string;
    EmployerOccupation:string;
    PDICDCode:number;
    TimeLag:string;
    ClaimStatusId:number;
    fourthClaimStatusId :number;
    ClaimStatusDate:string;
    OutcomesId:number;
    DateOfActivity:string;
    ClaimAssessorId:number;
    ActionId:number;
 ReferrerName: string;
 ReferrerEmail :string;
  ReferrerTele :string;

}

export class GetTelephoneActivity {
    HCBReferenceId: number;
    CallDate: string;
    CallTime: string;
    TypeofCall: number;
    CallExpectRTWDateOptimum: string;
    CallExpectRTWDateMaximum: string;
    CallCaseMngtRecom: string;
    CallNxtSchedule: string;
    FeeCharged: number;
}
export class GetSubsequentActivityGrid{
    Id:number;
    SubsequentActivity:String;
    Date:string;
}


export class SaveSubsequentActivity{
    Id:number;
    HCBReferenceId:number;
    SubsequentActivityName:string;
    ReminderCheck:boolean;
    ReminderDate:string;
}

export class GetddlClientModel {
    clientID: number;
    principalContactNo: string;
    principalContactEmail: string;
    companyName: string;
}

export class GetddlCasemanagmentModel {
    userID: number;
    CaseOwnerName: string;
    emailAddress:string;
    telephoneNumber:string;
}
export class GetddlServiceModel {
    ServiceId: number;
    ServiceName: string;
}

export class GetddlServiceType {
    ServiceTypeId: number;
    ServiceType: string;
}

export class GetddlClientContact {
    // ClientContactId: number;
    // ClientContactName: string;
    clientReferralContactsId:number;
    contactName:string;
    ContactName:string;
    contactEmail: string;
    phoneNo : string;
}

export class GetddlTypelistValues {
    ID: number;
    Name: string;
    id:number;
    name:string;
}


export class HCBNote {
    noteID:number;
    HCBReferenceId: number;
    noteText: string;
    noteCreatedDate: Date;
    fullName:string;
    comment:string;
}

export class GetHCBVisitActivity {
    HCBVisitId: number;
    HCBReferenceId: number;
    AppointmentDate: string;
    ReportCompDate: string;
    FeeCharged: number;
    TYpeOfVisit: number;
    Name: string;
}

export class GetAnnualReportSummary {
    AnnualReportId: number;
    HCBReferenceId: number;
    AnnualReportDate: string;
    AnnualReportOutcome: string;
    AnnualReportValue: number;
}

export class HCBDocumentUpload {
    HCBReferenceId: number;
}
export class HCBDocumentGrid {
    HCBReferenceId: number;
    DocumentName: string;
    DocumentPath: string;
    UploadedBy: number;
    DateCreated: Date;
}

export class GetAssessorDetails{
    Id:number;
    AssessorName:string;
    TeamEmailID:string;
    PhoneNum:string;
}

export class SaveAssessor{
    Id:number;
    AssessorName:string;
    TeamEmailID:string;
    PhoneNum:string;
}

