import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditCaseManagmentComponent } from './addedit-case-managment.component';

describe('AddeditCaseManagmentComponent', () => {
  let component: AddeditCaseManagmentComponent;
  let fixture: ComponentFixture<AddeditCaseManagmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditCaseManagmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditCaseManagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
