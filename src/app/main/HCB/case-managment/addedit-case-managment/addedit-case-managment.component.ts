import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, OnDestroy, AfterContentInit } from '@angular/core';
//import { MAT_STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatDialog, MatSnackBar, MatDatepicker } from '@angular/material';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { Router, ActivatedRoute, Params, UrlSegment } from '@angular/router';
import { GetCaseManagmentModel, GetddlClientModel, GetddlCasemanagmentModel, GetddlServiceModel, GetddlTypelistValues, GetddlClientContact, GetddlServiceType, GetTelephoneActivity, HCBNote, GetHCBVisitActivity, HCBDocumentGrid, HCBDocumentUpload, SaveUpdateCaseManagment, GetSubsequentActivityGrid, GetAssessorDetails, GetAnnualReportSummary } from './addedit-case-management.model';
import { CaseManagmentService } from '../case-managment.service';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import * as _moment from 'moment';
import { CommonService } from 'app/service/common.service';
import { AddeditVisitActivityComponent } from '../addedit-visit-activity/addedit-visit-activity.component';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { AddeditTelephoneActivityComponent } from '../addedit-telephone-activity/addedit-telephone-activity.component';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { MasterService } from 'app/main/CRM/administration/master/master.service';
import { ClientRecordService } from 'app/main/CRM/client-record/client-record.service';
import { AddeditSubsequentActivityComponent } from './addedit-subsequent-activity/addedit-subsequent-activity.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
import { AlertMessageComponent } from 'app/main/Common/alert-message/alert-message.component';
import { CaseCancelReasonComponent } from '../case-cancel-reason/case-cancel-reason.component';
import { AddEditActionComponent } from 'app/main/CRM/administration/master/action/add-edit-action/add-edit-action.component';
import { CaseActivitiesComponent } from '../case-activities/case-activities.component';
import swal from 'sweetalert2';

const moment = _moment;
import { AddEditBrokerComponent } from 'app/main/CRM/administration/master/broker/add-edit-broker/add-edit-broker.component';
import { GetddlNameOfTrust } from '../../dashboard/hcb-dashboard.model';
import { AppCustomDirective } from 'app/main/Common/appValidation';
import { AddeditAnnualReportSummaryComponent } from './addedit-annual-report-summary/addedit-annual-report-summary.component';
import * as FileSaver from 'file-saver';
import { Location } from '@angular/common';
import { from } from 'rxjs';
//import { url } from 'inspector';
import { Url } from 'url';
import * as XLSX from 'xlsx';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService } from '@syncfusion/ej2-angular-richtexteditor';


@Component({
  selector: 'app-addedit-case-managment',
  templateUrl: './addedit-case-managment.component.html',
  styleUrls: ['./addedit-case-managment.component.scss'],
//   providers: [{
//     provide: MAT_STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
//   },
//   ToolbarService, LinkService, ImageService, HtmlEditorService, TableService
// ],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddeditCaseManagmentComponent implements OnInit, OnDestroy, AfterContentInit {

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', '|',
      'FontName', 'FontColor', 'BackgroundColor', '|',
      'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList']
      
};
  //date = new FormControl(moment());
  CanWorkAsCM:number;
  IsworkAsCaseM:boolean;
  IsAdmin:boolean;
  SendmailId:boolean;
  IsImpotantCheck:boolean;
  ShowServiceEleven: boolean = false;
  ActionName: string;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixththFormGroup: FormGroup;                  //06/07/2020
  getddlClient: GetddlClientModel[];
  getddlClientContact: GetddlClientContact[];
  getddlCaseOwner: GetddlCasemanagmentModel[];
  getddlServiceRequired: GetddlServiceModel[];
  getddlServiceType: GetddlServiceType[];
  getddlInacpacityDefination: GetddlTypelistValues[];
  getddlOccupationalClassification: GetddlTypelistValues[];
  getddlDeferredPeriod: GetddlTypelistValues[];
  getddlSchemeName: GetddlTypelistValues[];
  getddlIllnessInjury: GetddlTypelistValues[];
  getddlFeesPayable: GetddlTypelistValues[];
  getddlClaimClosedReason: GetddlTypelistValues[];
  getddlTypeOfCall: GetddlTypelistValues[];
  getddlTypeOfContact: GetddlTypelistValues[];
  //getddlTypeOfVisit: GetddlTypelistValues[];
  getddlReasonClosed: GetddlTypelistValues[];
  getddlHealthTrust: GetddlTypelistValues[];
  getddlDisabilityCategoryType: GetddlTypelistValues[];
  getddlServiceRequested: GetddlTypelistValues[];
  getddlAssessorDetails: GetAssessorDetails[];
  getddlNameOfTrust: GetddlNameOfTrust[];
  getddlServiceProvider: any;
  getddlPDA: GetddlTypelistValues[];
  getddlClaimStatus: GetddlTypelistValues[];
  getddlClaimAssessor: GetddlTypelistValues[];
  getddlAction: GetddlTypelistValues[];
  getddlOutcomes: GetddlTypelistValues[];
  getddlPDAICDCode: any[];

  noteForm: FormGroup;
  HcbReferenceId: number;
  Telephone: string;
  email: string;
  errorText: string;
  da: any;
  errorTextGrid: string;
  dialogTitle: string;
  working: boolean;
  ClientId: number;
  dialogRef: any;
  message: string;
  valid: string;
  FullName: string;
  documentDataSource: any;
  noteHCBDataSource: any[] = [];
  CaseActivityGrid: any;
  visitActivityGrid: any;
  TelephoneActivityGrid: any;
  AnnualReportGrid: any;
  SubsequentActivityGrid: any;
  disableVisitButton: boolean;
  HCBNoteList: any[] = [];
  NotesList: any[] = [];
  progress: number;
  uploading: boolean;
  IncoClientId: number;
  UserTypeId: number;
  CaseOwnerId: number;
  Cost: number;
  ddlDisabled: boolean;
  editable: boolean = true;
  fileToUpload: File = null;
  selectedIndex: number = 0;
  ShowTab: boolean = false;
  hideService: boolean = true;
  ShowServiceThree: boolean = false;
  ShowServiceFive: boolean = false;
  ShowServiceSix: boolean = false;
  ShowServiceEight: boolean = false;
  ShowServiceSeven: boolean = false;
  ShowServiceNine: boolean = false;
  ShowOutcome: boolean = false;
  ShowClientRate: boolean;
  yourText: boolean;
  ChangeColor: string;
  DisableService: boolean = false;
  SendEmail: boolean;
  IsImportant: boolean;
  HCBReferenceNo: string;

  ShowCaseDetail: boolean = true;
  ShowNote: boolean;
  ShowActivities: boolean;
  ShowAdminAndFinc: boolean;
  ShowFinc: boolean;             ///06/07/2020
  ShowDoc: boolean;
  disablemailButton: boolean = false;
  disableDOCButton: boolean = true;
  CMUserdisableDOCButton:boolean= true;  //To enable  the view button to CM if Case is close
  hideService_Schem: boolean = true;
  editTELEandVISIT: boolean;

  ServiceEight_FirstForm: boolean;

  FeepayableValue: string;
  CFirstName: string;
  CLastName: string;
  CBirthDate: string;
  ExistingFirstName: string;
  ExistingLastName: string;
  ExistingBirthDate: string;

  EmpDetails: boolean = false;
  DisplayCompany: boolean = true;
  Displayheader: boolean = true;
  DOB: Date;
  x: boolean = false;
  ClaimantFirstName: string;
  ClaimantLastName: string;
  clintID: number;
  CompanyName: string;
  NoteID: number;
  CaseOwnerEmail: string;

  ExistingCancelDate: string;
  popupresult: string;
  closed_Date: Date;
  OldRefId: any;
  OldRefID: any;
  OldCaseId: any;
  private sub: any;

  previewUrl: any;
  RecDate: boolean;
  disableCloseDate: boolean;
  ShowDeleteButton: boolean;

  PDAvalue: boolean = false;
  OutcomeOrAdmin: string;
  ShowActivites: boolean;
  HideActivities: boolean;
  HideClaimStatus : boolean;
  TitleForEIandTPA: string;
  UserId: number;
  arrayBuffer:any;
  file:any;

  displayedColumns: string[] = ['position', 'Clientname', 'DOB'];
  displayedHCBDocument: string[] = ['Name', 'UploadedBy', 'Date', 'edit', 'view', 'delete'];
  displayTelephoneActivityColumns: string[] = ['CallDate', 'CaseMngRecom', 'TypeOfCall', 'feeCharged', 'edit']
  displayCaseActivityColumn: string[] = ['ActivityDate', 'Claimassessor', 'Action', 'ServiceProvider', 'ReportDurDate', 'edit']
  displayedVisitColumns: string[] = ['appointmentDate', 'name', 'reportCompDate', 'feeCharged', 'edit'];
  displayedSubsequentActivity: string[] = ['Name', 'ReminderCheck', 'Date', 'edit'];
  displayedAnnualReportColumns: string[] = ['AnnualReportDate', 'AnnualReportOutcome', 'AnnualReportValue', 'edit'];

  @ViewChild(FusePerfectScrollbarDirective, { static: true })
  @ViewChild('DocumentfileInput', { static: false }) DocumentfileInput: ElementRef;
  @ViewChild('myclick', { static: false }) myDiv: ElementRef<HTMLElement>;
  @ViewChild('AdminClick', { static: false }) AdminClick: ElementRef<HTMLElement>;
  @ViewChild('FinanClick', { static: false }) FinanClick: ElementRef<HTMLElement>;
  @ViewChild('ActivityClick', { static: false }) ActivityClick: ElementRef<HTMLElement>;
  @ViewChild('DocClick', { static: false }) DocClick: ElementRef<HTMLElement>;
  @ViewChild('DetalClick', { static: false }) DetalClick: ElementRef<HTMLElement>;
  directiveScroll: FusePerfectScrollbarDirective;

  constructor(private _formBuilder: FormBuilder,
    private _router: Router,
    // public maskTime = [/[0-9]/, /\d/,':',/\d/, /\d/],
    public dialog: MatDialog,
    private _route: ActivatedRoute,
    private _matSnackBar: MatSnackBar,
    private _clientRecord: ClientRecordService,
    private _commonService: CommonService,
    private _masterService: MasterService,
    private _caseManagment: CaseManagmentService,
    private _location: Location
  ) { }

  ngAfterContentInit() {

    if (this.firstFormGroup.get('ServiceType').value == null || this.firstFormGroup.get('ServiceType').value == undefined) {
      console.log('AfterInIt');
      this.ngOnInit();
    } else { }
  }

  ngOnInit() {

    this.sub = this._route.params.subscribe(
      params => {

        this.HcbReferenceId = params['id'];
        // this.getMovie(id);
      }
    );


    this._route.url.subscribe(
      value => {

        this.ActionName = value[1].path
        // this.getMovie(id);
      }
    );

    if (this.HcbReferenceId == null || this.HcbReferenceId == undefined) {
      this.HcbReferenceId = 0;
    }
    this.FullName = localStorage.getItem('UserName');
    this.IncoClientId = parseInt(localStorage.getItem('ClientId'));
    this.UserTypeId = parseInt(localStorage.getItem('UserTypeId'));
    this.CanWorkAsCM = parseInt(localStorage.getItem('CanWorkAsCM'));
    this.UserId = parseInt(localStorage.getItem('UserId'));

    this.GetClient();
    this.GetCaseOwner();
    this.GetServcieProviderddl();
    this.GetCaseActivityGrid();

    this._caseManagment.GetddlServiceRequired()
      .subscribe(
        (response: GetddlServiceModel[]) => this.getddlServiceRequired = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlInacpacityDefination(14)
      .subscribe((response: GetddlTypelistValues[]) => this.getddlInacpacityDefination = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlDeferredPeriod(11)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlDeferredPeriod = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlSchemeName(19)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlSchemeName = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlIllnessInjury(5)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlIllnessInjury = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlFeesPayable(30)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlFeesPayable = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlClaimClosedReason(6)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlClaimClosedReason = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlTypeOfContact(25)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlTypeOfContact = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlReasonClosed(7)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlReasonClosed = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlValues(35)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlPDA = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlOccupationalClassification(26)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlOccupationalClassification = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlHealthTrust(28)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlHealthTrust = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlValues(34)

      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlDisabilityCategoryType = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlValues(36)

      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlServiceRequested = response['result'],
        (error: any) => {
          this.errorText = error;
        });
    this._masterService.GetddlValues(37)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlClaimStatus = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlValues(38)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlClaimAssessor = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._masterService.GetddlValues(39)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlAction = response['result'],
        (error: any) => {
          this.errorText = error;
        });
    this._masterService.GetddlValues(40)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlOutcomes = response['result'],
        (error: any) => {
          this.errorText = error;
        });
    // this._masterService.GetddlValues(41)
    //  .subscribe(
    //    (response: GetddlTypelistValues[]) => this.getddlPDAICDCode= response['result'],
    //    (error: any) => {
    //      this.errorText = error;
    //     });

    this._caseManagment.GetPrimaryICD10Code()
      .subscribe(
        (response: any[]) => this.getddlPDAICDCode = response['result'],
        (error: any) => {
          this.errorText = error;
        });
    //23_07_2020
    // this._caseManagment.GetddlClientContact(this.ClientId)
    // .subscribe((response: GetddlClientContact[]) => this.getddlClientContact = response['result'],
    //   (error: any) => {
    //     this.errorText = error;
    //   });

    //this.sixththFormGroup.patchValue({ TimeSpent: '00:00'});
    this.GetAssessorddl();

    if (this.HcbReferenceId && this.ActionName == 'Reopen') {

      this.dialogTitle = "Reopen";
      this.firstFormGroup = this._formBuilder.group({
        HCBReferenceNo: [''],
        HCBReceivedDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        ClientID: [null, Validators.required],
        ClientContactId: [null],
        CaseMgrId: [null],
        DateReferredToCaseOwner: ['', AppCustomDirective.fromDateValidator],
        ServiceType: [null, Validators.required],
        Telephone: [''],
        EmailAddress: ['', Validators.email],
        ContactEmailId: [''],
        ContactTele: [''],
        ReferrerName: ['', Validators.required],
        ReferrerEmail: ['', Validators.required],
        ReferrerTele: ['', Validators.required],
        ClaimantFirstName: ['', Validators.required],
        ClaimantLastName: ['', Validators.required],
        ClaimantDOB: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        ClaimantGender: [null],
        ClaimantAddress1: [''],
        ClaimantAddress2: [''],
        ClaimantEmail: ['', Validators.email],
        ClaimantPhone: ['', Validators.maxLength],
        ClaimantMobile: ['', Validators.required],
        ClaimantServiceReq: [null, Validators.required],
        ClaimantCity: [''],
        ClaimantCounty: [''],
        ClaimantPostcode: ['', Validators.required],
        OccupationalClassification: [''],
        ClaimantOccupation: [''],
        EmployerName: [''],
        EmployerAddress: [''],
        EmployerOccupation: [''],
        EmployerContact: ['', Validators.maxLength],
        EmployerEmail: ['', Validators.email],
        // EmployerPhoneNo: ['', Validators.maxLength],
        EmployerTeleNo: ['', Validators.maxLength],
        EmployerContact1: ['', Validators.maxLength],
        EmployerContact2: ['', Validators.maxLength],
        ClientClaimRef: [''],
        DisabilityCategoryId: [''],
        DisabilityDescription: [''],
        IncapacityDefination: [null],
        DeferredPeriod: [null],
        EndOfBenefitDate: ['', AppCustomDirective.fromDateValidator],
        MonthlyBenefit: [0],
        SchemeId: [null],
        IsEligibleforEAP: [false],
        SchemeNumer: [''],
        FirstAbsentDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        IllnessInjuryId: [null],
        DisabilityType: [''],
        ClaimClosedDate: ['', AppCustomDirective.fromDateValidator],
        ClaimClosedReasonId: [null],
        ClaimDetails: [''],
        ClientRefEmail: [''],
        ClientRefTelephone: [''],
        DateFirstContactApplicant: ['', AppCustomDirective.fromDateValidator],
        DateClaimFormSent: ['', AppCustomDirective.fromDateValidator],
        SubsequentActivity: [''],
        ServiceCaseClosed: ['', AppCustomDirective.fromDateValidator],
        PDAID: [null],  //Validators.required
        SecondaryPDAID:[null],
        Outcome: [''],
        ReasonForDeclinature: [''],
        NameOfTrust: [''],
        LiveCoveredNumber: [''],
        ReportingIntervals: [''],
        SubmitedTraceSmart: ['', AppCustomDirective.fromDateValidator],
        DataReturned: ['', AppCustomDirective.fromDateValidator],
        PositiveTraceAge: [''],
        ReportSubmitted: ['', AppCustomDirective.fromDateValidator],
        DateProjectClosed: ['', AppCustomDirective.fromDateValidator],
        PensionSchmName: [''],
        OverallTraces: [''],
        // AnnualReportDate: ['', AppCustomDirective.fromDateValidator],
        HCBAppointedDate: ['', AppCustomDirective.fromDateValidator],
        Assessor: [''],
        AssessorRate: [''],
        ClientRate: [''],
        ContractStartDate: ['', AppCustomDirective.fromDateValidator],
        ContractEndDate: ['', AppCustomDirective.fromDateValidator],
        SubmitedToProvider: ['', AppCustomDirective.fromDateValidator],
        DateofAssessment: ['', AppCustomDirective.fromDateValidator],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator],
        ReportReceived: ['', AppCustomDirective.fromDateValidator],
        ReportClient: ['', AppCustomDirective.fromDateValidator],
        ServiceProviderId: [''],
        ServiceRequestedId: [''],
        TimeLag: [''],
        ClaimStatusId: [16087],
        ClaimStatusDate: [''],
        PDICDCode: ['']
      });

      this.secondFormGroup = this._formBuilder.group({
        Note: [''],
        SearchHCBNoteList: ['']
      });

      this.thirdFormGroup = this._formBuilder.group({
        ClaimantFirstContactedDate: ['', AppCustomDirective.fromDateValidator],
        FirstVerbalContactClaimantDate: ['', AppCustomDirective.fromDateValidator],
        TypeOfContactId: [null],
        TypeOfContactDetails: [''],
        NextReviewDate: ['', AppCustomDirective.fromDateValidator],
        Date: [''],
        Time: [''],
        TypeOfCall: [''],
        FailedCalls: [''],
        FeeCharged: [''],
        ExpectedOptimumRTWDate: [''],
        ExpectedMaximumRTWDate: [''],
        CaseManagementRecommended: [''],
        caseManagementApproved: [''],
        FundLimit: [''],
        CurrentCaseTotal: [''],
        FundCurrentCaseTotal: [''],
        SubmitedToProvider: ['', AppCustomDirective.fromDateValidator],
        DateofAssessment: ['', AppCustomDirective.fromDateValidator],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator],
        ReportReceived: ['', AppCustomDirective.fromDateValidator],
        ReportClient: ['', AppCustomDirective.fromDateValidator],
        ServiceProviderId: [''],
        ServiceRequestedId: [''],
        ClaimantTitle: [''],


      });

      this.fourthFormGroup = this._formBuilder.group({
        SentToNurseDate: [''],
        DateCancelled: [''],
        ReturnToWorkDate: ['', AppCustomDirective.fromDateValidator],
        ReturnToWorkReason: [null],
        CaseClosedDate: ['', AppCustomDirective.fromDateValidator],
        CaseClosedReasonId: [null],
        PDAID: [null],
        SecondaryPDAID: [null],
        OutcomesId: [''],
        HCBReceivedDate: [],
        fourthClaimStatusId: [16087], //19053 local
      });

      this.sixththFormGroup = this._formBuilder.group({
        FeeCharged: [0],
        TimeSpent: [],
        FeesPayable: [''],    //21_07_2020
        RehabCost: [0],
        TotalCaseCosts: [0]

      });
      this.fifthFormGroup = this._formBuilder.group({
        DocumentName: ['']
      });

      this.DisableService = true;
      //   this.editable = false;
      this.disableVisitButton = false;//14_08
      this.disablemailButton = true;
      // this.GetHCBVisitActivityGrid();
      //  this.GetTelephoneActivityGrid();
      //  this.GetAnnualReportSummaryGrid();
      //   this.GetHCBDocumentGrid(this.HcbReferenceId)
      //   this.loadHCBNotes();
      //   this.GetSubsequentActivityGrid();
      this.SetValuesToReopen();




      // [ Validators.required, Validators.composeAsync([this.createvalidators(this._commonService)])]

    }

    if (this.HcbReferenceId && this.ActionName != 'Reopen') {

      this.dialogTitle = "Edit";
      this.firstFormGroup = this._formBuilder.group({
        HCBReferenceNo: [''],
        HCBReceivedDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        ClientID: [null, Validators.required],
        ClientContactId: [null],
        CaseMgrId: [null],
        DateReferredToCaseOwner: ['', AppCustomDirective.fromDateValidator],
        ServiceType: [null, Validators.required],
        Telephone: [''],
        EmailAddress: ['', Validators.email],
        ContactEmailId: [''],
        ContactTele: [''],
        ReferrerName: ['', Validators.required],
        ReferrerEmail: ['', Validators.required],
        ReferrerTele: ['', Validators.required],
        ClaimantFirstName: ['', Validators.required],
        ClaimantLastName: ['', Validators.required],
        ClaimantDOB: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        ClaimantGender: [null],
        ClaimantAddress1: [''],
        ClaimantAddress2: [''],
        ClaimantEmail: ['', Validators.email],
        ClaimantPhone: ['', Validators.maxLength],
        ClaimantMobile: ['', Validators.required],
        ClaimantServiceReq: [null, Validators.required],
        ClaimantCity: [''],
        ClaimantCounty: [''],
        ClaimantPostcode: ['', Validators.required],
        OccupationalClassification: [''],
        ClaimantOccupation: [''],
        EmployerName: [''],
        EmployerAddress: [''],
        EmployerOccupation: [''],
        EmployerContact: ['', Validators.maxLength],
        EmployerEmail: ['', Validators.email],
        // EmployerPhoneNo: ['', Validators.maxLength],
        EmployerTeleNo: ['', Validators.maxLength],
        EmployerContact1: ['', Validators.maxLength],
        EmployerContact2: ['', Validators.maxLength],
        ClientClaimRef: [''],
        DisabilityCategoryId: [''],
        DisabilityDescription: [''],
        IncapacityDefination: [null],
        DeferredPeriod: [null],
        EndOfBenefitDate: ['', AppCustomDirective.fromDateValidator],
        MonthlyBenefit: [0],
        SchemeId: [null],
        IsEligibleforEAP: [false],
        SchemeNumer: [''],
        FirstAbsentDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        IllnessInjuryId: [null],
        DisabilityType: [''],
        ClaimClosedDate: ['', AppCustomDirective.fromDateValidator],
        ClaimClosedReasonId: [null],
        ClaimDetails: [''],
        ClientRefEmail: [''],
        ClientRefTelephone: [''],
        DateFirstContactApplicant: ['', AppCustomDirective.fromDateValidator],
        DateClaimFormSent: ['', AppCustomDirective.fromDateValidator],
        SubsequentActivity: [''],
        ServiceCaseClosed: ['', AppCustomDirective.fromDateValidator],
        PDAID: [null],
        SecondaryPDAID:[null],
        Outcome: [''],
        ReasonForDeclinature: [''],
        NameOfTrust: [''],
        LiveCoveredNumber: [''],
        ReportingIntervals: [''],
        SubmitedTraceSmart: ['', AppCustomDirective.fromDateValidator],
        DataReturned: ['', AppCustomDirective.fromDateValidator],
        PositiveTraceAge: [''],
        ReportSubmitted: ['', AppCustomDirective.fromDateValidator],
        DateProjectClosed: ['', AppCustomDirective.fromDateValidator],
        PensionSchmName: [''],
        OverallTraces: [''],
        // AnnualReportDate: ['', AppCustomDirective.fromDateValidator],
        HCBAppointedDate: ['', AppCustomDirective.fromDateValidator],
        Assessor: [''],
        AssessorRate: [''],
        ClientRate: [''],
        ContractStartDate: ['', AppCustomDirective.fromDateValidator],
        ContractEndDate: ['', AppCustomDirective.fromDateValidator],
        SubmitedToProvider: ['', AppCustomDirective.fromDateValidator],
        DateofAssessment: ['', AppCustomDirective.fromDateValidator],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator],
        ReportReceived: ['', AppCustomDirective.fromDateValidator],
        ReportClient: ['', AppCustomDirective.fromDateValidator],
        ServiceProviderId: [''],
        ServiceRequestedId: [''],
        ClaimantTitle: [''],
        TimeLag: [''],
        ClaimStatusId: ['', Validators.required],
        ClaimStatusDate: [''],
        PDICDCode: ['']
      });

      this.secondFormGroup = this._formBuilder.group({
        Note: [''],
        SearchHCBNoteList: ['']
      });

      this.thirdFormGroup = this._formBuilder.group({
        ClaimantFirstContactedDate: ['', AppCustomDirective.fromDateValidator],
        FirstVerbalContactClaimantDate: ['', AppCustomDirective.fromDateValidator],
        TypeOfContactId: [null],
        TypeOfContactDetails: [''],
        NextReviewDate: ['', AppCustomDirective.fromDateValidator],
        Date: [''],
        Time: [''],
        TypeOfCall: [''],
        FailedCalls: [''],
        FeeCharged: [''],
        ExpectedOptimumRTWDate: [''],
        ExpectedMaximumRTWDate: [''],
        CaseManagementRecommended: [''],
        caseManagementApproved: [''],
        FundLimit: [''],
        CurrentCaseTotal: [''],
        FundCurrentCaseTotal: [''],
        SubmitedToProvider: ['', AppCustomDirective.fromDateValidator],
        DateofAssessment: ['', AppCustomDirective.fromDateValidator],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator],
        ReportReceived: ['', AppCustomDirective.fromDateValidator],
        ReportClient: ['', AppCustomDirective.fromDateValidator],
        ServiceProviderId: [''],
        ServiceRequestedId: [''],

      });

      this.fourthFormGroup = this._formBuilder.group({
        SentToNurseDate: [''],
        DateCancelled: ['', AppCustomDirective.fromDateValidator],
        ReturnToWorkDate: ['', AppCustomDirective.fromDateValidator],
        ReturnToWorkReason: [null],
        CaseClosedDate: ['', AppCustomDirective.fromDateValidator],
        CaseClosedReasonId: [null],
        PDAID: [null],
        SecondaryPDAID:[null],
        OutcomesId: [''],
        HCBReceivedDate: [],
        fourthClaimStatusId: ['', Validators.required],
      });

      this.sixththFormGroup = this._formBuilder.group({
        FeeCharged: [0],
        TimeSpent: [],
        FeesPayable: [''],    //21_07_2020
        RehabCost: [0],
        TotalCaseCosts: [0]

      });
      this.fifthFormGroup = this._formBuilder.group({
        DocumentName: ['']
      });

      this.DisableService = true;
      //   this.editable = false;
      this.disableVisitButton = true;
      this.disablemailButton = false;
      this.GetHCBVisitActivityGrid();
      this.GetTelephoneActivityGrid();
      this.GetAnnualReportSummaryGrid();
      this.GetHCBDocumentGrid(this.HcbReferenceId)
      this.loadHCBNotes();
      this.GetSubsequentActivityGrid();
      this.SetValues();




      // [ Validators.required, Validators.composeAsync([this.createvalidators(this._commonService)])]

    }
    if (this.HcbReferenceId == 0) {

      this.dialogTitle = "Add";
      this.firstFormGroup = this._formBuilder.group({
        HCBReferenceNo: [''],
        HCBReceivedDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        ClientID: [null, Validators.required],
        ClientContactId: [null],
        CaseMgrId: [null],
        DateReferredToCaseOwner: ['', AppCustomDirective.fromDateValidator],
        ServiceType: [null, Validators.required],
        Telephone: [''],
        EmailAddress: ['', Validators.email],
        ContactEmailId: [''],
        ContactTele: [''],
        ReferrerName: ['', Validators.required],
        ReferrerEmail: ['', Validators.required],
        ReferrerTele: ['', Validators.required],
        ClaimantFirstName: ['', Validators.required],
        ClaimantLastName: ['', Validators.required],
        ClaimantDOB: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        ClaimantGender: [null],
        ClaimantAddress1: [''],
        ClaimantAddress2: [''],
        ClaimantEmail: [null, Validators.email],
        ClaimantPhone: ['', Validators.maxLength],
        ClaimantMobile: ['', Validators.required],
        ClaimantServiceReq: [null, Validators.required],
        ClaimantCity: [''],
        ClaimantCounty: [''],
        ClaimantPostcode: ['', Validators.required],
        OccupationalClassification: [''],
        ClaimantOccupation: [''],
        EmployerName: [''],
        EmployerAddress: [''],
        EmployerOccupation: [''],
        EmployerContact: ['', Validators.maxLength],
        EmployerEmail: ['', Validators.email],
        //EmployerPhoneNo: ['', Validators.maxLength],
        EmployerTeleNo: ['', Validators.maxLength],
        EmployerContact1: ['', Validators.maxLength],
        EmployerContact2: ['', Validators.maxLength],
        ClientClaimRef: [''],
        DisabilityCategoryId: [''],
        DisabilityDescription: [''],
        IncapacityDefination: [null],
        DeferredPeriod: [null],
        EndOfBenefitDate: ['', AppCustomDirective.fromDateValidator],
        MonthlyBenefit: [0],
        SchemeId: [null],
        IsEligibleforEAP: [false],
        SchemeNumer: [''],
        FirstAbsentDate: ['', [Validators.required, AppCustomDirective.fromDateValidator]],
        IllnessInjuryId: [null],
        DisabilityType: [''],
        ClaimClosedDate: ['', AppCustomDirective.fromDateValidator],
        ClaimClosedReasonId: [null],
        ClaimDetails: [''],
        ClientRefEmail: [''],
        ClientRefTelephone: [''],
        DateFirstContactApplicant: ['', AppCustomDirective.fromDateValidator],
        DateClaimFormSent: ['', AppCustomDirective.fromDateValidator],
        SubsequentActivity: [''],
        ServiceCaseClosed: ['', AppCustomDirective.fromDateValidator],
        PDAID: [null],
        SecondaryPDAID:[null],
        Outcome: [''],
        ReasonForDeclinature: [''],
        NameOfTrust: [''],
        LiveCoveredNumber: [''],
        ReportingIntervals: [''],

        SubmitedTraceSmart: ['', AppCustomDirective.fromDateValidator],
        DataReturned: ['', AppCustomDirective.fromDateValidator],
        PositiveTraceAge: [''],
        ReportSubmitted: ['', AppCustomDirective.fromDateValidator],
        DateProjectClosed: ['', AppCustomDirective.fromDateValidator],
        PensionSchmName: [''],      //07/07/020
        OverallTraces: [''],
        //  AnnualReportDate: ['', AppCustomDirective.fromDateValidator],
        HCBAppointedDate: ['', AppCustomDirective.fromDateValidator],
        Assessor: [''],
        AssessorRate: [''],
        ClientRate: [''],
        ContractStartDate: ['', AppCustomDirective.fromDateValidator],
        ContractEndDate: ['', AppCustomDirective.fromDateValidator],
        SubmitedToProvider: ['', AppCustomDirective.fromDateValidator],
        DateofAssessment: ['', AppCustomDirective.fromDateValidator],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator],
        ReportReceived: ['', AppCustomDirective.fromDateValidator],
        ReportClient: ['', AppCustomDirective.fromDateValidator],
        ServiceProviderId: [''],
        ServiceRequestedId: [''],
        ClaimantTitle: [''],
        // ClaimantEmail: [null, Validators.email],
        TimeLag: [''],
        ClaimStatusId: [16087],
        ClaimStatusDate: [''],
        PDICDCode: ['']
      });
      this.secondFormGroup = this._formBuilder.group({
        Note: [''],
        SearchHCBNoteList: ['']
      });
      this.thirdFormGroup = this._formBuilder.group({
        ClaimantFirstContactedDate: ['', AppCustomDirective.fromDateValidator],
        FirstVerbalContactClaimantDate: ['', AppCustomDirective.fromDateValidator],
        TypeOfContactId: [null],
        TypeOfContactDetails: [''],
        NextReviewDate: ['', AppCustomDirective.fromDateValidator],
        Date: [''],
        Time: [''],
        TypeOfCall: [''],
        FailedCalls: [''],
        FeeCharged: [''],

        ExpectedOptimumRTWDate: [''],
        ExpectedMaximumRTWDate: [''],
        CaseManagementRecommended: [''],
        caseManagementApproved: [''],
        FundLimit: [''],
        CurrentCaseTotal: [''],
        FundCurrentCaseTotal: [''],
        SubmitedToProvider: ['', AppCustomDirective.fromDateValidator],
        DateofAssessment: ['', AppCustomDirective.fromDateValidator],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator],
        ReportReceived: ['', AppCustomDirective.fromDateValidator],
        ReportClient: ['', AppCustomDirective.fromDateValidator],
        ServiceProviderId: [''],
        ServiceRequestedId: [''],

      });

      this.fourthFormGroup = this._formBuilder.group({
        SentToNurseDate: [''],
        DateCancelled: ['', AppCustomDirective.fromDateValidator],
        ReturnToWorkDate: ['', AppCustomDirective.fromDateValidator],
        ReturnToWorkReason: [null],
        CaseClosedDate: ['', AppCustomDirective.fromDateValidator],
        CaseClosedReasonId: [null],
        PDAID: [null],
        SecondaryPDAID:[null],
        OutcomesId: [''],
        HCBReceivedDate: [],
        fourthClaimStatusId: [16087],
      });

      this.sixththFormGroup = this._formBuilder.group({
        FeeCharged: [0],
        TimeSpent: [],
        FeesPayable: [''],   //21_07_2020
        RehabCost: [0],
        TotalCaseCosts: [0]

      });

      this.fifthFormGroup = this._formBuilder.group({
        DocumentName: ['']
      });
      //this.editable = true;
      this.ActionName = 'Add';
      this.disablemailButton = true;
      this.CMUserdisableDOCButton=false;
      this.disableDOCButton = false;
      this.Displayheader = false;
      // this.thirdFormGroup.get('TimeSpent').value =
      this.Set();

    }
    this.DisableNoteDelete();
    this.EnableActivityTab();
  }

  ngOnDestroy() {

    this.sub.unsubscribe();
  }

  // createvalidators(_commonService: CommonService) {
  //   return control => new Promise((resolve: any) => {
  //     resolve(null);
  //     this.message = control.value;
  //     //  console.log(this.message);
  //     const d = control.value._i.split("/");

  //     if ((this.da._i.month)) {
  //       resolve(null);
  //     }
  //     else {
  //       resolve({ dateformat: true });
  //     }

  //     if (d[0].length !== 2 || d[1].length !== 2 || d[1] > 12 || d[2].length !== 4) {
  //       this.valid = "Please Enter Date in 'DD/MM/YYYY' format";
  //     }
  //     else {
  //       resolve(null);
  //     }
  //   })
  // }
  EnableActivityTab(){
    if((this.UserTypeId==1) || (this.UserTypeId==2))
    {
      if(this.CanWorkAsCM==1)
      {
      this.thirdFormGroup.enable();
      this.disableVisitButton=true;
      }
      else{
        this.thirdFormGroup.disable();
        this.disableVisitButton=false;
      }
    }
    else{
      this.thirdFormGroup.enable();
      this.disableVisitButton=true;
    }
  }

  DisableNoteDelete() {  

    if (this.UserTypeId == 2) {  //only Supper User Can Delete Case Note
      this.ShowDeleteButton = false;
    } else {
      this.ShowDeleteButton = true;
    }
  }

  DateAssesment() {
    // var AssDate = this.firstFormGroup.value.DateofAssessment;
    // if (AssDate != null && AssDate != undefined && AssDate != "") {
    //   var date = new Date(AssDate);
    //   console.log(date);
    //   var newdate = new Date(date);
    //   console.log(newdate);

    //   newdate.setDate(newdate.getDate() + 14);

    //   var dd = newdate.getDate();
    //   var mm = newdate.getMonth() + 1;
    //   var y = newdate.getFullYear();

    //   var someFormattedDate = y + '-' + mm + '-' + dd;
    //   // document.getElementById('follow_Date').value = someFormattedDate;

    //   //var date=(this.firstFormGroup.value.DateofAssessment == "" || this.firstFormGroup.value.DateofAssessment == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateofAssessment) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateofAssessment) + 1) + "-" + (this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateofAssessment)+14);

    //   this.firstFormGroup.patchValue({ ReportDueBy: moment({ year: this._commonService.GetYearFromDateString(someFormattedDate), month: this._commonService.GetMonthFromDateString(someFormattedDate), date: this._commonService.GetDateFromDateString(someFormattedDate) }) })
    // }
    // else {
    //   this.firstFormGroup.patchValue({ ReportDueBy: null })
    // }

    var AssDate;
    if (this.firstFormGroup.value.ServiceType < 5 || this.firstFormGroup.value.ServiceType == 11) {
      AssDate = this.thirdFormGroup.value.DateofAssessment;
    } else {
      AssDate = this.firstFormGroup.value.DateofAssessment;
    }
    if (AssDate != null && AssDate != undefined && AssDate != "") {
      var date = new Date(AssDate);
      console.log(date);
      var newdate = new Date(date);
      console.log(newdate);

      newdate.setDate(newdate.getDate() + 14);

      var dd = newdate.getDate();
      var mm = newdate.getMonth() + 1;
      var y = newdate.getFullYear();

      var someFormattedDate = y + '-' + mm + '-' + dd;
      // document.getElementById('follow_Date').value = someFormattedDate;

      //var date=(this.firstFormGroup.value.DateofAssessment == "" || this.firstFormGroup.value.DateofAssessment == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateofAssessment) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateofAssessment) + 1) + "-" + (this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateofAssessment)+14);
      this.thirdFormGroup.patchValue({ ReportDueBy: moment({ year: this._commonService.GetYearFromDateString(someFormattedDate), month: this._commonService.GetMonthFromDateString(someFormattedDate), date: this._commonService.GetDateFromDateString(someFormattedDate) }) })

      this.firstFormGroup.patchValue({ ReportDueBy: moment({ year: this._commonService.GetYearFromDateString(someFormattedDate), month: this._commonService.GetMonthFromDateString(someFormattedDate), date: this._commonService.GetDateFromDateString(someFormattedDate) }) })
    }
    else {
      this.thirdFormGroup.patchValue({ ReportDueBy: null })

      this.firstFormGroup.patchValue({ ReportDueBy: null })
    }
  }

  TimeLagInDFAandReceiveDate() {

    var Date1;
    var Date2;
    var result;
    Date1 = this.firstFormGroup.value.HCBReceivedDate;
    Date2 = this.firstFormGroup.value.FirstAbsentDate;
    if (Date2 != null) {
      result = Math.ceil(Math.abs(Date2 - Date1) / (1000 * 60 * 60 * 24));
      console.log(result);
      this.firstFormGroup.patchValue({ TimeLag: result });
    } else { }
  }

  Set(): void {
    var today = new Date();
    this.firstFormGroup.patchValue({ HCBReceivedDate: moment({ year: this._commonService.GetYearFromDateString(today.toString()), month: this._commonService.GetMonthFromDateString(today.toString()), date: this._commonService.GetDateFromDateString(today.toString()) }) });
    if (this.UserTypeId == 4) {
      this.firstFormGroup.patchValue({ ClientID: this.IncoClientId });
      this.ddlDisabled = true;
    }
    else {
      this.ddlDisabled = false;
    }
  }

  GetClient(): void {

    this._caseManagment.GetddlClient()
      .subscribe((response: GetddlClientModel[]) => {

        this.getddlClient = response['result'];
        for (var i = 0; i < response['result'].length; i++) {
          if (this.clintID == response['result'][i].clientID) {
            this.CompanyName = response['result'][i].companyName;
          }
        }
        // this.onClientChange();
      },
        (error: any) => {
          this.errorText = error;
        });
  }
  GetCaseOwner(): void {
    this._caseManagment.GetddlCaseManager()
      .subscribe((response: GetddlCasemanagmentModel[]) => {
        this.getddlCaseOwner = response['result'];
        this.onCaseOwnerChange();
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  onPDAChange() {

    this.PDAvalue = false;
    if (this.firstFormGroup.get('ServiceType').value == 5) {
      this.fourthFormGroup.value.PDAID = this.firstFormGroup.get('PDAID').value;
      this.fourthFormGroup.value.SecondaryPDAID = this.firstFormGroup.get('SecondaryPDAID').value;
    }
  }

  SelectPDA() {
if(this.closed_Date ==null){
    if ((this.fourthFormGroup.get('CaseClosedDate').value != "") && (this.fourthFormGroup.get('CaseClosedDate').value != null)) {
      if ((this.fourthFormGroup.get('PDAID').value == "") || (this.fourthFormGroup.get('PDAID').value == undefined)) {
        this.PDAvalue =false;// true;
      //  swal.fire("Please complete the Principal Determinant of Absence field below, taking care to make sure that the selection accurately reflects the actual case of absence from work, and not necessarily what was the reported cause of absence");
     

      }else { this.PDAvalue = false; } 
    } else { this.PDAvalue = false; }
   } else { this.PDAvalue = false; }
    this.CheckForFirstFormGroup();
  }

  CheckForFirstFormGroup() {

    if (this.firstFormGroup.get('ServiceType').value == 5) {
      if(this.closed_Date ==null){
      if ((this.firstFormGroup.get('ServiceCaseClosed').value != "") && (this.firstFormGroup.get('ServiceCaseClosed').value != null)) {
        if ((this.firstFormGroup.get('PDAID').value == "") || (this.firstFormGroup.get('PDAID').value == undefined)) {
          this.PDAvalue =false;// true;
        //  swal.fire("Please complete the Principal Determinant of Absence field below, taking care to make sure that the selection accurately reflects the actual case of absence from work, and not necessarily what was the reported cause of absence");

          // swal.fire({title:'',
          //   text:"<span style='color:#F8BB86'>Please complete the Principal Determinant of Absence field below, taking care to make sure that the selection accurately reflects the actual case of absence from work, and not necessarily what was the reported cause of absence</span>"
          // ,html:true});

        }else { this.PDAvalue = false; }
      }else { this.PDAvalue = false; }
      } else { this.PDAvalue = false; }
    } else {}

  }
  scrollToTop(speed?: number): void {
    speed = speed || 10;
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToTop(0, speed);
      });
    }
  }

  SetValuesToReopen(): void {


    this.working = true;
    var today = new Date();
    this._caseManagment.GetCaseInfoById(this.HcbReferenceId)
      .subscribe((response: GetCaseManagmentModel[]) => {
debugger;
        // if(response['result'][0].serviceType <5)

        //  this.HCBReferenceNo=response['result'][0].hcbReferenceNo;
        this.HCBReferenceNo = response['result'][0].hcbReferenceNo;
        this.OldRefId = response['result'][0].hcbReferenceNo;
        
        this.OldCaseId = response['result'][0].hcbReferenceNo.split('-')[1];
      
        this.firstFormGroup.patchValue({ HCBReferenceNo: '' });
        // if (response['result'][0].hcbReceivedDate != "" && response['result'][0].hcbReceivedDate != null)
        this.firstFormGroup.patchValue({ HCBReceivedDate: moment({ year: this._commonService.GetYearFromDateString(today.toString()), month: this._commonService.GetMonthFromDateString(today.toString()), date: this._commonService.GetDateFromDateString(today.toString()) }) });
        this.firstFormGroup.patchValue({ ClientID: response['result'][0].clientID });
        this.clintID = response['result'][0].clientID;
        this.GetClient();
        this.onClientChangeForEdit();
        this.firstFormGroup.patchValue({ ServiceType: response['result'][0].serviceType });
        this.onServiceChange();
        this.firstFormGroup.patchValue({ CaseMgrId: response['result'][0].caseMgrId });
        this.onCaseOwnerChange();

        this.firstFormGroup.patchValue({ ClientContactId: response['result'][0].clientContactId });
        //  if (response['result'][0].clientContactId > 0) {
        //   var ClientContactId =response['result'][0].clientContactId;
        //   var Telephone = this.getddlClientContact.find(x => x.clientReferralContactsId == ClientContactId).phoneNo;
        //   var email = this.getddlClientContact.find(x => x.clientReferralContactsId === ClientContactId).contactEmail;
        //   this.firstFormGroup.patchValue({ ContactTele: Telephone });
        //   this.firstFormGroup.patchValue({ ContactEmailId: email });
     
        //       }
        // if (response['result'][0].dateReferredToCaseOwner != "" && response['result'][0].dateReferredToCaseOwner != null) {
  
        // this.Telephone = this.getddlCaseOwner.find(x => x.userID === this.CaseOwnerId).telephoneNumber;
        // this.email = this.getddlCaseOwner.find(x => x.userID === this.CaseOwnerId).emailAddress;
        this.firstFormGroup.patchValue({ ClaimantTitle: response['result'][0].title });

        this.firstFormGroup.patchValue({ Telephone: this.Telephone });
        this.firstFormGroup.patchValue({ EmailAddress: this.email });
        this.firstFormGroup.patchValue({ ReferrerName: response['result'][0].referrerName });
        this.firstFormGroup.patchValue({ ReferrerEmail: response['result'][0].referrerEmail });
        this.firstFormGroup.patchValue({ ReferrerTele: response['result'][0].referrerTele });
        // this.ExistingFirstName = response['result'][0].claimantFirstName;
        // this.ExistingLastName = response['result'][0].claimantLastName;
        this.DOB = response['result'][0].claimantDOB;
        this.ClaimantLastName = response['result'][0].claimantLastName;
        this.ClaimantFirstName = response['result'][0].claimantFirstName;

        if (response['result'][0].claimantDOB != null) {
          this.ExistingBirthDate = ((this._commonService.GetYearFromDateString(response['result'][0].claimantDOB)).toString() + "-" + (this._commonService.GetMonthFromDateString(response['result'][0].claimantDOB) + 1).toString() + "-" + (this._commonService.GetDateFromDateString(response['result'][0].claimantDOB)).toString() + " 00:00:00:000");
          var year = this._commonService.GetYearFromDateString(response['result'][0].claimantDOB).toString();
          var month = this._commonService.GetMonthFromDateString(response['result'][0].claimantDOB).toString() + 1;
          var month = this._commonService.GetDateFromDateString(response['result'][0].claimantDOB).toString();

        }
        else {
          this.ExistingBirthDate = response['result'][0].claimantDOB;
        }


        this.firstFormGroup.patchValue({ ClaimantFirstName: response['result'][0].claimantFirstName });
        this.firstFormGroup.patchValue({ ClaimantLastName: response['result'][0].claimantLastName });
        if (response['result'][0].claimantDOB != "" && response['result'][0].claimantDOB != null)
          this.firstFormGroup.patchValue({ ClaimantDOB: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].claimantDOB), month: this._commonService.GetMonthFromDateString(response['result'][0].claimantDOB), date: this._commonService.GetDateFromDateString(response['result'][0].claimantDOB) }) });
        this.firstFormGroup.patchValue({ ClaimantGender: response['result'][0].claimantGender });
        this.firstFormGroup.patchValue({ ClaimantAddress1: response['result'][0].claimantAddress1 });
        this.firstFormGroup.patchValue({ ClaimantAddress2: response['result'][0].claimantAddress2 });
        this.firstFormGroup.patchValue({ ClaimantPhone: response['result'][0].claimantPhone });
        this.firstFormGroup.patchValue({ ClaimantMobile: response['result'][0].claimantMobile });
        this.firstFormGroup.patchValue({ ClaimantServiceReq: response['result'][0].claimantServiceReq });
        // this.firstFormGroup.patchValue({ ClaimantOccupation: response['result'][0].claimantOccupation });
        this.firstFormGroup.patchValue({ ClaimantCity: response['result'][0].claimantCity });
        this.firstFormGroup.patchValue({ ClaimantCounty: response['result'][0].claimantCounty });
        this.firstFormGroup.patchValue({ OccupationalClassification: response['result'][0].occupationalClassification });
        this.firstFormGroup.patchValue({ EmployerName: response['result'][0].employerName });
        this.firstFormGroup.patchValue({ EmployerContact: response['result'][0].employerContact });
        this.firstFormGroup.patchValue({ EmployerEmail: response['result'][0].employerEmail });
        this.firstFormGroup.patchValue({ EmployerTeleNo: response['result'][0].employerTeleNo });
        this.firstFormGroup.patchValue({ EmployerContact1: response['result'][0].employerContact1 });
        this.firstFormGroup.patchValue({ EmployerContact2: response['result'][0].employerContact2 });
        // this.firstFormGroup.patchValue({ ClientClaimRef: response['result'][0].clientClaimRef });
        // this.firstFormGroup.patchValue({ IncapacityDefination: response['result'][0].incapacityDefination });
        // this.firstFormGroup.patchValue({ SchemeId: response['result'][0].schemeId });
        this.firstFormGroup.patchValue({ ClaimantPostcode: response['result'][0].claimantPostcode });

        // var total = (response['result'][0].feeCharged + response['result'][0].rehabCost)
        // this.sixththFormGroup.patchValue({ TotalCaseCosts: total });
        this.working = false;




      }, (error: any) => {
        this.HcbReferenceId = undefined;
        this.errorText = error;
        this.working = false;
      });
  }

  GetOldCase(x: any) {
debugger;

    // window.open(url,'_blank');
    localStorage.setItem('isLoggedin', "1");
    sessionStorage.setItem('isLoggedin', "1");
    // localStorage.setItem(, JSON.stringify(i));
  }


  SetValues(): void {

    this.working = true;
    var today = new Date();
    this._caseManagment.GetCaseInfoById(this.HcbReferenceId)
      .subscribe((response: GetCaseManagmentModel[]) => {

        this.OldRefId = response['result'][0].old_HCBRefId;
        if (this.OldRefId != null || this.OldRefId != undefined) {
          this.OldCaseId = response['result'][0].old_HCBRefId.split('-')[1];
        }
  
        this.HCBReferenceNo = response['result'][0].hcbReferenceNo;

        this.firstFormGroup.patchValue({ HCBReferenceNo: response['result'][0].hcbReferenceNo });
        if (response['result'][0].hcbReceivedDate != "" && response['result'][0].hcbReceivedDate != null)
          this.firstFormGroup.patchValue({ HCBReceivedDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].hcbReceivedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].hcbReceivedDate), date: this._commonService.GetDateFromDateString(response['result'][0].hcbReceivedDate) }) });
        this.firstFormGroup.patchValue({ ClientID: response['result'][0].clientID });
        this.clintID = response['result'][0].clientID;
        this.GetClient();
        this.onClientChangeForEdit();
        this.firstFormGroup.patchValue({ CaseMgrId: response['result'][0].caseMgrId });
        this.onCaseOwnerChange();

        this.firstFormGroup.patchValue({ ClientContactId: response['result'][0].clientContactId });
        
        if (response['result'][0].dateReferredToCaseOwner != "" && response['result'][0].dateReferredToCaseOwner != null) {
          this.firstFormGroup.patchValue({ DateReferredToCaseOwner: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateReferredToCaseOwner), month: this._commonService.GetMonthFromDateString(response['result'][0].dateReferredToCaseOwner), date: this._commonService.GetDateFromDateString(response['result'][0].dateReferredToCaseOwner) }) });
          this.fourthFormGroup.patchValue({ SentToNurseDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateReferredToCaseOwner), month: this._commonService.GetMonthFromDateString(response['result'][0].dateReferredToCaseOwner), date: this._commonService.GetDateFromDateString(response['result'][0].dateReferredToCaseOwner) }) });

        }
        this.firstFormGroup.patchValue({ ServiceType: response['result'][0].serviceType });
        this.onServiceChange();
        // this.Telephone = this.getddlCaseOwner.find(x => x.userID === this.CaseOwnerId).telephoneNumber;
        // this.email = this.getddlCaseOwner.find(x => x.userID === this.CaseOwnerId).emailAddress;
        this.firstFormGroup.patchValue({ Telephone: this.Telephone });
        this.firstFormGroup.patchValue({ EmailAddress: this.email });
        this.firstFormGroup.patchValue({ ReferrerName: response['result'][0].referrerName });
        this.firstFormGroup.patchValue({ ReferrerEmail: response['result'][0].referrerEmail });
        this.firstFormGroup.patchValue({ ReferrerTele: response['result'][0].referrerTele });
        this.ExistingFirstName = response['result'][0].claimantFirstName;
        this.ExistingLastName = response['result'][0].claimantLastName;

        if (response['result'][0].claimantDOB != null) {
          this.ExistingBirthDate = ((this._commonService.GetYearFromDateString(response['result'][0].claimantDOB)).toString() + "-" + (this._commonService.GetMonthFromDateString(response['result'][0].claimantDOB) + 1).toString() + "-" + (this._commonService.GetDateFromDateString(response['result'][0].claimantDOB)).toString() + " 00:00:00:000");
          var year = this._commonService.GetYearFromDateString(response['result'][0].claimantDOB).toString();
          var month = this._commonService.GetMonthFromDateString(response['result'][0].claimantDOB).toString() + 1;
          var month = this._commonService.GetDateFromDateString(response['result'][0].claimantDOB).toString();

        }
        else {
          this.ExistingBirthDate = response['result'][0].claimantDOB;
        }
        this.DOB = response['result'][0].claimantDOB;
        this.ClaimantLastName = response['result'][0].claimantLastName;
        this.ClaimantFirstName = response['result'][0].claimantFirstName;
        this.firstFormGroup.patchValue({ ClaimantTitle: response['result'][0].title });
        this.firstFormGroup.patchValue({ ClaimantFirstName: response['result'][0].claimantFirstName });
        this.firstFormGroup.patchValue({ ClaimantLastName: response['result'][0].claimantLastName });
        if (response['result'][0].claimantDOB != "" && response['result'][0].claimantDOB != null)
          this.firstFormGroup.patchValue({ ClaimantDOB: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].claimantDOB), month: this._commonService.GetMonthFromDateString(response['result'][0].claimantDOB), date: this._commonService.GetDateFromDateString(response['result'][0].claimantDOB) }) });
        this.firstFormGroup.patchValue({ ClaimantGender: response['result'][0].claimantGender });
        this.firstFormGroup.patchValue({ ClaimantAddress1: response['result'][0].claimantAddress1 });
        this.firstFormGroup.patchValue({ ClaimantAddress2: response['result'][0].claimantAddress2 });
        this.firstFormGroup.patchValue({ ClaimantPhone: response['result'][0].claimantPhone });
        this.firstFormGroup.patchValue({ ClaimantMobile: response['result'][0].claimantMobile });
        this.firstFormGroup.patchValue({ ClaimantServiceReq: response['result'][0].claimantServiceReq });
        this.firstFormGroup.patchValue({ ClaimantOccupation: response['result'][0].claimantOccupation });
        this.firstFormGroup.patchValue({ ClaimantCity: response['result'][0].claimantCity });
        this.firstFormGroup.patchValue({ ClaimantCounty: response['result'][0].claimantCounty });
        this.firstFormGroup.patchValue({ OccupationalClassification: response['result'][0].occupationalClassification });
        this.firstFormGroup.patchValue({ ClaimantEmail: response['result'][0].claimantEmail });

        this.firstFormGroup.patchValue({ EmployerName: response['result'][0].employerName });
        this.firstFormGroup.patchValue({ EmployerContact: response['result'][0].employerContact });
        this.firstFormGroup.patchValue({ EmployerEmail: response['result'][0].employerEmail });
        this.firstFormGroup.patchValue({ EmployerTeleNo: response['result'][0].employerTeleNo });
        this.firstFormGroup.patchValue({ EmployerContact1: response['result'][0].employerContact1 });
        this.firstFormGroup.patchValue({ EmployerContact2: response['result'][0].employerContact2 });
        this.firstFormGroup.patchValue({ EmployerAddress: response['result'][0].employerAddress });
        this.firstFormGroup.patchValue({ EmployerOccupation: response['result'][0].employerOccupation });

        this.firstFormGroup.patchValue({ ClientClaimRef: response['result'][0].clientClaimRef });
        this.firstFormGroup.patchValue({ DisabilityCategoryId: response['result'][0].disabilityCategoryId });
        this.firstFormGroup.patchValue({ DisabilityDescription: response['result'][0].disabilityDescription });
        this.firstFormGroup.patchValue({ TimeLag: response['result'][0].timeLag });
        this.firstFormGroup.patchValue({ ClaimStatusId: response['result'][0].claimStatusId });
        
        this.firstFormGroup.patchValue({ PDICDCode: response['result'][0].icD10CodeID });

        if (response['result'][0].claimStatusDate != "" && response['result'][0].claimStatusDate != null)
          this.firstFormGroup.patchValue({ ClaimStatusDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].claimStatusDate), month: this._commonService.GetMonthFromDateString(response['result'][0].claimStatusDate), date: this._commonService.GetDateFromDateString(response['result'][0].claimStatusDate) }) });

        this.firstFormGroup.patchValue({ IncapacityDefination: response['result'][0].incapacityDefination });
        this.firstFormGroup.patchValue({ SchemeId: response['result'][0].schemeId });
        this.firstFormGroup.patchValue({ ClaimantPostcode: response['result'][0].claimantPostcode });
        this.firstFormGroup.patchValue({ ClientRefEmail: response['result'][0].clientRefEmail });
        this.firstFormGroup.patchValue({ ClientRefTelephone: response['result'][0].clientRefTelephone });
        // this.firstFormGroup.patchValue({})
         this.firstFormGroup.patchValue({ DeferredPeriod: response['result'][0].waitingPeriodId });
        if (response['result'][0].dateFirstContactApplicant != "" && response['result'][0].dateFirstContactApplicant != null)
          this.firstFormGroup.patchValue({ DateFirstContactApplicant: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateFirstContactApplicant), month: this._commonService.GetMonthFromDateString(response['result'][0].dateFirstContactApplicant), date: this._commonService.GetDateFromDateString(response['result'][0].dateFirstContactApplicant) }) });
        this.firstFormGroup.patchValue({ SubsequentActivity: response['result'][0].subsequentActivity });
        this.firstFormGroup.patchValue({ PDAID: response['result'][0].pdaid });
        this.firstFormGroup.patchValue({ SecondaryPDAID: response['result'][0].secondaryPDAID });
        if (response['result'][0].caseClosedDate != "" && response['result'][0].caseClosedDate != null)
          this.firstFormGroup.patchValue({ ServiceCaseClosed: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].caseClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].caseClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].caseClosedDate) }) });
        if (response['result'][0].dateClaimFormSent != "" && response['result'][0].dateClaimFormSent != null)
          this.firstFormGroup.patchValue({ DateClaimFormSent: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateClaimFormSent), month: this._commonService.GetMonthFromDateString(response['result'][0].dateClaimFormSent), date: this._commonService.GetDateFromDateString(response['result'][0].dateClaimFormSent) }) });

        if (response['result'][0].hcbAppointedDate != "" && response['result'][0].hcbAppointedDate != null)
          this.firstFormGroup.patchValue({ HCBAppointedDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].hcbAppointedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].hcbAppointedDate), date: this._commonService.GetDateFromDateString(response['result'][0].hcbAppointedDate) }) });

        this.firstFormGroup.patchValue({ Outcome: response['result'][0].outcome });
        this.onOutcomeChange();
        this.firstFormGroup.patchValue({ ReasonForDeclinature: response['result'][0].reasonForDeclinature });

        if (response['result'][0].endOfBenefitDate != "" && response['result'][0].endOfBenefitDate != null)
          this.firstFormGroup.patchValue({ EndOfBenefitDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].endOfBenefitDate), month: this._commonService.GetMonthFromDateString(response['result'][0].endOfBenefitDate), date: this._commonService.GetDateFromDateString(response['result'][0].endOfBenefitDate) }) });

        this.firstFormGroup.patchValue({ MonthlyBenefit: response['result'][0].monthlyBenefit });
        if (response['result'][0].isEligibleforEAP == true) {
          this.firstFormGroup.patchValue({ IsEligibleforEAP: 1 });
        }
        else {
          this.firstFormGroup.patchValue({ IsEligibleforEAP: 0 });
        }
        this.firstFormGroup.patchValue({ SchemeNumer: response['result'][0].schemeNumer });

        if (response['result'][0].firstAbsentDate != "" && response['result'][0].firstAbsentDate != null)
          this.firstFormGroup.patchValue({ FirstAbsentDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].firstAbsentDate), month: this._commonService.GetMonthFromDateString(response['result'][0].firstAbsentDate), date: this._commonService.GetDateFromDateString(response['result'][0].firstAbsentDate) }) });

        this.firstFormGroup.patchValue({ IllnessInjuryId: response['result'][0].illnessInjuryId });
        this.firstFormGroup.patchValue({ DisabilityType: response['result'][0].disabilityType });

        if (response['result'][0].claimClosedDate != "" && response['result'][0].claimClosedDate != null)
          this.firstFormGroup.patchValue({ ClaimClosedDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].claimClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].claimClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].claimClosedDate) }) });

        this.firstFormGroup.patchValue({ ClaimClosedReasonId: response['result'][0].claimClosedReasonId });
        this.firstFormGroup.patchValue({ ClaimDetails: response['result'][0].claimDetails });

        if (response['result'][0].submitedTraceSmart != "" && response['result'][0].submitedTraceSmart != null)
          this.firstFormGroup.patchValue({ SubmitedTraceSmart: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].submitedTraceSmart), month: this._commonService.GetMonthFromDateString(response['result'][0].submitedTraceSmart), date: this._commonService.GetDateFromDateString(response['result'][0].submitedTraceSmart) }) });

        if (response['result'][0].dataReturned != "" && response['result'][0].dataReturned != null)
          this.firstFormGroup.patchValue({ DataReturned: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dataReturned), month: this._commonService.GetMonthFromDateString(response['result'][0].dataReturned), date: this._commonService.GetDateFromDateString(response['result'][0].dataReturned) }) });

        if (response['result'][0].caseClosedDate != "" && response['result'][0].caseClosedDate != null)
          this.firstFormGroup.patchValue({ DateProjectClosed: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].caseClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].caseClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].caseClosedDate) }) });

        if (response['result'][0].reportSubmitted != "" && response['result'][0].reportSubmitted != null)
          this.firstFormGroup.patchValue({ ReportSubmitted: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].reportSubmitted), month: this._commonService.GetMonthFromDateString(response['result'][0].reportSubmitted), date: this._commonService.GetDateFromDateString(response['result'][0].reportSubmitted) }) });

        this.firstFormGroup.patchValue({ PensionSchmName: response['result'][0].pensionSchemeName });

        this.firstFormGroup.patchValue({ PositiveTraceAge: response['result'][0].positiveTraceAge });
        this.firstFormGroup.patchValue({ OverallTraces: response['result'][0].overallTraces });
        this.firstFormGroup.patchValue({ NameOfTrust: response['result'][0].nameOfTrust });
        this.firstFormGroup.patchValue({ LiveCoveredNumber: response['result'][0].liveCoveredNumber });
        // this.firstFormGroup.patchValue({ AnnualReportOutcome: response['result'][0].annualReportOutcome });
        // this.firstFormGroup.patchValue({ AnnualReportValue: response['result'][0].annualReportValue });
        this.firstFormGroup.patchValue({ ReportingIntervals: response['result'][0].reportingIntervals });

        if (response['result'][0].caseClosedDate != "" && response['result'][0].caseClosedDate != null)
          this.firstFormGroup.patchValue({ ContractEndDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].caseClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].caseClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].caseClosedDate) }) });

        this.firstFormGroup.patchValue({ Assessor: response['result'][0].assessorId });
        this.firstFormGroup.patchValue({ AssessorRate: response['result'][0].assessorRate });
        this.firstFormGroup.patchValue({ ClientRate: response['result'][0].clientRate });

        if (response['result'][0].contractStartDate != "" && response['result'][0].contractStartDate != null)
          this.firstFormGroup.patchValue({ ContractStartDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].contractStartDate), month: this._commonService.GetMonthFromDateString(response['result'][0].contractStartDate), date: this._commonService.GetDateFromDateString(response['result'][0].contractStartDate) }) });
        this.firstFormGroup.patchValue({ ServiceProviderId: response['result'][0].serviceProviderId });
        this.firstFormGroup.patchValue({ ServiceRequestedId: response['result'][0].serviceRequestedId });
        this.thirdFormGroup.patchValue({ ServiceRequestedId: response['result'][0].serviceRequestedId });

        if (response['result'][0].submitedToProvider != "" && response['result'][0].submitedToProvider != null)
          this.firstFormGroup.patchValue({ SubmitedToProvider: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].submitedToProvider), month: this._commonService.GetMonthFromDateString(response['result'][0].submitedToProvider), date: this._commonService.GetDateFromDateString(response['result'][0].submitedToProvider) }) });
        //this.firstFormGroup.patchValue({ContractStartDate:response['result'][0].ContractStartDate})

        if (response['result'][0].dateOfAssessment != "" && response['result'][0].dateOfAssessment != null)
          this.firstFormGroup.patchValue({ DateofAssessment: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateOfAssessment), month: this._commonService.GetMonthFromDateString(response['result'][0].dateOfAssessment), date: this._commonService.GetDateFromDateString(response['result'][0].dateOfAssessment) }) });


        if (response['result'][0].reportDueBy != "" && response['result'][0].reportDueBy != null)
          this.firstFormGroup.patchValue({ ReportDueBy: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].reportDueBy), month: this._commonService.GetMonthFromDateString(response['result'][0].reportDueBy), date: this._commonService.GetDateFromDateString(response['result'][0].reportDueBy) }) });

        if (response['result'][0].reportReceived != "" && response['result'][0].reportReceived != null)
          this.firstFormGroup.patchValue({ ReportReceived: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].reportReceived), month: this._commonService.GetMonthFromDateString(response['result'][0].reportReceived), date: this._commonService.GetDateFromDateString(response['result'][0].reportReceived) }) });

        if (response['result'][0].caseClosedDate != "" && response['result'][0].caseClosedDate != null)
          this.firstFormGroup.patchValue({ ReportClient: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].caseClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].caseClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].caseClosedDate) }) });

        if (response['result'][0].claimantFirstContactedDate != "" && response['result'][0].claimantFirstContactedDate != null)
          this.thirdFormGroup.patchValue({ ClaimantFirstContactedDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].claimantFirstContactedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].claimantFirstContactedDate), date: this._commonService.GetDateFromDateString(response['result'][0].claimantFirstContactedDate) }) });

        //

        this.thirdFormGroup.patchValue({ ServiceProviderId: response['result'][0].serviceProviderId });

        if (response['result'][0].submitedToProvider != "" && response['result'][0].submitedToProvider != null)
          this.thirdFormGroup.patchValue({ SubmitedToProvider: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].submitedToProvider), month: this._commonService.GetMonthFromDateString(response['result'][0].submitedToProvider), date: this._commonService.GetDateFromDateString(response['result'][0].submitedToProvider) }) });
        //this.firstFormGroup.patchValue({ContractStartDate:response['result'][0].ContractStartDate})

        if (response['result'][0].dateOfAssessment != "" && response['result'][0].dateOfAssessment != null)
          this.thirdFormGroup.patchValue({ DateofAssessment: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateOfAssessment), month: this._commonService.GetMonthFromDateString(response['result'][0].dateOfAssessment), date: this._commonService.GetDateFromDateString(response['result'][0].dateOfAssessment) }) });


        if (response['result'][0].reportDueBy != "" && response['result'][0].reportDueBy != null)
          this.thirdFormGroup.patchValue({ ReportDueBy: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].reportDueBy), month: this._commonService.GetMonthFromDateString(response['result'][0].reportDueBy), date: this._commonService.GetDateFromDateString(response['result'][0].reportDueBy) }) });

        if (response['result'][0].reportReceived != "" && response['result'][0].reportReceived != null)
          this.thirdFormGroup.patchValue({ ReportReceived: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].reportReceived), month: this._commonService.GetMonthFromDateString(response['result'][0].reportReceived), date: this._commonService.GetDateFromDateString(response['result'][0].reportReceived) }) });

        if (response['result'][0].caseClosedDate != "" && response['result'][0].caseClosedDate != null)
          this.thirdFormGroup.patchValue({ ReportClient: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].caseClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].caseClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].caseClosedDate) }) });

        //
        if (response['result'][0].firstVerbalContactClaimantDate != "" && response['result'][0].firstVerbalContactClaimantDate != null)
          this.thirdFormGroup.patchValue({ FirstVerbalContactClaimantDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].firstVerbalContactClaimantDate), month: this._commonService.GetMonthFromDateString(response['result'][0].firstVerbalContactClaimantDate), date: this._commonService.GetDateFromDateString(response['result'][0].firstVerbalContactClaimantDate) }) });

        this.thirdFormGroup.patchValue({ TypeOfContactId: response['result'][0].typeOfContactId });
        this.thirdFormGroup.patchValue({ TypeOfContactDetails: response['result'][0].typeOfContactDetails });

        if (response['result'][0].nextReviewDate != "" && response['result'][0].nextReviewDate != null)
          this.thirdFormGroup.patchValue({ NextReviewDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].nextReviewDate), month: this._commonService.GetMonthFromDateString(response['result'][0].nextReviewDate), date: this._commonService.GetDateFromDateString(response['result'][0].nextReviewDate) }) });

        // if (response['result'][0].sentToNurseDate != "" && response['result'][0].sentToNurseDate != null)
        //   this.fourthFormGroup.patchValue({ SentToNurseDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].sentToNurseDate), month: this._commonService.GetMonthFromDateString(response['result'][0].sentToNurseDate), date: this._commonService.GetDateFromDateString(response['result'][0].sentToNurseDate) }) });

        if (response['result'][0].dateCancelled != "" && response['result'][0].dateCancelled != null)
          this.fourthFormGroup.patchValue({ DateCancelled: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateCancelled), month: this._commonService.GetMonthFromDateString(response['result'][0].dateCancelled), date: this._commonService.GetDateFromDateString(response['result'][0].dateCancelled) }) });

        if (response['result'][0].dateCancelled != null) {
          this.ExistingCancelDate = ((this._commonService.GetDateFromDateString(response['result'][0].dateCancelled)).toString() + "/" + (this._commonService.GetMonthFromDateString(response['result'][0].dateCancelled) + 1).toString() + "/" + (this._commonService.GetYearFromDateString(response['result'][0].dateCancelled)).toString());
          // this.ExistingCancelDate = (((this._commonService.GetDateFromDateString(response['result'][0].dateCancelled)).toString() + "/" + (this._commonService.GetMonthFromDateString(response['result'][0].dateCancelled) + 1).toString() + "/" + this._commonService.GetYearFromDateString(response['result'][0].dateCancelled)).toString());

        }
        else {
          this.ExistingCancelDate = response['result'][0].dateCancelled;
        }
        if (response['result'][0].returnToWorkDate != "" && response['result'][0].returnToWorkDate != null)
          this.fourthFormGroup.patchValue({ ReturnToWorkDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].returnToWorkDate), month: this._commonService.GetMonthFromDateString(response['result'][0].returnToWorkDate), date: this._commonService.GetDateFromDateString(response['result'][0].returnToWorkDate) }) });

        this.closed_Date = response['result'][0].caseClosedDate;
        if (response['result'][0].caseClosedDate != "" && response['result'][0].caseClosedDate != null)
          this.fourthFormGroup.patchValue({ CaseClosedDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].caseClosedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].caseClosedDate), date: this._commonService.GetDateFromDateString(response['result'][0].caseClosedDate) }) });
        this.fourthFormGroup.patchValue({ HCBReceivedDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].hcbReceivedDate), month: this._commonService.GetMonthFromDateString(response['result'][0].hcbReceivedDate), date: this._commonService.GetDateFromDateString(response['result'][0].hcbReceivedDate) }) });

        this.fourthFormGroup.patchValue({ CaseClosedReasonId: response['result'][0].caseClosedReasonId });
        this.fourthFormGroup.patchValue({ PDAID: response['result'][0].pdaid });
        this.fourthFormGroup.patchValue({ SecondaryPDAID: response['result'][0].secondaryPDAID });
        this.fourthFormGroup.patchValue({ ReturnToWorkReason: response['result'][0].returnToWorkReason });

        this.fourthFormGroup.patchValue({ OutcomesId: response['result'][0].outcomesId });
        this.fourthFormGroup.patchValue({ fourthClaimStatusId: response['result'][0].claimStatusId });
        this.sixththFormGroup.patchValue({ FeeCharged: response['result'][0].feeCharged });
        this.sixththFormGroup.patchValue({ RehabCost: response['result'][0].rehabCost });

        this.sixththFormGroup.patchValue({ TimeSpent: response['result'][0].timeSpent });
        this.sixththFormGroup.patchValue({ FeesPayable: response['result'][0].feesPayable });

        var total = (response['result'][0].feeCharged + response['result'][0].rehabCost)
        this.sixththFormGroup.patchValue({ TotalCaseCosts: total });
        this.working = false;

        if ((new Date(response['result'][0].caseClosedDate) < today) && (response['result'][0].caseClosedDate != null)) {
          console.log('Correct');
          this.ServiceEight_FirstForm = true;
          this.disableCloseDate = true;
          if(this.UserTypeId==3){
          this.disableDOCButton = false;
         
          this.RecDate = true;
          }else{
            this.disableDOCButton = true;
            
            this.RecDate = false;
          }
          this.thirdFormGroup.disable();
         
          // this.firstFormGroup.controls['ClientContactId'].disable();
          // this.firstFormGroup.controls['CaseMgrId'].disable();
          // this.firstFormGroup.controls['DateReferredToCaseOwner'].disable();
          if (response['result'][0].serviceType == 8) {
            this.FieldsDisabledForEight();

          }
          if (response['result'][0].serviceType == 7) {
            this.FieldsDisabledForSeven();
          }
          if (response['result'][0].serviceType == 5) {
            this.FieldsDisabledForfive();
          }
          if (response['result'][0].serviceType == 9) {
            this.FieldsDisabledForNine();
          }
          if (response['result'][0].serviceType < 5) {
            // this.firstFormGroup.disable();
            // this.fourthFormGroup.enable();
            this.FieldsDisabledForLessThanFive();
          }
          if (response['result'][0].serviceType == 11) {
            // FieldsDisabledForLessThanFive
            // this.fourthFormGroup.clearValidators();
            // this.fourthFormGroup.updateValueAndValidity();
            //  this.RecDate = true;
          //  this.disableCloseDate = false;    
            this.disableVisitButton = false;
            this.editTELEandVISIT = true;
            if (this.UserTypeId == 3) {
              //this.fourthFormGroup.controls['CaseClosedDate'].disable();
              this.disableCloseDate = true;
            }
          }

        }
       

      }, (error: any) => {
        this.HcbReferenceId = undefined;
        this.errorText = error;
        this.working = false;
      });
  }

  FieldsDisabledForLessThanFive() {
    this.RecDate = true;
   // this.disableCloseDate = false;    
    this.disableVisitButton = false;
    this.editTELEandVISIT = true;
    //     this.fourthFormGroup.value.clearValidators();
    // this.fourthFormGroup.value.updateValueAndValidity();
    if (this.UserTypeId == 3) {
      //this.fourthFormGroup.controls['CaseClosedDate'].disable();
      this.disableCloseDate = true;
    }
  }

  FieldsDisabledForNine() {
   // this.RecDate = true;
   // this.disableCloseDate = false;

    if (this.UserTypeId == 3) {
      // this.firstFormGroup.controls['ReportClient'].disable();
      this.disableCloseDate = true;
    }
  }
  FieldsDisabledForfive() {
   // this.RecDate = true;
    //this.disableCloseDate = false;
    this.disableVisitButton = false;
    if (this.UserTypeId == 3) {
      // this.firstFormGroup.controls['ServiceCaseClosed'].disable();
      this.disableCloseDate = true;
    }
  }
  FieldsDisabledForSeven() {
   // this.RecDate = true;
   // this.disableCloseDate = false;
    if (this.UserTypeId == 3) {
      //this.firstFormGroup.controls['ContractEndDate'].disable();
      this.disableCloseDate = true;
    }
  }

  FieldsDisabledForEight() {
    //this.RecDate = true;
   // this.disableCloseDate = false;     //make true if for Admin need to enable the closed date field
    if (this.UserTypeId == 3) {
      // this.firstFormGroup.controls['DateProjectClosed'].disable();
      this.disableCloseDate = true;
    }
  }

  onClientChangeForEdit() {
debugger;
    if (this.firstFormGroup.get('ClientID').value > 0) {
      this.ClientId = this.firstFormGroup.get('ClientID').value;
      // this.firstFormGroup.get('ServiceType').setValidators([Validators.required]);

      this._caseManagment.GetddlClientContact(this.ClientId)
        .subscribe((response: GetddlClientContact[]) => {
         
          this.getddlClientContact = response['result'],
            this.FillClientContactDetail();
            
          (error: any) => {
            this.errorText = error;
          }
        });
    }

    this._caseManagment.GetddlServiceTypeforEdit()
      .subscribe((response: GetddlServiceType[]) => this.getddlServiceType = response['result'],

        (error: any) => {
          this.errorText = error;
        });

  }
  FillClientContactDetail(){
  debugger;
  if (this.firstFormGroup.get('ClientContactId').value > 0) {
    var ClientContactId = this.firstFormGroup.get('ClientContactId').value;
   var Telephone = this.getddlClientContact.find(x => x.clientReferralContactsId == ClientContactId).phoneNo;
   var email = this.getddlClientContact.find(x => x.clientReferralContactsId === ClientContactId).contactEmail;
   this.firstFormGroup.patchValue({ ContactTele: Telephone });
   this.firstFormGroup.patchValue({ ContactEmailId: email });
  }
   
       }

  onClientChange() {
    this.firstFormGroup.get('ContactEmailId').reset();
    this.firstFormGroup.get('ContactTele').reset();

    this.firstFormGroup.get('ServiceType').reset();
    if (this.firstFormGroup.get('ClientID').value > 0) {
      this.ClientId = this.firstFormGroup.get('ClientID').value;
      // this.firstFormGroup.get('ServiceType').setValidators([Validators.required]);

      this._caseManagment.GetddlClientContact(this.ClientId)
        .subscribe((response: GetddlClientContact[]) => {
         
          this.getddlClientContact = response['result'],

            (error: any) => {
              this.errorText = error;
            }
        });

      this._caseManagment.GetddlServiceType(this.ClientId)
        .subscribe((response: GetddlServiceType[]) => this.getddlServiceType = response['result'],

          (error: any) => {
            this.errorText = error;
          });
    }
  }
  onContactChange() {
    debugger;
    if (this.firstFormGroup.get('ClientContactId').value > 0) {
      var ClientContactId = this.firstFormGroup.get('ClientContactId').value;
       var Telephone = this.getddlClientContact.find(x => x.clientReferralContactsId == ClientContactId).phoneNo;
       var email = this.getddlClientContact.find(x => x.clientReferralContactsId === ClientContactId).contactEmail;
      this.firstFormGroup.patchValue({ ContactTele: Telephone });
      this.firstFormGroup.patchValue({ ContactEmailId: email });
      // if(this.HcbReferenceId)
      // {
       var EmployerContact= this.getddlClientContact.find(x =>x.clientReferralContactsId==ClientContactId).contactName;
       this.firstFormGroup.patchValue({ EmployerContact: EmployerContact });
            this.firstFormGroup.patchValue({ EmployerTeleNo: Telephone });
            this.firstFormGroup.patchValue({ EmployerEmail: email });     
     // }
      
          }
        }

  onCaseOwnerChange() {
    if (this.firstFormGroup.get('CaseMgrId').value > 0) {
      this.CaseOwnerId = this.firstFormGroup.get('CaseMgrId').value;
      this.Telephone = this.getddlCaseOwner.find(x => x.userID === this.CaseOwnerId).telephoneNumber;
      this.email = this.getddlCaseOwner.find(x => x.userID === this.CaseOwnerId).emailAddress;
      this.firstFormGroup.patchValue({ Telephone: this.Telephone });
      this.firstFormGroup.patchValue({ EmailAddress: this.email });
    }
  }

  onOutcomeChange() {
    var value1 = this.firstFormGroup.get('Outcome').value;
    if (value1 == "Decline") {
      this.ShowOutcome = true;
    } else {
      this.ShowOutcome = false;
      this.firstFormGroup.get('ReasonForDeclinature').reset();
    }
  }

  UpdateFeePayable(event: any) {

    var feewith_time: any;
    var eventvalue = event.name;
    this.FeepayableValue = event;
    feewith_time = parseInt(this.sixththFormGroup.get('TimeSpent').value) * parseInt((event.split('$')[0]))
    var fee = (this.sixththFormGroup.get('RehabCost').value == null || this.sixththFormGroup.get('RehabCost').value == undefined || this.sixththFormGroup.get('FeeCharged').value == "") ? 0 : this.sixththFormGroup.get('RehabCost').value


    var total = (parseInt(feewith_time)) + parseInt(fee);
    this.sixththFormGroup.patchValue({ FeeCharged: feewith_time });
    this.sixththFormGroup.patchValue({ TotalCaseCosts: total });
  }


  updatefee(event: any) {

    if (this.sixththFormGroup.get('FeesPayable').value != null) {
      for (var i = 0; i < this.getddlFeesPayable.length; i++) {
        if (this.getddlFeesPayable[i].id == this.sixththFormGroup.get('FeesPayable').value) {
          this.FeepayableValue = this.getddlFeesPayable[i].name;
        }
      }
    } else {
      this.FeepayableValue == "";
    }

    var feewith_time: any;
    var Fee_Payble: any;
    var eventValue = (event == "" || event == null || event == undefined) ? 0 : event;
    var fee = (this.sixththFormGroup.get('RehabCost').value == null || this.sixththFormGroup.get('RehabCost').value == undefined || this.sixththFormGroup.get('FeeCharged').value == "") ? 0 : this.sixththFormGroup.get('RehabCost').value

    // var feewith_time =(this.sixththFormGroup.get('TimeSpent').value == null || this.sixththFormGroup.get('TimeSpent').value == undefined || this.sixththFormGroup.get('TimeSpent').value == "") ? 0 : this.sixththFormGroup.get('TimeSpent').value
    //(this.sixththFormGroup.get('FeesPayable').)
    Fee_Payble = this.FeepayableValue.split("$");
    feewith_time = (parseInt(eventValue)) * Fee_Payble[0]
    var total = (parseInt(feewith_time)) + parseInt(fee);
    this.sixththFormGroup.patchValue({ FeeCharged: feewith_time });
    this.sixththFormGroup.patchValue({ TotalCaseCosts: total });
  }

  updateRehab(event: any) {

    var eventValue1 = (event == "" || event == null || event == undefined) ? 0 : event;
    var rehab = (this.sixththFormGroup.get('FeeCharged').value == null || this.sixththFormGroup.get('FeeCharged').value == undefined || this.sixththFormGroup.get('FeeCharged').value == "") ? 0 : this.sixththFormGroup.get('FeeCharged').value;
    var total = (parseInt(eventValue1) + parseInt(rehab));
    this.sixththFormGroup.patchValue({ TotalCaseCosts: total });
  }
  checkNameNDOB() {

    var HCBRefId;
    this.CFirstName = this.firstFormGroup.value.ClaimantFirstName.replace(/\s/g, "");
    this.CLastName = this.firstFormGroup.value.ClaimantLastName.replace(/\s/g, "");
    // this.CBirthDate = updateCaseManagment.ClaimantDOB;
    this.CBirthDate = (this.firstFormGroup.value.ClaimantDOB == "" || this.firstFormGroup.value.ClaimantDOB == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ClaimantDOB) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ClaimantDOB) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ClaimantDOB) + " 00:00:00:000";
    // if ((this.ActionName == 'Reopen')) {
    //   HCBRefId = this.OldRefID;
    // } else {
    HCBRefId = this.HcbReferenceId
    // }
    if (this.RecDate != true) {
      this._caseManagment.CheckClaimantExistOrNot(this.CFirstName, this.CLastName, this.CBirthDate, this.HcbReferenceId)
        .subscribe((response: number) => {
          if ((response['result'].table.length >= 1)) {
            this.firstFormGroup.get('ClaimantFirstName').reset();
            this.firstFormGroup.get('ClaimantLastName').reset();
            this.firstFormGroup.get('ClaimantDOB').reset();
            var HCBReferenceno = response['result'].table[0].hcbReferenceNo;
            this.dialog.open(AlertMessageComponent, { width: '450px', data: { Firstname: this.CFirstName, Lastname: this.CLastName, HCBReferenceNo: HCBReferenceno } });
            this.working = false;
          } else { }
        })
    }
  }

  SaveCaseRecord(): void {

    // if (!this.firstFormGroup.valid)
    //   return;
    var today = new Date();
    // this.firstFormGroup.get('CaseClosedDate').value

    if (((this.firstFormGroup.get('ServiceType').value) < 5) && (this.UserTypeId != 1) && (this.closed_Date > today || this.closed_Date == null)) {
      if (this.thirdFormGroup.get('NextReviewDate').value == null || this.thirdFormGroup.get('NextReviewDate').value == undefined || this.thirdFormGroup.get('NextReviewDate').value == "") {
        swal.fire({

          text: 'Are you sure that no Review date is necessary for this Record',

          showCancelButton: true,
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            this.SaveCaseRecordData();
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === swal.DismissReason.cancel) {

          }
        })
      } else {

        this.SaveCaseRecordData();
      }
    } else {
      var da: string;
      this.fourthFormGroup.value.DateCancelled;
      if (this.fourthFormGroup.value.DateCancelled != "" && this.fourthFormGroup.value.DateCancelled != null && this.fourthFormGroup.value.DateCancelled != undefined) {
        da = (this._commonService.GetDateFromDateString(this.fourthFormGroup.value.DateCancelled) + "/" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.DateCancelled) + 1) + "/" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.DateCancelled));

        // da = (this._commonService.GetDateFromDateString(this.fourthFormGroup.value.DateCancelled) + "/" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.DateCancelled) + 1) + "/" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.DateCancelled));
        //  da = ((this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.DateCancelled._i) + 1).toString() + "/" + this._commonService.GetDateFromDateString(this.fourthFormGroup.value.DateCancelled._i).toString() + "/" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.DateCancelled._i).toString());
        if (da != this.ExistingCancelDate) {

          this.dialogRef = this.dialog.open(CaseCancelReasonComponent, {
            width: '600px', data: {
              HCBRefID: this.HcbReferenceId,
              noteID: this.NoteID
            }

          });
          // this.fourthFormGroup.patchValue({ CaseClosedDate: moment({ year: this._commonService.GetYearFromDateString(this.message ), month: this._commonService.GetMonthFromDateString(this.message ), date: this._commonService.GetDateFromDateString(this.message ) }) });
          this.fourthFormGroup.patchValue({ CaseClosedDate: this.fourthFormGroup.value.DateCancelled });

          this.dialogRef.afterClosed().subscribe(result => {

            this.popupresult = result;
            if (this.popupresult != undefined) {
              this.SaveCaseRecordData();
            } else {
              if (this.HcbReferenceId == 0) {
                var today = new Date();
                const note: HCBNote = {
                  HCBReferenceId: this.HcbReferenceId,
                  fullName: this.FullName,
                  noteCreatedDate: today,
                  noteText: this.popupresult,
                  comment: 'Cancelled Case',
                  noteID: this.NoteID
                };
                this.HCBNoteList.push(note);
                this.noteHCBDataSource = this.HCBNoteList;
              } else { this.SaveCaseRecordData(); }
            }
          })
          // this.fourthFormGroup.patchValu
        } else { this.SaveCaseRecordData(); }
      } else {
        this.SaveCaseRecordData();
        this.fourthFormGroup.patchValue({ CaseClosedDate: this.fourthFormGroup.value.DateCancelled });

      }
      // this.SaveCaseRecordData();
    }
  }
  SaveCaseRecordData(): void {
   debugger;
    this.working = true;
    const updateCaseManagment: SaveUpdateCaseManagment = Object.assign({}, this.firstFormGroup.value, this.thirdFormGroup.value, this.fourthFormGroup.value, this.sixththFormGroup.value);//, this.firstFormGroup.value, this.fourthFormGroup
    if (this.ActionName == 'Reopen') {
      updateCaseManagment.Old_HCBRefId = this.HCBReferenceNo;
    } else {
      updateCaseManagment.Old_HCBRefId = null;
    }
    updateCaseManagment.ServiceProviderId = this.firstFormGroup.get('ServiceProviderId').value;
    updateCaseManagment.ServiceRequestedId = this.firstFormGroup.get('ServiceRequestedId').value;
    updateCaseManagment.ClaimantFirstContactedDate = this.thirdFormGroup.get('ClaimantFirstContactedDate').value;
    updateCaseManagment.FirstVerbalContactClaimantDate = this.thirdFormGroup.get('FirstVerbalContactClaimantDate').value;
    updateCaseManagment.TypeOfContactId = this.thirdFormGroup.get('TypeOfContactId').value;
    updateCaseManagment.TypeOfContactDetails = this.thirdFormGroup.get('TypeOfContactDetails').value;
    updateCaseManagment.NextReviewDate = this.thirdFormGroup.get('NextReviewDate').value;

    updateCaseManagment.HCBReceivedDate = (this.firstFormGroup.value.HCBReceivedDate == "" || this.firstFormGroup.value.HCBReceivedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.HCBReceivedDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.HCBReceivedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.HCBReceivedDate) + " 00:00:00:000";
    updateCaseManagment.DateReferredToCaseOwner = (this.firstFormGroup.value.DateReferredToCaseOwner == "" || this.firstFormGroup.value.DateReferredToCaseOwner == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateReferredToCaseOwner) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateReferredToCaseOwner) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateReferredToCaseOwner) + " 00:00:00:000";
    updateCaseManagment.ClaimantDOB = (this.firstFormGroup.value.ClaimantDOB == "" || this.firstFormGroup.value.ClaimantDOB == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ClaimantDOB) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ClaimantDOB) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ClaimantDOB) + " 00:00:00:000";
    updateCaseManagment.EndOfBenefitDate = (this.firstFormGroup.value.EndOfBenefitDate == "" || this.firstFormGroup.value.EndOfBenefitDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.EndOfBenefitDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.EndOfBenefitDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.EndOfBenefitDate) + " 00:00:00:000";
    updateCaseManagment.FirstAbsentDate = (this.firstFormGroup.value.FirstAbsentDate == "" || this.firstFormGroup.value.FirstAbsentDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.FirstAbsentDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.FirstAbsentDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.FirstAbsentDate) + " 00:00:00:000";
    updateCaseManagment.ClaimClosedDate = (this.firstFormGroup.value.ClaimClosedDate == "" || this.firstFormGroup.value.ClaimClosedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ClaimClosedDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ClaimClosedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ClaimClosedDate) + " 00:00:00:000";

    updateCaseManagment.DateFirstContactApplicant = (this.firstFormGroup.value.DateFirstContactApplicant == "" || this.firstFormGroup.value.DateFirstContactApplicant == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateFirstContactApplicant) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateFirstContactApplicant) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateFirstContactApplicant) + " 00:00:00:000";

    updateCaseManagment.DateClaimFormSent = (this.firstFormGroup.value.DateClaimFormSent == "" || this.firstFormGroup.value.DateClaimFormSent == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateClaimFormSent) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateClaimFormSent) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateClaimFormSent) + " 00:00:00:000";
    updateCaseManagment.ServiceCaseClosed = (this.firstFormGroup.value.ServiceCaseClosed == "" || this.firstFormGroup.value.ServiceCaseClosed == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ServiceCaseClosed) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ServiceCaseClosed) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ServiceCaseClosed) + " 00:00:00:000";
    updateCaseManagment.SubmitedTraceSmart = (this.firstFormGroup.value.SubmitedTraceSmart == "" || this.firstFormGroup.value.SubmitedTraceSmart == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.SubmitedTraceSmart) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.SubmitedTraceSmart) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.SubmitedTraceSmart) + " 00:00:00:000";
    updateCaseManagment.DataReturned = (this.firstFormGroup.value.DataReturned == "" || this.firstFormGroup.value.DataReturned == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DataReturned) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DataReturned) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DataReturned) + " 00:00:00:000";
    updateCaseManagment.ReportSubmitted = (this.firstFormGroup.value.ReportSubmitted == "" || this.firstFormGroup.value.ReportSubmitted == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ReportSubmitted) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ReportSubmitted) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ReportSubmitted) + " 00:00:00:000";

    updateCaseManagment.DateProjectClosed = (this.firstFormGroup.value.DateProjectClosed == "" || this.firstFormGroup.value.DateProjectClosed == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateProjectClosed) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateProjectClosed) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateProjectClosed) + " 00:00:00:000";
    updateCaseManagment.HCBAppointedDate = (this.firstFormGroup.value.HCBAppointedDate == "" || this.firstFormGroup.value.HCBAppointedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.HCBAppointedDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.HCBAppointedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.HCBAppointedDate) + " 00:00:00:000";
    //  updateCaseManagment.AnnualReportDate = (this.firstFormGroup.value.AnnualReportDate == "" || this.firstFormGroup.value.AnnualReportDate == null) ? "" : "" + this.firstFormGroup.value.AnnualReportDate._i.year + "-" + (this.firstFormGroup.value.AnnualReportDate._i.month + 1) + "-" + this.firstFormGroup.value.AnnualReportDate._i.date + " 00:00:00:000";
    updateCaseManagment.ContractStartDate = (this.firstFormGroup.value.ContractStartDate == "" || this.firstFormGroup.value.ContractStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ContractStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ContractStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ContractStartDate) + " 00:00:00:000";
    updateCaseManagment.ContractEndDate = (this.firstFormGroup.value.ContractEndDate == "" || this.firstFormGroup.value.ContractEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ContractEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ContractEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ContractEndDate) + " 00:00:00:000";

    updateCaseManagment.SubmitedToProvider = (this.firstFormGroup.value.SubmitedToProvider == "" || this.firstFormGroup.value.SubmitedToProvider == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.SubmitedToProvider) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.SubmitedToProvider) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.SubmitedToProvider) + " 00:00:00:000";

    updateCaseManagment.ReportDueBy = (this.firstFormGroup.value.ReportDueBy == "" || this.firstFormGroup.value.ReportDueBy == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ReportDueBy) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ReportDueBy) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ReportDueBy) + " 00:00:00:000";

    updateCaseManagment.DateofAssessment = (this.firstFormGroup.value.DateofAssessment == "" || this.firstFormGroup.value.DateofAssessment == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateofAssessment) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateofAssessment) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateofAssessment) + " 00:00:00:000";


    updateCaseManagment.ReportReceived = (this.firstFormGroup.value.ReportReceived == "" || this.firstFormGroup.value.ReportReceived == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ReportReceived) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ReportReceived) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ReportReceived) + " 00:00:00:000";

    updateCaseManagment.ReportClient = (this.firstFormGroup.value.ReportClient == "" || this.firstFormGroup.value.ReportClient == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ReportClient) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ReportClient) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ReportClient) + " 00:00:00:000";
    updateCaseManagment.ClaimStatusDate = (this.firstFormGroup.value.ClaimStatusDate == "" || this.firstFormGroup.value.ClaimStatusDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.ClaimStatusDate) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.ClaimStatusDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.ClaimStatusDate) + " 00:00:00:000";


    // HCBAppointedDate
    if (this.firstFormGroup.get('ServiceType').value < 5 || this.firstFormGroup.get('ServiceType').value == 11) {

      updateCaseManagment.ServiceProviderId = this.thirdFormGroup.get('ServiceProviderId').value;
      updateCaseManagment.ServiceRequestedId = this.thirdFormGroup.get('ServiceRequestedId').value;

      updateCaseManagment.ContractStartDate = (this.thirdFormGroup.value.ContractStartDate == "" || this.thirdFormGroup.value.ContractStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ContractStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ContractStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ContractStartDate) + " 00:00:00:000";
      updateCaseManagment.ContractEndDate = (this.thirdFormGroup.value.ContractEndDate == "" || this.thirdFormGroup.value.ContractEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ContractEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ContractEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ContractEndDate) + " 00:00:00:000";

      updateCaseManagment.SubmitedToProvider = (this.thirdFormGroup.value.SubmitedToProvider == "" || this.thirdFormGroup.value.SubmitedToProvider == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.SubmitedToProvider) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.SubmitedToProvider) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.SubmitedToProvider) + " 00:00:00:000";

      updateCaseManagment.ReportDueBy = (this.thirdFormGroup.value.ReportDueBy == "" || this.thirdFormGroup.value.ReportDueBy == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ReportDueBy) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ReportDueBy) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ReportDueBy) + " 00:00:00:000";

      updateCaseManagment.DateofAssessment = (this.thirdFormGroup.value.DateofAssessment == "" || this.thirdFormGroup.value.DateofAssessment == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.DateofAssessment) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.DateofAssessment) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.DateofAssessment) + " 00:00:00:000";


      updateCaseManagment.ReportReceived = (this.thirdFormGroup.value.ReportReceived == "" || this.thirdFormGroup.value.ReportReceived == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ReportReceived) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ReportReceived) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ReportReceived) + " 00:00:00:000";

      updateCaseManagment.ReportClient = (this.thirdFormGroup.value.ReportClient == "" || this.thirdFormGroup.value.ReportClient == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ReportClient) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ReportClient) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ReportClient) + " 00:00:00:000";
    }
    updateCaseManagment.ClaimantFirstContactedDate = (this.thirdFormGroup.value.ClaimantFirstContactedDate == "" || this.thirdFormGroup.value.ClaimantFirstContactedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.ClaimantFirstContactedDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.ClaimantFirstContactedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.ClaimantFirstContactedDate) + " 00:00:00:000";

    updateCaseManagment.FirstVerbalContactClaimantDate = (this.thirdFormGroup.value.FirstVerbalContactClaimantDate == "" || this.thirdFormGroup.value.FirstVerbalContactClaimantDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.FirstVerbalContactClaimantDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.FirstVerbalContactClaimantDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.FirstVerbalContactClaimantDate) + " 00:00:00:000";

    updateCaseManagment.NextReviewDate = (this.thirdFormGroup.value.NextReviewDate == "" || this.thirdFormGroup.value.NextReviewDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.thirdFormGroup.value.NextReviewDate) + "-" + (this._commonService.GetMonthFromDateString(this.thirdFormGroup.value.NextReviewDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.thirdFormGroup.value.NextReviewDate) + " 00:00:00:000";

    updateCaseManagment.SentToNurseDate = (this.firstFormGroup.value.DateReferredToCaseOwner == "" || this.firstFormGroup.value.DateReferredToCaseOwner == null) ? null : "" + this._commonService.GetYearFromDateString(this.firstFormGroup.value.DateReferredToCaseOwner) + "-" + (this._commonService.GetMonthFromDateString(this.firstFormGroup.value.DateReferredToCaseOwner) + 1) + "-" + this._commonService.GetDateFromDateString(this.firstFormGroup.value.DateReferredToCaseOwner) + " 00:00:00:000";

    updateCaseManagment.DateCancelled = (this.fourthFormGroup.value.DateCancelled == "" || this.fourthFormGroup.value.DateCancelled == null) ? null : "" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.DateCancelled) + "-" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.DateCancelled) + 1) + "-" + this._commonService.GetDateFromDateString(this.fourthFormGroup.value.DateCancelled) + " 00:00:00:000";

    updateCaseManagment.ReturnToWorkDate = (this.fourthFormGroup.value.ReturnToWorkDate == "" || this.fourthFormGroup.value.ReturnToWorkDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.ReturnToWorkDate) + "-" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.ReturnToWorkDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.fourthFormGroup.value.ReturnToWorkDate) + " 00:00:00:000";

    updateCaseManagment.CaseClosedDate = (this.fourthFormGroup.value.CaseClosedDate == "" || this.fourthFormGroup.value.CaseClosedDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.fourthFormGroup.value.CaseClosedDate) + "-" + (this._commonService.GetMonthFromDateString(this.fourthFormGroup.value.CaseClosedDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.fourthFormGroup.value.CaseClosedDate) + " 00:00:00:000";

    //22_07_2020
    updateCaseManagment.TimeSpent = (this.sixththFormGroup.value.TimeSpent == "" || this.sixththFormGroup.value.TimeSpent == null) ? null : "" + this.sixththFormGroup.value.TimeSpent;


    this.CFirstName = this.firstFormGroup.value.ClaimantFirstName;
    this.CLastName = this.firstFormGroup.value.ClaimantLastName;
    this.CBirthDate = updateCaseManagment.ClaimantDOB;

    updateCaseManagment.HCBReferenceId = this.HcbReferenceId;

    if (this.HCBReferenceNo == undefined) {
      updateCaseManagment.HCBReferenceNo = '0';
    } else {
      updateCaseManagment.HCBReferenceNo = this.HCBReferenceNo.toString();
    }

    if (this.HcbReferenceId > 0 && this.ActionName != 'Reopen') {

      this.working = true;
      // if ((this.ExistingFirstName == this.CFirstName) && (this.ExistingLastName == this.CLastName) && (this.ExistingBirthDate == this.CBirthDate)) {
      //   updateCaseManagment.HCBReferenceId = this.HcbReferenceId;
      this._caseManagment.UpdateCaseManagment(updateCaseManagment)
        .subscribe((response: number) => {
          var Result = response['result'];

          this.ConfirmDialog();
          this._router.navigateByUrl('/case-managment', { skipLocationChange: true }).then(() => {
            this._router.navigate(['/case-managment/addedit-case-managment/edit/', this.HcbReferenceId]);
          });

          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          // this.errorMessage(this.errorText);
          this.working = false;
        });
    }

    else {

      var ID = 0;
      this.working = true;
      var obj = {
        SaveHCBNote: this.NotesList,
        SaveUpdateCaseManagmentModel: updateCaseManagment
        // saveReferralContact: this.refferalContactList,
        // saveAccountContact: this.accountcontactList,       
      };
      this._caseManagment.SaveCaseManagment(obj)
        .subscribe((response: number) => {

          ID = response['result'];
          this.ConfirmDialog();
          this._router.navigateByUrl('/case-managment', { skipLocationChange: true }).then(() => {
            this._router.navigate(['/case-managment/addedit-case-managment/edit/', ID]);
          });
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          //this.errorMessage(this.errorText);
          this.working = true;
        });

      // this.CaseOwnerEmail = this.firstFormGroup.get('EmailAddress').value

      // this._caseManagment.SendNewCaseMail(this.CaseOwnerEmail,  this.firstFormGroup.get('HCBReferenceNo').value)
      //   .subscribe((response) => {
      //     debugger;
      //     if (response['result'] == true) {
      //       alert("Mail Send to Case Owner")
      //     }
      //     else { alert("Mail Send Failed") }
      //   })
      // }
      // }
    }
  }

  EditCaseActivity(hcbCaseActivityId: number) {
   
    this.dialogRef = this.dialog.open(CaseActivitiesComponent, {
      disableClose: true,
      width: '600px',
      data: {
        HCBReferenceId: this.HcbReferenceId,
        CaseActivityId: hcbCaseActivityId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetCaseActivityGrid();
      }
    });
  }

  GetCaseActivityGrid() {

    this._caseManagment.GetCaseActivity(this.HcbReferenceId)
      .subscribe((response: any) => {
       
        this.CaseActivityGrid = new MatTableDataSource<GetTelephoneActivity>(response['result']);

      }, (error: any) => {
        this.errorText = error;
      });
  }


  AddCaseActivity() {
    this.dialogRef = this.dialog.open(CaseActivitiesComponent, {
      disableClose: true,
      width: '600px',
      // height: '600px',
      // maxHeight: '10vh !important',
      data: {
        HCBReferenceId: this.HcbReferenceId,
        CaseActivityId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.ConfirmDialog();
        this.GetCaseActivityGrid();
      }
    });
  }

  DeleteCaseActivity(CaseActivityId: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.DeleteCaseActivity(CaseActivityId)
          .subscribe((response: any) => {

            this.ConfirmDialog();
            this.GetCaseActivityGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }

  GetTelephoneActivityGrid() {

    this._caseManagment.GetTelephoneGrid(this.HcbReferenceId)
      .subscribe((response: GetTelephoneActivity) => {

        this.TelephoneActivityGrid = new MatTableDataSource<GetTelephoneActivity>(response['result']);

      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddTelephoneActivity(): void {

    this.dialogRef = this.dialog.open(AddeditTelephoneActivityComponent, {
      disableClose: true,
      width: '600px',
      // height: '600px',
      // maxHeight: '10vh !important',
      data: {
        HCBReferenceId: this.HcbReferenceId,
        HCBTelephoneID: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.ConfirmDialog();
        this.GetTelephoneActivityGrid();
      }
    });
  }

  EditTelephoneActivity(HCBTelephoneID: number): void {

    this.dialogRef = this.dialog.open(AddeditTelephoneActivityComponent, {
      disableClose: true,
      width: '600px',
      data: {
        HCBReferenceId: this.HcbReferenceId,
        HCBTelephoneID: HCBTelephoneID
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetTelephoneActivityGrid();
      }
    });
  }

  DeleteTelephoneActivity(HCBTelephoneID: number): void {

    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.DeleteTelephoneActivity(HCBTelephoneID)
          .subscribe((response: any) => {

            this.ConfirmDialog();
            this.GetTelephoneActivityGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }


  GetHCBVisitActivityGrid() {

    this._caseManagment.GetHCBVisitGrid(this.HcbReferenceId)
      .subscribe((response: GetHCBVisitActivity) => {
        this.visitActivityGrid = new MatTableDataSource<GetHCBVisitActivity>(response['result']);
        //this.visitActivityGrid.sort = this.sort;
        // this.visitActivityGrid.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddVisitActivity(): void {

    this.dialogRef = this.dialog.open(AddeditVisitActivityComponent, {
      disableClose: true,
      width: '600px',
      data: {
        HCBReferenceId: this.HcbReferenceId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.ConfirmDialog();
        this.GetHCBVisitActivityGrid();
      }
    });
  }

  EditVisitActivity(hcbVisitId: number): void {
    this.dialogRef = this.dialog.open(AddeditVisitActivityComponent, {
      disableClose: true,
      width: '600px',
      data: {
        HCBReferenceId: this.HcbReferenceId,
        HCBvisitId: hcbVisitId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetHCBVisitActivityGrid();
      }
    });
  }

  DeleteVisitActivity(Id: number): void {

    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.DeleteVisitActivity(Id)
          .subscribe((response: any) => {

            this.ConfirmDialog();
            this.GetHCBVisitActivityGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }



  GetAnnualReportSummaryGrid() {
    this._caseManagment.GetAnnualReportSummaryGrid(this.HcbReferenceId)
      .subscribe((response: GetAnnualReportSummary) => {
        this.AnnualReportGrid = new MatTableDataSource<GetAnnualReportSummary>(response['result']);
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddAnnualReportSummary(): void {
    this.dialogRef = this.dialog.open(AddeditAnnualReportSummaryComponent, {
      disableClose: true,
      width: '400px',
      data: {
        HCBReferenceId: this.HcbReferenceId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetAnnualReportSummaryGrid();
      }
    });
  }


  EditAnnualReportSummary(annualReportId: number): void {
    this.dialogRef = this.dialog.open(AddeditAnnualReportSummaryComponent, {
      disableClose: true,
      width: '400px',
      data: {
        HCBReferenceId: this.HcbReferenceId,
        AnnualReportId: annualReportId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetAnnualReportSummaryGrid();
      }
    });
  }

  DeleteAnnualReportSummary(annualReportId: number) {

    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.DeleteAnnualReportsummary(annualReportId)
          .subscribe((response: any) => {

            this.ConfirmDialog();
            this.GetAnnualReportSummaryGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }

  GetHCBDocumentGrid(HCBReferenceId: number): void {
    this.working = true;
    this._caseManagment.GetHCBDocuments(HCBReferenceId)
      .subscribe((response: HCBDocumentGrid[]) => {
        this.documentDataSource = new MatTableDataSource<HCBDocumentGrid>(response['result']);
        this.working = false;
      }, (error: any) => {
        this.errorTextGrid = error;
        this.working = false;
      });
  }

  UploadHCBDocument(): void {

    this.progress = 0;
    this.uploading = true;
    const myData: HCBDocumentUpload = {
      HCBReferenceId: this.HcbReferenceId,
    };
    this._caseManagment.UploadHCBFile(this.fileToUpload, myData)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          // this.showAddDocument = false;
          this.GetHCBDocumentGrid(this.HcbReferenceId);
          this.DocumentfileInput.nativeElement.value = null;
          this.progress = 0;
        }
      }, (error: any) => {
        this.errorText = error;
      });


  }


  DownloadDocument(id: number, Ext: any) {

    this._caseManagment.DownloadDocument(id)
      .subscribe(fileData => {
        const downloadedFile = new Blob([fileData], { type: fileData.type });
        FileSaver.saveAs(downloadedFile, Ext);
        // window.open(URL.createObjectURL(downloadedFile));
      }
      );
  }

  viewDoc(id: number) {
    this._router.navigate(["/case-managment/ngx-document-viewer", id]);
  }

  DeleteDocument(id: number) {

    // const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px', height: '180px', data: {
        dispalymessage: 'Are you sure you want to delete this Item?'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.DeleteDocument(id)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetHCBDocumentGrid(this.HcbReferenceId);
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }

  handleFileInput(files: FileList) {

    this.fileToUpload = files.item(0);
    // var mimeType = this.fileToUpload.type;
    // if (mimeType.match(/image\/*/) == null) {
    //   return;
    // }

    // var reader = new FileReader();      
    // reader.readAsDataURL(this.fileToUpload); 
    // reader.onload = (_event) => { 
    //   this.previewUrl = reader.result; 
    // }
  }
  addfile(event)     
  {   
    debugger; 
  this.file= event.target.files[0];     
  let fileReader = new FileReader();    
  fileReader.readAsArrayBuffer(this.file);     
  fileReader.onload = (e) => {    
      this.arrayBuffer = fileReader.result;    
      var data = new Uint8Array(this.arrayBuffer);    
      var arr = new Array();    
      for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);    
      var bstr = arr.join("");    
      var workbook = XLSX.read(bstr, {type:"binary"});    
      var first_sheet_name = workbook.SheetNames[0];    
      var worksheet = workbook.Sheets[first_sheet_name];    
      console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));      
        var arraylist  = XLSX.utils.sheet_to_json(worksheet, { raw: true, defval: null });

         arraylist  = JSON.parse(JSON.stringify(arraylist).replace(/\s(?=\w+":)/g, "_"));
  
  
  
            console.log(arraylist);    
    
  }    
} 

  DeleteNote(noteId: any) {

    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px', height: '180px', data: {
        dispalymessage: 'Are you sure you want to delete this Note?'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.DeleteNote(noteId)
          .subscribe((response: any) => {

            this.ConfirmDialog();
            this.loadHCBNotes();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });

  }

  loadHCBNotes(): void {
    this.working = true;
    this._caseManagment.GetHCBNote(this.HcbReferenceId)
      .subscribe(
        (response: HCBNote[]) => {

          this.noteHCBDataSource = response['result'];
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
  }

  IsImpotant(event) {

    this.IsImportant = event.checked;
  }

  SendNotification(event) {

    this.SendEmail = event.checked;
    if (this.IsImportant == undefined) {
      this.IsImportant = false;
    }

  }

  SearchHCBNote(event: string): void {

    this.working = true;
    // const value = event;

    if (event == "" || event == null) {
      this.loadHCBNotes();
    }
    else {

      this._caseManagment.SearchHCBNote(event, this.HcbReferenceId)
        .subscribe(
          (response: HCBNote[]) => {

            this.noteHCBDataSource = response['result'];
            this.working = false;
          }, (error: any) => {
            this.errorText = error;
            this.working = false;
          });
    }
  }

  Clear() {
    this.secondFormGroup.get('SearchHCBNoteList').reset();
    this.SearchHCBNote('');
    // this.loadHCBNotes();
  }

  SaveHCBNote(): void {

    this.IsImpotantCheck = false;
    // if (this.UserTypeId == 1) {
    //  // this.SendmailId = false;
    //  this.SendEmail ==false;
    // }
    var today = new Date();
    const note: HCBNote = {
      HCBReferenceId: this.HcbReferenceId,
      fullName: this.FullName,
      noteCreatedDate: today,
      noteText: this.secondFormGroup.value.Note,
      comment: null,
      noteID: this.NoteID
    };
    if (this.IsImportant == undefined) {
      this.IsImportant = false;
    }
    this.HCBReferenceNo = this.firstFormGroup.value.HCBReferenceNo;
    if (this.HcbReferenceId > 0 && note.noteText != null && note.noteText != "") {
      if (this.SendEmail == true) {
        
        var createdDate: string;
        //  createdDate=this._commonService.GetDateFromDateString(note.noteCreatedDate.toString()) + "/" + (this._commonService.GetMonthFromDateString(note.noteCreatedDate.toString())) + "/" + this._commonService.GetYearFromDateString(note.noteCreatedDate.toString()) ;
        createdDate = moment({ year: this._commonService.GetYearFromDateString(note.noteCreatedDate.toString()), month: this._commonService.GetMonthFromDateString(note.noteCreatedDate.toString()), date: this._commonService.GetDateFromDateString(note.noteCreatedDate.toString()) }).format("DD/MM/YYYY")
        this._caseManagment.SendEmail(this.IsImportant, this.UserTypeId, createdDate, this.HCBReferenceNo,this.UserId,this.CanWorkAsCM )
          .subscribe((response: any) => {
console.log(response.result);
            //swal.fire(response.result);
          }, (error: any) => {
            this.errorText = error;
          });
      }
      else {

      }

      this._caseManagment.SaveHCBNote(note)
        .subscribe((response: number) => {
          this.secondFormGroup.reset();
          // if(this.UserTypeId == 3)
          // {
          // this.SendEmail = true;
          // }else{
         // this.IsImportant = false;
          this.SendEmail = false;
         this.IsImpotantCheck= false;
          this.IsImportant = false;
          this.loadHCBNotes();
          this.ConfirmDialog();
          //this.needRefresh = true;
        }, (error: any) => {
          this.errorText = error;
        });
    }
    else {

      if (note.noteText != undefined && note.noteText != null && note.noteText != "") {
        this.HCBNoteList.push(note);
        this.noteHCBDataSource = this.HCBNoteList;
      }
      this.fourthFormGroup.reset();
    }
  }

  back() {
    // this._router.navigateByUrl('/case-managment');
    this._router.navigateByUrl('/case-managment', { skipLocationChange: true }).then(() => {
      this._router.navigate([this._location.back()]);
    });
    //this._location.back();
  }

  SetFormat(event): boolean {
    return this._clientRecord.ValidatePhoneFormat(event);
  }
  AddAction() {
    this.dialogRef = this.dialog.open(AddEditActionComponent, {
      disableClose: true,
      width: '400px',
      data: {
        IllnessInjuryId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();

      }
    });
  }



  onServiceChange(): void {

    this.ClearServicesFields();
    //
    if ((this.firstFormGroup.get('ServiceType').value) == 11 && ((this.UserTypeId == 1) ||(this.UserTypeId == 2))){

      this.ShowServiceEleven = true;
      //
      this.ShowActivites = false;
      this.HideActivities = true;
      this.ShowCaseDetail = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.ShowActivities = true;
      this.ShowAdminAndFinc = true;
      this.ShowFinc = true;
      this.ShowServiceThree = false;
      this.hideService_Schem = false;
      this.ShowServiceNine = false;
      this.hideService = false;
      this.ShowServiceEight = false;
      this.ShowServiceFive = false;
      this.ShowServiceSeven = false;
      this.ShowServiceSix = false;
      this.EmpDetails = true;
      this.disableVisitButton = true;   // + button enabled of Visit and Activity table.
      // this.disableDOCButton =true;
      // this.disablemailButton =false;
      this.editTELEandVISIT = false;

      // this.thirdFormGroup.disable();
      this.OutcomeOrAdmin = 'Outcome';
      this.TitleForEIandTPA = "IME/FCE Facilitation";
      //
      this.firstFormGroup.get('FirstAbsentDate').setValidators(Validators.required);
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();

      this.firstFormGroup.get('ReferrerName').setValidators(Validators.required);
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').setValidators(Validators.required);
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').setValidators(Validators.required);
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantFirstName').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').setValidators(Validators.required);
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      // this.fourthFormGroup.get('PDAID').setValidators(Validators.required);
      // this.fourthFormGroup.get('PDAID').updateValueAndValidity();

      if (this.HcbReferenceId == 0 || this.ActionName == 'Reopen') {
        
        this.disableVisitButton = false;
      }
    }

    if ((this.firstFormGroup.get('ServiceType').value == 11) && (this.UserTypeId == 3)) {
      this.ShowServiceEleven = true;
      //
      this.ShowActivites = false;
      this.HideActivities = true;

      this.ShowCaseDetail = true;
      this.ShowActivities = true;
      this.ShowAdminAndFinc = true;
      this.ShowFinc = true;
      this.ShowNote = true;
      this.ShowDoc = true;

      this.ShowServiceThree = false;

      this.disableVisitButton = true;
      this.editTELEandVISIT = false;
      this.EmpDetails = true;
      this.DisplayCompany = true;
      this.OutcomeOrAdmin = 'Outcome';
      this.TitleForEIandTPA = "IME/FCE Facilitation";
      // this.disableDOCButton=true;
      // this.disablemailButton =false;
      this.thirdFormGroup.enable();
      this.firstFormGroup.disable();
      this.fourthFormGroup.disable();
      //this.RecDate = true;
      this.sixththFormGroup.disable();
      //
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      if (this.firstFormGroup.get('ServiceType').value == 3) {
        this.ShowServiceThree = true;

      }
      this.hideService_Schem = false;
      this.ShowServiceNine = false;
      this.hideService = false;
      this.ShowServiceEight = false;
      this.ShowServiceFive = false;
      this.ShowServiceSeven = false;
      this.ShowServiceSix = false;
    }
    ///
    // 03/07/2020 PM
    if ((this.firstFormGroup.get('ServiceType').value == 2 || this.firstFormGroup.get('ServiceType').value == 4) && ((this.UserTypeId == 1) ||(this.UserTypeId == 2))) {
      this.hideService_Schem = false;

    }
    if ((this.firstFormGroup.get('ServiceType').value == 2 || this.firstFormGroup.get('ServiceType').value == 4) && (this.UserTypeId == 3)) {
      this.hideService_Schem = false;

    }
    //
    if (this.firstFormGroup.get('ServiceType').value == 3 && ((this.UserTypeId == 1) ||(this.UserTypeId == 2))) {
      this.ShowCaseDetail = true;
      this.ShowActivities = true;
      this.ShowAdminAndFinc = true;
      this.ShowFinc = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.hideService = true;
      this.ShowServiceThree = true;
      this.disableVisitButton = false;
      this.OutcomeOrAdmin = 'Administration';
      this.thirdFormGroup.disable();
      this.editTELEandVISIT = true;
      this.hideService_Schem = true; //03/07/2020
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
    }
    if ((this.firstFormGroup.get('ServiceType').value == 3) && (this.UserTypeId == 3)) {
      this.ShowServiceThree = true;
      this.ShowCaseDetail = false;
      this.ShowActivities = true;
      this.ShowAdminAndFinc = false;
      this.ShowFinc = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.hideService = true;
      this.disableVisitButton = true;
      this.thirdFormGroup.enable();
      this.sixththFormGroup.disable();
      this.editTELEandVISIT = false;
      this.hideService_Schem = true;//03/07/2020
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
    }
    if (this.firstFormGroup.get('ServiceType').value == 5) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = false;

      this.ShowCaseDetail = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.ShowActivities = false;
      this.ShowAdminAndFinc = false;
      this.EmpDetails = true;
      this.ShowFinc = true;
      // this.sixththFormGroup.disable();

      this.hideService_Schem = false;
      this.ShowServiceFive = true;
      this.hideService = false;

      this.ShowServiceEight = false;
      this.ShowServiceSix = false;
      this.ShowServiceSeven = false;
      this.ShowServiceNine = false;
      //03/07/2020
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //03/07/2020

      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      // this.firstFormGroup.get('PDAID').clearValidators();
      // this.firstFormGroup.get('PDAID').updateValueAndValidity();
      // this.fourthFormGroup.get('PDAID').clearValidators();
      // this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      //////////
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      // this.firstFormGroup.get('PDAID').setValidators(Validators.required);
      // this.firstFormGroup.get('PDAID').updateValueAndValidity();
      /////////
    }
    // else {
    //   this.ShowServiceFive = false;
    // }
    if (this.firstFormGroup.get('ServiceType').value == 6) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = true;

      this.ShowCaseDetail = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.ShowActivities = false;
      this.ShowAdminAndFinc = false;

      this.ShowFinc = true;

      this.hideService_Schem = false;
      this.ShowServiceSix = true;
      this.hideService = false;

      this.ShowServiceEight = false;
      this.ShowServiceFive = false;
      this.ShowServiceSeven = false;
      this.ShowServiceNine = false;
      //03/07/2020
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //30/07/2020
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
    }
    // else {
    //   this.ShowServiceSix = false;
    // }

    if ((this.firstFormGroup.get('ServiceType').value == 8) && ((this.UserTypeId == 1) ||(this.UserTypeId == 2))) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = true;

      this.ShowCaseDetail = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.ShowActivities = false;
      this.ShowAdminAndFinc = false;
      this.ShowFinc = true;
      this.disableVisitButton = true;
      // this.disablemailButton = false;  commented this  on 20_07_2020

      this.hideService_Schem = false;
      this.ShowServiceEight = true;
      this.hideService = false;

      this.ShowServiceFive = false;
      this.ShowServiceSix = false;
      this.ShowServiceSeven = false;
      this.ShowServiceNine = false;


      //30/07/2020
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
    }
    if ((this.firstFormGroup.get('ServiceType').value == 8) && (this.UserTypeId == 3)) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = true;

      // this.ShowCaseDetail = false;
      // this.ShowNote = false;
      // this.ShowDoc = false;
      // this.ShowActivities = false;
      // this.ShowAdminAndFinc = false;
      this.hideService_Schem = false;
      this.ShowCaseDetail = true;
      // this.ShowActivities = false;
      // this.ShowAdminAndFinc = false;

      this.ShowFinc = true;

      this.ShowNote = true;
      this.ShowDoc = true;
      this.disablemailButton = true;
      // this.thirdFormGroup.enable();
      this.firstFormGroup.disable();
      this.secondFormGroup.disable();
      this.fifthFormGroup.disable();

      this.sixththFormGroup.disable();

      this.disableVisitButton = false;
      this.CMUserdisableDOCButton =true;
      this.disableDOCButton = false;
      // this.fourthFormGroup.disable()


      this.ShowServiceEight = true;
      this.hideService = false;
      //
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
    }

    // else {
    //   this.ShowServiceEight = false;
    // }

    if ((this.firstFormGroup.get('ServiceType').value == 7) && (this.UserTypeId != 3)) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = true;

      this.ShowCaseDetail = true;
      this.ShowActivities = false;
      this.ShowAdminAndFinc = false;

      this.ShowFinc = true;

      this.ShowNote = true;
      this.ShowDoc = true;
      this.disableVisitButton = true;
      //this.disablemailButton = false;  20_07_2020

      this.hideService_Schem = false;
      this.ShowServiceSeven = true;
      this.ShowClientRate = true;
      this.hideService = false;

      this.ShowServiceEight = false;
      this.ShowServiceFive = false;
      this.ShowServiceSix = false;
      this.ShowServiceNine = false;
      //
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      if (this.UserTypeId == 3) {
        this.ShowClientRate = false;
      }
    }

    if ((this.firstFormGroup.get('ServiceType').value == 7) && (this.UserTypeId == 3)) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = true;

      this.ShowCaseDetail = true;
      // this.ShowActivities = false;
      // this.ShowAdminAndFinc = false;

      this.ShowFinc = true;

      this.hideService_Schem = false;
      this.ShowNote = true;
      this.ShowDoc = true;
      // this.thirdFormGroup.enable();
      this.firstFormGroup.disable();
      this.secondFormGroup.disable();
      this.fifthFormGroup.disable();
      // this.fourthFormGroup.disable()
      this.sixththFormGroup.disable();

      this.disableVisitButton = false;
      this.CMUserdisableDOCButton =true;
      this.disableDOCButton = false;
      this.disablemailButton = true;



      this.ShowServiceSeven = true;
      this.ShowClientRate = true;
      this.hideService = false;
      //
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      if (this.UserTypeId == 3) {
        this.ShowClientRate = false;
      }
    }

    // else {
    //   this.ShowServiceSeven = false;
    //   this.ShowClientRate = false;
    // }

    if (this.firstFormGroup.get('ServiceType').value == 9) {
      this.ShowServiceEleven = false;
      //
      // this.ShowActivites = false;
      // this.HideActivities = true;


      this.ShowCaseDetail = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.ShowActivities = false;
      this.ShowAdminAndFinc = false;
      this.ShowFinc = true;

      this.hideService_Schem = false;
      this.ShowServiceNine = true;
      this.hideService = false;

      this.ShowServiceEight = false;
      this.ShowServiceFive = false;
      this.ShowServiceSeven = false;
      this.ShowServiceSix = false;
      //
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('PDAID').clearValidators();
      this.firstFormGroup.get('PDAID').updateValueAndValidity();
      this.fourthFormGroup.get('PDAID').clearValidators();
      this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
    }
    // else {
    //   this.ShowServiceNine = false;
    // }
    if ((this.firstFormGroup.get('ServiceType').value < 5) && ((this.UserTypeId == 1) ||(this.UserTypeId == 2))) {
      this.ShowServiceEleven = false;
      //
      this.ShowActivites = true;
      this.HideActivities = false;
      this.HideClaimStatus = true;

      this.ShowCaseDetail = true;
      this.ShowActivities = true;
      this.ShowAdminAndFinc = true;
      this.ShowFinc = true;
      this.ShowNote = true;
      this.ShowDoc = true;
      this.ShowServiceThree = false;
      this.hideService = true;
      this.disableVisitButton = false;
      // this.disableDOCButton =true;
      // this.disablemailButton =false;
      this.editTELEandVISIT = true; ////
      this.ShowServiceSix = false;
      this.ShowServiceEight = false;
      this.ShowServiceFive = false;
      this.ShowServiceSeven = false;
      this.ShowServiceNine = false;
      this.EmpDetails = true;
      this.DisplayCompany = true;
      this.OutcomeOrAdmin = 'Administration';
      this.TitleForEIandTPA = "External Service Request";
      this.thirdFormGroup.disable();

      this.firstFormGroup.get('FirstAbsentDate').setValidators(Validators.required);
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();

      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').setValidators(Validators.required);
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.fourthFormGroup.get('fourthClaimStatusId').setValidators(Validators.required);
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      // this.firstFormGroup.get('PDAID').clearValidators();
      // this.firstFormGroup.get('PDAID').updateValueAndValidity();
      // this.fourthFormGroup.get('PDAID').setValidators(Validators.required);
      // this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      if (this.firstFormGroup.get('ServiceType').value == 3) {
        this.ShowServiceThree = true;
        // this.fourthFormGroup.get('PDAID').clearValidators();
        // this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      }
    }
    if ((this.firstFormGroup.get('ServiceType').value < 5) && ((this.UserTypeId != 1) && (this.UserTypeId != 2))) {
      this.ShowServiceEleven = false;
      //
      this.ShowActivites = true;
      this.HideActivities = false;

      this.HideClaimStatus = true;
      this.ShowCaseDetail = true;
      this.ShowActivities = true;
      this.ShowAdminAndFinc = true;
      this.ShowFinc = true;
      this.ShowNote = true;
      this.ShowDoc = true;

      this.ShowServiceThree = false;

      this.disableVisitButton = true;
      this.editTELEandVISIT = false;
      this.EmpDetails = true;
      this.DisplayCompany = true;
      this.OutcomeOrAdmin = 'Administration';
      this.TitleForEIandTPA = "External Service Request";
      // this.disableDOCButton=true;
      // this.disablemailButton =false;
      this.thirdFormGroup.enable();
      this.firstFormGroup.disable();
      this.fourthFormGroup.disable();
      this.RecDate = true;
      this.sixththFormGroup.disable();
      //
      this.firstFormGroup.get('FirstAbsentDate').clearValidators();
      this.firstFormGroup.get('FirstAbsentDate').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerName').clearValidators();
      this.firstFormGroup.get('ReferrerName').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerEmail').clearValidators();
      this.firstFormGroup.get('ReferrerEmail').updateValueAndValidity();
      this.firstFormGroup.get('ReferrerTele').clearValidators();
      this.firstFormGroup.get('ReferrerTele').updateValueAndValidity();
      //
      this.firstFormGroup.get('ClaimantFirstName').clearValidators();
      this.firstFormGroup.get('ClaimantFirstName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantLastName').clearValidators();
      this.firstFormGroup.get('ClaimantLastName').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantDOB').clearValidators();
      this.firstFormGroup.get('ClaimantDOB').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantPostcode').clearValidators();
      this.firstFormGroup.get('ClaimantPostcode').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantMobile').clearValidators();
      this.firstFormGroup.get('ClaimantMobile').updateValueAndValidity();
      this.firstFormGroup.get('ClaimantServiceReq').clearValidators();
      this.firstFormGroup.get('ClaimantServiceReq').updateValueAndValidity();
      this.firstFormGroup.get('ClaimStatusId').clearValidators();
      this.firstFormGroup.get('ClaimStatusId').updateValueAndValidity();
      this.fourthFormGroup.get('fourthClaimStatusId').clearValidators();
      this.fourthFormGroup.get('fourthClaimStatusId').updateValueAndValidity();
      // this.firstFormGroup.get('PDAID').clearValidators();
      // this.firstFormGroup.get('PDAID').updateValueAndValidity();
      // this.fourthFormGroup.get('PDAID').clearValidators();
      // this.fourthFormGroup.get('PDAID').updateValueAndValidity();
      if (this.firstFormGroup.get('ServiceType').value == 3) {
        this.ShowServiceThree = true;

      }
      this.hideService = true;
    }
    // if(this.HcbReferenceId==0 || this.ActionName=='Reopen')
    // {
    //   debugger;
    //   this.disableVisitButton=true;
    // }


    // this.ShowTab = true;

    //  else if(this.firstFormGroup.get('ServiceType').value!=3){    //uncomment if we have to completely disable the tab
    //   // this.ShowTab = false;
    //   this.ShowActivities = false;
    //   this.ShowAdminAndFinc = false;

    // }
    this.EnableActivityTab();
  }

  ClearServicesFields(): void {
    this.firstFormGroup.get('FirstAbsentDate').reset();   //03/07/2020
    this.firstFormGroup.get('ClaimantFirstName').reset();
    this.firstFormGroup.get('ClaimantLastName').reset();
    this.firstFormGroup.get('ClaimantDOB').reset();
    this.firstFormGroup.get('ClaimantGender').reset();
    this.firstFormGroup.get('ClaimantCity').reset();
    this.firstFormGroup.get('ClaimantCounty').reset();
    this.firstFormGroup.get('ClaimantAddress1').reset();
    this.firstFormGroup.get('ClientClaimRef').reset();
    this.firstFormGroup.get('DisabilityDescription').reset();
    this.firstFormGroup.get('ClaimantOccupation').reset();
    this.firstFormGroup.get('DateFirstContactApplicant').reset();
    this.firstFormGroup.get('DateClaimFormSent').reset();
    this.firstFormGroup.get('ClaimantPostcode').reset();
    this.firstFormGroup.get('SubsequentActivity').reset();
    this.firstFormGroup.get('ServiceCaseClosed').reset();
    this.firstFormGroup.get('Outcome').reset();
    this.firstFormGroup.get('ReasonForDeclinature').reset();
    this.firstFormGroup.get('HCBAppointedDate').reset();
    this.firstFormGroup.get('PDAID').reset();
    this.firstFormGroup.get('SecondaryPDAID').reset();
    this.firstFormGroup.get('ClaimantMobile').reset();
    this.firstFormGroup.get('ClaimantServiceReq').reset();
    this.fourthFormGroup.get('PDAID').reset();
    this.fourthFormGroup.get('SecondaryPDAID').reset();
    this.firstFormGroup.get('ReferrerName').reset();
    this.firstFormGroup.get('ReferrerEmail').reset();
    this.firstFormGroup.get('ReferrerTele').reset();

  }

  GetSubsequentActivityGrid() {
    this._caseManagment.GetSubsequentActivityGrid(this.HcbReferenceId)
      .subscribe((response: GetSubsequentActivityGrid) => {
        this.SubsequentActivityGrid = new MatTableDataSource<GetSubsequentActivityGrid>(response['result']);
        response['result'].forEach(childObj => {
          if (childObj.reminderCheck == true) {
            childObj.reminderCheck = 'Yes'
          }
          else {
            childObj.reminderCheck = 'No'
          }
        });
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddSubsequentActivity(): void {
    this.dialogRef = this.dialog.open(AddeditSubsequentActivityComponent, {
      disableClose: true,
      width: '500px',
      data: {
        Id: 0,
        HCBReferenceId: this.HcbReferenceId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetSubsequentActivityGrid();
      }
    });
  }

  EditSubsequentActivity(Id: number): void {
    this.dialogRef = this.dialog.open(AddeditSubsequentActivityComponent, {
      disableClose: true,
      width: '400px',
      data: {
        Id: Id
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetSubsequentActivityGrid();
      }
    });
  }

  DeleteSubsequentActivity(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Activity?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._caseManagment.deleteSubsequentbyId(Id)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetSubsequentActivityGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }

  AddAssessor(): void {
    this.dialogRef = this.dialog.open(AddEditBrokerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        AssesorId: 0,
        heading: 'Assessor'
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetAssessorddl();
      }
    });
  }

  GetAssessorddl(): void {
    this.working = true;
    this._masterService.GetAssessorGrid()
      .subscribe((response: any) => {
        this.getddlAssessorDetails = response['result'];
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      });
  }

  GetServcieProviderddl(): void {
    // this.working = true;
    // this._masterService.GetServiceProviderCompany()
    // .subscribe((response: any) => {
    //     this.getddlServiceProvider = response['result'];
    //     this.working = false;
    //   }, (error: any) => {
    //     this.errorText = error;
    //     this.working = false;
    //   });
  }

  GetddlNameOfTrust(): void {
    this._caseManagment.GetddlNameOfTrust()
      .subscribe((response: GetddlNameOfTrust[]) => {
        this.getddlNameOfTrust = response['result'];
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  public tabChanged(tabChangeEvent): void {
    this.selectedIndex = tabChangeEvent.index;
  }

  public nextStepCaseNote() {

    //this.scrollToTop();
    this.selectedIndex += 1;
    let el: HTMLElement = this.myDiv.nativeElement;
    el.click();
    // window.scrollTo(0, 0)
  }
  nextStepAdmin() {
    //this.scrollToTop();  
    if ((this.firstFormGroup.get('ServiceType').value) < 5) {
      this.selectedIndex += 1;
      let el: HTMLElement = this.AdminClick.nativeElement;
      el.click();
    } else {
      this.nextStepFinan();
    }
    // window.scrollTo(0, 0)
  }
  nextStepFinan() {
    //this.scrollToTop();
    this.selectedIndex += 1;
    let el: HTMLElement = this.FinanClick.nativeElement;
    el.click();
    // window.scrollTo(0, 0)
  }

  nextStepActivity() {
    //this.scrollToTop();
    this.selectedIndex += 1;
    let el: HTMLElement = this.ActivityClick.nativeElement;
    el.click();
    // window.scrollTo(0, 0)
  }

  public nextStep() {
    this.selectedIndex += 1;
    let el: HTMLElement = this.DocClick.nativeElement;
    el.click();
  }
  previousStepDetail() {
    let el: HTMLElement = this.DetalClick.nativeElement;
    el.click();
    this.selectedIndex -= 1;
  }
  previousStepFinan() {
    let el: HTMLElement = this.FinanClick.nativeElement;
    el.click();
    this.selectedIndex -= 1;
  }
  previousStepToNote() {
    this.selectedIndex -= 1;
  }
  public previousStep() {
    // this.scrollToTop();
    if ((this.firstFormGroup.get('ServiceType').value) < 5) {
      this.selectedIndex -= 1;
      let el: HTMLElement = this.ActivityClick.nativeElement;
      el.click();
    } else {
      this.previousStepFinan();
    }
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}

