import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { SaveSubsequentActivity } from '../addedit-case-management.model';
import { CaseManagmentService } from '../../case-managment.service';
import * as _moment from 'moment';
import { CommonService } from 'app/service/common.service';
import { AppCustomDirective } from 'app/main/Common/appValidation';
const moment = _moment;

@Component({
  selector: 'app-addedit-subsequent-activity',
  templateUrl: './addedit-subsequent-activity.component.html',
  styleUrls: ['./addedit-subsequent-activity.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddeditSubsequentActivityComponent implements OnInit {

  SubsequentActivity:FormGroup;
  errorText: string;
  working: boolean;
  dialog: any;
  reminder:any;
  Showdate:boolean;
  disabled:boolean;
  CheckStatus:boolean;
  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _caseManagment:CaseManagmentService,
    private _commonService: CommonService,
    public dialogRef: MatDialogRef<AddeditSubsequentActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.Id) {
      this.SubsequentActivity = this._formbuilder.group({
        Id:[this.data.Id],
      //  HCBReferenceId:[this.data.HCBReferenceId],
        SubsequentActivityName:['', Validators.required],
        ReminderCheck:[false],
        ReminderDate:['', [ AppCustomDirective.fromDateValidator, AppCustomDirective.fromDateValidator1]]
      });
    //  this.SetVisitActivity();
    this.SetValues();
    }
    else {
      this.SubsequentActivity = this._formbuilder.group({
        HCBReferenceId:[this.data.HCBReferenceId],
        SubsequentActivityName:['', Validators.required],
        ReminderCheck:[false],
        ReminderDate:['']
      });
    }
  }


  onCheckChange() {
    if(this.SubsequentActivity.get('ReminderCheck').value==true)
    {
      this.Showdate=true;
      this.SubsequentActivity.get('ReminderDate').setValidators([ AppCustomDirective.fromDateValidator, AppCustomDirective.fromDateValidator1]);
    }

    else {
      
      this.Showdate = false;
      this.SubsequentActivity.get('ReminderDate').clearValidators();
      this.SubsequentActivity.get('ReminderDate').updateValueAndValidity();
      this.SubsequentActivity.get('ReminderDate').reset();
    }
  }

  SetValues(): void {
    this._caseManagment.GetSubsequentActivityId(this.data.Id)
      .subscribe((response: any) => {
        this.CheckStatus=response['result'][0].remindercheck;
        this.SubsequentActivity.patchValue({ SubsequentActivityName: response['result'][0].subsequentActivityName });
        this.SubsequentActivity.patchValue({ReminderCheck:response['result'][0].remindercheck});
        this.onCheckChange();
        if (response['result'][0].date != "" && response['result'][0].date != null)
       
          this.SubsequentActivity.patchValue({ ReminderDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].date), month: this._commonService.GetMonthFromDateString(response['result'][0].date), date: this._commonService.GetDateFromDateString(response['result'][0].date) }) });
       //  console.log(this.SubsequentActivity.value.ReminderDate);
        }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
      if(this.CheckStatus != true)
      {
        this.SubsequentActivity.get('ReminderDate').clearValidators();
       this.SubsequentActivity.get('ReminderDate').updateValueAndValidity();
      }
  }

  SaveSubsequentActivity(): void {
  
    if (this.SubsequentActivity.invalid)
      return;

    this.working = true;
    const updateSubsequentActivity: SaveSubsequentActivity = Object.assign({}, this.SubsequentActivity.value);
    // updateOwner.OwnerId=this.data.OwnerId;
    updateSubsequentActivity.ReminderDate=(this.SubsequentActivity.value.ReminderDate == "" || this.SubsequentActivity.value.ReminderDate == null) ? "" : "" +   this._commonService.GetYearFromDateString(this.SubsequentActivity.value.ReminderDate) + "-" + (  this._commonService.GetMonthFromDateString(this.SubsequentActivity.value.ReminderDate) + 1) + "-" +  this._commonService.GetDateFromDateString(this.SubsequentActivity.value.ReminderDate) + " 00:00:00:000";
   //updateSubsequentActivity.ReminderDate= ({ year: this._commonService.GetYearFromDateString(this.SubsequentActivity.value.ReminderDate), month: this._commonService.GetMonthFromDateString(this.SubsequentActivity.value.ReminderDate), date: this._commonService.GetDateFromDateString(this.SubsequentActivity.value.ReminderDate) }).toString() ;
   if (this.data.Id) {
      this._caseManagment.UpdateSubsequentActivity(updateSubsequentActivity)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
       this.working=true;
      this._caseManagment.SaveSubSequentActivity(this.SubsequentActivity.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}



