import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HCBCaseNoteComponent } from './hcbcase-note.component';

describe('HCBCaseNoteComponent', () => {
  let component: HCBCaseNoteComponent;
  let fixture: ComponentFixture<HCBCaseNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HCBCaseNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HCBCaseNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
