
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { CommonService } from 'app/service/common.service';
import { CaseManagmentService } from '../case-managment.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { SaveUpdateTelephoneActivity } from './telephone-activity.model';
import { GetddlTypelistValues } from '../addedit-case-managment/addedit-case-management.model';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import * as _moment from 'moment';
import { AddRemoveFailedCallComponent } from './add-remove-failed-call/add-remove-failed-call.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { FlexAlignDirective } from '@angular/flex-layout';
const moment = _moment;


@Component({
  selector: 'app-addedit-telephone-activity',
  templateUrl: './addedit-telephone-activity.component.html',
  styleUrls: ['./addedit-telephone-activity.component.scss'],
  animations: [fuseAnimations],
  encapsulation: ViewEncapsulation.None
})
export class AddeditTelephoneActivityComponent implements OnInit {

  TelephoneActivity: FormGroup;
  getddlTypeOfVisit: GetddlTypelistValues[];
  getddlTypeOfCall: GetddlTypelistValues[];
  HCBFailedCallList: any[] = [];
  FailedCallList: MatTableDataSource<any>;
  errorText: string;
  working: boolean;
  dialogRef1: any;
  //dialog: any;
  DisAddFailedCall:boolean;

  displayedFailedCallColumns: string[] = ['Id','failedCallDateTime', 'edit'];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _caseManagment: CaseManagmentService,
    private _commonService: CommonService,
    private _matSnackBar: MatSnackBar,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AddeditTelephoneActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit() {

    this._caseManagment.GetddlTypeOfVisit(15)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlTypeOfVisit = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._caseManagment.GetddlTypeOfCall(16)
      .subscribe(
        (response: GetddlTypelistValues[]) => this.getddlTypeOfCall = response['result'],
        (error: any) => {

          console.log(this.getddlTypeOfCall);
          this.errorText = error;
        });

    if (this.data.HCBTelephoneID) {
      this.DisAddFailedCall=false;
      this.TelephoneActivity = this._formbuilder.group({
        HCBTelephoneID: [this.data.HCBTelephoneID],
        HCBReferenceId: [this.data.HCBReferenceId],
        CallDate: [''],
        CallTime: [''],
        TypeofCall: [''],
        CallExpectRTWDateOptimum: [''],
        CallExpectRTWDateMaximum: [''],
        CallCaseMngtRecom: [''],
        FeeCharged: [0],
        //  Note: []
      });

      this.SetTelephoneActivity();
      this.GetFailedCallList();
    }
    else {
      this.TelephoneActivity = this._formbuilder.group({
        HCBReferenceId: [this.data.HCBReferenceId],
        CallDate: [],
        CallTime: [],
        TypeofCall: [''],
        CallExpectRTWDateOptimum: [''],
        CallExpectRTWDateMaximum: [''],
        CallCaseMngtRecom: [''],
        FeeCharged: [0],
        // FailedCall: []
      });
      this.DisAddFailedCall=true;
    }
  }

  SetTelephoneActivity(): void {

    this._caseManagment.GetHCBTelephoneActivity(this.data.HCBTelephoneID)
      .subscribe((response: any) => {

        if (response['result'][0].callDate != "" && response['result'][0].callDate != null)
          this.TelephoneActivity.patchValue({ CallDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].callDate), month: this._commonService.GetMonthFromDateString(response['result'][0].callDate), date: this._commonService.GetDateFromDateString(response['result'][0].callDate) }) });

        var time = (response['result'][0].callTime);
        if (time != null && time != " ") {
          var TelephonecallTime = time.split("T");
          this.TelephoneActivity.patchValue({ CallTime: TelephonecallTime[1] });
        }
        else {
          this.TelephoneActivity.patchValue({ CallTime: 0 });
        }
        // this.TelephoneActivity.patchValue({ CallTime: TelephonecallTime[1] });
        this.TelephoneActivity.patchValue({ TypeofCall: response['result'][0].typeofCall });

        if (response['result'][0].callExpectRTWDateOptimum != "" && response['result'][0].callExpectRTWDateOptimum != null)
          this.TelephoneActivity.patchValue({ CallExpectRTWDateOptimum: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].callExpectRTWDateOptimum), month: this._commonService.GetMonthFromDateString(response['result'][0].callExpectRTWDateOptimum), date: this._commonService.GetDateFromDateString(response['result'][0].callExpectRTWDateOptimum) }) });

        if (response['result'][0].callExpectRTWDateMaximum != "" && response['result'][0].callExpectRTWDateMaximum != null)
          this.TelephoneActivity.patchValue({ CallExpectRTWDateMaximum: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].callExpectRTWDateMaximum), month: this._commonService.GetMonthFromDateString(response['result'][0].callExpectRTWDateMaximum), date: this._commonService.GetDateFromDateString(response['result'][0].callExpectRTWDateMaximum) }) });

        this.TelephoneActivity.patchValue({ CallCaseMngtRecom: response['result'][0].callCaseMngtRecom });
        this.TelephoneActivity.patchValue({ FeeCharged: response['result'][0].feeCharged });
        // this.TelephoneActivity.patchValue({ FailedCall: response['result'][1].failedCallList });
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      })
  }

  AddFailedCall() {

    this.dialogRef1 = this.dialog.open(AddRemoveFailedCallComponent, {
      disableClose: true,
      width: '500px',
      data: {
        HCBReferenceId: this.data.HCBReferenceId,
        HCBTelephoneID: this.data.HCBTelephoneID,
        FailedCallList: this.HCBFailedCallList
      }
    });

    this.dialogRef1.afterClosed().subscribe(result => {
      if (result) {
        if (this.data.HCBTelephoneID > 0) {
          this.GetFailedCallList();
        }

        if (this.HCBFailedCallList.length > 0) {
          this.FailedCallList = new MatTableDataSource<any>(this.HCBFailedCallList);
          this.FailedCallList.sort = this.sort;
          this.FailedCallList.paginator = this.paginator;
        }

      
        // this.ConfirmDialog();

      }
    });
  }



  DeleteFailedCall(failedCallID: number,dta:any): void {


console.log(dta);
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

        if(this.data.HCBTelephoneID==0)
        {
          this.HCBFailedCallList.splice(dta,1); 
          this.FailedCallList = new MatTableDataSource<any>(this.HCBFailedCallList);
        
        }else{
        this._caseManagment.DeleteFailedCall(failedCallID)
          .subscribe((response: any) => {

            this.ConfirmDialog();
            this.GetFailedCallList();
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
        }
      }
    });
  }


  SaveTelephoneActivity(): void {

    const updateHCBTelephoneActivity: SaveUpdateTelephoneActivity = Object.assign({}, this.TelephoneActivity.value);
    updateHCBTelephoneActivity.CallDate = (this.TelephoneActivity.value.CallDate == "" || this.TelephoneActivity.value.CallDate == null) ? "" : "" + this.TelephoneActivity.value.CallDate._i.year + "-" + (this.TelephoneActivity.value.CallDate._i.month + 1) + "-" + this.TelephoneActivity.value.CallDate._i.date + " 00:00:00:000";
    updateHCBTelephoneActivity.CallTime = (this.TelephoneActivity.value.CallTime == "" || this.TelephoneActivity.value.CallTime == null) ? null : "" + this.TelephoneActivity.value.CallTime;
    updateHCBTelephoneActivity.CallExpectRTWDateOptimum = (this.TelephoneActivity.value.CallExpectRTWDateOptimum == "" || this.TelephoneActivity.value.CallExpectRTWDateOptimum == null) ? "" : "" + this.TelephoneActivity.value.CallExpectRTWDateOptimum._i.year + "-" + (this.TelephoneActivity.value.CallExpectRTWDateOptimum._i.month + 1) + "-" + this.TelephoneActivity.value.CallExpectRTWDateOptimum._i.date + " 00:00:00:000";
    updateHCBTelephoneActivity.CallExpectRTWDateMaximum = (this.TelephoneActivity.value.CallExpectRTWDateMaximum == "" || this.TelephoneActivity.value.CallExpectRTWDateMaximum == null) ? "" : "" + this.TelephoneActivity.value.CallExpectRTWDateMaximum._i.year + "-" + (this.TelephoneActivity.value.CallExpectRTWDateMaximum._i.month + 1) + "-" + this.TelephoneActivity.value.CallExpectRTWDateMaximum._i.date + " 00:00:00:000";
    //updateHCBTelephoneActivity.AppointmentDate = (this.TelephoneActivity.value.AppointmentDate == "" || this.TelephoneActivity.value.AppointmentDate == null) ? "" : "" + this.VisitActivity.value.AppointmentDate._i.year + "-" + (this.VisitActivity.value.AppointmentDate._i.month + 1) + "-" + this.VisitActivity.value.AppointmentDate._i.date + " 00:00:00:000";
    updateHCBTelephoneActivity.HCBReferenceId = this.data.HCBReferenceId;
    if (this.data.HCBTelephoneID) {
      this.working = true;
      this._caseManagment.UpdateHCBTelephoneActivity(updateHCBTelephoneActivity)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working = false;
        });

    } else {
 
      this.working = true;
      updateHCBTelephoneActivity.CallTime = (this.TelephoneActivity.value.CallTime == "" || this.TelephoneActivity.value.CallTime == null) ? null : "" + this.TelephoneActivity.value.CallTime + ":00";
      var obj = {
        saveUpdateTelephoneActivity: updateHCBTelephoneActivity,
        saveFailedcall: this.HCBFailedCallList
      };
     
      this._caseManagment.SaveHCBTelephoneActivity(obj)
        .subscribe((response: number) => {
          var ID = response['result'];
          this.dialogRef.close(true);
          this.working=false;
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working=false;
        });
    }
  }


  GetFailedCallList(): void {

    this._caseManagment.GetFailedCallList(this.data.HCBTelephoneID)
      .subscribe((response: any) => {
       
      console.log(response);
        this.FailedCallList = response['result'];

      }, (error: any) => {
        this.errorText = error;

      });
  }

  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }
}