export class SaveUpdateTelephoneActivity
{
    HCBTelephoneID:number;
    HCBReferenceId:number;
    CallDate:string;
    CallTime:string;
    TypeofCall:number;
    CallExpectRTWDateOptimum:string;
    CallExpectRTWDateMaximum:string;
    CallCaseMngtRecom:string;
   // CallNxtSchedule:string;
    FeeCharged:number;
   
}

export class GetTelephoneActivityGrid
{
    HCBTelephoneID:number;
    HCBReferenceId:number;
    CallDate:string;
    CallTime:string;
    TypeofCall:number;
    CallExpectRTWDateOptimum:string;
    CallExpectRTWDateMaximum:string;
    CallCaseMngtRecom:string;
   // CallNxtSchedule:string;
    FeeCharged:number;
}

export class SaveUpdateFailedCall
{
    Id:number;
    FailedCallId:number;
    HCBTelephoneID:number;
    HCBReferenceId:number;
    failCallDate:string;
    failCallTime:string;
}