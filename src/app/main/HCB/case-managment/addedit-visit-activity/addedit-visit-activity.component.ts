import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CaseManagmentService } from '../case-managment.service';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GetVisitActivityById, SaveUpdateHCBVisitActivity } from './visit-activity.model';
import { GetddlTypelistValues } from '../addedit-case-managment/addedit-case-management.model';
import { CommonService } from 'app/service/common.service';
import * as _moment from 'moment';
const moment = _moment;

@Component({
  selector: 'app-addedit-visit-activity',
  templateUrl: './addedit-visit-activity.component.html',
  styleUrls: ['./addedit-visit-activity.component.scss'],
  animations: [fuseAnimations],
  encapsulation: ViewEncapsulation.None
})
export class AddeditVisitActivityComponent implements OnInit {

  VisitActivity: FormGroup;
  getddlTypeOfVisit: GetddlTypelistValues[];
  errorText: string;
  working: boolean;
  dialog: any;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _caseManagment: CaseManagmentService,
    private _commonService:CommonService,
    private _matSnackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AddeditVisitActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  
  ngOnInit() {
    this._caseManagment.GetddlTypeOfVisit(15)
    .subscribe(
      (response: GetddlTypelistValues[]) => this.getddlTypeOfVisit = response['result'],
      (error: any) => {
        this.errorText = error;
      });
    if (this.data.HCBvisitId) {
      this.VisitActivity = this._formbuilder.group({
        HCBVisitId: [this.data.HCBvisitId],
        HCBReferenceId: [this.data.HCBReferenceId],
        AppointmentDate: [''],
        ReportCompDate: [''],
        TYpeOfVisit: [''],
        FeeChargedVisit: ['']
      });
      this.SetVisitActivity();
    }
    else {
      this.VisitActivity = this._formbuilder.group({
        HCBReferenceId: [this.data.HCBReferenceId],
        AppointmentDate: [''],
        ReportCompDate: [''],
        TYpeOfVisit: [''],
        FeeChargedVisit: [0]
      });
    }
  }

  SetVisitActivity(): void {
  
    this._caseManagment.GetVisitActivity(this.data.HCBvisitId)
      .subscribe((response: GetVisitActivityById) => {
        
      //  this.VisitActivity.patchValue({ AppointmentDate: response['result'][0].appointmentDate });
        if (response['result'][0].appointmentDate != "" && response['result'][0].appointmentDate != null)
        this.VisitActivity.patchValue({ AppointmentDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].appointmentDate), month: this._commonService.GetMonthFromDateString(response['result'][0].appointmentDate), date: this._commonService.GetDateFromDateString(response['result'][0].appointmentDate) }) });
       // this.VisitActivity.patchValue({ ReportCompDate: response['result'][0].reportCompDate });
        if (response['result'][0].reportCompDate != "" && response['result'][0].reportCompDate != null)
        this.VisitActivity.patchValue({ ReportCompDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].reportCompDate), month: this._commonService.GetMonthFromDateString(response['result'][0].reportCompDate), date: this._commonService.GetDateFromDateString(response['result'][0].reportCompDate) }) });
        this.VisitActivity.patchValue({ TYpeOfVisit: response['result'][0].tYpeOfVisit });
        this.VisitActivity.patchValue({ FeeChargedVisit: response['result'][0].feeCharged });
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      })
  }

  SaveVisitActivity(): void {

    const updateHCBActivity: SaveUpdateHCBVisitActivity = Object.assign({}, this.VisitActivity.value);
    updateHCBActivity.ReportCompDate = (this.VisitActivity.value.ReportCompDate == "" || this.VisitActivity.value.ReportCompDate == null) ? "" : "" + this.VisitActivity.value.ReportCompDate._i.year + "-" + (this.VisitActivity.value.ReportCompDate._i.month + 1) + "-" + this.VisitActivity.value.ReportCompDate._i.date + " 00:00:00:000";
    updateHCBActivity.AppointmentDate = (this.VisitActivity.value.AppointmentDate == "" || this.VisitActivity.value.AppointmentDate == null) ? "" : "" + this.VisitActivity.value.AppointmentDate._i.year + "-" + (this.VisitActivity.value.AppointmentDate._i.month + 1) + "-" + this.VisitActivity.value.AppointmentDate._i.date + " 00:00:00:000";
    updateHCBActivity.HCBReferenceId = this.data.HCBReferenceId;
    if (this.data.HCBvisitId) {
 
      this.working = true;
      this._caseManagment.UpdateHCBActivity(updateHCBActivity)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working = false;
        });

    } else {

      this.working = true;
      this._caseManagment.SaveHCBActivity(updateHCBActivity)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
        });
    }
  }
  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }

}
