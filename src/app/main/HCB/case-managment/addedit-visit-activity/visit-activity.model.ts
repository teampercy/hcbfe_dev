export class GetVisitActivityById
{
    HCBVisitId:number;
    HCBReferenceId:number;
    AppointmentDate:string;
    ReportCompDate:string;
    FeeCharged:number;
    TYpeOfVisit:number;
}

export class SaveUpdateHCBVisitActivity
{
    HCBReferenceId:number;
    HCBVisitId:number;
    AppointmentDate:string;
    ReportCompDate:string;
    FeeChargedVisit:number;
    TYpeOfVisit:number;
   // Name:string;
}