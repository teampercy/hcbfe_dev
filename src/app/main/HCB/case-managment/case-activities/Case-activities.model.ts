export class SaveUpdateCaseActivity
{
    CaseActivityId:number;
    HCBReferenceId: number;
    DateOfActivity: string;
    ClaimAssessorId: number;
    ActionId: number;
    ServiceProviderId: number;
    ReportDueBy: string;
}