import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA , MatDialog} from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { promise } from 'selenium-webdriver';
import { resolve } from 'q';
import { CaseManagmentService } from '../case-managment.service';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { HCBNote } from '../addedit-case-managment/addedit-case-management.model';

@Component({
  selector: 'app-case-cancel-reason',
  templateUrl: './case-cancel-reason.component.html',
  styleUrls: ['./case-cancel-reason.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class CaseCancelReasonComponent implements OnInit {

  NotesList: any[] = [];
FullName:string;
  errorText: string;
  CaseCancelReason:FormGroup;
  constructor(
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _caseManagment: CaseManagmentService,
    public dialogRef: MatDialogRef<CaseCancelReasonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.FullName = localStorage.getItem('UserName');
    
    this.CaseCancelReason = this._formBuilder.group({
      Reason: ['',Validators.required]
    });

  }

  SaveCancelReason()
  {

 
    if(this.data.HCBRefID>0)
    {
      var today = new Date(); 
      const note: HCBNote = {
        HCBReferenceId: this.data.HCBRefID,
        fullName: this.FullName,
        noteCreatedDate: today,
        noteText: this.CaseCancelReason.value.Reason,
        comment:'Case Cancelled',
        noteID: this.data.noteID
      };
      this.NotesList.push(note);
      this._caseManagment.SaveHCBNote(note)
    .subscribe((response) => {
      // this.ConfirmDialog();
      this.dialogRef.close(this.CaseCancelReason.value.Reason);
      this.ConfirmDialog();
      this.dialogRef.close(this.CaseCancelReason.value.Reason);
      // this.dialogRef.close();
    },
  
      (error: any) => {
        this.errorText = error;
      });
    }else{
      //var today = new Date(); 
      // const note: HCBNote = {
      //   HCBReferenceId: this.data.HcbReferenceId,
      //   fullName: this.FullName,
      //   noteCreatedDate: today,
      //   noteText: this.CaseCancelReason.value.Reason,
      //   comment:'Cancel Case'
      // };
       
      // this.NotesList.push(note);

      // var obj = {

      //   SaveHCBNote: this.NotesList,
      //   SaveUpdateCaseManagmentModel: this.data.UpdateCaseManagment
      //   // saveReferralContact: this.refferalContactList,
      //   // saveAccountContact: this.accountcontactList,
      
      // };

      // this._caseManagment.SaveCaseManagment(obj)
      // .subscribe((response) => {
      //   // this.ConfirmDialog();
         this.dialogRef.close(this.CaseCancelReason.value.Reason);
         this.ConfirmDialog();
         this.dialogRef.close(this.CaseCancelReason.value.Reason);
      // },
  
      // (error: any) => {
      //   this.errorText = error;
      // });

    }
    
      
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }
}
