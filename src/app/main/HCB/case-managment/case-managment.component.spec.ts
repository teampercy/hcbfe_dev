import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseManagmentComponent } from './case-managment.component';

describe('CaseManagmentComponent', () => {
  let component: CaseManagmentComponent;
  let fixture: ComponentFixture<CaseManagmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseManagmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseManagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
