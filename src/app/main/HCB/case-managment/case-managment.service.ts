import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientsService } from 'app/service/http-client.service';
import { GetCaseManagmentModel, GetddlClientModel, GetddlCasemanagmentModel, GetddlServiceModel, GetddlTypelistValues, GetddlServiceType,GetTelephoneActivity, HCBNote, GetHCBVisitActivity, HCBDocumentGrid, HCBDocumentUpload, SaveUpdateCaseManagment, SaveSubsequentActivity, SaveAssessor, GetAnnualReportSummary, } from './addedit-case-managment/addedit-case-management.model';
import { GetVisitActivityById, SaveUpdateHCBVisitActivity } from './addedit-visit-activity/visit-activity.model';
import {SaveUpdateTelephoneActivity, SaveUpdateFailedCall} from './addedit-telephone-activity/telephone-activity.model';
import { environment } from 'environments/environment';
import { AuthenticationService } from 'app/service/authentication.service';
import { HttpClient } from '@angular/common/http';
import { GetHCBClientGrid } from './case-managment.model';
import { GetddlNameOfTrust } from '../dashboard/hcb-dashboard.model';
import{SaveUpdateCaseActivity} from './case-activities/Case-activities.model';

@Injectable({
  providedIn: 'root'
})
export class CaseManagmentService {

  constructor(
    private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { }

    GetHCBClientGrid(model:GetHCBClientGrid): Observable<GetHCBClientGrid|{}> {
    return this._http.postAPI("CaseManagment/GetCaseManagmentList",model);
  }

  GetCaseInfoById(HcbReferenceId: number): Observable<GetCaseManagmentModel | {}> {
    return this._http.getAPI("CaseManagment/GetCaseInfoById?HCBReferenceId=" + HcbReferenceId);
  }

  GetddlClient(): Observable<GetddlClientModel[] | {}> {
    return this._http.getAPI("CaseManagment/GetddlClient");
  }

  GetddlClientContact(ClientId: number): Observable<GetCaseManagmentModel | {}> {
    return this._http.getAPI("CaseManagment/GetddlClientContacts?ClientId=" + ClientId);
  }

  GetddlCaseManager(): Observable<GetddlCasemanagmentModel[] | {}> {
    return this._http.getAPI("CaseManagment/GetddlCaseManagment");
  }

  GetddlServiceRequired(): Observable<GetddlServiceModel[] | {}> {
    return this._http.getAPI("CaseManagment/GetddlServiceRequired");
  }

  GetddlServiceTypeforEdit(): Observable<GetddlServiceType | {}> {
    return this._http.getAPI("CaseManagment/GetddlServiceTypeforEdit");
  }
  GetddlServiceType(ClientId: number): Observable<GetddlServiceType | {}> {
    return this._http.getAPI("CaseManagment/GetddlServiceType?ClientId=" + ClientId);
  }

  GetddlTypeOfCall(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlTypeOfVisit(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetPrimaryICD10Code(){
    return this._http.getAPI("CaseManagment/GetDdlPrimaryICD10Code");
  }

  UpdateCaseManagment(model: SaveUpdateCaseManagment): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/UpdateCaseManagment", model);
  }

  SaveCaseManagment(model: any): Observable<number | {}> {
 
    return this._http.postAPI("CaseManagment/SaveCaseManagment", model);
  }
  SaveCaseManagment1(model: SaveUpdateCaseManagment): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/SaveCaseManagment", model);
  }

  GetTelephoneGrid(HcbReferenceId: number): Observable<GetTelephoneActivity | {}> {
    return this._http.getAPI("CaseManagment/GetTelephoneActivityList?HCBReferenceId=" + HcbReferenceId);
  }

  GetHCBTelephoneActivity(HCBTelephoneID: number): Observable<SaveUpdateTelephoneActivity | {}> {
    return this._http.getAPI("CaseManagment/GetHCBTelephoneActivity?HCBTelephoneID=" + HCBTelephoneID);
  }

  UpdateHCBTelephoneActivity(model: SaveUpdateTelephoneActivity): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/UpdateTelephoneActivity", model);
  }

  SaveHCBTelephoneActivity(model: any): Observable<number | {}> {

    return this._http.postAPI("CaseManagment/SaveTelephoneActivity", model);
  }

  DeleteTelephoneActivity(HCBTelephoneID:number):Observable<any | {}> {
    return this._http.getAPI("CaseManagment/DeleteTelephoneActivity?HCBTelephoneID=" + HCBTelephoneID);
  }

  SaveFailedCall(model: SaveUpdateFailedCall): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/SaveFailedCall", model);
  }

  GetFailedCallList(HCBTelephoneID: number): Observable<any | {}> {
  
    return this._http.getAPI("CaseManagment/GetFailedCallList?HCBTelephoneID=" + HCBTelephoneID);
  }

  DeleteFailedCall(failedCallID:number):Observable<any | {}> {
    return this._http.getAPI("CaseManagment/DeleteFailedCall?FailedCallID=" + failedCallID);
  }

  GetHCBVisitGrid(HcbReferenceId: number): Observable<GetHCBVisitActivity | {}> {
    return this._http.getAPI("CaseManagment/GetHCBVisitActivityGrid?HCBReferenceId=" + HcbReferenceId);
  }

  GetVisitActivity(HCBVisitId: number): Observable<GetVisitActivityById | {}> {
    return this._http.getAPI("CaseManagment/GetHCBVisitActivity?HCBVisitId=" + HCBVisitId);
  }

  UpdateHCBActivity(model: SaveUpdateHCBVisitActivity): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/UpdateHCBVisitActivity", model);
  }

  DeleteVisitActivity(HCBVisitId:number):Observable<GetVisitActivityById | {}> {
    return this._http.getAPI("CaseManagment/DeleteVisitActivity?HCBVisitId=" + HCBVisitId);
  }

  SaveHCBActivity(model: SaveUpdateHCBVisitActivity): Observable<number | {}> {
   
    return this._http.postAPI("CaseManagment/SaveHCBVisitActivity", model);
  }
  
  GetCaseActivityDetails(CaseActivityId:number){
    return this._http.getAPI("CaseManagment/GetCaseActivityDetail?CaseActivityId="+ CaseActivityId)
  }

  GetCaseActivity(HcbReferenceId:number){
    return this._http.getAPI("CaseManagment/GetCaseActivityGrid?HcbReferenceId="+ HcbReferenceId)
  }

  UpdateCasesActivity(model: SaveUpdateCaseActivity): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/UpdateCaseActivity", model);
  }

  SaveCaseActivity(model: SaveUpdateCaseActivity): Observable<number | {}> {
   
    return this._http.postAPI("CaseManagment/SaveCaseActivity", model);
  }

  DeleteCaseActivity(CaseActivityId:number){
    return this._http.getAPI("CaseManagment/DeleteCaseActivity?CaseActivityId="+ CaseActivityId)
  }

  GetSubsequentActivityGrid(HCBReferenceId: number): Observable<GetTelephoneActivity | {}> {
    return this._http.getAPI("CaseManagment/GetSubsequentActivityGrid?HCBReferenceId=" + HCBReferenceId);
  }

  GetSubsequentActivityId(Id: number): Observable<GetVisitActivityById | {}> {
    return this._http.getAPI("CaseManagment/GetSubsequentActivity?Id=" + Id);
  }

  UpdateSubsequentActivity(model: SaveSubsequentActivity): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/UpdateSubsequentActivity", model);
  }

  SaveSubSequentActivity(model: SaveSubsequentActivity): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/SaveSubsequentActivity", model);
  }
  GetImportedResultList()
  {
    return this._http.getAPI("CaseManagment/GetImportedResultList");
  }
  SaveImportedFile(model: File)
  {
    debugger;
    const formData: FormData = new FormData();
    formData.append('fileKey', model, model.name);
    return this.http.post<any>(environment.baseUrl + "CaseManagment/SaveImoprtedDocument", formData, {
      reportProgress: true,
      observe: 'events',
     headers: this._authentication.GetFileHeader()
    });
  }
  // SaveImportedFile(model: File)
  // {
  //   debugger;
  //     const formData: FormData = new FormData();
  //   formData.append('fileKey', model, model.name);
  //   return this._http.postAPI("CaseManagment/SaveImoprtedDocument", model);
    
  // }
  UploadHCBFile(model: File,DocumentModel: HCBDocumentUpload): Observable<any | {}> {
    const formData: FormData = new FormData();
    formData.append('fileKey', model, model.name);
    formData.append('HCBReferenceId',DocumentModel.HCBReferenceId.toString());
    return this.http.post<any>(environment.baseUrl + "CaseManagment/UploadHCBDocument", formData, {
      reportProgress: true,
      observe: 'events',
     headers: this._authentication.GetFileHeader()
    });
  }
  DownloadDocument(Id: number): Observable<any> {
    return this.http.get(environment.baseUrl + "CaseManagment/DownloadDocument?DocumentId=" + Id, {
      reportProgress: true,
      responseType: 'blob'
    });

  }
  ImportNewCases(Cases:any,UserId:any):Observable<any> 
  {
    debugger;
    // const formData: FormData = new FormData();
   
    // formData.append('UserId',UserId.toString());
    // return this.http.post<any>(environment.baseUrl + "CaseManagment/UploadHCBDocument", formData, {
    //   reportProgress: true,
    //   observe: 'events',
    //  headers: this._authentication.GetFileHeader()
    // });
    return this._http.postAPI("CaseManagment/uploadSubscriberfile", Cases);
  }

  ViewDocument(Id: number): Observable<any> {
    return this._http.getAPI("CaseManagment/ViewDcument?DocumentId=" + Id);
  }

  DeleteDocument(Id:number)
  {
    return this._http.getAPI("CaseManagment/DeleteDocument?DocumentId=" + Id);
  }

  GetHCBDocuments(HCBReferenceId:number): Observable<HCBDocumentGrid | {}> {
    return this._http.getAPI("CaseManagment/GetHCBDocuments?HCBReferenceId="+HCBReferenceId);
  }

  GetHCBNote(HcbReferenceId:number): Observable<HCBNote | {}> {
    
    return this._http.getAPI("CaseManagment/GetHCBNote?HCBReferenceId="+HcbReferenceId);
  }
  DeleteNote(noteId:number): Observable<number | {}> {
    return this._http.getAPI("CaseManagment/DeleteHCBNote?Id=" + noteId);

  }
  SearchHCBNote(NoteInput: string,ClientId:number) {
   
    return this._http.getAPI("CaseManagment/SearchHCBNote?NoteInput=" + NoteInput + "&ClientId="+ ClientId);
  }

  SaveHCBNote(model: HCBNote): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/SaveHCBNote", model);
  }

  SendEmail(IsImportant:boolean, usertypeId:number,noteCreatedDate:string,HcbReferenceNo:string, UserID:number,CanWorkAsCM:number) : Observable<number | {}> {
  
    return this._http.getAPI("CaseManagment/SendEmail?IsImportant="+ IsImportant+"&usertypeId="+usertypeId+"&NoteCreatedDate=" +noteCreatedDate+ "&HCBReferenceNo="+HcbReferenceNo+"&UserId="+UserID+ "&CanWorkAsCM="+CanWorkAsCM );
  }
  deleteSubsequentbyId(Id: number): Observable<number | {}> {
    return this._http.getAPI("CaseManagment/DeleteSubsequentActivity?Id=" + Id);
  }

  GetddlNameOfTrust(): Observable<GetddlNameOfTrust | {}> {
    return this._http.getAPI("CaseManagment/GetddlNameOfTrust");
  }

  GetAnnualReportSummaryGrid(HcbReferenceId: number): Observable<GetAnnualReportSummary| {}> {
    return this._http.getAPI("CaseManagment/GetAnnualReportSummaryGrid?HCBReferenceId=" + HcbReferenceId);
  }

  GetAnnualReportSummaryById(AnnualReportId: number): Observable<GetAnnualReportSummary | {}> {
    return this._http.getAPI("CaseManagment/GetAnnualReportSummaryById?AnnualReportId=" + AnnualReportId);
  }

  UpdateAnnualReportSummary(model: GetAnnualReportSummary): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/UpdateAnnualReportSummary", model);
  }

  SaveAnnualReportSummary(model: GetAnnualReportSummary): Observable<number | {}> {
    return this._http.postAPI("CaseManagment/SaveAnnualReportSummary", model);
  }

  DeleteAnnualReportsummary(annualReportId:number): Observable<number | {}> {

    return this._http.getAPI("CaseManagment/DeleteAnnualReportSummary?AnnualReportId=" + annualReportId);

  }

  CheckClaimantExistOrNot(FirstName:string,LastName:string, BirthDate:string,HCBRefId:number): Observable<number | {}> {

    return this._http.getAPI("CaseManagment/CheckClaimantExistOrNot?FirstName=" + FirstName +"&LastName="+LastName+"&BirthDate="+BirthDate+"&HCBRefId="+HCBRefId);

  }

  SaveReason(RefId:number,Reason:string): Observable<number | {}> {

    return this._http.getAPI("CaseManagment/SaveCaseCancelReason?HCBRefId=" + RefId +"&Reason="+Reason);

  }
  SendNewCaseMail(EmailAddress:string,RefID:number): Observable<boolean | {}> {
   
    return this._http.getAPI("CaseManagment/SendNewCaseEmail?EmailAddress=" + EmailAddress + "&RefID=" +RefID);
    
  }

  GetServcieProviderddl() {
    return this._http.getAPI("UserRecord/GetlServiceProviderlist");
  }
}
