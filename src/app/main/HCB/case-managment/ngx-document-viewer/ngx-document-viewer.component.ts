
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params, UrlSegment } from '@angular/router';
import { Location } from '@angular/common';
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { HttpClient, HttpBackend } from '@angular/common/http';
import { CaseManagmentService } from '../case-managment.service';
import { count } from 'console';
import swal from 'sweetalert2';

@Component({
  selector: 'app-ngx-document-viewer',
  templateUrl: './ngx-document-viewer.component.html',
  styleUrls: ['./ngx-document-viewer.component.scss']
})
export class NgxDocumentViewerComponent implements OnInit {

  ID: number;
  hide: boolean = false;
  working: boolean;
  ViewUrl: SafeUrl = '';
  viewer = 'google';
  ViewerUrl: string;
  tabNo: number = 5;
  private sub: any;
  DisplyNgDocViewer: boolean;
  //DisplatIframe:boolean;
  trustedDashboardUrl: SafeUrl;
  i: number = 0;

  @ViewChild('iframeId', { static: false }) iframeId: ElementRef<HTMLElement>;
  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _route: ActivatedRoute,
    private _location: Location,
    private _caseManagment: CaseManagmentService,
    // private _addeditCaseManagment:AddeditCaseManagmentComponent,
    private domSanitizer: DomSanitizer
    // const url: Observable<string> = _route.url.pipe(map(segments => segments.join('')));
  ) { }



  ngOnInit() {
   
    this.working = true;
    this.sub = this._route.params.subscribe(
      params => {
        
        this.ID = params['docName'];
      }
    );
    this.viewDoc();
  }

  viewDoc() {
    this._caseManagment.ViewDocument(this.ID)
      .subscribe(result => {
       
        //        //this._router.navigate(["/case-managment/ngx-document-viewer/","262020165851/CRM_HCBCredentials.txt"]);
        //     //this._router.navigate(["/case-managment/ngx-document-viewer/","291202016432/OOAD using UML.ppt"]);
        //  //   const DocUrl=result.toString();
        //  this.ViewUrl=result.result;
        // this.trustedDashboardUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(result.resul);
        // this.ViewUrl = this.trustedDashboardUrl;
        // this.ViewUrl= "https://azuredev.rhealtech.com/HCBDocView/2032020181759/file%20(5).pdf";

        //////////////////working/////////////////////////////
        var x = result.result.split('.').length;
        console.log(result.result.split('.')[x - 1]);
        if (result.result.split('.')[x - 1] == "MSG" ||
         result.result.split('.')[x - 1] == "msg")
         {
          swal.fire("Can't View this type of File");
        }
        else {
          if (result.result.split('.')[x - 1] == "pdf" || result.result.split('.')[x - 1] == "PDF" ||
            result.result.split('.')[x - 1] == "JPG" || result.result.split('.')[x - 1] == "jpg"
            ||result.result.split('.')[x - 1] == "PNG"||result.result.split('.')[x - 1] == "png"
            ||result.result.split('.')[x - 1] == "txt"||result.result.split('.')[x - 1] == "TXT")
             {
            this.trustedDashboardUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(result.result);
            this.ViewUrl = this.trustedDashboardUrl;
          } else {
            this.trustedDashboardUrl = this.domSanitizer.bypassSecurityTrustResourceUrl
              ('https://view.officeapps.live.com/op/embed.aspx?src=' + result.result);
            this.ViewUrl = this.trustedDashboardUrl;
          }
        }
      }
      );
  }

  close() {

  this._router.navigate([this._location.back()]);
  }

}
