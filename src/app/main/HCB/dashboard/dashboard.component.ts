import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Form } from '@angular/forms';
import { HcbDashboardService } from './hcb-dashboard.service';
import { CaseManagmentService } from '../case-managment/case-managment.service';
import { GetddlBrokerList, GetddlSchemeList } from './hcb-dashboard.model';
import * as _moment from 'moment';
import { CommonService } from 'app/service/common.service';
import { MasterService } from 'app/main/CRM/administration/master/master.service';
import { GetAssessorDetails } from '../case-managment/addedit-case-managment/addedit-case-management.model';
import { OpenCloseSchemeActivityComponent } from './open-close-scheme-activity/open-close-scheme-activity.component';
import { MatDialog } from '@angular/material';
import { GetddlClientModel } from 'app/main/HCB/case-managment/addedit-case-managment/addedit-case-management.model';
import { Router } from '@angular/router';
import { ListOfTotalValuesComponent } from './list-of-total-values/list-of-total-values.component';
import * as shape from 'd3-shape';
import { ElementSchemaRegistry } from '@angular/compiler';

const moment = _moment;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class DashboardComponent implements OnInit {

  single: any[];
  multi: any[];

  // view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Number of Cases';
  curve=shape.curveBasis;
  colorScheme = {
    domain: ['#0088cc']
  };

  colorSchemeOPN={
    domain: ['#669933']
  }
  colorSchemeV={
    domain: ['#669933','#0088cc']
  }
  customColors = [
    { 
      name: 'Biological',
      value: '#70AD47'
    },
    { 
      name: 'Psychological',
      value: '#5B9BD5'
    },
    { 
      name: 'Social',
      value: '#FFC000'
    }
  ];

  ClientActivityErrorMsg:boolean;
  errorText: string;
  ErrorMessage: string;
  ErrorMessage1: string;
  ReasonForReferalMessage: string;
  ClosedCaseAnalysisMessage: string;
  CDMessage: string;
  InMessage: string;
  ReMessage: string;
  ACIDisplay: boolean;
  ACRDisplay: boolean;
  CDDisplay: boolean;
  CCADisplay: boolean;
  ReasonForReferalMessagedisplay: boolean;
  working: boolean;
  dialogRef: any;
  SchemeActivity: FormGroup;
  ActiveClientsInsurance: FormGroup;
  ActiveClients: FormGroup;
  filterProfile: FormGroup;
  ClaimantEmpDemographics: FormGroup;
  ClaimantGender: FormGroup;
  form: FormGroup;
  AverageRTWDuration: FormGroup;
  AverageInterventionDuration: FormGroup;
  ClosedCaseAnalysis: FormGroup;
  ReasonForReferalGrid: FormGroup;
  NoActivityReport: FormGroup;
  ClientActivityByWeek: FormGroup;
  BPSByDefinedDate: FormGroup;
  BPSByDefinedDate1: FormGroup;
  SECBPSByDefinedDate: FormGroup;
  SECBPSByDefinedDate1: FormGroup;
  OutcomesFormGroup: FormGroup;
  widget6: any = {};
  widget5: any = {};
  view: any[] = [418, 420];
  areaview: any[] = [300, 100];
  name: string;
  Id: number;
  SchemeStartDate: any;
  SchemeEndDate: string;
  getBrokerList: any;
  getSchemeList: any;
  getMostActiveClients: any;
  getMostActiveClientsR: any;
  GetClientDemographicGrid: any;
  getReturnToWorkReason: any;
  getReasonForReferal: any;
  getOpenScheme: any[];
  getClosedScheme: any[];
  getListForTotalValues: any[];
  GetBPSGrid: any;
  GetSECBPSGrid: any;
  ClosedCaseAnalysisData: any;
  ReasonForReferalGridData: any;
  OpenScheme: number;
  ClosedScheme: number;
  avgRTWDuration: number;
  ClaimantAvgAge: number;
  InterventionDuration: number;
  UserType: string;
  UserId: number;
  getTotalEmployersI: number;
  getTotalEmployersR: number;
  TotalClosedCasesI: number;
  TotalClosedCasesR: number;
  TotalOpenCasesI: number;
  TotalOpenCasesR: number;
  TotalActiveCaseCountI: number;
  TotalActiveCaseCountR: number;
  TotalAverageDurationI: number;
  TotalAverageDurationR: number;
  ClaimantGenderPercentageMale: number;
  ClaimantGenderPercentageFemale: number;
  NoActivityMessage: string;
  getNoActivityReport: any[];
  getddlNoActivityInterval: any;
  ShowHomeScreen:true;
  selectedIndex: number = 0;
  CloseReasonDisplay: boolean;
  BPSDisplay: boolean;
  BPSMessage:string;
  SECBPSDisplay: boolean;
  SECBPSMessage:string;
  OutcomeDisplay:boolean;
  OutcomeMessage: string;
  openI :number;
  openR : number;
  closeI :number;
  closeR :number;
  CaseActivityByWeekDataOPN:any;
  CaseActivityByWeekDataCLS:any;

  public Top5Schemes: any;
  public ReturnToWorkReason: any;
  public ReasonForReferal: any;
  public BrokerList: any;
  public SchemeList: any;
  public MostActiveClientList: any;
  

  getddlBrokerList: GetddlBrokerList[];
  getddlSchemeList: GetddlSchemeList[];
  getddlAssessorDetails: GetAssessorDetails[];
  ddlCCAAssessorDetails: GetAssessorDetails[];
  ddlCEDAssessorDetails: GetAssessorDetails[];
  ddlCRAAssessorDetails: GetAssessorDetails[];
  ddlARDAssessorDetails: GetAssessorDetails[];
  ddlAIDAssessorDetails: GetAssessorDetails[];
  ddlBPSClientDetails: GetAssessorDetails[];
  getddlClient: GetddlClientModel[];
  CaseActivityByWeekData:any;
  getBPS:any;
  getCaseCloseReasonOutcome:any;
  BPSErrorMsg:boolean;
  SECBPSErrorMsg:boolean;
  ShowBPS:boolean;

  displayedColumns: string[] = ['ClosedCaseReason', 'Percentage'];
  displayColumns: string[] = ['IllnessInjuryName', 'Percentage'];
  NoActivityColumns: string[] = ['ReferenceNo', 'ClientName', 'Id'];
  displayActiveClients: string[] = ['Client', 'ClientType', 'ActiveCases', 'Percentage'];
  DisplayClientDemographic: string[] = ['Gender', 'AverageAge', 'Percentage'];
  displayReasonClose: string[] = ['CloseReason', 'Count', 'Percentage'];
  BPSDetailsClumns: string[] =  ['BPSName', 'Count', 'Percentage'];

  @ViewChild('ThisWeek', { static: false }) ThisWeek: ElementRef<HTMLElement>;
  // @ViewChild('AdminClick', { static: false }) AdminClick: ElementRef<HTMLElement>;
  // @ViewChild('AdminClick', { static: false }) AdminClick: ElementRef<HTMLElement>;

  constructor(
    private _formBuilder: FormBuilder,
    private _hcbDashboardService: HcbDashboardService,
    private _masterService: MasterService,
    private _caseManagment: CaseManagmentService,
    private _commonService: CommonService,
    private _router: Router,
    public dialog: MatDialog
  ) { }

  title = 'Top 5 Schemes';
  type = 'PieChart';
  data = []

  columnNames = ['Scheme Name', 'Percentage'];
  options = { is3D: true };
  width = 500;
  height = 250;

  // title1 = 'Most Active Clients (Retail)';
  title1 = 'Most Active Clients (Insurance)';
  type1 = 'PieChart';
  data1 = []

  title2 = 'Most Active Clients (Retail)';
  // title2 = 'Most Active Clients (Insurance)';
  type2 = 'PieChart';
  data2 = []
  BPSResult =[];
  SECBPSResult =[];
  CaseCloseReasondata=[];
  columnNames1 = ['Company Name', 'Percentage'];
  options1 = { is3D: true };
  width1 = 500;
  height1 = 200;

  ReturnToWorkReasonTitle = 'Return To Work Reason';
  ReturnToWorkReasonType = 'ColumnChart';
  ReturnToWorkReasonData = []

  ReturnToWorkReasonColumnNames = ['ReturnToWorkReason', 'Return To Work Reason'];
  ReturnToWorkReasonOptions = {
    vAxis: { minValue: 0, maxValue: 100, format: '#\'%\'' },
  };
  ReturnToWorkReasonWidth = 500;
  ReturnToWorkReasonHeight = 500;

  ReasonForReferalTitle = 'Reason For Referal';
  ReasonForReferalType = 'ColumnChart';
  ReasonForReferalData = []

  ReasonForReferalColumnNames = ['IllnessInjuryName', 'Claims'];
  ReasonForReferalOptions = {
    vAxis: { minValue: 0, maxValue: 100, format: '#\'%\'' },
  };
  ReasonForReferalWidth = 500;
  ReasonForReferalHeight = 500;

  ngOnInit() {
  
    this.UserId = parseInt(localStorage.getItem('UserId'));
    this.UserType = localStorage.getItem('UserType');

    this.GetddlSchemeList();
    this.GetSchemeList();
    // this.GetAssessorddl();
    this.GetClientBySectorddl(0);

    this.GetReturnToWorkReason();
    this.GetReasonForReferal();
    this.GetddlNoActivityInterval();
    //  this.SchemeChart();
if(this.UserType=='Case Manager')
{
this.ShowBPS=false;
}else{
  this.ShowBPS=true;
}

    this._caseManagment.GetddlClient()
      .subscribe((response: GetddlClientModel[]) => this.getddlClient = response['result'],
        (error: any) => {
          this.errorText = error;
        });


    this.filterProfile = this._formBuilder.group({
      //  Broker: [null],
      daterangepicker: [null],
      Scheme: [null]
    });

    this.AverageRTWDuration = this._formBuilder.group({
      AverageDurationStartDate: [null],
      AverageDurationEndDate: [null],
      RTWClientTypeId: [0],
      RTWClientId: [null]
    });

    this.ClaimantEmpDemographics = this._formBuilder.group({
      ClaimantReceivedStartDate: [null],
      ClaimantReceivedEndDate: [null],
      CEDClientTypeId: [0],
      CEDClientId: [null]
    });

    this.AverageInterventionDuration = this._formBuilder.group({
      AIDClientTypeId:[null],
      AIDClientId:[null],
      InterventionStartDate: [null],
      InterventionEndDate: [null],
      InterventionSchemeName: [null],
      InterventionBrokerName: [null]
    });

    this.ClaimantGender = this._formBuilder.group({
      ClaimantGenderStartDate: [null],
      ClaimantGenderEndDate: [null],
      ClaimantGenderSchemeName: [null],
      ClaimantGenderBrokerName: [null]
    });

    this.form = this._formBuilder.group({
      company: [],
      firstName: [''],
      lastName: [''],
      address: [''],
      address2: [''],
      city: [''],
      state: [''],
      postalCode: [''],
      country: ['']
    });

    this.SchemeActivity = this._formBuilder.group({
      Scheme: [''],
      SchemeStartDate: [''],
      SchemeEndDate: ['']
    });

    this.ClosedCaseAnalysis = this._formBuilder.group({
      //ClosedCaseSchemeName:[''],
      CCAClientTypeId: [0],
      CCABrokerName: [''],
      ClosedCaseStartDate: [''],
      ClosedCaseEndDate: ['']
    });

    this.ReasonForReferalGrid = this._formBuilder.group({
      ClientTypeId: [0],
      RFRClientId: [null],
      ReasonReferalStartDate: [''],
      ReasonReferalEndDate: ['']
    });
    this.ActiveClients = this._formBuilder.group({
      StartDate: [''],
      EndDate: ['']
    });

    this.ActiveClientsInsurance = this._formBuilder.group({
      StartDateI: [''],
      EndDateI: ['']
    });

    this.OutcomesFormGroup= this._formBuilder.group({
      StartDate: [''],
      EndDate: ['']
    });

    this.NoActivityReport = this._formBuilder.group({
      ClientName: [],
      NoActivityInterval: ['']
    });
    this.ClientActivityByWeek= this._formBuilder.group({
      ActivityFilter: [0]
     
    });
    this.BPSByDefinedDate= this._formBuilder.group({
      BPSDuration: [0],
      BPSClientTypeId: [],
      BPSClientId: [],
    });
    this.BPSByDefinedDate1= this._formBuilder.group({
      BPSClientTypeId: [],
      BPSClientId: [],
      BPSStartDate:[],
      BPSEndDate:[]
    });
    this.SECBPSByDefinedDate= this._formBuilder.group({
      SECBPSDuration: [0],
      SECBPSClientTypeId: [],
      SECBPSClientId: [],
    });
    this.SECBPSByDefinedDate1= this._formBuilder.group({
      SECBPSClientTypeId: [],
      SECBPSClientId: [],
      SECBPSStartDate:[],
      SECBPSEndDate:[]
    });
    // this.SearchCase();
    this.SearchActiveClientsI(0);
    this.SearchActiveClientsR(0);
    this.SearchRTW();
    this.SearchInterventionDuration();
    this.GetHCBDashboardTotalValues();
    this.SearchClientDemographic();
    this.SearchClaimantGender();
    this.GetClosedCaseAnalysisGrid();
    this.SearchReasonForReferal();
    this.SearchNoActivityReport();
   // this.ActivityByDefinedWeekDefault();
   this.ActivityByDefinedWeek(0);
    this.BPSByDefinedDates(0);
    this.SECBPSByDefinedDates(0);
    this.CaseCloseReasonOutcome();
    this.SearchBPS();
    this.SearchSECBPS();
    this.ActivityByDefinedDateMaster(0);
  
  //  this.ActivityByDefinedDateDetailOPN(0);
     
    // function percentTickFormatting(val: any): string {
    //   return val.toLacaleString('en-us', {syle: 'percent', 
    //   maximumSignificantDigits: 1});
    //   } 
  } 
  yAxisTickFormatting(value){ 
  
    return this.percentTickFormatting(value);
  }

   percentTickFormatting(val: any) {
    return val.toLocaleString();
  }

  // ActivityByDefinedDateDetailOPN(event:any):void{
  //   debugger;
  //       this._hcbDashboardService.ActivityByDefinedDateDetailOPN(0,this.UserId, this.UserType)
  //       .subscribe((response) => {
  //         debugger;
  //         let mainChart3 = [];
  //         this.CaseActivityByWeekData = response['result'];
  //         if(response['result'].length==0)
  //         {
  //           this.CaseActivityByWeekData.forEach(element => {
    
  //             let mod1 = new ChartBasicElements();
  //             // let mode2 = new ChartBasicElements()
  //             mod1.name = element.daysName;
  //             mod1.value=element.noCases;
              
  //             // mod1.series.push();
  //             mainChart3.push(mod1);
  //           });
  //           this.ClientActivityErrorMsg=true;
          
  //        Object.assign(this, {mainChart3 })
  //         }else{
          
  //         if (this.CaseActivityByWeekData.length > 0) {
  //           this.CaseActivityByWeekData.forEach(element => {
    
  //             let mod1 = new ChartBasicElements();
  //             mod1.name = element.daysName;
  //             mod1.value = element.noCases;
  //             mainChart3.push(mod1);
  //           });
  //           this.ClientActivityErrorMsg=false;
  //        Object.assign(this, {mainChart3 })
  //         }
  //       }
  //     });
  //     }
    

  ActivityByDefinedDateMaster(value:any):void{

        this._hcbDashboardService.ActivityByDefinedDateMaster(value,this.UserId, this.UserType)
        .subscribe((response) => {
     
          this.openI = response['result'][0].openI;
          this.openR = response['result'][0].openR;
          this.closeI = response['result'][0].closeI;
          this.closeR = response['result'][0].closeR;
        },
        (error: any) => {
          this.errorText = error;
        });
      }

  BPSByDefinedDates(value:any):void{
    
        this._hcbDashboardService.BPSByDefinedDates(value,this.UserId, this.UserType)
        .subscribe((response) => {
          
          this.getBPS = response['result'];
       
          let mainChart1 = [];
          if (response['result'].length > 0) {
            response['result'].forEach(element => {
  
              let mod1 = new ChartBasicElements();
              mod1.name = element.name;
              mod1.value = element.pdaCount;
             // mod1.labels = element.tempPercentage;
              mainChart1.push(mod1);
            });
          
  
          // this.MostActiveClientList = mainChart;
          const mappedToArray = mainChart1.map(d => Array.from(Object.values(d)))
          this.MostActiveClientList = mappedToArray;
          //  this.data1 = this.MostActiveClientList;
          this.BPSResult = mainChart1;
          this.BPSErrorMsg=false;
           console.log(mainChart1);
          }else{
            this.BPSResult = mainChart1;
            this.BPSErrorMsg=true;
          }
          this.working = false;
        },
          (error: any) => {
            this.errorText = error;
          });
  
      }
      SECBPSByDefinedDates(value:any):void{
    
        this._hcbDashboardService.SECBPSByDefinedDates(value,this.UserId, this.UserType)
        .subscribe((response) => {
  
          let mainChart1 = [];
          if (response['result'].length > 0) {
            response['result'].forEach(element => {
  
              let mod1 = new ChartBasicElements();
              mod1.name = element.name;
              mod1.value = element.pdaCount;
             // mod1.labels = element.tempPercentage;
              mainChart1.push(mod1);
            });
          
  
          // this.MostActiveClientList = mainChart;
          const mappedToArray = mainChart1.map(d => Array.from(Object.values(d)))
          // this.MostActiveClientList = mappedToArray;
          // this.data1 = this.MostActiveClientList;
          this.SECBPSResult = mainChart1;
          this.SECBPSErrorMsg=false;
           console.log(mainChart1);
          }else{
            this.SECBPSResult = mainChart1;
            this.SECBPSErrorMsg=true;
          }
          this.working = false;
        },
          (error: any) => {
            this.errorText = error;
          });
  
      }
    


      ActivityByDefinedWeek(event:any):void{

this.ActivityByDefinedDateMaster(event);
this.GetDashboardAreaChartOPN(event,this.UserId, this.UserType);
this.GetDashboardAreaChartCLS(event,this.UserId, this.UserType);
this.GetDashboardAreaChartOPNI(event,this.UserId, this.UserType);
this.GetDashboardAreaChartOPNR(event,this.UserId, this.UserType);
this.GetDashboardAreaChartCLSI(event,this.UserId, this.UserType);
this.GetDashboardAreaChartCLSR(event,this.UserId, this.UserType);
//this.GetDashboardVertiChart(0,this.UserId, this.UserType);



    this._hcbDashboardService.GetCaseActivityDefinedByWeek(event,this.UserId, this.UserType)
    .subscribe((response) => {
   
      let mainChart4 = [];
      let x=0;
      this.CaseActivityByWeekData = response['result'];
      if(response['result'].length==0)
      {

        // this.CaseActivityByWeekData.forEach(element => {

        //   let mod1 = new ChartBasicElements();
        //   // let mode2 = new ChartBasicElements()
        //   mod1.name = element.daysName;
        //   mod1.value=element.noCases;
          
        //   // mod1.series.push();
        //   mainChart3.push(mod1);
        // });
        this.ClientActivityErrorMsg=true;
      
     Object.assign(this, {mainChart4 })
   
    
      }else{
      
      if (this.CaseActivityByWeekData.length > 0) {
 
        let count=0;
        let m=0;
        let repeat:any[]=[]; 
        let k=0;
        for(let i =0;i<this.CaseActivityByWeekData.length;i++)
        {
         
     
          if(repeat.length>0 && i==repeat[k])
          {
            console.log("repeated I: "+i);
           k++; 
          }else{
          let mod1 = new ChartBasicElements();
          mod1.series=[];
          console.log(this.CaseActivityByWeekData[i]);
          mod1.name=this.CaseActivityByWeekData[i].daysName;
          console.log(mod1.name);
          for(let j =i+1;(j<this.CaseActivityByWeekData.length) ;j++)
          {
           
            console.log(this.CaseActivityByWeekData[j]);
           
             if((this.CaseActivityByWeekData[i].rowNo==this.CaseActivityByWeekData[j].rowNo))
             {
                console.log(this.CaseActivityByWeekData[j].rowNo);
                m=this.CaseActivityByWeekData[j];
                if(this.CaseActivityByWeekData[i].caseType=='open')
                {
                  let mod2 = new serieselement();
                  let mod3 = new serieselement();
                  mod2.name=this.CaseActivityByWeekData[i].caseType;
                  mod2.value=this.CaseActivityByWeekData[i].noCases;
                  mod1.series.push(mod2);
               
                  mod3.name=this.CaseActivityByWeekData[j].caseType;
                  mod3.value=this.CaseActivityByWeekData[j].noCases;
                  mod1.series.push(mod3);
                  
                }
                else{
                  let mod2 = new serieselement();
        let mod3 = new serieselement();
                  mod2.name=this.CaseActivityByWeekData[j].caseType;
                  mod2.value=this.CaseActivityByWeekData[j].noCases;
                  mod1.series.push(mod2);
               
                  mod3.name=this.CaseActivityByWeekData[i].caseType;
                  mod3.value=this.CaseActivityByWeekData[i].noCases;
                  mod1.series.push(mod3);
                
                }
                count++; 
                repeat.push(j);
                console.log("reapeted:"+repeat);
             }
             else{
              //j++;
              
             }
            
          }
          if(count==0)
          {
            let y=0;
               if(this.CaseActivityByWeekData[i].caseType=='open')
              {
                let mod2 = new serieselement();
                let mod3 = new serieselement(); 
                mod2.name=this.CaseActivityByWeekData[i].caseType;
                mod2.value=this.CaseActivityByWeekData[i].noCases;
                // mod1.series.push(mod2);
                mod1.series.push(mod2);
                
                mod3.name='close';
                mod3.value=0;
                mod1.series.push(mod3);
              }
              else{
                let mod2 = new serieselement();
        let mod3 = new serieselement();
                mod2.name='open';
                mod2.value=0;
                mod1.series.push(mod2);
              
                mod3.name=this.CaseActivityByWeekData[i].caseType;
                mod3.value=this.CaseActivityByWeekData[i].noCases;
                mod1.series.push(mod3);
              
              }
          }
          //this.CaseActivityByWeekData[i]
          mainChart4.push(mod1);
          console.log(mainChart4); 
          count=0;
        //  mod1.series=[];
          x++;
        }
      }
        // this.CaseActivityByWeekData.forEach(element => {

        //   let mod1 = new ChartBasicElements();
        //   mod1.name = element.daysName;
        //   mod1.value = element.noCases;
        //   mainChart3.push(mod1);
        // });
        this.ClientActivityErrorMsg=false;
     Object.assign(this, {mainChart4 })
 
      }
    }
  });
  }

  ActivityByDefinedWeekD(event: any):void{

  
    this._hcbDashboardService.GetCaseActivityDefinedByWeek(event.value,this.UserId, this.UserType)
    .subscribe((response) => {

      let mainChart3 = [];
 
    
      this.CaseActivityByWeekData = response['result'];
      if(response['result'].length==0)
      {
        this.CaseActivityByWeekData.forEach(element => {

          let mod1 = new ChartBasicElements();
          mod1.name = element.daysName;
          mod1.value = element.noCases;
          mainChart3.push(mod1);
        });
        this.ClientActivityErrorMsg=true;
       
Object.assign(this, {mainChart3 })
this.ActivityByDefinedDateMaster(event.value);
this.GetDashboardAreaChartOPN(event.value,this.UserId, this.UserType);
this.GetDashboardAreaChartCLS(event.value,this.UserId, this.UserType);
this.GetDashboardAreaChartOPNI(event.value,this.UserId, this.UserType);
this.GetDashboardAreaChartOPNR(event.value,this.UserId, this.UserType);
this.GetDashboardAreaChartCLSI(event.value,this.UserId, this.UserType);
this.GetDashboardAreaChartCLSR(event.value,this.UserId, this.UserType);
      }else{
      
      if (this.CaseActivityByWeekData.length > 0) {
        this.CaseActivityByWeekData.forEach(element => {

          let mod1 = new ChartBasicElements();
          mod1.name = element.daysName;
          mod1.value = element.noCases;
          mainChart3.push(mod1);
          // mod1.series.push(mainChart3);
          // mainChart4.push(mod1.series);
        });
        this.ClientActivityErrorMsg=false;
       // Object.assign(this, {mainChart4})
     Object.assign(this, {mainChart3 })
     this.ActivityByDefinedDateMaster(event.value);
     this.GetDashboardAreaChartOPN(event.value,this.UserId, this.UserType);
     this.GetDashboardAreaChartCLS(event.value,this.UserId, this.UserType);
     this.GetDashboardAreaChartOPNI(event.value,this.UserId, this.UserType);
     this.GetDashboardAreaChartOPNR(event.value,this.UserId, this.UserType);
     this.GetDashboardAreaChartCLSI(event.value,this.UserId, this.UserType);
     this.GetDashboardAreaChartCLSR(event.value,this.UserId, this.UserType);
      }
    }
  });
  }


  GetddlNoActivityInterval(): void {
    this._masterService.GetNoActivityInterval(27)
      .subscribe((response) => {
        this.getddlNoActivityInterval = response['result'];
      })
  }

//   SearchCloseReasonOutcomes(): void{
//     this.working = true;

//     var StartDate = (this.OutcomesFormGroup.value.StartDate == "" || this.OutcomesFormGroup.value.StartDate == null) ? "" : "" + this.OutcomesFormGroup.value.StartDate._i.year + "-" + (this.OutcomesFormGroup.value.StartDate._i.month + 1) + "-" + this.OutcomesFormGroup.value.StartDate._i.date + " 00:00:00:000";
//     var EndDate = (this.OutcomesFormGroup.value.EndDate == "" || this.OutcomesFormGroup.value.EndDate == null) ? "" : "" + this.OutcomesFormGroup.value.EndDate._i.year + "-" + (this.OutcomesFormGroup.value.EndDate._i.month + 1) + "-" + this.OutcomesFormGroup.value.EndDate._i.date + " 00:00:00:000";

//     this._hcbDashboardService.GetCloseReasonOutcomesDetail(StartDate, EndDate, this.UserId, this.UserType)
//       .subscribe((response: any) => {
// debugger;
//         this.getCaseCloseReasonOutcome = response['result'].table;
   
//         this.working = false;
//       },
//         (error: any) => {
//           this.errorText = error;
//         });
//   }

  CaseCloseReasonOutcome(): void {

    this.working = true;

    var StartDate = (this.OutcomesFormGroup.value.StartDate == "" || this.OutcomesFormGroup.value.StartDate == null) ? "" : "" + this.OutcomesFormGroup.value.StartDate._i.year + "-" + (this.OutcomesFormGroup.value.StartDate._i.month + 1) + "-" + this.OutcomesFormGroup.value.StartDate._i.date + " 00:00:00:000";
    var EndDate = (this.OutcomesFormGroup.value.EndDate == "" || this.OutcomesFormGroup.value.EndDate == null) ? "" : "" + this.OutcomesFormGroup.value.EndDate._i.year + "-" + (this.OutcomesFormGroup.value.EndDate._i.month + 1) + "-" + this.OutcomesFormGroup.value.EndDate._i.date + " 00:00:00:000";

    this._hcbDashboardService.CaseCloseReasonOutcome(this.UserId, this.UserType,StartDate, EndDate)
      .subscribe((response: any) => {

        this.getCaseCloseReasonOutcome = response['result'];
        if (response['result'].length > 0) {
         
          this.getCaseCloseReasonOutcome = response['result']
          this.OutcomeDisplay = false;
          this.OutcomeMessage = " ";
          let mainChart1 = [];
          if (this.getCaseCloseReasonOutcome.length > 0) {
            this.getCaseCloseReasonOutcome.forEach(element => {
  
              let mod1 = new ChartBasicElements();
              mod1.name = element.tempReasonName;
              mod1.value = element.tempReasonCount;
             // mod1.labels = element.tempPercentage;
              mainChart1.push(mod1);
            });
          }
  
          // this.MostActiveClientList = mainChart;
          const mappedToArray = mainChart1.map(d => Array.from(Object.values(d)))
         // this.MostActiveClientList = mappedToArray;
          //  this.data1 = this.MostActiveClientList;
          this.CaseCloseReasondata = mainChart1;
           console.log(mainChart1);
           console.log(this.data1);
         
        } else {
          this.getCaseCloseReasonOutcome = response['result']
          let mainChart1 = [];
          this.CaseCloseReasondata = mainChart1;
          this.OutcomeDisplay = true;
          this.OutcomeMessage = "No Data Found";
        }
        this.working = false;
      
      },
        (error: any) => {
          this.errorText = error;
        });
  }


  SearchActiveClientsI(clientTypeId: number): void {

    this.working = true;

    var ActiveStartDate = (this.ActiveClientsInsurance.value.StartDateI == "" || this.ActiveClientsInsurance.value.StartDateI == null) ? "" : "" + this.ActiveClientsInsurance.value.StartDateI._i.year + "-" + (this.ActiveClientsInsurance.value.StartDateI._i.month + 1) + "-" + this.ActiveClientsInsurance.value.StartDateI._i.date + " 00:00:00:000";
    var ActiveEndDate = (this.ActiveClientsInsurance.value.EndDateI == "" || this.ActiveClientsInsurance.value.EndDateI == null) ? "" : "" + this.ActiveClientsInsurance.value.EndDateI._i.year + "-" + (this.ActiveClientsInsurance.value.EndDateI._i.month + 1) + "-" + this.ActiveClientsInsurance.value.EndDateI._i.date + " 00:00:00:000";

    this._hcbDashboardService.GetMostActiveClients(ActiveStartDate, ActiveEndDate, this.UserId, this.UserType, clientTypeId)
      .subscribe((response: any) => {

        this.getMostActiveClients = response['result'].table;
        this.getMostActiveClients.forEach(element => {

          if (element.tempClientTypeId == 1) {
            element.tempClientType = 'Insurance';
          } else {
            element.tempClientType = 'Retail';
          }
        });
        let mainChart1 = [];
        if (this.getMostActiveClients.length > 0) {
          this.getMostActiveClients.forEach(element => {

            let mod1 = new ChartBasicElements();
            mod1.name = element.tempCompanyName;
            mod1.value = element.tempClientCount;
           // mod1.labels = element.tempPercentage;
            mainChart1.push(mod1);
          });
        }

        // this.MostActiveClientList = mainChart;
        const mappedToArray = mainChart1.map(d => Array.from(Object.values(d)))
        this.MostActiveClientList = mappedToArray;
        //  this.data1 = this.MostActiveClientList;
        this.data1 = mainChart1;
         console.log(mainChart1);
         console.log(this.data1);
        // this.data1  = [
        //   {
        //     "name": "Retired",
        //     "value": 20,
        //     "label": "20%"
        //   },
        //   {
        //     "name": "Employed",
        //     "value": 70,
        //     "label": "70%"
        //   },
        //   {
        //     "name": "Unemployed",
        //     "value": 10,
        //     "label": "10%"
        //   }
        // ];
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
        });
  }



  SearchActiveClientsR(clientTypeId: number): void {

    this.working = true;

    var ActiveStartDate = (this.ActiveClients.value.StartDate == "" || this.ActiveClients.value.StartDate == null) ? "" : "" + this.ActiveClients.value.StartDate._i.year + "-" + (this.ActiveClients.value.StartDate._i.month + 1) + "-" + this.ActiveClients.value.StartDate._i.date + " 00:00:00:000";
    var ActiveEndDate = (this.ActiveClients.value.EndDate == "" || this.ActiveClients.value.EndDate == null) ? "" : "" + this.ActiveClients.value.EndDate._i.year + "-" + (this.ActiveClients.value.EndDate._i.month + 1) + "-" + this.ActiveClients.value.EndDate._i.date + " 00:00:00:000";

    this._hcbDashboardService.GetMostActiveClients(ActiveStartDate, ActiveEndDate, this.UserId, this.UserType,clientTypeId)
      .subscribe((response: any) => {

        this.getMostActiveClientsR = response['result'].table1;
        this.getMostActiveClientsR.forEach(element => {
          if (element.tempClientTypeId == 1) {
            element.tempClientType = 'Insurance';
          } else {
            element.tempClientType = 'Retail';
          }
        });
        let mainChart2 = [];
        if (this.getMostActiveClientsR.length > 0) {
          this.getMostActiveClientsR.forEach(element => {

            let mod1 = new ChartBasicElements();
            mod1.name = element.tempCompanyName;
            mod1.value = element.tempClientCount;
            mainChart2.push(mod1);
          });
        }

        // this.MostActiveClientList = mainChart;
        const mappedToArray = mainChart2.map(d => Array.from(Object.values(d)))
        this.MostActiveClientList = mappedToArray;
       // this.data2 = this.MostActiveClientList;
       this.data2 = mainChart2;
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  GetHCBDashboardTotalValues(): void {

    this._hcbDashboardService.GetHCBDashboardTotalValues(this.UserId, this.UserType)
      .subscribe((response: any) => {

        this.getTotalEmployersI = response['result'][0].tempTotalEmployersI
        this.getTotalEmployersR = response['result'][0].tempTotalEmployersR
        this.TotalClosedCasesI = response['result'][0].tempTotalClosedCasesI
        this.TotalClosedCasesR = response['result'][0].tempTotalClosedCasesR
        this.TotalOpenCasesI = response['result'][0].tempTotalOpenCasesI
        this.TotalOpenCasesR = response['result'][0].tempTotalOpenCasesR
        this.TotalAverageDurationI = response['result'][0].tempAverageRTWDurationI
        this.TotalAverageDurationR = response['result'][0].tempAverageRTWDurationR
        this.TotalActiveCaseCountI = response['result'][0].tempTotalCaseCountI
        this.TotalActiveCaseCountR = response['result'][0].tempTotalCaseCountR
      },
        (error: any) => {
          this.errorText = error;
        });

  }

  GetSchemeList(): void {
    this.working = true;
    this._hcbDashboardService.GetDashboardTopFiveSchemeList(this.UserId, this.UserType)
      .subscribe((response: any) => {
        this.getSchemeList = response['result'];
        let mainChart = [];
        if (this.getSchemeList) {
          this.getSchemeList.forEach(element => {
            let mod = new ChartBasicElements();
            mod.name = element.schemeName;
            mod.value = element.schemeCount;
            mainChart.push(mod);
          });
        }

        const mappedToArray = this.getSchemeList.map(d => Array.from(Object.values(d)))
        this.Top5Schemes = mappedToArray;
        this.data = this.Top5Schemes;
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  GetddlSchemeList(): void {
    this._hcbDashboardService.GetddlSchemeList(this.UserId, this.UserType)
      .subscribe((response: GetddlSchemeList[]) => {
        this.getddlSchemeList = response['result']
      },
        (error: any) => {
          this.errorText = error;
        });
  }


  // onCaseChange(event: any): void {

  //   this.GetClientBySectorddl(event.source.value);
  // }

  onChangeBPSClientType(event: any): void {

    if (event != 0) {
      //this.AverageRTWDuration.controls['RTWClientId'].setValue(0, { onlySelf: true });
      this.BPSByDefinedDate1.get('BPSClientId').reset()
    }
    this._hcbDashboardService.GetClientBySectorGrid(event.source.value,this.UserId,this.UserType)
      .subscribe((response: any) => {
   
        this.ddlBPSClientDetails = response['result'];
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
        this.working = false;

      });
  }


  onCaseChange(event: any): void {
    if (event != 0) {
      //this.AverageRTWDuration.controls['RTWClientId'].setValue(0, { onlySelf: true });
      this.AverageRTWDuration.get('RTWClientId').reset()
    }
    this._hcbDashboardService.GetClientBySectorGrid(event.source.value,this.UserId,this.UserType)
      .subscribe((response: any) => {

        this.ddlARDAssessorDetails = response['result'];
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
        this.working = false;

      });
  }

  onClosedCaseChange(event: any): void {
    if (event != 0) {
     // this.ClosedCaseAnalysis.controls['CCABrokerName'].setValue(0, { onlySelf: true });
     this.ClosedCaseAnalysis.get('CCABrokerName').reset();
    }
    this._hcbDashboardService.GetClientBySectorGrid(event.source.value,this.UserId,this.UserType)
      .subscribe((response: any) => {

        this.ddlCCAAssessorDetails = response['result'];
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
        this.working = false;

      });
    // this.ddlCCAAssessorDetails=this.GetClientBySectorddl(event.source.value);
    //this.ddlCCAAssessorDetails=this.getddlAssessorDetails;
  }

  onAIDCaseChange(event: any): void {
    if (event != 0) {
     // this.AverageInterventionDuration.controls['AIDClientId'].setValue(0, { onlySelf: true });
     this.AverageInterventionDuration.get('AIDClientId').reset();
    }
    this._hcbDashboardService.GetClientBySectorGrid(event.source.value,this.UserId,this.UserType)
      .subscribe((response: any) => {

        this.ddlAIDAssessorDetails = response['result'];
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
        this.working = false;

      });
    // this.ddlCCAAssessorDetails=this.GetClientBySectorddl(event.source.value);
    //this.ddlCCAAssessorDetails=this.getddlAssessorDetails;
  }

  onReferralCaseChange(event: any): void {

    if (event != 0) {
      //this.ReasonForReferalGrid.controls['RFRClientId'].setValue(0, { onlySelf: true });
      this.ReasonForReferalGrid.get('RFRClientId').reset();
    }
    this._hcbDashboardService.GetClientBySectorGrid(event.source.value,this.UserId,this.UserType)
      .subscribe((response: any) => {

        this.ddlCRAAssessorDetails = response['result'];
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
        this.working = false;

      });

  }

  onClaimantCaseChange(event: any): void {
   
    if (event != 0) {
     // this.ClaimantEmpDemographics.controls['CEDClientId'].setValue(0, { onlySelf: true });
     this.ClaimantEmpDemographics.get('CEDClientId').reset();
    }
    this._hcbDashboardService.GetClientBySectorGrid(event.source.value,this.UserId,this.UserType)
      .subscribe((response: any) => {

        this.ddlCEDAssessorDetails = response['result'];
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
        this.working = false;

      });
    //this.ddlCEDAssessorDetails=this.GetClientBySectorddl(event.source.value);
    // this.ddlCEDAssessorDetails=this.getddlAssessorDetails;
  }


  GetClientBySectorddl(clientTypeId: any): void {

    this.working = true;

    this._hcbDashboardService.GetClientBySectorGrid(clientTypeId,this.UserId,this.UserType)
      .subscribe((response: any) => {

        this.getddlAssessorDetails = response['result'];
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      });
  }

  // end
  SearchBPS(){
  
  this.working = true;
  // var SchemeAgeId= this.AverageClaimantAge.value.ClaimantAgeBrokerName;
  var BPSTypeId = this.BPSByDefinedDate1.value.BPSClientTypeId == '' || this.BPSByDefinedDate1.value.BPSClientTypeId == undefined ? 0 : this.BPSByDefinedDate1.value.BPSClientTypeId;
  var BPSClientId = this.BPSByDefinedDate1.value.BPSClientId == '' || this.BPSByDefinedDate1.value.BPSClientId == undefined ? 0 : this.BPSByDefinedDate1.value.BPSClientId;
  // var AssessorAgeId = this.AverageClaimantAge.get('ClaimantAgeBrokerName').value == '' || this.AverageClaimantAge.get('ClaimantAgeBrokerName').value == undefined ? 0 : this.AverageClaimantAge.get('ClaimantAgeBrokerName').value;
  var BPSStartDate = (this.BPSByDefinedDate1.value.BPSStartDate == "" || this.BPSByDefinedDate1.value.BPSStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.BPSByDefinedDate1.value.BPSStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.BPSByDefinedDate1.value.BPSStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.BPSByDefinedDate1.value.BPSStartDate) + " 00:00:00:000";
  var BPSEndDate = (this.BPSByDefinedDate1.value.BPSEndDate == "" || this.BPSByDefinedDate1.value.BPSEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.BPSByDefinedDate1.value.BPSEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.BPSByDefinedDate1.value.BPSEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.BPSByDefinedDate1.value.BPSEndDate) + " 00:00:00:000";

  this._hcbDashboardService.GetBPSDetails(this.UserId, this.UserType, BPSTypeId, BPSClientId, BPSStartDate, BPSEndDate)
    .subscribe((response: any[]) => {

      if (response['result'].length > 0) {
        
        this.GetBPSGrid = response['result']
        this.BPSDisplay = false;
        this.BPSMessage = " ";
      } else {
        this.GetBPSGrid = null;
        this.BPSDisplay = true;
        this.BPSMessage = "No Data Found";
      }

      this.working = false;
    },
      (error: any) => {
       // this.errorText = error;
        this.working = false;
      });
    }
    SearchSECBPS(){
  
      this.working = true;
      // var SchemeAgeId= this.AverageClaimantAge.value.ClaimantAgeBrokerName;
      var SECBPSTypeId = this.SECBPSByDefinedDate1.value.SECBPSClientTypeId == '' || this.SECBPSByDefinedDate1.value.SECBPSClientTypeId == undefined ? 0 : this.SECBPSByDefinedDate1.value.SECBPSClientTypeId;
      var SECBPSClientId = this.SECBPSByDefinedDate1.value.SECBPSClientId == '' || this.SECBPSByDefinedDate1.value.SECBPSClientId == undefined ? 0 : this.SECBPSByDefinedDate1.value.SECBPSClientId;
      // var AssessorAgeId = this.AverageClaimantAge.get('ClaimantAgeBrokerName').value == '' || this.AverageClaimantAge.get('ClaimantAgeBrokerName').value == undefined ? 0 : this.AverageClaimantAge.get('ClaimantAgeBrokerName').value;
      var SECBPSStartDate = (this.SECBPSByDefinedDate1.value.SECBPSStartDate == "" || this.SECBPSByDefinedDate1.value.SECBPSStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.SECBPSByDefinedDate1.value.SECBPSStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.SECBPSByDefinedDate1.value.SECBPSStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.SECBPSByDefinedDate1.value.SECBPSStartDate) + " 00:00:00:000";
      var SECBPSEndDate = (this.SECBPSByDefinedDate1.value.SECBPSEndDate == "" || this.SECBPSByDefinedDate1.value.SECBPSEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.SECBPSByDefinedDate1.value.SECBPSEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.SECBPSByDefinedDate1.value.SECBPSEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.SECBPSByDefinedDate1.value.SECBPSEndDate) + " 00:00:00:000";
    
      this._hcbDashboardService.GetSECBPSDetails(this.UserId, this.UserType, SECBPSTypeId, SECBPSClientId, SECBPSStartDate, SECBPSEndDate)
        .subscribe((response: any[]) => {
    
          if (response['result'].length > 0) {
            
            this.GetSECBPSGrid = response['result']
            this.SECBPSDisplay = false;
            this.SECBPSMessage = " ";
          } else {
            this.GetSECBPSGrid = null;
            this.SECBPSDisplay = true;
            this.SECBPSMessage = "No Data Found";
          }
    
          this.working = false;
        },
          (error: any) => {
           // this.errorText = error;
            this.working = false;
          });
        }

  SearchActiveClientsIn(clientTypeId: number) {

    this.working = true;

    var ActiveStartDate = (this.ActiveClientsInsurance.value.StartDateI == "" || this.ActiveClientsInsurance.value.StartDateI == null) ? null : "" + this._commonService.GetYearFromDateString(this.ActiveClientsInsurance.value.StartDateI) + "-" + (this._commonService.GetMonthFromDateString(this.ActiveClientsInsurance.value.StartDateI) + 1) + "-" + this._commonService.GetDateFromDateString(this.ActiveClientsInsurance.value.StartDateI) + " 00:00:00:000";

    var ActiveEndDate = (this.ActiveClientsInsurance.value.EndDateI == "" || this.ActiveClientsInsurance.value.EndDateI == null) ? null : "" + this._commonService.GetYearFromDateString(this.ActiveClientsInsurance.value.EndDateI) + "-" + (this._commonService.GetMonthFromDateString(this.ActiveClientsInsurance.value.EndDateI) + 1) + "-" + this._commonService.GetDateFromDateString(this.ActiveClientsInsurance.value.EndDateI) + " 00:00:00:000";

    //var ActiveStartDate = (this.ActiveClients.value.StartDate == "" || this.ActiveClients.value.StartDate == null) ? "" : "" + this.ActiveClients.value.StartDate._i.year + "-" + (this.ActiveClients.value.StartDate._i.month + 1) + "-" + this.ActiveClients.value.StartDate._i.date + " 00:00:00:000";
    // var ActiveEndDate = (this.ActiveClients.value.EndDate == "" || this.ActiveClients.value.EndDate == null) ? "" : "" + this.ActiveClients.value.EndDate._i.year + "-" + (this.ActiveClients.value.EndDate._i.month + 1) + "-" + this.ActiveClients.value.EndDate._i.date + " 00:00:00:000";

    this._hcbDashboardService.GetMostActiveClients(ActiveStartDate, ActiveEndDate, this.UserId, this.UserType, clientTypeId)
      .subscribe((response: any) => {

        if (response['result'].table == null || response['result'].table.length==0) {
          this.data1 = null;
          this.ACIDisplay = true;
          this.getMostActiveClients=null;
          this.InMessage = "No Data Found";
        }
        else {
          this.getMostActiveClients = response['result'].table;
          this.ACIDisplay = false;
          this.getMostActiveClients.forEach(element => {

            if (element.tempClientTypeId == 1) {
              element.tempClientType = 'Insurance';
            } else {
              element.tempClientType = 'Retail';
            }
          });
          let mainChart1 = [];
          if (this.getMostActiveClients.length > 0) {
            this.getMostActiveClients.forEach(element => {

              let mod1 = new ChartBasicElements();
              mod1.name = element.tempCompanyName;
              mod1.value = element.tempClientCount;
              mainChart1.push(mod1);
            });
          }

          // this.MostActiveClientList = mainChart;
          const mappedToArray = mainChart1.map(d => Array.from(Object.values(d)))
          this.MostActiveClientList = mappedToArray;
         // this.data1 = this.MostActiveClientList; 
         this.data1 = mainChart1;       
        }
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
        }); 
  }

  SearchActiveClientsRe(clientTypeId: number) {


    this.working = true;
    var ActiveStartDate = (this.ActiveClients.value.StartDate == "" || this.ActiveClients.value.StartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ActiveClients.value.StartDate) + "-" + (this._commonService.GetMonthFromDateString(this.ActiveClients.value.StartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ActiveClients.value.StartDate) + " 00:00:00:000";
    var ActiveEndDate = (this.ActiveClients.value.EndDate == "" || this.ActiveClients.value.EndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ActiveClients.value.EndDate) + "-" + (this._commonService.GetMonthFromDateString(this.ActiveClients.value.EndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ActiveClients.value.EndDate) + " 00:00:00:000";

    //  var ActiveStartDate = (this.ActiveClients.value.StartDate == "" || this.ActiveClients.value.StartDate == null) ? "" : "" + this.ActiveClients.value.StartDate._i.year + "-" + (this.ActiveClients.value.StartDate._i.month + 1) + "-" + this.ActiveClients.value.StartDate._i.date + " 00:00:00:000";
    // var ActiveEndDate = (this.ActiveClients.value.EndDate == "" || this.ActiveClients.value.EndDate == null) ? "" : "" + this.ActiveClients.value.EndDate._i.year + "-" + (this.ActiveClients.value.EndDate._i.month + 1) + "-" + this.ActiveClients.value.EndDate._i.date + " 00:00:00:000";

    this._hcbDashboardService.GetMostActiveClients(ActiveStartDate, ActiveEndDate, this.UserId, this.UserType, clientTypeId)
      .subscribe((response: any) => {
      
        if (response['result'].table == null || response['result'].table.length==0) {
          this.data2 = null;
          this.ACRDisplay = true;
          this.getMostActiveClientsR =null; 
          this.ReMessage = "No Data Found";
        }
        // this.getMostActiveClientsR = response['result'].table;
        else {
          this.getMostActiveClientsR = response['result'].table;

          this.getMostActiveClientsR.forEach(element => {
            if (element.tempClientTypeId == 1) {
              element.tempClientType = 'Insurance';
            } else {
              element.tempClientType = 'Retail';
            }
          });
          let mainChart2 = [];
          if (this.getMostActiveClientsR.length > 0) {
            this.getMostActiveClientsR.forEach(element => {

              let mod1 = new ChartBasicElements();
              mod1.name = element.tempCompanyName;
              mod1.value = element.tempClientCount;
              mainChart2.push(mod1);
            });
          }

          // this.MostActiveClientList = mainChart;
          const mappedToArray = mainChart2.map(d => Array.from(Object.values(d)))
          this.MostActiveClientList = mappedToArray;
        //  this.data2 = this.MostActiveClientList;
          this.data2 = mainChart2;
          
        }
        this.working = false;

      },
        (error: any) => {
          this.errorText = error;
        });

  }


  SearchCase(): void {

    var CientId = this.SchemeActivity.get('Scheme').value == '' || this.SchemeActivity.get('Scheme').value == undefined ? 0 : this.SchemeActivity.get('Scheme').value;

    var SchStartDate = (this.SchemeActivity.value.SchemeStartDate == "" || this.SchemeActivity.value.SchemeStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.SchemeActivity.value.SchemeStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.SchemeActivity.value.SchemeStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.SchemeActivity.value.SchemeStartDate) + " 00:00:00:000";
    var SchEndDate = (this.SchemeActivity.value.SchemeEndDate == "" || this.SchemeActivity.value.SchemeEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.SchemeActivity.value.SchemeEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.SchemeActivity.value.SchemeEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.SchemeActivity.value.SchemeEndDate) + " 00:00:00:000";


    // var SchemeStartDate = (this.SchemeActivity.value.SchemeStartDate == "" || this.SchemeActivity.value.SchemeStartDate == null) ? "" : "" + this.SchemeActivity.value.SchemeStartDate._i.year + "-" + (this.SchemeActivity.value.SchemeStartDate._i.month + 1) + "-" + this.SchemeActivity.value.SchemeStartDate._i.date + " 00:00:00:000";
    // var SchemeEndDate = (this.SchemeActivity.value.SchemeEndDate == "" || this.SchemeActivity.value.SchemeEndDate == null) ? "" : "" + this.SchemeActivity.value.SchemeEndDate._i.year + "-" + (this.SchemeActivity.value.SchemeEndDate._i.month + 1) + "-" + this.SchemeActivity.value.SchemeEndDate._i.date + " 00:00:00:000";

    this._hcbDashboardService.GetScheme(this.UserId, this.UserType, CientId, SchStartDate, SchEndDate)
      .subscribe((response: any[]) => {

        this.getOpenScheme = response['result'];
        this.OpenScheme = response['result'].length;
      },
        (error: any) => {
          this.errorText = error;
        });

  }

  OpenCloseCaseSchemeActivity(value: number): void {

    if (value == 1) {
      var CaseSchemeActivity = this.getOpenScheme
      var ID = 1
    }
    else {
      CaseSchemeActivity = this.getClosedScheme
      ID = 0
    }
    this.dialogRef = this.dialog.open(OpenCloseSchemeActivityComponent, {
      disableClose: true,
      width: '800px',
      height: 'auto',
      maxHeight: '10vh !important',
      data: {
        case: CaseSchemeActivity,
        Id: ID
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  SearchRTW(): void {

    var RTWClientId = this.AverageRTWDuration.value.RTWClientId == '' || this.AverageRTWDuration.value.RTWClientId == undefined ? 0 : this.AverageRTWDuration.value.RTWClientId;
    //var AssessorRTWId = this.AverageRTWDuration.get('ClientTypeId').value == '' || this.AverageRTWDuration.get('RTWBrokerName').value == undefined ? 0 : this.AverageRTWDuration.get('RTWBrokerName').value;
    var RTWTypeId = this.AverageRTWDuration.value.RTWClientTypeId == '' || this.AverageRTWDuration.value.RTWClientTypeId == undefined ? 0 : this.AverageRTWDuration.value.RTWClientTypeId;

    var RTWStartDate = (this.AverageRTWDuration.value.AverageDurationStartDate == "" || this.AverageRTWDuration.value.AverageDurationStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.AverageRTWDuration.value.AverageDurationStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.AverageRTWDuration.value.AverageDurationStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.AverageRTWDuration.value.AverageDurationStartDate) + " 00:00:00:000";
    var RTWEndDate = (this.AverageRTWDuration.value.AverageDurationEndDate == "" || this.AverageRTWDuration.value.AverageDurationEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.AverageRTWDuration.value.AverageDurationEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.AverageRTWDuration.value.AverageDurationEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.AverageRTWDuration.value.AverageDurationEndDate) + " 00:00:00:000";


    this._hcbDashboardService.GetDashboardAverageRTWDuration(this.UserId, this.UserType, RTWClientId, RTWTypeId, RTWStartDate, RTWEndDate)
      .subscribe((response: any[]) => {

        this.avgRTWDuration = response['result'][0].avgRTWDuration;
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  SearchClientDemographic(): void {

    this.working = true;
    // var SchemeAgeId= this.AverageClaimantAge.value.ClaimantAgeBrokerName;
    var CDTypeId = this.ClaimantEmpDemographics.value.CEDClientTypeId == '' || this.ClaimantEmpDemographics.value.CEDClientTypeId == undefined ? 0 : this.ClaimantEmpDemographics.value.CEDClientTypeId;

    var ClientId = this.ClaimantEmpDemographics.value.CEDClientId == '' || this.ClaimantEmpDemographics.value.CEDClientId == undefined ? 0 : this.ClaimantEmpDemographics.value.CEDClientId;
    // var AssessorAgeId = this.AverageClaimantAge.get('ClaimantAgeBrokerName').value == '' || this.AverageClaimantAge.get('ClaimantAgeBrokerName').value == undefined ? 0 : this.AverageClaimantAge.get('ClaimantAgeBrokerName').value;
    var CEDStartDate = (this.ClaimantEmpDemographics.value.ClaimantReceivedStartDate == "" || this.ClaimantEmpDemographics.value.ClaimantReceivedStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ClaimantEmpDemographics.value.ClaimantReceivedStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.ClaimantEmpDemographics.value.ClaimantReceivedStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ClaimantEmpDemographics.value.ClaimantReceivedStartDate) + " 00:00:00:000";
    var CEDEndDate = (this.ClaimantEmpDemographics.value.ClaimantReceivedEndDate == "" || this.ClaimantEmpDemographics.value.ClaimantReceivedEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ClaimantEmpDemographics.value.ClaimantReceivedEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.ClaimantEmpDemographics.value.ClaimantReceivedEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ClaimantEmpDemographics.value.ClaimantReceivedEndDate) + " 00:00:00:000";

    this._hcbDashboardService.GetDashboardClaimantGender(this.UserId, this.UserType, CDTypeId, ClientId, CEDStartDate, CEDEndDate)
      .subscribe((response: any[]) => {

        if (response['result'].length > 0) {
          this.GetClientDemographicGrid = response['result']
          this.CDDisplay = false;
          this.CDMessage = " ";
        } else {
          this.GetClientDemographicGrid = null;
          this.CDDisplay = true;
          this.CDMessage = "No Data Found";
        }

        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
          this.working = false;
        });


  }

  GetReturnToWorkReason(): void {
    this.working = true;
    this._hcbDashboardService.GetDashboardReturnToWorkReason(this.UserId, this.UserType)
      .subscribe((response: any) => {
        if (response['result'].length > 0) {
          this.getReturnToWorkReason = response['result'];
          let mainChart = [];
          this.ErrorMessage1 = " ";
          if (this.getReturnToWorkReason) {
            this.getReturnToWorkReason.forEach(element => {
              let mod = new ChartBasicElements();
              mod.name = element.returnToWorkReason;
              mod.value = element.returnToWorkReasonPercentage;
              mainChart.push(mod);
            });
          }

          const mappedToArray = this.getReturnToWorkReason.map(d => Array.from(Object.values(d)))
          this.ReturnToWorkReason = mappedToArray;
          this.ReturnToWorkReasonData = this.ReturnToWorkReason;
          this.working = false;  
        } else {
          this.ReturnToWorkReasonData = [0, 0];
          this.ErrorMessage1 = "No Data Found";
        }

      },
        (error: any) => {
          this.errorText = error;
        });
  }
  SearchInterventionDuration(): void {
   
    this.working = true;
    var InterventionSectorId = this.AverageInterventionDuration.get('AIDClientTypeId').value == '' || this.AverageInterventionDuration.get('AIDClientTypeId').value == undefined ? 0 : this.AverageInterventionDuration.get('AIDClientTypeId').value;
    var InterventionClientId = this.AverageInterventionDuration.get('AIDClientId').value == '' || this.AverageInterventionDuration.get('AIDClientId').value == undefined ? 0 : this.AverageInterventionDuration.get('AIDClientId').value;

    //var InterventionSchemeId = this.AverageInterventionDuration.get('InterventionSchemeName').value == '' || this.AverageInterventionDuration.get('InterventionSchemeName').value == undefined ? 0 : this.AverageInterventionDuration.get('InterventionSchemeName').value;
    //var InterventionAssessorId = this.AverageInterventionDuration.get('InterventionBrokerName').value == '' || this.AverageInterventionDuration.get('InterventionBrokerName').value == undefined ? 0 : this.AverageInterventionDuration.get('InterventionBrokerName').value;

    var InStartDate = (this.AverageInterventionDuration.value.InterventionStartDate == "" || this.AverageInterventionDuration.value.InterventionStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.AverageInterventionDuration.value.InterventionStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.AverageInterventionDuration.value.InterventionStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.AverageInterventionDuration.value.InterventionStartDate) + " 00:00:00:000";
    var InEndDate = (this.AverageInterventionDuration.value.InterventionEndDate == "" || this.AverageInterventionDuration.value.InterventionEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.AverageInterventionDuration.value.InterventionEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.AverageInterventionDuration.value.InterventionEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.AverageInterventionDuration.value.InterventionEndDate) + " 00:00:00:000";


    this._hcbDashboardService.GetDashboardInterventionDuration(this.UserId, this.UserType, InterventionSectorId, InterventionClientId, InStartDate, InEndDate)
      .subscribe((response: any[]) => {
        this.InterventionDuration = response['result'][0].avgClaimDuration;
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
          this.working = false;
        });
  }

  SearchClaimantGender(): void {
    this.working = true;
    var ClaimantGenderSchemeId = this.ClaimantGender.get('ClaimantGenderSchemeName').value == '' || this.ClaimantGender.get('ClaimantGenderSchemeName').value == undefined ? 0 : this.ClaimantGender.get('ClaimantGenderSchemeName').value;
    var ClaimantGenderAssessorId = this.ClaimantGender.get('ClaimantGenderBrokerName').value == '' || this.ClaimantGender.get('ClaimantGenderBrokerName').value == undefined ? 0 : this.ClaimantGender.get('ClaimantGenderBrokerName').value;

    var CStartDate = (this.ClaimantGender.value.ClaimantGenderStartDate == "" || this.ClaimantGender.value.ClaimantGenderStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ClaimantGender.value.ClaimantGenderStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.ClaimantGender.value.ClaimantGenderStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ClaimantGender.value.ClaimantGenderStartDate) + " 00:00:00:000";
    var ClEndDate = (this.ClaimantGender.value.ClaimantGenderEndDate == "" || this.ClaimantGender.value.ClaimantGenderEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ClaimantGender.value.ClaimantGenderEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.ClaimantGender.value.ClaimantGenderEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ClaimantGender.value.ClaimantGenderEndDate) + " 00:00:00:000";

  }

  GetClosedCaseAnalysisGrid(): void {
    this.working = true;
    var TypeId = this.ClosedCaseAnalysis.value.CCAClientTypeId == '' || this.ClosedCaseAnalysis.value.CCAClientTypeId == undefined ? 0 : this.ClosedCaseAnalysis.value.CCAClientTypeId;

    var ClosedCaseClientId = this.ClosedCaseAnalysis.value.CCABrokerName == '' || this.ClosedCaseAnalysis.value.CCABrokerName == undefined ? 0 : this.ClosedCaseAnalysis.value.CCABrokerName;

    //var ClosedCaseSchemeId = this.ClosedCaseAnalysis.get('ClosedCaseSchemeName').value == '' || this.ClosedCaseAnalysis.get('ClosedCaseSchemeName').value == undefined ? 0 : this.ClosedCaseAnalysis.get('ClosedCaseSchemeName').value;
    //var ClosedCaseAssessorId = this.ClosedCaseAnalysis.get('ClosedCaseBrokerName').value == '' || this.ClosedCaseAnalysis.get('ClosedCaseBrokerName').value == undefined ? 0 : this.ClosedCaseAnalysis.get('ClosedCaseBrokerName').value;
    var ClosedCaseStartDate = (this.ClosedCaseAnalysis.value.ClosedCaseStartDate == "" || this.ClosedCaseAnalysis.value.ClosedCaseStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ClosedCaseAnalysis.value.ClosedCaseStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.ClosedCaseAnalysis.value.ClosedCaseStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ClosedCaseAnalysis.value.ClosedCaseStartDate) + " 00:00:00:000";
    var ClosedCaseEndDate = (this.ClosedCaseAnalysis.value.ClosedCaseEndDate == "" || this.ClosedCaseAnalysis.value.ClosedCaseEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ClosedCaseAnalysis.value.ClosedCaseEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.ClosedCaseAnalysis.value.ClosedCaseEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ClosedCaseAnalysis.value.ClosedCaseEndDate) + " 00:00:00:000";

    this._hcbDashboardService.GetDashboardClosedCaseAnalysis(this.UserId, this.UserType, TypeId, ClosedCaseClientId, ClosedCaseStartDate, ClosedCaseEndDate)
      .subscribe((response: any[]) => {
        // this.ClosedCaseAnalysisData = response['result'];
        if (response['result'].length > 0) {
          this.ClosedCaseAnalysisData = response['result'];
          this.CCADisplay = false;
          this.ClosedCaseAnalysisMessage = " ";
        } else {
          this.ClosedCaseAnalysisData = null;
          this.CCADisplay = true;
          this.ClosedCaseAnalysisMessage = "No Data Found";
        }
        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
          this.working = false;
        });
  }


  GetReasonForReferal(): void {
    this.working = true;
    this._hcbDashboardService.GetDashboardReasonForReferal(this.UserId, this.UserType)
      .subscribe((response: any) => {
        if (response['result'].length > 0) {
          this.getReasonForReferal = response['result'];
          this.ErrorMessage = " ";
          let mainChart = [];
          if (this.getReasonForReferal) {
            this.getReasonForReferal.forEach(element => {
              let mod = new ChartBasicElements();
              mod.name = element.illnessInjuryName;
              mod.value = element.illnessInjuryNamePercentage;
              mainChart.push(mod);
            });
          }

          const mappedToArray = this.getReasonForReferal.map(d => Array.from(Object.values(d)))
          this.ReasonForReferal = mappedToArray;
          this.ReasonForReferalData = this.ReasonForReferal;
          this.working = false;
        } else {
          this.ReasonForReferalData = [0, 0];
          this.ErrorMessage = "No Data Found";
        }
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  SearchReasonForReferal(): void {

    this.working = true;
    var RFRTypeId = this.ReasonForReferalGrid.value.ClientTypeId == '' || this.ReasonForReferalGrid.value.ClientTypeId == undefined ? 0 : this.ReasonForReferalGrid.value.ClientTypeId;
    // var ReasonReferalAssessorId = this.ReasonForReferalGrid.get('ReasonReferalCientName').value == '' || this.ReasonForReferalGrid.get('ReasonReferalCientName').value == undefined ? 0 : this.ReasonForReferalGrid.get('ReasonReferalCientName').value;
    var RFRClientId = this.ReasonForReferalGrid.value.RFRClientId == '' || this.ReasonForReferalGrid.value.RFRClientId == undefined ? 0 : this.ReasonForReferalGrid.value.RFRClientId;

    var ReasonReferalStartDate = (this.ReasonForReferalGrid.value.ReasonReferalStartDate == "" || this.ReasonForReferalGrid.value.ReasonReferalStartDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ReasonForReferalGrid.value.ReasonReferalStartDate) + "-" + (this._commonService.GetMonthFromDateString(this.ReasonForReferalGrid.value.ReasonReferalStartDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ReasonForReferalGrid.value.ReasonReferalStartDate) + " 00:00:00:000";
    var ReasonReferalEndDate = (this.ReasonForReferalGrid.value.ReasonReferalEndDate == "" || this.ReasonForReferalGrid.value.ReasonReferalEndDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.ReasonForReferalGrid.value.ReasonReferalEndDate) + "-" + (this._commonService.GetMonthFromDateString(this.ReasonForReferalGrid.value.ReasonReferalEndDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.ReasonForReferalGrid.value.ReasonReferalEndDate) + " 00:00:00:000";

    var ReasonReferalEndDate = (this.ReasonForReferalGrid.value.ReasonReferalEndDate == "" || this.ReasonForReferalGrid.value.ReasonReferalEndDate == null) ? "" : "" + this.ReasonForReferalGrid.value.ReasonReferalEndDate._i.year + "-" + (this.ReasonForReferalGrid.value.ReasonReferalEndDate._i.month + 1) + "-" + this.ReasonForReferalGrid.value.ReasonReferalEndDate._i.date + " 00:00:00:000";

    this._hcbDashboardService.GetDashboardReasonReferalGrid(this.UserId, this.UserType, RFRTypeId, RFRClientId, ReasonReferalStartDate, ReasonReferalEndDate)
      .subscribe((response: any[]) => {

        if (response['result'].length > 0) {
          this.ReasonForReferalGridData = response['result'];
          this.ReasonForReferalMessage = " ";
          this.ReasonForReferalMessagedisplay = false;
        } else {
          this.ReasonForReferalGridData = null;
          this.ReasonForReferalMessagedisplay = true;
          this.ReasonForReferalMessage = "No Data Found";
        }

        this.working = false;
      },
        (error: any) => {
          this.errorText = error;
          this.working = false;
        });
  }

  SearchNoActivityReport(): void {
    var ClientId = this.NoActivityReport.get('ClientName').value == '' || this.NoActivityReport.get('ClientName').value == undefined ? 0 : this.NoActivityReport.get('ClientName').value;
    var NoActivityInterval = this.NoActivityReport.get('NoActivityInterval').value == '' || this.NoActivityReport.get('NoActivityInterval').value == undefined ? 60 : parseInt(this.NoActivityReport.get('NoActivityInterval').value);

    this._hcbDashboardService.GetDashboardNoActivityReport(ClientId, NoActivityInterval, this.UserId, this.UserType)
      .subscribe((response: any[]) => {

        if (response['result'].length > 0) {
          this.NoActivityMessage = ' ';
        } else {
          this.NoActivityMessage = 'No Record Available';
        }
        this.getNoActivityReport = response['result'];
      },
        (error: any) => {
          this.errorText = error;
        });
  }

  EditCaseRecord(hcbReferenceId: number): void {
    this._router.navigate(["/case-managment/addedit-case-managment/edit", hcbReferenceId]);
  }

  GetListForTotalValues(Index: number, ClientTypeId: number) {

    this._hcbDashboardService.GetListForTotalValues(Index, this.UserId, this.UserType, ClientTypeId)
      .subscribe((response: any) => {

        this.getListForTotalValues = response['result'];

        this.dialogRef = this.dialog.open(ListOfTotalValuesComponent, {
          disableClose: true,
          width: '500px',
          height: 'auto',
          maxHeight: '10vh !important',
          data: {
            case: this.getListForTotalValues,
            Id: Index

          }
        });

        this.dialogRef.afterClosed().subscribe(result => {
          if (result) {

          }
        });
      })
  }

  ClearCloseReasonOutcomes():void{
    this.OutcomesFormGroup.reset();
    this.CaseCloseReasonOutcome();
  }

  ClearBPS():void{
    this.BPSByDefinedDate1.reset();
    this.SearchBPS();
  }
  ClearSECBPS():void{
    this.SECBPSByDefinedDate1.reset();
    this.SearchSECBPS();
  }

  ClearActiveClientsIn(ClientTypeId: number): void {

    this.ActiveClientsInsurance.reset();
    this.SearchActiveClientsIn(1);
  }

  ClearActiveClientsRe(ClientTypeId: number): void {
    this.ActiveClients.reset();
    this.SearchActiveClientsRe(2);
  }

  ClearScheme(): void {
 
    this.SchemeActivity.reset();
    this.SearchCase();
  }

  ClearRTW(): void {
    this.AverageRTWDuration.reset();
    this.SearchRTW();
  }

  ClearClaimantDemographic(): void {
    this.ClaimantEmpDemographics.reset();
    this.SearchClientDemographic();
  }

  ClearClosedCaseAnalysis(): void {
    this.ClosedCaseAnalysis.reset();
    this.GetClosedCaseAnalysisGrid();
  }

  ClearInterventionDuration(): void {
    this.AverageInterventionDuration.reset();
    this.SearchInterventionDuration();
  }
  ClearClaimantGender(): void {
    this.ClaimantGender.reset();
    this.SearchClaimantGender();
  }

  ClearReasonForReferal(): void {
    this.ReasonForReferalGrid.reset();
    this.SearchReasonForReferal();
  }

  ClearNoActivityReport(): void {
    this.NoActivityReport.reset();
    this.SearchNoActivityReport();
  }

  public tabChanged(tabChangeEvent): void {
   
    this.selectedIndex = tabChangeEvent.index;
  }
  GetDashboardAreaChartOPN(value:number,UserId:number,UserType:string){
    this._hcbDashboardService.GetDashboardAreaChartOPN(value,UserId,UserType)
    .subscribe((response) => {
  
      let mainChart = [];
      this.CaseActivityByWeekDataOPN = response['result'];
      this.CaseActivityByWeekDataOPN.forEach(element => {

        let mod1 = new ChartBasicElements();
        mod1.name = element.daysName;
        mod1.value = element.noCases;
        mainChart.push(mod1);
      });

    
    var mainChartOPN:any[] = [
      {
        "name": "Open",
        "series": mainChart
        
      }
    
    ];
    Object.assign(this, {mainChartOPN})

  });
  }
  GetDashboardAreaChartCLS(value:number,UserId:number,UserType:string){
    this._hcbDashboardService.GetDashboardAreaChartCLS(value,UserId,UserType)
    .subscribe((response) => {
      
      let mainChart3 = [];
      this.CaseActivityByWeekDataCLS = response['result'];
      this.CaseActivityByWeekDataCLS.forEach(element => {

        let mod1 = new ChartBasicElements();
        mod1.name = element.daysName;
        mod1.value = element.noCases;
        mainChart3.push(mod1);
      });

    
    var mainChartCLS:any[] = [
      {
        "name": "Close",
        "series": mainChart3
        
      }
    
    ];
    Object.assign(this, {mainChartCLS})
  });
  }
  GetDashboardAreaChartOPNI(value:number,UserId:number,UserType:string){
    this._hcbDashboardService.GetDashboardAreaChartOPNI(value,UserId,UserType)
    .subscribe((response) => {

      let mainChart3 = [];
      this.CaseActivityByWeekData = response['result'];
      this.CaseActivityByWeekData.forEach(element => {

        let mod1 = new ChartBasicElements();
        mod1.name = element.daysName;
        mod1.value = element.noCases;
        mainChart3.push(mod1);
      });

    
    var mainChartOPNI:any[] = [
      {
        "name": "Open(Insurance)",
        "series": mainChart3
        
      }
    
    ];
    Object.assign(this, {mainChartOPNI})
  });
  }
  GetDashboardAreaChartCLSI(value:number,UserId:number,UserType:string){
    this._hcbDashboardService.GetDashboardAreaChartCLSI(value,UserId,UserType)
    .subscribe((response) => {
     
      let mainChart3 = [];
      this.CaseActivityByWeekData = response['result'];
      this.CaseActivityByWeekData.forEach(element => {

        let mod1 = new ChartBasicElements();
        mod1.name = element.daysName;
        mod1.value = element.noCases;
        mainChart3.push(mod1);
      });

    
    var mainChartCLSI:any[] = [
      {
        "name": "close(Insurance)",
        "series": mainChart3
        
      }
    
    ];
    Object.assign(this, {mainChartCLSI})
  });
  }
  GetDashboardAreaChartOPNR(value:number,UserId:number,UserType:string){
    this._hcbDashboardService.GetDashboardAreaChartOPNR(value,UserId,UserType)
    .subscribe((response) => {
     
      let mainChart3 = [];
      this.CaseActivityByWeekData = response['result'];
      this.CaseActivityByWeekData.forEach(element => {

        let mod1 = new ChartBasicElements();
        mod1.name = element.daysName;
        mod1.value = element.noCases;
        mainChart3.push(mod1);
      });

    
    var mainChartOPNR:any[] = [
      {
        "name": "Open(Retail)",
        "series": mainChart3
        
      }
    
    ];
    Object.assign(this, {mainChartOPNR})
  });
  }
  GetDashboardAreaChartCLSR(value:number,UserId:number,UserType:string){
    this._hcbDashboardService.GetDashboardAreaChartCLSR(value,UserId,UserType)
    .subscribe((response) => {
  
      let mainChart3 = [];
      this.CaseActivityByWeekData = response['result'];
      this.CaseActivityByWeekData.forEach(element => {

        let mod1 = new ChartBasicElements();
        mod1.name = element.daysName;
        mod1.value = element.noCases;
        mainChart3.push(mod1);
      });

    
    var mainChartCLSR:any[] = [
      {
        "name": "Close(Retail)",
        "series": mainChart3
        
      }
    
    ];
    Object.assign(this, {mainChartCLSR})
  });
  }

  // GetDashboardVertiChart(value:number,UserId:number,UserType:string){
  //   debugger;
  //   var opnlength=this.CaseActivityByWeekDataOPN.length;
  //   var clslength=this.CaseActivityByWeekDataCLS.length;
  //   let totalopn =[];
  //   let totalcls =[];
  //   let mainChart3 = [];
  //   this.CaseActivityByWeekDataOPN.forEach(element => {
  //     let mod1 = new ChartBasicElements();
  //   let mod2 = new serieselement();
    
  //   mod1.name=element.daysName;
  //   mod2.name="open";
  //   mod2.value=element.noCases
  //   mod1.series.push(mod2);
  //   mainChart3.push(mod1)
  //   });
  //   this.CaseActivityByWeekDataCLS.forEach(element => {
    
  //     totalcls.push(element.daysName);
      
  //     });
  //   console.log(mainChart3);
  //     Object.assign(this, {mainChart3 })

  //   this._hcbDashboardService.GetDashboardAreaChartCLSR(value,UserId,UserType)
  //   .subscribe((response) => {
  //     debugger;
  //     let mainChart3 = [];
  //     this.CaseActivityByWeekData = response['result'];
  //     this.CaseActivityByWeekData.forEach(element => {

  //       let mod1 = new ChartBasicElements();
  //       mod1.name = element.daysName;
  //       mod1.value = element.noCases;
  //       mainChart3.push(mod1);
  //     });

    
  //   var mainChartCLSR:any[] = [
  //     {
  //       "name": "Close(Retail)",
  //       "series": mainChart3
        
  //     }
    
  //   ];
  //   Object.assign(this, {mainChartCLSR})
  // });
  // }
}

export class ChartBasicElements {
  public name: any;
  public value: any;
  public labels:any;
  public series:serieselement[];
}
export class serieselement {
  public name: any;
  public value: any;
}



