export class GetddlBrokerList{
    ID:number;
    AssessorTeamName:string;
}
export class GetddlSchemeList{
    ID:number;
    SchemeNumber:number;
    SchemeName:string;
}
export class GetddlNameOfTrust{
    Id:number;
    ClientId:number;
    CompanyName:string;
}

export class GetddlAverageRTWDuration{
    ID:number;
    AvgRTWDuration:number;
}

export class ClaimClosedGrid{
    ClaimClosedReason:string;
    ClaimClosedReasonCount:number;
    ClaimClosedReasonPercentage:number;
}