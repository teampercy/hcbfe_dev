import { Injectable } from '@angular/core';
import { HttpClientsService } from 'app/service/http-client.service';
import { AuthenticationService } from 'app/service/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetddlBrokerList, GetddlSchemeList, GetddlNameOfTrust } from './hcb-dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class HcbDashboardService {

  constructor(  private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { }

    GetDashboardTopFiveAssessorList(userId: number,usertype:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardTopFiveAssessorList?userId=" + userId+ "&usertype=" + usertype);
    }

    GetMostActiveClients(startdate: string,enddate:string,userId: number,usertype:string,clientTypeId:number): Observable<any | {}> {
      return this._http.getAPI("CRMDashboard/GetCRMMostActiveClients?startdate=" + startdate+ "&enddate=" + enddate+ "&userId=" + userId+ "&usertype=" + usertype + "&clientTypeId=" + clientTypeId);
    }

    GetDashboardTopFiveSchemeList(userId: number,usertype:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardTopFiveSchemeList?userId=" + userId+ "&usertype=" + usertype);
    }

    GetddlBrokerList(userId: number,usertype:string): Observable<GetddlBrokerList | {}> {
      return this._http.getAPI("HCBDashboard/GetddlBrokerList?userId=" + userId+ "&usertype=" + usertype);
    }
    GetddlSchemeList(userId: number,usertype:string): Observable<GetddlSchemeList | {}> {
      return this._http.getAPI("HCBDashboard/GetddlSchemeList?userId=" + userId+ "&usertype=" + usertype);
    }
    GetScheme(userId: number,usertype:string,SchemeId:number,SchemeStartDate: string,SchemeEndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetScheme?UserId="+userId+"&UserType="+usertype+"&SchemeId=" + SchemeId+"&SchemeStartDate=" + SchemeStartDate+ "&SchemeEndDate=" + SchemeEndDate);
    }
    GetDashboardAverageRTWDuration(userId: number,usertype:string,RTWClientId:number,RTWClientTypeId:number,RTWStartDate: string,RTWEndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardAverageRTWDuration?UserId="+userId+"&UserType="+usertype+"&RTWclientId=" + RTWClientId+"&RTWClientTypeId="+RTWClientTypeId+"&RTWStartDate=" +RTWStartDate+ "&RTWEndDate=" +RTWEndDate);
    }

    GetDashboardClaimantAverageAge(userId: number,usertype:string,SchemeAgeId:number,AgeStartDate: string,AgeEndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardClaimantAverageAge?UserId="+userId+"&UserType="+usertype+"&SchemeAgeId=" + SchemeAgeId+"&AgeStartDate=" + AgeStartDate+ "&AgeEndDate=" + AgeEndDate);
    } 

    GetHCBDashboardTotalValues(userId: number,usertype:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetHCBDashboardTotalValues?userId=" + userId+ "&usertype=" + usertype);
    }
    GetListForTotalValues(Index:number,userId: number,usertype:string, ClientTypeId:number): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetListForTotalValues?Index="+Index +"&userId=" + userId+ "&usertype=" + usertype +"&ClientTypeId=" + ClientTypeId);
    }
    GetDashboardReturnToWorkReason(userId: number,usertype:string): Observable<any| {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardReturnToWorkReason?userId=" + userId+ "&usertype=" + usertype);
    }

    GetDashboardReasonForReferal(userId: number,usertype:string): Observable<any| {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardReasonForReferal?userId=" + userId+ "&usertype=" + usertype);
    }

    GetDashboardInterventionDuration(userId: number,usertype:string,InterventionSectorId:number,InterventionClientId:number,InterventionStartDate: string,InterventionEndDate:string): Observable<any | {}> {
      
      return this._http.getAPI("HCBDashboard/GetDashboardInterventionDuration?UserId="+userId+"&UserType="+usertype+"&InterventionSectorId="+InterventionSectorId+"&InterventionClientId=" + InterventionClientId+"&InterventionStartDate=" + InterventionStartDate+ "&InterventionEndDate=" + InterventionEndDate);
    } 

    GetDashboardClaimantGender(userId: number,usertype:string,CDTypeId:number,ClaimantGenderSchemeId:number,ClaimantGenderStartDate: string,ClaimantGenderEndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardClaimantGender?UserId="+userId+"&UserType="+usertype+"&typeId="+CDTypeId+"&ClientId=" + ClaimantGenderSchemeId+"&ClaimantGenderStartDate=" + ClaimantGenderStartDate+ "&ClaimantGenderEndDate=" + ClaimantGenderEndDate);
    } 

    GetDashboardClosedCaseAnalysis(userId: number,usertype:string, typeId:number,ClosedCaseSchemeId:number,ClosedCaseStartDate: string,ClosedCaseEndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardClosedCaseAnalysis?UserId="+userId+"&UserType="+usertype+"&typeId="+typeId+"&ClientId=" + ClosedCaseSchemeId+"&ClosedCaseStartDate=" + ClosedCaseStartDate+ "&ClosedCaseEndDate=" + ClosedCaseEndDate);
    } 

    GetDashboardReasonReferalGrid(userId: number,usertype:string,RFRTypeId:number,RFRClientId:number,ReasonReferalStartDate: string,ReasonReferalEndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetDashboardReasonReferalGrid?UserId="+userId+"&UserType="+usertype+"&RFRTypeId=" + RFRTypeId+"&RFRClientId="+RFRClientId+"&ReasonReferalStartDate=" + ReasonReferalStartDate+ "&ReasonReferalEndDate=" + ReasonReferalEndDate);
    } 
    GetDashboardNoActivityReport(ClientId:number,NoActivityInterval: number,userId: number,usertype:string): Observable<any | {}> {
      return this._http.getAPI("CRMDashboard/GetDashboardNoActivityReport?ClientId=" + ClientId+"&NoActivityInterval=" + NoActivityInterval+"&UserId="+userId+"&UserType="+usertype);
    }
    GetClientBySectorGrid(clientTypeId:number,UserId:any,UserType:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/GetClientBySectorGrid?clientTypeId=" + clientTypeId+"&UserId="+UserId+"&UserType="+UserType);
    }
    // GetCasesPerCaseMgr(IsOpen:boolean,CaseMgrId:number,startdate: string,enddate:string): Observable<any | {}> {
    //   return this._http.getAPI("CRMDashboard/GetCasesPerCaseMgr?IsOpen="+IsOpen+"&CaseMgrId=" + CaseMgrId+"&startdate=" + startdate+ "&enddate=" + enddate);
    // }
    GetCaseActivityDefinedByWeek(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetCaseActivityDefinedByWeek?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    BPSByDefinedDates(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/BPSByDefinedDates?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    SECBPSByDefinedDates(weekdata:number, UserId:number,UserType:string)
    { 
      return this._http.getAPI("HCBDashboard/SECBPSByDefinedDates?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetBPSDetails(userId: number,usertype:string,RFRTypeId:number,RFRClientId:number,ReasonReferalStartDate: string,ReasonReferalEndDate:string):Observable<any | {}> 
    {
      return this._http.getAPI("HCBDashboard/GetBPSDetails?UserId="+userId+"&UserType="+usertype+"&RFRTypeId=" + RFRTypeId+"&RFRClientId="+RFRClientId+"&ReasonReferalStartDate=" + ReasonReferalStartDate+ "&ReasonReferalEndDate=" + ReasonReferalEndDate);
    }
    GetSECBPSDetails(userId: number,usertype:string,RFRTypeId:number,RFRClientId:number,ReasonReferalStartDate: string,ReasonReferalEndDate:string):Observable<any | {}> 
    {
      return this._http.getAPI("HCBDashboard/GetSECBPSDetails?UserId="+userId+"&UserType="+usertype+"&RFRTypeId=" + RFRTypeId+"&RFRClientId="+RFRClientId+"&ReasonReferalStartDate=" + ReasonReferalStartDate+ "&ReasonReferalEndDate=" + ReasonReferalEndDate);
    }
    CaseCloseReasonOutcome(userId: number,usertype:string,StartDate: string,EndDate:string): Observable<any | {}> {
      return this._http.getAPI("HCBDashboard/CaseCloseReasonOutcome?UserId="+userId+"&UserType="+usertype+"&StartDate=" + StartDate+ "&EndDate=" + EndDate);
    }

    GetCloseReasonOutcomesDetail(StartDate: string,EndDate:string, userId: number,usertype:string)
    {
      return this._http.getAPI("HCBDashboard/GetCloseReasonOutcomesDetail?UserId="+userId+"&UserType="+usertype+"&StartDate=" + StartDate+ "&EndDate=" + EndDate);
    }
    ActivityByDefinedDateMaster(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/ActivityByDefinedDateMaster?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetDashboardAreaChartOPN(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetDashboardAreaChartOPN?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetDashboardAreaChartCLS(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetDashboardAreaChartCLS?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetDashboardAreaChartOPNI(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetDashboardAreaChartOPNI?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetDashboardAreaChartCLSI(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetDashboardAreaChartCLSI?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetDashboardAreaChartOPNR(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetDashboardAreaChartOPNR?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
    GetDashboardAreaChartCLSR(weekdata:number, UserId:number,UserType:string)
    {
      return this._http.getAPI("HCBDashboard/GetDashboardAreaChartCLSR?WeekData=" + weekdata+"&UserId="+UserId+"&UserType="+UserType);
    }
}
