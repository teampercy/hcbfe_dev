import { Component, OnInit, ViewEncapsulation, ViewChild , ElementRef} from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { CaseManagmentService } from 'app/main/HCB/case-managment/case-managment.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-import-cases',
  templateUrl: './import-cases.component.html',
  styleUrls: ['./import-cases.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ImportCasesComponent implements OnInit {

  UserTypeId : any;
  CanWorkAsCM: any;
  UserId:any;
  arrayBuffer:any;
  file:any;
  fileToUpload: File = null;
  FirstName:any;
  ErrorGrid:any;
  pageOfItems: Array<any>;
  TotalRecords:any;
 working :boolean=false;
 ShowerrorTBL:boolean=false;
 successMSG:boolean=false;
 Response :string;
 displayedColumns = ['No','Message'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('DocumentfileInput', { static: false }) DocumentfileInput: ElementRef;
  constructor(  private _caseManagment: CaseManagmentService,) { }

  ngOnInit() {
    debugger;
    this.UserTypeId = parseInt(localStorage.getItem('UserTypeId'));
    this.CanWorkAsCM = parseInt(localStorage.getItem('CanWorkAsCM'));
    this.UserId = parseInt(localStorage.getItem('UserId'));
   // this.getdata();
 
  }
  
  getdata()
  {
    debugger;
    var data: any[]=[ {Name: "Mr Leonard Lucero", GPE: "GPE0529089", GIP: 137184, DOB: 27965, Address: "6 Borfa Place,Cefn Fforest,BLACKWOOD,NP12 3FE"}
    , {Name: " Mr Andrew Norton", GPE: "GPE0529054", GIP: 137182, DOB: 29963, Address: "11a The Crescent,Holmesfield,DRONFIELD,S18 7WY"}
   ,{Name: " Mr Neil  Fieldhouse", GPE: "GPE0529035", GIP: 137180, DOB: 30604, Address: "16 Green Gardens,Cleland,MOTHERWELL,ML1 5PH"}
    , {Name: " Ms Joanna Wilson", GPE: "GPE0529020", GIP: 137178, DOB: 27593, Address: "2 Drum Road,KELTY,KY4 0DQ"}]
    this.ErrorGrid = new MatTableDataSource<any>(data);
         this.ErrorGrid.paginator = this.paginator;
  }

  handleFileInput(files: FileList) {

    this.fileToUpload = files.item(0);
    // var mimeType = this.fileToUpload.type;
    // if (mimeType.match(/image\/*/) == null) {
    //   return;
    // }

    // var reader = new FileReader();      
    // reader.readAsDataURL(this.fileToUpload); 
    // reader.onload = (_event) => { 
    //   this.previewUrl = reader.result; 
    // }
  }
  UploadFile(){
    debugger;
    this.SaveImportFile();
    this.working =true;
    // let fileReader = new FileReader();    
    // fileReader.readAsArrayBuffer(this.file);     
    // fileReader.onload = (e) => { 
      debugger;   
      // this.SaveImportFile();
    
        // this.arrayBuffer = fileReader.result;    
        // var data = new Uint8Array(this.arrayBuffer);    
        // var arr = new Array();    
        // for(var i = 0; i != data.length; ++i)
        // { arr[i] = String.fromCharCode(data[i]); 
        // }
        // debugger;   
        // var bstr = arr.join("");    
        // var workbook = XLSX.read(bstr, {type:"binary"});    
        // var first_sheet_name = workbook.SheetNames[0];    
        // var worksheet = workbook.Sheets[first_sheet_name]; 
  
        // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));      
        //   var arraylist  = XLSX.utils.sheet_to_json(worksheet, { raw: true, defval: null });
  
        //    arraylist  = JSON.parse(JSON.stringify(arraylist).replace(/\s(?=\w+":)/g, "_"));
        //       console.log(arraylist);   
            // for(var i=0 ; i<arraylist.length;i++)
            // {


        //       this._caseManagment.GetImportedResultList()
        // .subscribe((response:any) => {
        //   debugger;
        //  this.working =false;
        //   console.log(response);
        //   this.ErrorGrid=response['result'];
        //   this.TotalRecords=response['result'].length;
        //    this.ErrorGrid = new MatTableDataSource<any>(response['result']);
           
        //   // this.ErrorGrid.sortingDataAccessor = (data: any, sortHeaderId: string) => {
        //   //   if (!data[sortHeaderId]) {
        //   //     return this.sort.direction === "asc" ? '3' : '1';
        //   //   }
        //   //   return '2' + data[sortHeaderId].toLocaleLowerCase();
        //   // }
       
        //   // this.ErrorGrid.paginator = this.paginator;
        //  if(response['result'].length>0)
        //  {
        //   this.ShowerrorTBL=true;
        //   this.successMSG=false;
        //   this.DocumentfileInput.nativeElement.value = null;
        //   // this.ErrorGrid = new MatTableDataSource<any>(response['result']);
        //   // this.ErrorGrid.paginator = this.paginator;
        //   console.log("Some Message");
        //  }
        //  else{
        //   this.ShowerrorTBL=false;
        //   this.successMSG=true;
        //   console.log("Some Message in succsess block");
        //   this.Response="Case Imported Successfuly"
        //  }
        // }, (error: any) => {
        //  // this.errorText = error;
        // });

        
   // }
 
      
   // }
   // this.GetList()
   
  }
  GetList()
  {
    debugger;
    this._caseManagment.GetImportedResultList()
    .subscribe((response:any) => {
      debugger;
     this.working =false;
      console.log(response);
      this.ErrorGrid=response['result'];
      this.TotalRecords=response['result'].length;
       this.ErrorGrid = new MatTableDataSource<any>(response['result']);
      
     if(response['result'].length>0)
     {
      this.ShowerrorTBL=true;
      this.successMSG=false;
      this.DocumentfileInput.nativeElement.value = null;
      console.log("Some Message in success");
     }
     else{
      this.ShowerrorTBL=false;
      this.successMSG=true;
      console.log("Some Message in Error block");
      this.Response="Case Imported Successfuly"
     }
    }, (error: any) => {
     // this.errorText = error;
    });
  }
  addfile(event,files: FileList)     
  {   

  this.file= event.target.files[0];
  this.fileToUpload = files.item(0);     
  
} 
SaveImportFile()
{
  this._caseManagment.SaveImportedFile(this.fileToUpload)
//  .subscribe(response => {
//     // .subscribe((response:any) => {
//       debugger;
//       console.log("This is response:  "+ response);
//   })
.subscribe(event => {
  if (event.type === HttpEventType.UploadProgress) {
   // this.progress = Math.round(100 * event.loaded / event.total);
  } else if (event instanceof HttpResponse) {
    // this.showAddDocument = false;
    this.GetList();
    this.DocumentfileInput.nativeElement.value = null;
  //  this.progress = 0;
  }
}, (error: any) => {
 // this.errorText = error;
});
}
onChangePage(pageOfItems: Array<any>) {
  // update current page of items
  this.ErrorGrid = pageOfItems;
}

}
