import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../Material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { LoginComponent } from './login/login.component';

const routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  // { 
  //   path: '**',
  //   redirectTo: 'login'
  // },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    FuseSharedModule
  ]
})
export class AuthModule { }
