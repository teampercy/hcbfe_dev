import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        
        type: 'item',
        icon: 'dashboard',
        url: '/dashboard',
    },
    // {
    //     id: 'open-cases',
    //     title: 'Open Cases',
        
    //     type: 'item',
    //     icon: 'folder_open',
    //     url: '/open-cases',
    // },

    {
        id: 'case-managment',
        title: 'Manage Cases',
        
        type: 'item',
        icon: 'folder_open',
        url: '/case-managment',
    },
    {
        id: 'import-Cases',
        title: 'L&G Import Case',
        
        type: 'item',
        icon: 'list',
        url: '/import-cases',
    },
    {
        id: 'manage-list',
        title: 'Manage List',
        type: 'collapsable',
        icon: 'ac_unit',
      //  url: 'administration/master',

        children: [
            // {
            //     id: 'relationship-owner',
            //     title: 'Relationship Owner',
            //     type: 'item',
            //     icon: 'person_outline',
            //     url: 'administration/master/relationship-owner',
            // },
            {
                id: 'illness-injury',
                title: 'Illness or Injury',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/illness-injury',
            },
            {
                id: 'claim-closed-reason',
                title: 'Claim Closed Reason',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/claim-closed-reason',
            },
            {
                id: 'reason-closed',
                title: 'Reason Closed',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/reason-closed',
            },
            {
                id: 'assessor',
                title: 'Assessor',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/broker',
            },
            {
                id: 'services-required',
                title: 'Services Required List',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/services-required',
            },
            // {
            //     id: 'corporate-partner',
            //     title: 'Corporate Partner',
            //     type: 'item',
            //     icon: 'person_outline',
            //     url: 'administration/master/corporate-partner',
            // },
            // {
            //     id: 'product-type',
            //     title: 'Product Type',
            //     type: 'item',
            //     icon: 'person_outline',
            //     url: 'administration/master/product-type',
            // },
            {   
                id: 'waiting-period',
                title: 'Waiting Period',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/waiting-period',
            },
            // {
            //     id: 'brand',
            //     title: 'Brand',
            //     type: 'item',
            //     icon: 'person_outline',
            //     url: 'administration/master/brand',
            // },
            {
                id: 'employment-status',
                title: 'Employment status',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/employment-status',
            },
            {
                id: 'incapacity-defination',
                title: 'Incapacity Definition',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/incapacity-defination',
            },
            {
                id: 'type-of-visit',
                title: 'Type of Visit',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/type-of-visit',
            },
            {
                id: 'type-of-call',
                title: 'Type of Call',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/type-of-call',
            },
            // {
            //     id: 'funded-treatement-type',
            //     title: 'Funded Treatment Type',
            //     type: 'item',
            //     icon: 'person_outline', //pan_tool
            //     url: 'administration/master/funded-treatement-type',
            // },
            // {
            //     id: 'funded-treatement-provider',
            //     title: 'Funded Treatment Provider',
            //     type: 'item',
            //     icon: 'person_outline', 
            //     url: 'administration/master/funded-treatement-provider',
            // },
            {
                id: 'scheme-name',
                title: 'Scheme Name',
                type: 'item',
                icon: 'person_outline', 
                url: 'administration/master/scheme-name',
            },
            {
                id: 'occupational-classification',
                title: 'Occupational Classification',
                type: 'item',
                icon: 'person_outline', 
                url: 'administration/master/occupational-classification',
            },
            {
                id: 'health-trust',
                title: 'Health Trust',
                type: 'item',
                icon: 'person_outline', 
                url: 'administration/master/health-trust',
            },
            {
                id: 'Fees-Payable',
                title: 'Fees Payable',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/fees-payable',
            },
            {               
                id: 'Disability-Category',
                title: 'Disability Category',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/disability-category',
            },
            {               
                id: 'principal-determinant-absence',
                title: 'Determinant of Absence',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/principal-determinant-absence',
            },

            {               
                id: 'service-requested',
                title: 'Service Requested',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/service-requested',
            },
            {               
                id: 'service-provider',
                title: 'Service Provider',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/service-provider',  ///
            },
            {               
                id: 'claim-status',
                title: 'Claim Status',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/claim-status',
            },
            {               
                id: 'claims-assessor',
                title: 'Claim Assessor',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/claims-assessor',
            },
            {               
                id: 'action',
                title: 'Action',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/action',
            },
            {               
                id: 'outcome',
                title: 'Outcomes',
                type: 'item',
                icon: 'person_outline',
                url: 'administration/master/outcome',
            },
            
        ]
    }
    // {
    //     id: 'sample',
    //     title: 'Sample',
    //     type: 'item',
    //     icon: 'email',
    //     url: '/sample',
       
    // }
];


export const navigationCRM: FuseNavigation[] = [
    
    {
        id: 'crm-dashboard',
        title: 'Dashboard',
        
        type: 'item',
        icon: 'dashboard',
        url: '/crm-dashboard',
    },

    {
        id: 'client-record',
        title: 'Client Record',
        
        type: 'item',
        icon: 'list',
        url: '/client-record',
    },
    {
        id: 'Administration',
        title: 'Administration',
        type: 'collapsable',
        icon: 'settings',
        children: [
            {
                id: 'user-records',
                title: 'User Profiles',
                type: 'item',
                icon: 'book',
                url: 'administration/user-records',
            },
            {
                id: 'client-report',
                title: 'Client Report',
                type: 'item',
                icon: 'receipt',
                url: 'administration/client-report',
            },
            {
                id: 'manage-list',
                title: 'Manage List',
                type: 'collapsable',
                icon: 'ac_unit',
              //  url: 'administration/master',

                children: [
                    {   
                        id: 'relationship-owner',
                        title: 'Relationship Owner',
                        type: 'item',
                        icon: 'person_outline',
                        url: 'administration/master/relationship-owner',
                    },
                    {
                        id: 'region',
                        title: 'Region',
                        type: 'item',
                        icon: 'person_outline',
                        url: 'administration/master/region',
                    },
                       {
                        id: 'assessor',
                        title: 'Brokers',
                        type: 'item',
                        icon: 'person_outline',
                        url: 'administration/master/broker',
                        
                    },

                    {
                        id: 'ClientReferralSource',
                        title: 'Referral Source',
                        type: 'item',
                        icon: 'person_outline',
                        url: 'administration/master/client-referral-source',
                        
                    },
                    // {//
                    //     id: 'ClientReferralSource',
                    //     title: 'Agreed SLAs',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/client-referral-source',
                        
                    // },
                    // {
                    //     id: 'ClientReferralSource',
                    //     title: 'Reporting Format',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/client-referral-source',
                        
                    // },
                    // {
                    //     id: 'ClientReferralSource',
                    //     title: 'Invoice Frequency',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/client-referral-source',
                        
                    // },
                    // {
                    //     id: 'brokers',
                    //     title: 'Brokers',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/brokers',
                    // },
                    // {
                    //     id: 'reason-closed',
                    //     title: 'Reason Closed',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/reason-closed',
                    // },
                    // {
                    //     id: 'assessor',
                    //     title: 'Assessor',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/broker',
                    // },
                    // {
                    //     id: 'services-required',
                    //     title: 'Services Required List',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/services-required',
                    // },
                    // {
                    //     id: 'corporate-partner',
                    //     title: 'Corporate Partner',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/corporate-partner',
                    // },
                    // {
                    //     id: 'product-type',
                    //     title: 'Product Type',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/product-type',
                    // },
                    // {
                    //     id: 'waiting-period',
                    //     title: 'Waiting Period',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/waiting-period',
                    // },
                    // // {
                    // //     id: 'brand',
                    // //     title: 'Brand',
                    // //     type: 'item',
                    // //     icon: 'person_outline',
                    // //     url: 'administration/master/brand',
                    // // },
                    // {
                    //     id: 'employment-status',
                    //     title: 'Employment status',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/employment-status',
                    // },
                    // {
                    //     id: 'incapacity-defination',
                    //     title: 'Incapacity Definition',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/incapacity-defination',
                    // },
                    // {
                    //     id: 'type-of-visit',
                    //     title: 'Type of Visit',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/type-of-visit',
                    // },
                    // {
                    //     id: 'type-of-call',
                    //     title: 'Type of Call',
                    //     type: 'item',
                    //     icon: 'person_outline',
                    //     url: 'administration/master/type-of-call',
                    // },
                    // {
                    //     id: 'funded-treatement-type',
                    //     title: 'Funded Treatment Type',
                    //     type: 'item',
                    //     icon: 'person_outline', //pan_tool
                    //     url: 'administration/master/funded-treatement-type',
                    // },
                    // {
                    //     id: 'funded-treatement-provider',
                    //     title: 'Funded Treatment Provider',
                    //     type: 'item',
                    //     icon: 'person_outline', 
                    //     url: 'administration/master/funded-treatement-provider',
                    // },
                    // {
                    //     id: 'scheme-name',
                    //     title: 'Scheme Name',
                    //     type: 'item',
                    //     icon: 'person_outline', 
                    //     url: 'administration/master/scheme-name',
                    // },
                    // {
                    //     id: 'occupational-classification',
                    //     title: 'Occupational Classification',
                    //     type: 'item',
                    //     icon: 'person_outline', 
                    //     url: 'administration/master/occupational-classification',
                    // },
                    // {
                    //     id: 'health-trust',
                    //     title: 'Health Trust',
                    //     type: 'item',
                    //     icon: 'person_outline', 
                    //     url: 'administration/master/health-trust',
                    // }
                    
                ]
            }
            
        ]
    }
];
