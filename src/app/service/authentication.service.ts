import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { User } from 'app/model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  GetHeader(): any {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'bearer ' + this.GetToken());
    return headers;
  }

  GetFileHeader(): any {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'bearer ' + this.GetToken());
    return headers;
  }

  SetToken(token: string) {
    localStorage.setItem("Token", token);
  }

  GetToken() {
    return localStorage.getItem("Token");
  }
}
