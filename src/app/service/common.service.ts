import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  GetYearFromDateString(date: string) {
    return (new Date(date)).getFullYear();
  }

  GetMonthFromDateString(date: string) {
    return (new Date(date)).getMonth();
  }

  GetDateFromDateString(date: string) {
    return (new Date(date)).getDate();
  }
}
